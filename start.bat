@echo off
pip install -r deploy_config/requirements.txt
START python deploy_config/manage.py runserver
cd frontend
npm i
START npm run dev
exit