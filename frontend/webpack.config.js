const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BundleTracker = require('webpack-bundle-tracker');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const images = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

const plugins = [
  new BundleTracker({filename: '../backend/static/webpack-stats.json'}),
  
  new CleanWebpackPlugin(['../backend/static/', '../backend/static/*.*', ]),

  new HtmlWebpackPlugin({
    title: 'SmartCinema',
    template: 'index.html'
  }),

  new webpack.HotModuleReplacementPlugin(),

 new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "bundle-[name][id].css"
  }),

  new webpack.ProvidePlugin({
      React: 'react',
      Component: ['react', 'Component']
  }),

  new CopyWebpackPlugin([
    ...images.map(ext => ({ from: `**/*/*.${ext}`, to: 'img/[name].[ext]' }))
  ])
];

module.exports = {
  entry: ['babel-polyfill', './app.js'],
  context: path.resolve('src'),
  output: {
    path: path.resolve('../backend/static'),
    filename: 'bundle-[name].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        }
      },

      {
        test: /\.s?css$/,
        use: [
            MiniCssExtractPlugin.loader,
            "css-loader",
            "sass-loader",
        ]
      },

      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "static/fonts/[name].[ext]",
            outputPath: 'img/',
            publicPath: '/'
          },
        },
      },

      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 8000,
            name: 'static/img/[name].[ext]',
            outputPath: 'img/',
            publicPath: '/'
          }
        }]
      },
    ]
  },

  plugins,

  optimization: {
    splitChunks: {
      chunks: 'all'
    },
  },
};

