import {Route, Switch} from 'react-router-dom';
import {
    AboutUs,
    Bonus,
    Feedback,
    Login,
    Main,
    Movies,
    OneShare,
    Reservation,
    OneMovie,
    Schedule,
    User,
    NotFound,
    Prices,
    Contacts,
    ComingSoon,
    Reset,
    Registred,
    BuyTicketMobile,
    ResetPassMobile,
    Ticket
} from './containers';


export const Pages = () => (
    <Switch>
        <Route path="/registred/" exact component={Registred}/>
        <Route path="/reservation/:params/get/" exact component={BuyTicketMobile}/>
        <Route path="/resetpass/" exact component={ResetPassMobile}/>
        <Route path="/tickets/" exact component={Ticket}/>
        <Route path="/account/change_password/:params" component={Reset}/>
        <Route path="/" exact component={Main}/>
        <Route path="/about_us/" exact component={AboutUs}/>
        <Route path="/movies/" exact component={Movies}/>
        <Route path="/movies/:movie/" component={OneMovie}/>
        <Route path="/shares/:share_name/" exact component={OneShare}/>
        <Route path="/login/" exact component={Login}/>
        <Route path="/feedback/" exact component={Feedback}/>
        <Route path="/schedule/" exact component={Schedule}/>
        <Route path="/reservation/:id/" component={Reservation}/>
        <Route path="/bonus/" exact component={Bonus}/>
        <Route path="/prices/" exact component={Prices}/>
        <Route path="/contacts/" exact component={Contacts}/>
        <Route path="/user/" exact component={User}/>
        <Route path="/coming_soon/" exact component={ComingSoon}/>
        <Route component={NotFound}/>
    </Switch>
);
