import { connect } from 'react-redux';
import {Route, withRouter} from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { Pages } from './Pages';
import {
  setDataAsync,
  setSeoAsync,
  setCurrentToken,
  checkUserAsync
} from './store';

import { getCookie } from "./consts";

import {
  FooterComponent,
  HeaderComponent,
  Wrapper
} from './components';
import {BuyTicketMobile} from "./containers/containerComponents";


export class AppComponent extends Component {
  state = {
    currentPathname: this.props.location.pathname,
    token: getCookie('csrftoken'),
    mobileComponent: false
  };

  componentDidMount() {
    this.props.dispatch(setCurrentToken(this.state.token));
    this.props.dispatch(setDataAsync({token: this.state.token, cityId: this.props.cityId}));
    this.props.dispatch(setSeoAsync({ token: this.state.token, pathname: this.state.currentPathname }));
    this.props.dispatch(checkUserAsync(this.state.token));
      const queryString = require('query-string');

      const parsed = queryString.parse(location.search);


      if (parsed.session_id && parsed.user){
        this.setState({
            mobileComponent: true
        })
      }

  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname != this.props.location.pathname) {
      this.props.dispatch(setSeoAsync({ token: this.state.token, pathname: this.props.location.pathname }));
    }
    if(prevProps.cityId != this.props.cityId){
      this.props.dispatch(setDataAsync({token: this.state.token, cityId: this.props.cityId}));
    }
  }

  render() {
    const { data, seo } = this.props;

    return (
      <React.Fragment>
        {
          seo &&
          <Helmet>
            <title>{seo.title}</title>
            <meta name="description" content={seo.description} />
            <meta name="keywords" content={seo.keywords} />
            <link rel="shortcut icon" type="image/png" href="/static/img/title.png" sizes="16x16" />
          </Helmet>
        }
        {
          data &&
              this.state.mobileComponent?
              <Route path="/reservation/:params/get/" exact component={BuyTicketMobile}/>
              :
          <React.Fragment>
            <HeaderComponent />
            <Wrapper className="pages_wrapper">
              <Pages />

            </Wrapper>
            <FooterComponent />
          </React.Fragment>
        }
      </React.Fragment>
    );
  }
}

export const App = withRouter(connect(({ state }) => ({
  data: state.data,
  seo: state.seo,
  city: state.city,
  cityId: state.cityId
}))(AppComponent));
