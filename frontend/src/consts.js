export const LINKS = [
    {title: 'Розклад', to: '/schedule/'},
    {title: 'Фільми', to: '/movies/'},
    {title: 'Про нас', to: '/about_us/'},
    {title: 'Бонусна програма', to: '/bonus/'},
    {title: 'Ціни', to: '/prices/'},
    {title: 'Зворотній зв\'язок', openForm: 'feedback'},
    {title: 'Кар\'єра у нас', openForm: 'career'},
    {title: 'Відгуки', to: '/feedback/'},
    {title: 'Рекламодавцям', slideTo: '/#footer'},
    {title: 'Новини', slideTo: '/#news'},
    {title: 'Контакти', to: '/contacts/'},
];

export const FOOTER_LINKS = [
    {title: 'Головна', to: '/'},
    {title: 'Розклад', to: '/schedule/'},
    {title: 'Фільми', to: '/movies/'},
    {title: 'Особистий кабінет', to: '/user/'},
    {title: 'Про нас', to: '/about_us/'},
    {title: 'Новини', slideTo: '/#news'},
    {title: 'Бонусна програма', to: '/bonus/'},
    {title: 'Зворотній зв\'язок', openForm: 'feedback'},
    {title: 'Кар\'єра у нас', openForm: 'career'},
    {title: 'Відгуки', to: '/feedback/'},
    {title: 'Контакти', to: '/contacts/'}
];

export const emailReg = /(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/


export const getCookie = (name) => {
    let cookieValue = null;

    if (document.cookie && document.cookie !== '') {

        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            }
        }
    }

    return cookieValue;
};
