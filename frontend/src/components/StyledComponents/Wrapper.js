import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: ${p => p.wrapDirection ? p.wrapDirection : 'column' };
  justify-content: ${p => p.wrapContent ? p.wrapContent : 'flex-start' };
  align-items: ${p => p.wrapItems ? p.wrapItems : 'center' };
  min-width: ${p => p.wrapMinWidth ? p.wrapMinWidth : '100%'};
  max-width: ${p => p.wrapMaxWidth ? p.wrapMaxWidth : '100%'};
  margin: ${p => p.wrapMargin ? p.wrapMargin : '0'};
  padding: ${p => p.wrapPadding ? p.wrapPadding : '0'};
  flex-wrap: ${p => p.noWrap ? 'nowrap' : 'wrap'};
  
  .one_movie_bottom_slider {
    display: block;
    width: 100%;
  }
  .one_movie_frame_title {
    display: block;
    margin: 0 0 80px 0;
  }
  .modalZipWrap{
    min-width: 500px;
  }
    &.reload {
        cursor: pointer;
      }
  .modalWidth{
    max-width: 50%;
    min-width: 50%;
  }
  .hr_aboutAs {
    border-top: 1px solid #fff;
    opacity: 0;
    margin: 12px 0;
  }
  
  &.thank_modal {
    margin: 0 50px;
    border-top: 1px solid #c7c7c7;
  }
  
  &.orange_wrapper {
    border: 2px solid #fd5f00;
    border-radius: 10px;
  }
  
  &.card_wrapper {
    border: 1px solid #c7c7c7;
    border-radius: 10px;
    margin: 20px;
    cursor: pointer;
    
    &:hover {
      border-color: #00a6ea;
    }
    
    &.chosen {
      background-color: #e0f6ff;
      
      &:hover {
        border-color: #00a6ea;
      }
    }
  }
  
  &.city_wrap {
    position: relative;
  }
  
  &.file_input_wrapper {
    .file_input_label {
      color: #00a6ea;
      cursor: pointer;
      display: flex;
      align-items: center;
    }
  }
  
  &.pointer {
    cursor: pointer;
    
    &.error {
      padding: 5px 10px;
      border: 1px solid red;
      border-radius: 10px;
    }
  }
  
  &.preloader {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.8);
    z-index: 10;
    
    .preloader_gif {
      position: absolute;
      top: 35%;
    }
  }
  
  &.tickets_first {
    border-right: 1px solid #c7c7c7;
    padding-right: 20px;
  }
  
  &.tickets_block {
    height: 280px;
    overflow: auto;
    display: block;
    
    &.small {
      height: auto;
    }
  }
  
  &.age_buttons {
    .button {
      width: 120px;
      text-align: center;
      
      &:first-child {
        margin-right: 40px;
      }
    }
  }
  
  &.modal_background {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(0,0,0,0.3);
    padding: 7.5% 0;
    z-index: 4;
    overflow-y: auto;
    
    .modal_all {
      z-index: 5;
      display: block;
      background-color: #fff;
      border-radius: 15px;
      padding: 15px;
    }
    
    &.selectModal {
      .modal_all {
        background-color: #22262f;
        padding: 0;
      }
    }
  }
  
  &.seats_map {
    border-top: 1px solid #fff;
    border-bottom: 1px solid #fff;
    position: relative;
  }
  
  &.schedule_table_wrap {
    table {
      width: auto;
      border-collapse: separate;
      border-spacing: 10px;
      
      td {
        color: #fff;
        width: 120px;
        height: 60px;
        text-align: center;
        background-color: #00a6ea;
        border-radius: 6px;
        margin: 7px 8px;
        padding: 14px 0;
        display: inline-block;
        cursor: pointer;
        position: relative;
        
        span {
          display: none;
        }
        
        &:hover {
          span {
            position: absolute;
            bottom: -45px;
            right: 0;
            display: inline-block;
            background-color: #373b42;
            padding: 8px 12px;
            border-radius: 6px;
            z-index: 2;
            
            &:before{
              content: '';
              display: block;
              width: 20px;
              height: 0;
              position: absolute;
              border-top: 8px solid transparent;
              border-bottom: 8px solid #373b42;
              border-right: 10px solid transparent;
              border-left: 10px solid transparent;
              left: 20px;
              top: -16px;
            }
          }
        }
        
        
        
        &.genre {
          width: 60px;
          background-color: transparent;
        }
        
        &.past_film {
          background-color: transparent;
          border: 1px solid #fff;
          cursor: auto;
        }
      }
    }
  }
  
  .feedback_disqus {
    width: 100%;
  }
  
  &.schedule_item {
    border-top: 1px solid #fff;
  }
  
  .slider_movie_title {
    max-width: 65%;
  }
  
  &.pdf_link_prices {
  
    div {
      min-width: 32px;
    }
    
    a {
      color: #00a6ea;
      
      span {
        border-bottom: 1px dashed #00a6ea;
      }
      
      &:hover {
        span {
          border-bottom: 1px solid #00a6ea;
        }
      }
    }
  }
  
  &.pdf_link {
    a {
      display: flex;
      
      &:hover {
        p {
          color: #00a6ea;
          
          &:before {
            border-bottom: 1px solid #00a6ea;
          }
        }
        
      }
      
      p {
        position: relative;
        
        &:before {
          content: '';
          position: absolute;
          bottom: -2px;
          left: 2px;
          width: 265px;
          height: 1px;
          border-bottom: 1px dashed #fff;
        }
      }
    }
  }
  &.pdf_link_partners {
    a {
      display: flex;
      
      &:hover {
        p {
          color: #00a6ea;
          
          &:before {
            border-bottom: 1px solid #00a6ea;
          }
        }
        
      }
      
      p {
        position: relative;
        
        &:before {
          content: '';
          position: absolute;
          bottom: -2px;
          left: 2px;
          width: 265px;
          height: 1px;
          border-bottom: 1px dashed #fff;
        }
      }
    }
  }
  
  &.floated_with_map {
    float: left;
  }
  
  &.blue_box {
    background-color: #00a6ea;
    min-height: 128px;
    border-radius: 10px;
  }
  
  &.white_box {
    background-color: #fff;
    min-height: 180px;
    border-radius: 10px;
    
    &.about_us {
      min-height: 198px;
    }
    
    &.bonus {
      min-height: 378px;
      margin: 0 50px;
    }
  }
  
  &.not_found {
    background-color: #fff;
    box-shadow: 0 0 133px rgba(255, 255, 255, 0.58);
    height: auto;
    border-radius: 10px;
  }
  
  &.reservation {
    background-color: #fff;
    box-shadow: 0 0 133px rgba(255, 255, 255, 0.58);
    min-height: 622px;
    border-radius: 10px;
  }
  
  &.film_description {
    min-height: 360px;
    
    table {
    
      th {
        color: #fff;
        min-width: 180px;
        text-align: left;
        line-height: 32px;
      }
      
      td {
        color: #c1c1c1;
        line-height: 32px;
      }
    }
  }
  
  table {
    
    th {
      color: #fff;
      min-width: 180px;
      text-align: left;
    }
    
    td {
      color: #c1c1c1;
    }
  }
  
  &.darken {
    background: linear-gradient(to bottom, transparent 1%, #000 100%);
  }
  
  
  &.image_bg {
    background: url(${p => p.picture}) no-repeat center;
    background-size: cover;
    padding-top: 32.25%
  }
  
  &.film_wrapper {
    position: relative;
    transition: transform .2s;
    
    &.one_movie {
      .film_btn {
        display: block;
      }
    }
    
    .film_labels {
      position: absolute;
      top: 12px;
      left: 10px;
      z-index: 3;
    }
    
    &.hovered {
      &:hover {
        transform: scale(1.25);
        z-index: 2;
        
        &:before {
          content: '';
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background-color: rgba(0, 92, 210, 0.3);
          z-index: 2;
          border-radius: 15px;
        }
        
        .film_btn {
          display: block;
        }
      }
    }
  }
  
  &.bordered_title {
    border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  }
  
  &.pages_wrapper {
    padding-top: 95px;
    align-items: flex-start;
    background-color: #12161f;
  }
  
  &.logo_wrapper {
    min-width: 260px;
    height: 95px;
    background-color: #12161f;
  }
  
  &.header_dropdown {
    margin: 0 10px;
    
    &:hover {
      svg {
      cursor: pointer;
      
        .icon {
          fill: #009ad7;
        }
      }
    }
  }
  
  &.input_wrapper {
    position: relative;
    
    //&.focused {
    //  label {
    //    z-index: 1;
    //  }
    //  
    //  input {
    //    z-index: 0;
    //  }
    //}
    
    &.datepicker_wrapper {
      label {
        &.focused {
          z-index: 4;
        }
      }
    }
    
    &.select_wrapper {
      label {
        z-index: 0;
      }
      
      &.focused {
        label {
          z-index: 1;
        }
        
        .select_block {
          z-index: 0;
        }
      }
      
      .select_block {
        z-index: 1;
      
        .Select-control {
          width: 100%;
          height: 44px;
          border: 1px solid #c7c7c7;
          border-radius: 10px;
          color: #0d1017;
          outline: none;
          padding: 0 10px 0 20px;
          font-size: 17px;
          z-index: 1;
          background-color: transparent;
  
          .Select-multi-value-wrapper {
            .Select-value {
              left: 10px;
              top: 5px;
            }
            
            .Select-input {
              display: none !important;
            }
          }
          
          .Select-arrow-zone {
            .Select-arrow {
              border-color: #00a6ea transparent transparent;
            }
          }
        }
        
        &.error {
          .Select-control {
            border-color: red;
          }
        }
        
        .Select-menu-outer {
          position: relative;
          z-index: 4 !important;
        }
        
        &.is-focused {
          .Select-control {
            border: 1px solid #c7c7c7 !important;
            box-shadow: none !important;
          }
        }
        
        &.is-open {
          .Select-arrow-zone {
            .Select-arrow {
              border-color: transparent transparent #00a6ea;
            }
          }
        }
      } 
    }
    &.input_on_input {
    
      label {
      
        left: 45px;
        
        &.focused {
          background-color: #22262f;
        }
      }
      
      
      input {
        color: #fff;
        min-width: 100%;
        
          &:-webkit-autofill,
          &:-webkit-autofill:hover, 
          &:-webkit-autofill:focus,
          & textarea:-webkit-autofill,
          & textarea:-webkit-autofill:hover,
          & textarea:-webkit-autofill:focus,
          & select:-webkit-autofill,
          & select:-webkit-autofill:hover,
          & select:-webkit-autofill:focus {
            -webkit-text-fill-color: #fff;
          }
      }
    }
    &.input_on_dark {
    
      label {
      
        left: 20px;
        
        &.focused {
          background-color: #22262f;
        }
      }
      
      
      input {
        color: #fff;
        min-width: 100%;
        
          &:-webkit-autofill,
          &:-webkit-autofill:hover, 
          &:-webkit-autofill:focus,
          & textarea:-webkit-autofill,
          & textarea:-webkit-autofill:hover,
          & textarea:-webkit-autofill:focus,
          & select:-webkit-autofill,
          & select:-webkit-autofill:hover,
          & select:-webkit-autofill:focus {
            -webkit-text-fill-color: #fff;
          }
      }
    }
    
    label {
      color: #9a9a9a;
      position: absolute;
      top: -3px;
      left: 20px;
      z-index: 0;
      
      transition: transform 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms;
      transform: translate(0, 17px) scale(1);
      transform-origin: top left;
      
      &.focused {
        color: #0f74a8;
        background-color: #fff;
        padding: 3px 5px;
        transform: translate(0, -5px) scale(0.75);
        transform-origin: top left;
        z-index: 1;
      }
    }
    
    input {
      z-index: 0;
      background-color: transparent;
    }
    
    & > div,
    .react-datepicker-wrapper,
    .react-datepicker__input-container{
      width: 100%;
      z-index: 2;
      
      .datepicker {
        &.error {
          border: 1px solid red;
        }
      }
    }
  }
  
  .icon_link {
    margin: 0 10px;

    svg {
      color: #fff;
    }
    
    &:hover {
  
      .faFacebook {
        color: #3a5998;
      }
  
      .faInstagram {
        color: #e4405f;
      }
  
      .faFacebookMessenger {
        color: #0084ff;
      }
  
      .faTelegram {
        color: #0088cc;
      }
  
      .faViber {
        color: #713f86;
      }
    }
  }
  
  &.footer_icons {
    min-width: 15%;
  }
  
  &.footer_wrapper_bottom {
    min-width: 53%;
    max-width: 53%;
    
    &:last-child {
      min-width: 45%;
      max-width: 45%;
    }
  }
  
  &.slider_small {
    display: none;
  }
  
  &.schedule_item {
    flex-wrap: nowrap;
  }
  
  &.schedule_table_block {
    min-width: auto;
  }
  
  @media only screen and (max-width : 1195px) {
    &.header_dropdown {
      &:first-child, &:nth-child(2) {
        display: none;
      }
      
      &:last-child {
        margin: 0;
      }
    }
  }
   
  @media only screen and (max-width : 1024px) {
  .modalWidth{
    max-width: 90%;
    min-width: 90%;
  }
    &.footer_wrapper {
      justify-content: center;
    }
    
    &.footer_icon_wrapper,
    &.footer_partners_nav_wrapper {
      margin: 10px;
      justify-content: center;
    }
    
    &.share_image_wrapper {
      min-width: 40%;
      max-width: 40%;
    }
    
    &.share_text {
      min-width: 60%;
      max-width: 60%;
    }
    
    &.share_wrap_block {
      margin-top: 30px;
      align-items: flex-start;
    }
    
    &.bonus_wrapper {
      margin: 0 0 20px 0;
      
      .white_box {
        &:first-child {
          margin-bottom: 20px;
        }
      }
    }
    
    &.one_movie_img {
      height: 350px;
      padding: 0;
    }
  }
  
  &.schedule_table_block {
    width: auto !important;
  }
 
  @media only screen and (max-width : 768px) {
    &.footer_navlinks {
      justify-content: center;
      margin-top: 15px;
    }
    
    &.footer_wrapper_bottom {
      min-width: 100%;
      max-width: 100%;
      
      &:last-child {
        min-width: 100%;
        max-width: 100%;
        justify-content: center;
      }
    }
    
    &.share_image_wrapper {
      min-width: 100%;
      max-width: 100%;
    }
    
    &.share_text {
      padding: 0;
      margin-top: 20px;
      min-width: 100%;
      max-width: 100%;
    }
    
    &.share_wrap_block {
      margin-top: 80px;
      align-items: flex-start;
    }
    
    &.schedule_table_wrap {
      table {
        td {
          width: 100px;
          padding: 10px 0;
        }
      }
    }
    
    &.one_movie_title {
      display: none;
    }
    
    &.one_movie_block {
      flex-wrap: wrap;
      justify-content: center;
    }
    
    &.film_description {
      padding: 20px 0 0 0; 
      table {
        th {
          line-height: 20px;
          font-size: 14px;
          padding: 0;
        }
        
        td {
          line-height: 20px;
          font-size: 14px;
          padding: 0;
        }
      }
    }
    
  }
  
  @media only screen and (max-width: 768px) {
    &.about_us_block {
      flex-direction: column;
    }
    
    &.about_us {
      min-width: 100%;
      max-width: 100%;
      
      &:first-child {
        margin-bottom: 20px;
      }
    }
    
    &.blue_box {
      min-width: 100%;
    }
    
    &.prices {
      min-width: 366px;
    }
    
    &.one_movie_img {
      background: transparent;
      height: auto;
      padding: 0 0;
     
     .darken {
        background: transparent;
      }
    }
    
    &.white_box_wrapper {
      min-width: auto;
      
      .white_box {
        min-width: 100%;
        margin: 10px 0;
      }
    }
   
  }  
  
  @media only screen and (max-width : 680px) {
    &.title_block {
      margin: 30px 0;
    }
  }
  
  @media only screen and (max-width : 585px) {
    .one_movie_frame_title {
     margin: 0 0 0 0;
    }
    .modal_all {
      padding: 8px 8px !important;
    }
    &.news {
      padding: 80px 20px 0;
    }
    
    &.slider_small {
      display: flex;
    }

    &.slider_big {
      display: none;
    }
    
    &.about_us {
      padding: 15px 10px;
    }
    
    &.blue_box {
      padding: 15px 20px;
    }
    
    &.prices {
      min-width: 90%;
    }
    
    &.title_block {
      padding: 0 20px;
    }
    
    &.smaller_padding {
      padding: 0 20px;
      margin: 20px 0;
    }
    
    &.schedule_item {
      flex-wrap: wrap;
    }
    
    &.schedule_image {
      min-width: 100%;
    }
    
    &.schedule_table_block {
      margin: 10px 0 0;
      align-items: center;
    }
    
    &.schedule_table_wrap {
      margin-left: 13%;
    }
    
    &.title {
      padding: 0;
    }
    
    &.bordered_title {
      margin: 10px 0;
    }
    
    &.white_box {
      &.bonus {
        min-width: 90%;
      }
    }
    
    &.not_for_mobile {
      display: none;
    }
    
    &.floated_with_map {
      float: none;
      min-width: 100%;
    }
    
    &.not_found {
      padding: 20px;
    }
  }
  
  @media only screen and (max-width : 375px) {
    &.schedule_table_wrap {
      margin-left: 25%;
    }
  }
  
  @media only screen and (max-width : 355px) {
    &.logo_wrapper {
      min-width: 227px;
    }
  }
  
  @media only screen and (max-width : 320px) {
    &.schedule_table_wrap {
      margin-left: 10%;
    }
    
    &.pdf_link {
      a {
        p {
          font-size: 13px;
          
          &:before {
            width: 215px;
          }
        }
      }
    }
  }
`;
