import styled from 'styled-components';

export const Title = styled.h1`
  margin: 0;
  padding: 0;
  text-align: ${p => p.centered ? 'center' : 'left'};
  font-size: ${p => p.fontSize ? p.fontSize : '18px'};
  font-family: ${p => p.fontFamily};
  line-height: ${p => p.lineHeight};
  color: ${p => p.blue ? '#00a6ea' : '#fff'};
  font-weight: ${p => p.bold ? '600' : p.semiBold ? '500' : '400'};
  
  &.schedule_title {
    a {
      color: #00a6ea
    }
  }
  
  &.not_found_title {
    text-transform: uppercase;
    margin-top: -60px;
    z-index: 1;
    
    &.reservation {
      margin-top: -40px;
    }
  }
  
  &.bordered_blue_title {
    border-bottom: 3px solid #009ad7;
    padding-bottom: 10px;
  }
  
  &.tabs_title {
    cursor: pointer;
    margin-right: 30px;
  }
  
  @media only screen and (max-width : 585px) {
    &.share_title {
      padding-left: 25px;
    }
    
    &.title {
      font-size: 25px;
    }
    
    &.not_found_title {
      margin-top: -20px;
      text-align: center;
    }
    
    &.tabs_title {
      margin-right: 20px;
      font-size: 15px;
      
      &:last-child {
        margin-right: 0;
      }
    }
  }
`;

export const TitleH2 = styled.h2`
  margin: 0;
  padding: 0;
  text-align: ${p => p.centered ? 'center' : 'left'};
  font-size: ${p => p.fontSize ? p.fontSize : '18px'};
  font-family: ${p => p.fontFamily};
  line-height: ${p => p.lineHeight};
  color: ${p => p.blue ? '#00a6ea' : '#fff'};
  font-weight: ${p => p.bold ? '600' : p.semiBold ? '500' : '400'};
  
  &.schedule_title {
    a {
      color: #00a6ea
    }
  }
  
  &.not_found_title {
    text-transform: uppercase;
    margin-top: -60px;
    z-index: 1;
    
    &.reservation {
      margin-top: -40px;
    }
  }
  
  &.bordered_blue_title {
    border-bottom: 3px solid #009ad7;
    padding-bottom: 10px;
  }
  
  &.tabs_title {
    cursor: pointer;
    margin-right: 30px;
  }
  
  @media only screen and (max-width : 585px) {
    &.share_title {
      padding-left: 25px;
    }
    
    &.title {
      font-size: 25px;
    }
    
    &.not_found_title {
      margin-top: -20px;
      text-align: center;
    }
    
    &.tabs_title {
      margin-right: 20px;
      font-size: 15px;
      
      &:last-child {
        margin-right: 0;
      }
    }
  }
`;

export const TitleH3 = styled.h3`
  margin: 0;
  padding: 0;
  text-align: ${p => p.centered ? 'center' : 'left'};
  font-size: ${p => p.fontSize ? p.fontSize : '18px'};
  font-family: ${p => p.fontFamily};
  line-height: ${p => p.lineHeight};
  color: ${p => p.blue ? '#00a6ea' : '#fff'};
  font-weight: ${p => p.bold ? '600' : p.semiBold ? '500' : '400'};
  
  &.schedule_title {
    a {
      color: #00a6ea
    }
  }
  
  &.not_found_title {
    text-transform: uppercase;
    margin-top: -60px;
    z-index: 1;
    
    &.reservation {
      margin-top: -40px;
    }
  }
  
  &.bordered_blue_title {
    border-bottom: 3px solid #009ad7;
    padding-bottom: 10px;
  }
  
  &.tabs_title {
    cursor: pointer;
    margin-right: 30px;
  }
  
  @media only screen and (max-width : 585px) {
    &.share_title {
      padding-left: 25px;
    }
    
    &.title {
      font-size: 25px;
    }
    
    &.not_found_title {
      margin-top: -20px;
      text-align: center;
    }
    
    &.tabs_title {
      margin-right: 20px;
      font-size: 15px;
      
      &:last-child {
        margin-right: 0;
      }
    }
  }
`;