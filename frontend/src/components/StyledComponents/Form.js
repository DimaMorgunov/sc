import styled from 'styled-components';

export const Form = styled.form`
  width: 100%;
  
  &.with_border {
    border-bottom: 1px solid #c7c7c7;
  }
`;