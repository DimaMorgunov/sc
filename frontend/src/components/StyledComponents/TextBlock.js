import styled from 'styled-components';

export const TextBlock = styled.p`
  color: ${p => p.color ? p.color : '#fff'};
  text-align: ${p => p.centered ? 'center' : 'left'};
  margin: ${p => p.textMargin ? p.textMargin : '5px'};
  padding: ${p => p.textPadding ? p.textPadding : '0'};
  font-size: ${p => p.small ? '11px' : p.medium ? '14px' : p.font};
  line-height: ${p => p.small ? '12px' : p.medium ? '16px' : p.lineHeight};
  font-weight: ${p => p.bold ? '600' : '400'};
  max-width: ${p => p.textMaxWidth};
  width: ${p => p.textWidth};
 
  
  .pdf_link {
    color: #fff;
    border-bottom: 1px dashed #fff;
    padding-bottom: 5px;
    
    &:hover {
      border-bottom: 1px solid #fff;
    }
  }
  
  &.justified_text {
 
    text-align: justify;
    a { color: #6488bc;
     &:hover { color: #2660b5; }
     }
    
  }
  
  &.reservation_text {
    a {
      color: #009ad7;
      position: relative;
      padding-bottom: 3px;
      border-bottom: 1px dashed #00a6ea;
      
      &:hover {
        border-bottom: 1px solid #00a6ea;
      }
    }
  }
  
  &.not_found_block {
    color: #5b5f7d;
    max-width: 460px;
    text-align: center;
    line-height: 26px;
    font-weight: 600;
    margin-top: 50px;
    
    a {
      color: #009ad7;
      position: relative;
      padding-bottom: 3px;
      border-bottom: 1px dashed #00a6ea;
      
      &:hover {
        border-bottom: 1px solid #00a6ea;
      }
    }
  }
  
  &.link_text {
    &:hover {
      color: #009ad7;
    }
  }
  
  &.header_nav_item {
    padding: 5px 25px;
    margin: 0;
    font-size: 16px;
    
    a {
      text-decoration: none;
      text-transform: uppercase;
      outline: none;
      color: #fff;
      
      &:hover,
      &.active {
        color: #009ad7;
      }
    }
  }
  
  &.linked_text {
    cursor: pointer;
    font-size: 18px;
    padding: 10px;
    
    &:hover {
      color: #009ad7;
    }
  }
  
  &.footer_text {
    font-size: 12pt;
    line-height: 24px;
  }
  
  &.rights_text {
    max-width: 200px;
    font-size: 13pt;
    line-height: 20px;
    margin-left: 30px;
  }
  
  &.form_title {
    color: #00a6ea;
    font-size: 35px;
    line-height: 24px;
  }
  
  &.sign_up_text {
    padding: 14px 0;
    margin: 0;
    font-size: 17px;
  }
  
  @media only screen and (max-width : 1024px) {
    &.footer_text,
    &.rights_text {
      font-size: 13px;
    }
  }
  
  @media only screen and (max-width : 768px) {
    &.footer_text,
    &.rights_text {
      text-align: center;
    }
    &.form_title {
    font-size: 24px;
    margin-right: 50px;
  }
  }
  
  @media only screen and (max-width : 585px) {
    &.not_found_block {
      margin-top: 10px;
    }
  }
`;