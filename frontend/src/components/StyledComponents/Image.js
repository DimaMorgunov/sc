import styled from 'styled-components';

export const Image = styled.div`
  background: url(${p => p.src}) center no-repeat;
  height: ${p => p.imgHeight ? p.imgHeight+'px' : '100%'};
  width: ${p => p.imgWidth ? p.imgWidth+'px' : '100%'};
  position: relative;
  margin: ${p => p.imgMargin ? p.imgMargin : '0' };
  background-size: cover;
  
  &.city_img {
    animation: MoveUpDown 0.7s linear infinite;
    position: absolute;
    left: 50px;
    bottom: 15px;
    
    &.img_mobile {
      animation: MoveUpDownMobile 0.7s linear infinite;
      left: 33px;
      bottom: 25px;
    }
  }
  
  @keyframes MoveUpDown {
    0%, 100% {
      bottom: 15px;
    }
    50% {
      bottom: 20px;
    }
  }
  
  @keyframes MoveUpDownMobile {
    0%, 100% {
      bottom: 25px;
    }
    50% {
      bottom: 30px;
    }
  }
  
  &.canvas_image {
    transform: perspective(400px) rotateX(-20deg);
    border-radius: 5px;
    box-shadow: 0 4px 12px rgba(255, 255, 255, 0.2);
  }
  
  &.close_modal {
    cursor: pointer;
  }
  
  &.film_img {
    border-radius: 15px;
  }
  
  &.footer_partner_img {
    margin-right: 30px;
  }
  
  &.share_slider_img {
    width: 90%;
    height: 390px;
  }
  
  /*@media only screen and (max-width: 768px) {
    &.film_img {
      height: 400px;
    }
  }*/
  
  @media only screen and (max-width: 585px) {
    &.share_slider_img {
      width: 95%;
    }
  }
  
  @media only screen and (max-width : 396px) {
    &.film_img {
      width: 100%;
    }
  }
  
  @media only screen and (max-width: 585px) {
    &.share_slider_img {
      height: 300px;
    }
  }
 
`;
