import styled from 'styled-components';

export const Header = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  min-height: 95px;
  width: 100%;
  background-color: #12161f;
  position: fixed;
  top: 0;
  right: 0;
  padding: 0;
  z-index: 4;
`;