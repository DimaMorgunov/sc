import styled from 'styled-components';

export const Textarea = styled.textarea`
  width: 100%;
  min-width: ${p => p.minWidth ? p.minWidth : '265px'};
  min-height: 114px;
  border: 1px solid #c7c7c7;
  border-radius: 10px;
  color: #0d1017;
  outline: none;
  padding: 10px 20px;
  font-size: 17px;
  z-index: 2;
  background-color: transparent;
  resize: none;
  
  &::placeholder {
    color: #9a9a9a;
  }
  
  &.error {
    border-color: red;
  }
`;