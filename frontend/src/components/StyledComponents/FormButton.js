import styled from 'styled-components';

export const FormButton = styled.button`
  z-index: 1;
  background-color: ${p => p.theme.backgroundColor};
  margin: ${p => p.btnMargin ? p.btnMargin : '0'};
  padding: ${p => p.btnPadding ? p.btnPadding : '12px 25px'};
  border: none;
  border-radius: ${p => p.btnRadius ? p.btnRadius : '23px'};
  cursor: pointer;
  color: #fff;
  font-size: 17px;
  font-weight: 500;
  outline: none;
  
  &:hover {
    background-color: ${p => p.theme.hoveredBackgroundColor};
  }
  
  &.disabled {
    background-color: #6e6f6f;
  }
`;