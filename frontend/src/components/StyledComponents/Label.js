import styled from 'styled-components';

export const Label = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 9px;
  background-color: ${p => p.blue ? '#00a6ea' : '#fd5f00'};
  margin-right: ${p => p.blue ? '10px' : '0'};
  border-radius: 8px;
  color: #fff;
  font-size: 16px;
  box-shadow: 0 0 9px rgba(4, 43, 57, 0.39);
  
  &.timer {
    background-color: transparent;
    color: #fd5f00;
    border: 1px solid #fd5f00;
  }
  
  &.tickets {
    width: 77%;
    justify-content: space-between;
    padding: 0;
    
    &.transparent {
      background-color: transparent;
      border: 1px solid #fff;
    }
    
    &.small {
      width: 80%;
    }
  }
  
  &.price_from {
    width: 21px;
    height: 21px;
    background-color: #00a6ea;
    border-radius: 50%;
    margin: 0 15px 0 25px;
  }
  
  &.price_to {
    width: 56px;
    height: 24px;
    background-color: #4bb143;
    border-radius: 12px;
    margin: 0 15px 0 25px;
  }
  
  &.price_sold {
    width: 21px;
    height: 21px;
    background-color: #8f8f8f;
    border-radius: 50%;
    margin: 0 15px 0 25px;
  }
  
  &.block_label {
    display: inline-block;
    padding: 5px 8px 4px;
    border-radius: 8px;
    margin: 0 7px;
  }
  
  &.white_label {
    background-color: #fff;
    color: #00a6ea;
    margin-left: 15px;
  }
  &.orange_label {
    background-color: #fd5f00;
    color: #fff;
    margin-left: 15px;
  }
  
  &.black_label {
    background-color: black;
    border: 1px solid white;
    margin-right: 25px;
  }
  
  &.margined_right {
    margin-right: 25px;
  }
`;