import styled from 'styled-components';

export const Input = styled.input`
  width: 100%;
  min-width: ${p => p.minWidth ? p.minWidth : '220px'};
  height: 44px;
  border: 1px solid #c7c7c7;
  border-radius: 10px;
  color: #0d1017;
  outline: none;
  padding: 0 15px;
  font-size: 17px;
  z-index: 1;
  background-color: transparent;
  
  &::placeholder {
    color: #9a9a9a;
  }
  
  &.error {
    border-color: red;
    
    &:-webkit-autofill,
    &:-webkit-autofill:hover, 
    &:-webkit-autofill:focus,
    & textarea:-webkit-autofill,
    & textarea:-webkit-autofill:hover,
    & textarea:-webkit-autofill:focus,
    & select:-webkit-autofill,
    & select:-webkit-autofill:hover,
    & select:-webkit-autofill:focus {
      border: 1px solid red;
    }
  }
  
  &.file_input {
    opacity: 0;
    z-index: 0;
    height: 0.1px;
  }
  
  &[type=number]::-webkit-inner-spin-button, 
  &[type=number]::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
    margin: 0; 
  }
  
  &:-webkit-autofill,
  &:-webkit-autofill:hover, 
  &:-webkit-autofill:focus,
  & textarea:-webkit-autofill,
  & textarea:-webkit-autofill:hover,
  & textarea:-webkit-autofill:focus,
  & select:-webkit-autofill,
  & select:-webkit-autofill:hover,
  & select:-webkit-autofill:focus {
    border: 1px solid #c7c7c7;
    -webkit-text-fill-color: #0d1017;
    transition: background-color 5000s ease-in-out 0s;
  }
  /*@media only screen and (max-width : 585px) {
    font-size: 12px;
    }*/
   
`;