import styled from 'styled-components';

export const Span = styled.span`
  display: inline-block;
  color: ${p => p.color ? p.color : '#fff'};
  text-align: ${p => p.centered ? 'center' : 'left'};
  margin: ${p => p.textMargin ? p.textMargin : '0'};
  font-size: ${p => p.small ? '11px' : p.medium ? '14px' : p.font};
  line-height: ${p => p.small ? '12px' : p.medium ? '16px' : p.lineHeight};
  font-weight: ${p => p.bold ? '600' : '400'};
  
  &.ticket {
    padding: 15px 25px;
    text-align: center;
    
    &:last-child {
      width: 130px;
      border-left: 1px dashed #fff;
    }
  }
  
  &.ticket_span {
    padding: 25px;
    text-align: center;
    
    &:last-child {
      width: 130px;
      border-left: 1px dashed #22262f;
    }
  }
  
  @media only screen and (max-width : 400px) {
    &.ticket {
    padding: 10px 10px;
    text-align: center;
    }
  }
`;