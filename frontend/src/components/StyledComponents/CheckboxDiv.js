import styled from 'styled-components';

export const CheckboxDiv = styled.div`
  width: 24px;
  height: 24px;
  background-color: transparent;
  border: 1px solid #c7c7c7;
  border-radius: 6px;
  position: relative;
  margin-right: 15px;
  cursor: pointer;
  
  &.checked {
    
    &:before {
      content: '';
      position:absolute;
      height: 19px;
      width: 19px;
      background: url(${p => p.src}) center no-repeat;
      border-radius: 50%;
      top: 1px;
      left: 2px;
    }
  }
`;