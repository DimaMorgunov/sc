import styled from 'styled-components';

export const ListItemLi = styled.li`
  padding: 5px;
  
  &.footer_navlinks_li {
    padding: 5px 10px;
  }
  
  .faTwitter {
      &:hover {
        color: #55acee;
      }
    }

    .faFacebook {
      &:hover {
        color: #3b5999;
      }
    }

    .faInstagram {
      &:hover {
        color: #e4405f;
      }
    }

    .faFacebookMessenger {
      &:hover {
        color: #0084ff;
      }
    }

    .faViber {
      &:hover {
        color: #8f5db7;
      }
    }

    .faTelegram {
      &:hover {
        color: #0088cc;
      }
    }
    
    &.without_padding {
      padding: 0;
      
      &:first-child,
      &:nth-child(1),
      &:nth-child(2),
      &:nth-child(3),
      &:nth-child(4),
      &:nth-child(5) {
        padding-left: 30px;
      }
    }
    
    
`;
