import styled from 'styled-components';

export const Footer = styled.footer`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: #12161f;
  min-height: 165px;
  color: #fff;
  padding: 0 5%;
`;
