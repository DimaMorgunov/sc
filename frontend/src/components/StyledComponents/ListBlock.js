import styled from 'styled-components';

export const ListBlock = styled.ul`
  list-style: none;
  display: flex;
  flex-wrap: wrap;
  flex-direction: ${p => p.column ? 'column' : 'row'};
  padding: 0;
  margin: 0;
  
  &.footer_navlinks {
    max-width: 80%;
    font-size: 13pt;
  }
  
  &.header_nav_list {
    margin-left: 50px;
  }
  
  @media only screen and (max-width : 1325px) {
    &.footer_navlinks {
      max-width: 60%;
    }
  }
    
  @media only screen and (max-width : 1150px) {
    &.header_nav_list {
      display: none;
    }
  }
  
  @media only screen and (max-width : 1024px) {
    &.footer_navlinks {
      display: none;
    }
  }
  
  @media only screen and (max-width : 768px) {
    .link_a {
      margin: 0 auto;
    }
  }
`;
