import styled from 'styled-components';

export const ImageBlock = styled.img`
  height: ${p => p.imgHeight ? p.imgHeight+'px' : 'auto'};
  width: ${p => p.imgWidth ? p.imgWidth+'px' : '100%'};
  position: relative;
  margin: ${p => p.imgMargin ? p.imgMargin : '0' };
  
  
  &.share_image {
    width: 50%;
    max-width: 98%;
    border-radius: 15px;
    margin: 0 40px 20px 0;
    float: left;
  }
  
  &.share_img {
    border-radius: 15px;
  }
  
  &.film_img {
    border-radius: 15px;
  }

  &.bonus_img {
    margin: 0 auto;
  }
  
  &.mobile_img {
    border-radius: 15px;
    display: none;
    width: 90%;
  }
  
  @media only screen and (max-width: 1795px) {
    &.film_picture {
      width: 80%;
      height: auto;
    }
  }
  
  @media only screen and (max-width: 768px) {
    /*width: 100%;
    height: auto;*/
  }
  
  @media only screen and (max-width : 585px) {
    &.share_image {
      width: 100%;
      margin: 0 0 20px 0;
    }
    
    &.mobile_img {
      display: block;
    }
  }
  
  @media only screen and (max-width : 425px) {
    &.bonus_img {
      width: 100%;
      height: auto;
    }
  }
`;