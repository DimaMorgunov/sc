import styled from 'styled-components';

export const Button = styled.div`
  z-index: 1;
  background-color: ${p => p.theme.backgroundColor};
  margin: ${p => p.btnMargin ? p.btnMargin : '0'};
  padding: ${p => p.btnPadding ? p.btnPadding : '12px 25px'};
  border-radius: ${p => p.btnRadius ? p.btnRadius : '23px'};
  cursor: pointer;
  color: #fff;
  font-size: 17px;
  font-weight: 500;
  text-align: ${p => p.centered ? 'center' : 'left'};
  
  &:hover {
    background-color: ${p => p.theme.hoveredBackgroundColor};
  }
  
  &.disabled {
    background-color: #6e6f6f;
  }
    
  a {
    outline: none;
    text-decoration: none;
    
    &:hover {
      color: #fff;
    }
  }

  &.film_btn {
    position: absolute;
    bottom: 5px;
    left: 33px;
    z-index: 3;
    width: 184px;
    padding: 7px 10px 8px 10px;
    text-align: center;
    display: none;
    font-size: 15px;
    
    a {
      font-size: 15px;
      color: #fff;
      outline: none;
      text-decoration: none;
    }
  }
  
  &.sign_up_link {
    color: #00a6ea;
    padding: 14px;
    margin-left: 10px;
    position: relative;
    
    &:before {
      content: '';
      position: absolute;
      bottom: 7px;
      left: 12px;
      width: 90px;
      height: 1px;
      border-bottom: 1px dashed #00a6ea;
    }
    
    &:hover {
      color: #0f74a8;
    }
  }
  
  &.underlined_link_btn {
    color: #00a6ea;
    padding: 3px 0;
    margin-left: 10px;
    border-bottom: 1px dashed #00a6ea;
    border-radius: 0;
    
    &:hover {
      border-bottom: 1px solid #00a6ea;
    }
  }
  
  &.forgot_password {
    color: #00a6ea;
    padding: 14px;
    margin-left: 10px;
    position: relative;
    
    &:before {
      content: '';
      position: absolute;
      bottom: 7px;
      left: 12px;
      width: 157px;
      height: 1px;
      background-color: #00a6ea;
    }
    
    &:hover {
      color: #0f74a8;
    }
  }
`;
