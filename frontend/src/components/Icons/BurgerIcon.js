export class BurgerIcon extends Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="45" height="30" viewBox="0 0 45 30">
        <defs>
          <path className="icon" id="y7kra" d="M1848 36v-4h45v4zm0 13v-4h45v4zm0 13v-4h45v4z"/>
        </defs>
        <g>
          <g transform="translate(-1848 -32)">
            <use fill="#fff" xlinkHref="#y7kra"/>
          </g>
        </g>
      </svg>
    )
  }
}
