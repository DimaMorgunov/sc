import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import classNames from 'classnames';
import {
  Image,
  Wrapper,
  ImageBlock
} from "../index";

export default class FilmScreenSliderWrapper extends Component {
  render() {
    const { screens } = this.props;

    const settings = {
      focusOnSelect: true,
      infinite: true,
      centerMode: true,
      autoplay: true,
      autoplaySpeed: 3000,
      slidesToShow: 2,
      slidesToScroll: 1,
      speed: 500,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    };
    return (
      <Slider {...settings}>
        {
          screens &&
          screens.map((screen, i) => (
            <Wrapper key={i}>
              <ImageBlock
                src={screen.poster}
                imgHeight={531}
                imgWidth={790}
                imgMargin="0 auto"
                className={classNames("film_img", "film_picture")}
              />
            </Wrapper>

          ))
        }
      </Slider>
    );
  }
}
