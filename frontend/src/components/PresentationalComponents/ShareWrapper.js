import { Link } from 'react-router-dom';
import {
  Wrapper,
  ImageBlock,
  TextBlock,
  Button
} from "../index";
import {orangeButton} from "../../themes";

export class ShareWrapper extends Component {


  render() {
    const { share, big } = this.props;

    return (
      big ?
      <React.Fragment>
        <Wrapper
          wrapDirection="row"
          wrapItems="flex-start"
        >
          <Wrapper
            wrapMaxWidth="60%"
            wrapMinWidth="60%"
            className="share_image_wrapper"
          >
            <ImageBlock src={share[0].image} className="share_img" />
          </Wrapper>
          <Wrapper
            wrapMaxWidth="40%"
            wrapMinWidth="40%"
            className="share_text"
            wrapItems="flex-start"
            wrapPadding="0 0 0 30px"
          >
            <TextBlock font="20px" lineHeight="19px" color={ share[0].type_of_post === 'Акція' ? '#fd5f00' : '#009ad7' } >
              {share[0].type_of_post}
            </TextBlock>
            <Link to={share[0].seo_inline.url}>
              <TextBlock font="30px" textMargin="5px 5px 15px 5px">
                {share[0].title}
              </TextBlock>
            </Link>
            <TextBlock font="19px" lineHeight="36px" color="#c1c1c1">
              {share[0].short_description}
            </TextBlock>
            <Wrapper
              wrapMargin="20px 0 0 0"
              wrapItems="flex-start"
            >
              <Link to={share[0].seo_inline.url}>
                <Button theme={orangeButton}>
                  Детальніше
                </Button>
              </Link>
            </Wrapper>
          </Wrapper>
        </Wrapper>

        <Wrapper
          wrapDirection="row-reverse"
          wrapItems="flex-end"
          wrapMargin="20px 0 0 0"
          wrapContent="flex-end"
          className="share_wrap_block"
        >
          <Wrapper
            wrapMaxWidth="60%"
            wrapMinWidth="60%"
            className="share_image_wrapper"
          >
            <ImageBlock src={share[1].image} className="share_img" />
          </Wrapper>
          <Wrapper
            wrapMaxWidth="40%"
            wrapMinWidth="40%"
            className="share_text"
            wrapItems="flex-start"
            wrapPadding="20px 30px 0 0"
          >
            <TextBlock font="20px" lineHeight="19px" color={ share[1].type_of_post === 'Акція' ? '#fd5f00' : '#009ad7' } >
              {share[1].type_of_post}
            </TextBlock>
            <Link to={share[1].seo_inline.url}>
              <TextBlock font="30px" textMargin="5px 5px 15px 5px">
                {share[1].title}
              </TextBlock>
            </Link>
            <TextBlock font="19px" lineHeight="36px" color="#c1c1c1">
              {share[1].short_description}
            </TextBlock>
            <Wrapper
              wrapMargin="20px 0 0 0"
              wrapItems="flex-start"
            >
              <Link to={share[1].seo_inline.url}>
                <Button theme={orangeButton}>
                  Детальніше
                </Button>
              </Link>
            </Wrapper>
          </Wrapper>
        </Wrapper>
      </React.Fragment>
        :
      <React.Fragment>
          <Wrapper
            wrapDirection="row"
            wrapItems="flex-start"
          >
            <Wrapper
              wrapMaxWidth="60%"
              wrapMinWidth="60%"
              className="share_image_wrapper"
            >
              <ImageBlock src={share.image} className="share_img" />
            </Wrapper>
            <Wrapper
              wrapMaxWidth="40%"
              wrapMinWidth="40%"
              className="share_text"
              wrapItems="flex-start"
              wrapPadding="0 0 0 30px"
            >
              <TextBlock font="20px" lineHeight="19px" color={ share.type_of_post === 'Акція' ? '#fd5f00' : '#009ad7' } >
                {share.type_of_post}
              </TextBlock>
              <Link to={share.seo_inline.url}>
                <TextBlock font="27px" textMargin="5px 5px 15px 5px">
                  {share.title}
                </TextBlock>
              </Link>
              <TextBlock font="16px" lineHeight="34px" color="#c1c1c1" textMaxWidth="98%">
                {share.short_description}
              </TextBlock>
              <Wrapper
                wrapMargin="20px 0 0 0"
                wrapItems="flex-start"
              >
                <Link to={share.seo_inline.url}>
                  <Button theme={orangeButton}>
                    Детальніше
                  </Button>
                </Link>
              </Wrapper>
            </Wrapper>
          </Wrapper>
        </React.Fragment>
    )
  }
}