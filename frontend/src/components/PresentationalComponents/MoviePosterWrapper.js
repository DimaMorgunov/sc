import { Link } from 'react-router-dom';
import classNames from 'classnames';

import {
  Wrapper,
  TextBlock,
  Image,
  Button,
  Label
} from "../index";
import play from '../../img/play.png';
import {orangeButton} from "../../themes";
import {setDateToPremier, setFilm} from "../../store";
import connect from "react-redux/es/connect/connect";

export class MoviePosterWrapperComponent extends Component {

  setFilmForFilter = (data) => {


    this.props.dispatch(setFilm(data))
    this.props.dispatch(setDateToPremier(this.props.currentFilm.film_release_date))
  };

  render() {
    const { currentFilm, mainPage } = this.props;

    return (
      <Wrapper
        wrapContent="flex-end"
        className={classNames("image_bg", mainPage ? 'main_slider' : 'one_movie_img')}
        picture={mainPage ? currentFilm.pictures : currentFilm.detail_background}
      >
        <Wrapper
          /*wrapPadding="30px 65px"*/
          wrapItems="flex-start"
          className={classNames("darken", "smaller_padding", "wrapPadding")}
        >
          <a href={currentFilm.trailer} target="_blank">
            <Wrapper wrapDirection="row">
              <Image src='/static/img/play.png' imgHeight={37} imgWidth={37} imgMargin="0 10px 0 0"/>
              <TextBlock className="link_text" font="18px">
                Дивитись трейлер
              </TextBlock>
            </Wrapper>
          </a>

          <Wrapper
            wrapDirection="row"
            wrapMargin="20px 0 0"
          >
            <Link to={currentFilm.seo_inline.url} className="slider_movie_title">
              <TextBlock
                color="#009ad7"
                font="35px"
                textMargin="0 30px 0 0"
                lineHeight="40px"
              >
                {currentFilm.title}
              </TextBlock>
            </Link>
            {
              mainPage &&
              <React.Fragment>
                <Label className="black_label">{currentFilm.age}+</Label>
                <Label className="margined_right" blue>{currentFilm.format}</Label>
              </React.Fragment>

            }
            <Link to="/schedule/">
              <Button theme={orangeButton} onClick={() => this.setFilmForFilter(currentFilm.title)}>
                Придбати квиток
              </Button>
            </Link>
          </Wrapper>
        </Wrapper>
      </Wrapper>
    )
  }
}

export const MoviePosterWrapper = connect(({ state }) => ({
    currentFilmToFilter: state.currentFilmToFilter
}))(MoviePosterWrapperComponent);