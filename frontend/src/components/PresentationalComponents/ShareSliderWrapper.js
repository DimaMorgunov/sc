import React from "react";
import Slider from "react-slick";
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {
  Image,
  Wrapper,
  TextBlock
} from "../index";

export default class ShareSliderWrapper extends Component {
  render() {
    const { shares, aboutUsPage } = this.props;

    const settings = {
      focusOnSelect: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      centerMode: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      speed: 500,
      responsive: [
        {
          breakpoint: 1325,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 940,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    };
    return (
      aboutUsPage ?
        <Slider {...settings}>
          {
            shares &&
            shares.map((screen, i) => (
              <div key={i}>
                <Wrapper>
                  <Image
                    src={screen}
                    imgHeight={481}
                    imgWidth={500}
                    imgMargin="0 auto"
                    className={classNames("film_img", "share_slider_img")}
                  />
                </Wrapper>
              </div>
            ))
          }
        </Slider>
        :
        <Slider {...settings}>
          {
            shares &&
            shares.map((screen, i) => (
              <Link to={screen.seo_inline.url} key={i}>
                <div>
                  <Wrapper>
                    <Image
                      src={screen.image}
                      alt={screen.title}
                      imgHeight={481}
                      imgWidth={500}
                      imgMargin="0 auto"
                      className={classNames("film_img", "share_slider_img")}
                    />
                    <TextBlock centered textMargin="10px 0 0 0" textPadding="0 15px">
                      {screen.title}
                    </TextBlock>
                  </Wrapper>
                </div>
              </Link>
            ))
          }
        </Slider>
    );
  }
}
