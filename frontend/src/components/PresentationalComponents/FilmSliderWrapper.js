import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css"

import {
  MovieContainer
} from "../index";

export default class FilmSliderWrapper extends Component {
  state = {
    slideIndex: 0,
    updateCount: 0
  };


  render() {
    const { films, rent } = this.props;

    const settings = {
      className: "center",
      centerMode: true,
      autoplay: true,
      autoplaySpeed: 4000,
      infinite: true,
      centerPadding: "10px",
      slidesToShow: 5,
      slidesToScroll: 1,
      initialSlide: 3,
      speed: 500,
      responsive: [
        {
          breakpoint: 1440,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 1019,
          settings: {
            slidesToShow: 1
          }
        },
      ],
      afterChange: () =>
        this.setState(state => ({ updateCount: state.updateCount + 1 })),
      beforeChange: (current, next) => this.setState({ slideIndex: next })
    };
    return (
      <React.Fragment>
        <Slider ref={slider => (this.slider = slider)} {...settings}>
          {
            films &&
            films.map(film => (
              <div key={film.id}>
                <MovieContainer
                  film={film}
                  rent={rent}
                  sliderFilm
                />
              </div>
            ))
          }
        </Slider>
      </React.Fragment>
    );
  }
}