import { Link } from 'react-router-dom';

import {
  Footer,
  Wrapper,
  TextBlock,
  ListBlock,
  ListItemLi,
  Image,
  Span
} from '../index';

import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faTelegram from '@fortawesome/fontawesome-free-brands/faTelegram';
import faFacebook from '@fortawesome/fontawesome-free-brands/faFacebookF';
import faInstagram from '@fortawesome/fontawesome-free-brands/faInstagram';
import faFacebookMessenger from '@fortawesome/fontawesome-free-brands/faFacebookMessenger';
import faViber from '@fortawesome/fontawesome-free-brands/faViber';

import { FOOTER_LINKS } from '../../consts';
import {CareerModal, FeedbackModal} from "../../containers";

export class FooterComponent extends Component {
  state = {
    feedback: false,
    career: false
  };

  toggleDrawer = (side, open) => {

    this.setState({
      [side]: open
    });
    document.body.classList.toggle('modal_opened');
  };

  render() {
    const { feedback, career } = this.state;

    return (
      <Footer id="footer">
        <Wrapper
          wrapDirection="row"
          wrapContent="space-between"
          wrapMargin="5px 0"
          className="footer_navlinks"
        >
          <ListBlock className="footer_navlinks">
            {
              FOOTER_LINKS.map((link) => {
                if (link.to) {
                  return (
                    <ListItemLi key={link.title} className="footer_navlinks_li">
                      <Link to={link.to}>
                        {link.title}
                      </Link>
                    </ListItemLi>
                  );
                }

                if (link.openForm) {
                  return (
                    <ListItemLi
                      key={link.title}
                      className="footer_navlinks_li"
                      onClick={() => this.toggleDrawer(link.openForm, true)}
                    >
                      <Link to="#">
                        {link.title}
                      </Link>
                    </ListItemLi>
                  );
                }

                if (link.slideTo) {
                  return (
                    <ListItemLi key={link.title} className="footer_navlinks_li">
                      <Link to={link.slideTo}>
                        {link.title}
                      </Link>
                    </ListItemLi>
                  );
                }
              })
            }
          </ListBlock>
          <Wrapper
            wrapDirection="row"
            wrapContent="center"
            className="footer_icons"
          >
            <a href="https://www.facebook.com/smartcinemaua/" target="_blank" className="icon_link">
              <FontAwesomeIcon
                icon={faFacebook}
                size="2x"
                className="faFacebook"
              />
            </a>

            <a href="https://www.instagram.com/smart_cinema/" target="_blank" className="icon_link">
              <FontAwesomeIcon
                icon={faInstagram}
                size="2x"
                className="faInstagram"
              />
            </a>

            <a href="https://wep.wf/eyqf6m" target="_blank" className="icon_link">
              <FontAwesomeIcon
                icon={faFacebookMessenger}
                size="2x"
                className="faFacebookMessenger"
              />
            </a>

            <a href="https://wep.wf/9u8b3x" target="_blank" className="icon_link">
              <FontAwesomeIcon
                icon={faTelegram}
                size="2x"
                className="faTelegram"
              />
            </a>

            <a href="https://wep.wf/6egkx9" target="_blank" className="icon_link">
              <FontAwesomeIcon
                icon={faViber}
                size="2x"
                className="faViber"
              />
            </a>

          </Wrapper>
        </Wrapper>

        <Wrapper
          wrapDirection="row"
          wrapContent="space-between"
          wrapMargin="5px 0"
        >
          <Wrapper
            wrapDirection="row"
            wrapContent="space-between"
            className="footer_wrapper_bottom"
          >
            <TextBlock className="footer_text">
                Гаряча лінія кінотеатру: <Span bold> 096-003-50-50</Span> у Вінниці, <Span bold>073-002-50-50</Span> у Хмельницькому.
                За детальною інформацією з питань розміщення реклами та співробітництва, звертайтесь за телефоном <Span bold>067-788-71-54</Span> або <Span bold>olenapodolyanik@gmail.com</Span>
            </TextBlock>
          </Wrapper>

          <Wrapper
            wrapDirection="row"
            wrapContent="flex-end"
            className="footer_wrapper_bottom"
          >
            <ListBlock>
              <a className="link_a" href="https://itunes.apple.com/ru/app/smartcinema/id1186208172?mt=6" target="_blank">
                <ListItemLi>
                  <Image src='/static/img/appStore.png' imgHeight={40} imgWidth={135}/>
                </ListItemLi>
              </a>
              <a className="link_a" href="https://play.google.com/store/apps/details?id=ua.kinoteatr.SmartCinema" target="_blank">
                <ListItemLi>
                  <Image src='/static/img/googlePlay.png' imgHeight={40} imgWidth={135}/>
                </ListItemLi>
              </a>
            </ListBlock>

            <TextBlock className="rights_text">
              © 2018 SmartCinema.ua.
              Всі права захищено.
            </TextBlock>
          </Wrapper>
        </Wrapper>

        <FeedbackModal
          show={feedback}
          closeModal={this.toggleDrawer}
        />

        <CareerModal
          show={career}
          closeModal={this.toggleDrawer}
        />

      </Footer>
    )
  }
}
