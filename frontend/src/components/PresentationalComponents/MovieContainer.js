import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import {
  Wrapper,
  Image,
  Label,
  Button
} from "../index";
import {orangeButton} from "../../themes";
import {setDateToPremier, setFilm} from "../../store";

export class MovieContainerComponent extends Component {

  setFilmForFilter = (data) => {

    this.props.dispatch(setFilm(data.title));
    this.props.dispatch(setDateToPremier(data.film_release_date))
  };

  render() {
    const { film, rent, hovered, sliderFilm } = this.props;

    return (
      <React.Fragment>
        <Link to={hovered ? film.seo_inline.url : sliderFilm ? film.seo_inline.url : '#'}>
          <Wrapper
            wrapMinWidth={sliderFilm ? "316px" : "250px"}
            wrapMaxWidth={sliderFilm ? "316px" : "250px"}
            wrapMargin={sliderFilm ? "24px auto" : "24px 25px"}
            className={classNames("film_wrapper", {hovered: hovered})}
          >
            <Wrapper
              wrapItems="flex-start"
              wrapDirection="row"
              className="film_labels"
            >
              <Label blue>{film.format}</Label>
              <Label>{film.age}+</Label>
            </Wrapper>

              <Image
                src={film.poster}
                alt={"Фильм " + film.title}
                imgHeight={sliderFilm ? 480 : 380}
                imgWidth={sliderFilm ? 316 : 250}
                className="film_img"
              />
            {
              rent ?
              <Button theme={orangeButton} className="film_btn" onClick={() => this.setFilmForFilter(film)}>
                <Link to="/schedule/">
                  Придбати квиток
                </Link>
              </Button> :

              <Button theme={orangeButton} className="film_btn">
                {film.film_release_date}
              </Button>
            }
          </Wrapper>
        </Link>
      </React.Fragment>
    )
  }
}


export const MovieContainer = connect(({ state }) => ({
  currentFilm: state.currentFilm,
  token: state.token,
  data: state.data
}))(MovieContainerComponent);