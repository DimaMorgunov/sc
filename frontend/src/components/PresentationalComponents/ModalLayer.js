import classNames from 'classnames';
import {
  Wrapper,
  Image,
  TextBlock
} from "../../components";
import close from '../../img/close.png';
import back from '../../img/back.png';

export class ModalLayer extends Component {
  render() {

    if(!this.props.show) {
      return null;
    }
    return (
      <Wrapper
        className={classNames("modal_background", { "selectModal": this.props.selectModal })}
      >
        <Wrapper
          className="modal_all"
          wrapItems="flex-end"
          wrapMinWidth={this.props.buyModal ? "265px" : this.props.selectModal ? "265px" : this.props.showZip ? "500px": "320px"}
          wrapMaxWidth={this.props.buyModal ? "890px" : this.props.selectModal ? "1000px": this.props.showZip ? "500px" : "1000px"}
        >
          <Wrapper
            wrapDirection="row"
            wrapContent={this.props.buyModal || this.props.registerModal ? "space-between" : "flex-end"}
            wrapItems={this.props.buyModal || this.props.registerModal ? "space-between" : "flex-end"}
          >
            {
              this.props.buyModal &&
              <Image
                src={back}
                imgHeight={21}
                imgWidth={10}
                onClick={this.props.backToSelectModal}
                className="close_modal"
                imgMargin="15px"
              />
            }
            {
              this.props.registerModal &&
                <Wrapper
                  wrapDirection="row"
                  wrapMinWidth="30%"
                  wrapMaxWidth="30%"
                  onClick={this.props.backToLogin}
                  className={classNames("pointer", "sign_in_link")}
                >
                  <Image
                    src={back}
                    imgHeight={21}
                    imgWidth={10}
                    className={classNames("close_modal", "sign_in_link")}
                    imgMargin="15px"
                  />
                  <TextBlock color="#00a6ea" className="sign_in_link" >
                    назад до входу
                  </TextBlock>
                </Wrapper>
            }
            {
              !this.props.confirm &&
                <Image
                  src={close}
                  imgHeight={21}
                  imgWidth={21}
                  onClick={this.props.closeModal}
                  className={classNames("close_modal", { buyModal: this.props.buyModal })}
                  imgMargin="15px"
                />
            }

          </Wrapper>

          <Wrapper
            className="modal"
            wrapItems="flex-start"
          >
            {this.props.children}
          </Wrapper>
        </Wrapper>
      </Wrapper>
    )
  }
}