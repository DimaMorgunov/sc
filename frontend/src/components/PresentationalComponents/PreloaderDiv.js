import loader from '../../img/loader.gif';
import {
  Image,
  Wrapper
} from "../index";

export const PreloaderDiv = (props) => (
  props.show &&
  <Wrapper className="preloader">
    <Image
      src={loader}
      imgHeight={150}
      imgWidth={150}
      className="preloader_gif"
    />
  </Wrapper>
);
