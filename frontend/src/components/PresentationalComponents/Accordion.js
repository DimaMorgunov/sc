export class Accordion extends Component {

  componentDidMount() {
    this.handleClick();
  }

  handleClick() {
    const acc = this.accordion.children;
    for (let i = 0; i < acc.length; i++) {
      let a = acc[i];
      a.onclick = () => a.classList.toggle("active");
    }
  }

  render() {

    return (
      <div
        style={{ width: '100%' }}
        ref={el => this.accordion = el}
        onClick={this.handleClick}>
        {this.props.children}
      </div>
    )
  }
}
