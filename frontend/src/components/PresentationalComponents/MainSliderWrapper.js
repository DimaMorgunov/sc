import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css"

import {
  MoviePosterWrapper,
  ShareWrapper
} from "./index";

export default class MainSliderWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shares: null
    };
    props.shares && this.props.big && this.getShares(props.shares);
  };

  getShares(shares) {
    let newArr = [];

    shares.map((share, i) => {

      if(i % 2 === 0) {
        newArr[i] = [];
        newArr[i].push(share);
      } else {
        newArr[i-1].push(share);
      }

      this.state = { shares: newArr };
    })
  }

  componentDidUpdate(prevProps) {

    if(this.props.big) {
      if(prevProps.shares !== this.props.shares) {
        let newArr = [];

        this.props.shares.map((share, i) => {

          if(i % 2 === 0) {
            newArr[i] = [];
            newArr[i].push(share);
          } else {
            newArr[i-1].push(share);
          }

          this.setState({ shares: newArr });
        })
      }
    }
  }

  render() {
    const { films, mainPage, big } = this.props;
    const { shares } = this.state;

    const settings = {
      infinite: true,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      slidesToScroll: 1,
      speed: 500
    };
    return (
      <Slider {...settings}>
        {
          films &&
          films.map(film => (
            <div key={film.id}>
              <MoviePosterWrapper currentFilm={film} mainPage={mainPage} />
            </div>
          ))
        }
        {
          big ?
            shares &&
            shares.map((share, i) => (
              <div key={`${share.title}_${i}`}>
                <ShareWrapper big share={share} />
              </div>
            )) :
            this.props.shares &&
            this.props.shares.map((share, i) => (
              <div key={`${share.title}_${i}`}>
                <ShareWrapper share={share} />
              </div>
            ))
        }
      </Slider>
    );
  }
}
