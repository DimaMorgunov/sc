import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';
import classNames from 'classnames';
import 'moment/locale/uk';
import 'react-datepicker/dist/react-datepicker.css';
import {
  Header,
  Image,
  ListBlock,
  ListItemLi,
  Wrapper,
  TextBlock,
  BurgerIcon
} from "../index";

import {
    SignInModal,
    SignUpModal,
    FeedbackModal,
    CareerModal,
    ConfirmModal,
    CardModal,
    ThankModal,
    LostPassModal,
    SendPassModal,
} from "../../containers";

import Menu from '@material-ui/core/Menu';
import ListItem from '@material-ui/core/ListItem';
import Drawer from '@material-ui/core/Drawer';

import {setCity, setCityId, logoutAsync, setFilm, setDateToPremier} from "../../store";
import { LINKS } from "../../consts";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faTelegram from '@fortawesome/fontawesome-free-brands/faTelegram';
import faFacebook from '@fortawesome/fontawesome-free-brands/faFacebookF';
import faInstagram from '@fortawesome/fontawesome-free-brands/faInstagram';
import faFacebookMessenger from '@fortawesome/fontawesome-free-brands/faFacebookMessenger';
import faViber from '@fortawesome/fontawesome-free-brands/faViber';

export class HeaderComponentWrapper extends Component {
  state = {
    right: false,
    anchorEl: null,
    anchorElLogin: null,
    signIn: false,
    signUp: false,
    feedback: false,
    career: false,
    confirm: false,
    card: false,
    thank: false,
      lostPass: false,
      sendpass: false,
      bindcard: false,
  };

  componentDidUpdate(prevProps) {
    // if(!this.props.userInfo) return;
//
    // if(prevProps.userInfo !== this.props.userInfo && !this.props.success) {
    //   document.body.classList.toggle('modal_opened');
    // }
    if (prevProps.userInfo !== this.props.userInfo) {
        if(!this.state.card && !this.state.thank && this.state.signIn){
          this.toggleDrawer('signIn', false);
        }
      }
  }

  handleClickOpen = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = (city, id) => {


    this.setState({ anchorEl: null });
    if(city !== 'close') {

      this.props.dispatch(setCityId(id));
      this.props.dispatch(setCity(city));
      window.localStorage.setItem('chosen_city_id', id);
      window.localStorage.setItem('chosen_city', city);
    }
  };

  handleClickOpenLogin = (event) => {
    this.setState({ anchorElLogin: event.currentTarget });
  };

  handleCloseLogin = (type, e) => {
    this.setState({ anchorElLogin: null });

    if(type) {
      this.toggleDrawer(type, true, e);
    }
  };

  handleLogout = () => {
    this.setState({
      anchorElLogin: null,
      signIn: false,
      signUp: false,
      feedback: false,
      career: false,
      confirm: false,
      card: false,
      thank: false,
        lostPass: false,
        edit: false,
        sendpass: false,
        bindcard: false,
    });

    this.props.dispatch(logoutAsync({
      token: this.props.token,
      userToken: this.props.userToken
    }))
  };

  purgeCurrentFilm = () =>{
    this.props.dispatch(setFilm(null));
    this.props.dispatch(setDateToPremier(null))
  }

  toggleDrawer = (side, open, e) => {
      if(e && (side === 'feedback' || side === 'career' || side === 'thank')) {
          e.stopPropagation();

      }

    this.setState({
        right: false,
        signIn: false,
        signUp: false,
        feedback: false,
        career: false,
        confirm: false,
        card: false,
        thank:false,
        lostPass: false,
        edit: false,
        sendpass: false,
        [side]: open
    });

    if( open === true ) {
        document.body.classList.add('modal_opened');
    }
    if ( open === false ) {
        document.body.classList.remove('modal_opened');
    }

  };

  render() {
    const {
      anchorEl,
      anchorElLogin,
      signIn,
      signUp,
      feedback,
      career,
      confirm,
      card,
      thank,
      lostPass,
      sendpass,
    } = this.state;
    const {
      data,
      city,
      userLogin,
      userName
    } = this.props;

    return (
      <Header>

        <Wrapper
          wrapDirection="row"
          wrapMinWidth="auto"
        >
          <Wrapper
            wrapContent="center"
            className="logo_wrapper"
          >
            <Link to="/">
              <Image src='/static/img/logo.png' alt='SmartCinema - кінотеатр у Винниці та Хмельницькому' imgHeight={50} imgWidth={207}/>
            </Link>
          </Wrapper>
          <ListBlock className="header_nav_list">
            <ListItemLi>
              <TextBlock className="header_nav_item">
                <NavLink to="/schedule" activeClassName="active" onClick={() => this.purgeCurrentFilm()}>
                  Розклад
                </NavLink>
              </TextBlock>
            </ListItemLi>
            <ListItemLi>
              <TextBlock className="header_nav_item">
                <NavLink to="/movies" exact activeClassName="active">
                  Фільми
                </NavLink>
              </TextBlock>
            </ListItemLi>
          </ListBlock>
        </Wrapper>

        <Wrapper
          wrapDirection="row"
          wrapMinWidth="auto"
        >
          <Wrapper
            wrapDirection="row"
            wrapContent="flex-end"
            wrapMinWidth="185px"
            className={classNames("header_dropdown", 'city_wrap')}
            aria-owns={anchorEl ? 'simple-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClickOpen}
          >
            <Image src='/static/img/city_img.png' imgHeight={19} imgWidth={13} className={ city ? '' : "city_img" }/>
            <TextBlock className="linked_text" >
              {
                city ? city : 'Ваше місто'
              }
            </TextBlock>
          </Wrapper>

          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={() => this.handleClose('close')}
          >
            {
              Object.keys(data) != 0 &&

              data.branch.map(city => {

                return (
                  <ListItem
                    button
                    onClick={() => this.handleClose(city.name, city.id)}
                    key={city.id}
                    className="header_menu_city"
                  >
                    {city.name}
                  </ListItem>
                )
              })
            }
          </Menu>

          {
            userLogin && userName ?
              <React.Fragment>
                <Wrapper
                  wrapDirection="row"
                  wrapMinWidth="190px"
                  className="header_dropdown"
                  aria-owns={anchorElLogin ? 'simple-menu2' : null}
                  aria-haspopup="true"
                  onClick={this.handleClickOpenLogin}
                >
                  <Image src='/static/img/login.png' imgHeight={29} imgWidth={29}/>
                  <TextBlock className="linked_text" >
                    {userName}
                  </TextBlock>
                </Wrapper>

                <Menu
                  id="simple-menu2"
                  anchorEl={anchorElLogin}
                  open={Boolean(anchorElLogin)}
                  onClose={() => this.handleCloseLogin()}
                >
                  <Link to="/user/">
                    <ListItem
                      button
                      onClick={() => this.handleCloseLogin()}
                    >
                      Особистий кабінет
                    </ListItem>
                  </Link>

                  <Link to="/tickets">
                    <ListItem
                      button
                      onClick={() => this.handleCloseLogin()}
                    >
                      Мої квитки
                    </ListItem>
                  </Link>

                  <a>
                    <ListItem
                      button
                      onClick={() => this.handleLogout()}
                    >
                      Вийти
                    </ListItem>
                  </a>
                </Menu>

                <CardModal
                  show={card}
                  closeModal={this.toggleDrawer}
                />

                <ThankModal
                  show={thank}
                  closeModal={this.toggleDrawer}
                />

              </React.Fragment>
              :
              <React.Fragment>
                <Wrapper
                  wrapDirection="row"
                  wrapMinWidth="190px"
                  className="header_dropdown"
                  aria-owns={anchorElLogin ? 'simple-menu2' : null}
                  aria-haspopup="true"
                  onClick={this.handleClickOpenLogin}
                >
                  <Image src='/static/img/login.png' imgHeight={29} imgWidth={29}/>
                  <TextBlock className="linked_text" >
                    Особистий кабінет
                  </TextBlock>
                </Wrapper>

                <Menu
                  id="simple-menu2"
                  anchorEl={anchorElLogin}
                  open={Boolean(anchorElLogin)}
                  onClose={() => this.handleCloseLogin()}
                >
                  <ListItem
                    button
                    onClick={(e) => this.handleCloseLogin('signUp', e)}
                  >
                    Зареєструватися
                  </ListItem>

                  <ListItem
                    button
                    onClick={(e) => this.handleCloseLogin('signIn', e)}
                  >
                    Увійти
                  </ListItem>
                </Menu>

                <SignInModal
                  show={signIn}
                  closeModal={this.toggleDrawer}
                />
                  {this.state.signUp == true ? (
                      <SignUpModal
                          show={signUp}
                          name={'signUp'}
                          cities={Object.keys(data) != 0 && data.branch}
                          closeModal={this.toggleDrawer}
                      />
                  ) : <LostPassModal
                      show={lostPass}
                      name="lostPass"
                      closeModal={this.toggleDrawer}
                  />}
                  <SendPassModal
                      show={sendpass}
                      closeModal={this.toggleDrawer}
                  />
                <ConfirmModal
                  show={confirm}
                  closeModal={this.toggleDrawer}
                />

              </React.Fragment>
          }

          <Wrapper
            wrapDirection="row"
            wrapMinWidth="70px"
            className="header_dropdown"
          >
            <div onClick={() => this.toggleDrawer('right', true)}>
              <BurgerIcon />
            </div>

              <Drawer
                anchor="right"
                open={this.state.right}
                onClose={() => this.toggleDrawer('right', false)}
                className="right_drawer"
              >
                <div
                  tabIndex={0}
                  role="button"
                  onClick={() => this.toggleDrawer('right', false)}
                  onKeyDown={() => this.toggleDrawer('right', false)}
                  className="div_inside"
                >
                  <Wrapper>

                    <ListItem className="mobile_header_link">
                      <Wrapper
                        wrapDirection="row"
                        wrapPadding={ city ? "10px 5px 10px 30px" : "10px 5px 10px 50px" }
                        className={classNames('city_wrap')}
                        aria-owns={anchorEl ? 'simple-menu' : null}
                        aria-haspopup="true"
                        onClick={this.handleClickOpen}
                      >
                        <Image src='/static/img/city_img.png' imgHeight={19} imgWidth={13} className={classNames("img_mobile", { city_img: !city } )}/>
                        <TextBlock className="linked_text" >
                          {
                            city ? city : 'Ваше місто'
                          }
                        </TextBlock>
                      </Wrapper>
                    </ListItem>

                    <ListItem className="mobile_header_link">
                      {
                        userLogin ?
                        <Wrapper
                          wrapDirection="row"
                          wrapPadding="10px 5px 10px 25px"
                          aria-owns={anchorElLogin ? 'simple-menu2' : null}
                          aria-haspopup="true"
                          onClick={this.handleClickOpenLogin}
                        >
                          <Image src='/static/img/login.png' imgHeight={29} imgWidth={29}/>
                          <TextBlock className="linked_text" >
                            Особистий кабінет
                          </TextBlock>
                        </Wrapper>
                          :
                        <Wrapper
                          wrapDirection="row"
                          wrapPadding="10px 70px 10px 25px"
                          aria-owns={anchorElLogin ? 'simple-menu2' : null}
                          aria-haspopup="true"
                          onClick={this.handleClickOpenLogin}
                        >
                          <Image src='/static/img/login.png' imgHeight={29} imgWidth={29}/>
                          <TextBlock className="linked_text" >
                            Особистий кабінет
                          </TextBlock>
                        </Wrapper>
                      }
                    </ListItem>

                    {
                      LINKS.map(li => {
                        if(li.to) {
                          return (
                            <Link key={li.title} to={li.to} >
                              <ListItem>
                                {li.title}
                              </ListItem>
                            </Link>
                          )
                        }

                          if(li.openForm) {
                              return (
                                      <ListItem key={li.title} onClick={(e) =>{
                                      this.toggleDrawer('right', false);
                                      this.toggleDrawer(li.openForm, true, e)}}>
                                          {li.title}
                                      </ListItem>
                              )}

                        if(li.slideTo) {
                          return (
                            <ListItem key={li.title}>
                              <Link to={li.slideTo} >
                                {li.title}
                              </Link >
                            </ListItem>
                          )
                        }
                      })
                    }
                  </Wrapper>

                  <Wrapper className="social_wrap">
                    <p>
                      Ми в соцмережах
                    </p>

                    <Wrapper
                      wrapDirection="row"
                      wrapContent="center"
                      className="my_icons_block"
                    >
                      <a href="https://www.facebook.com/smartcinemaua/" target="_blank" className="icon_link">
                        <FontAwesomeIcon
                          icon={faFacebook}
                          size="2x"
                          className="faFacebook"
                        />
                      </a>

                      <a href="https://www.instagram.com/smart_cinema/" target="_blank" className="icon_link">
                        <FontAwesomeIcon
                          icon={faInstagram}
                          size="2x"
                          className="faInstagram"
                        />
                      </a>

                      <a href="https://wep.wf/eyqf6m" target="_blank" className="icon_link">
                        <FontAwesomeIcon
                          icon={faFacebookMessenger}
                          size="2x"
                          className="faFacebookMessenger"
                        />
                      </a>

                      <a href="https://wep.wf/9u8b3x" target="_blank" className="icon_link">
                        <FontAwesomeIcon
                          icon={faTelegram}
                          size="2x"
                          className="faTelegram"
                        />
                      </a>

                      <a href="https://wep.wf/6egkx9" target="_blank" className="icon_link">
                        <FontAwesomeIcon
                          icon={faViber}
                          size="2x"
                          className="faViber"
                        />
                      </a>
                    </Wrapper>
                  </Wrapper>

                </div>
              </Drawer>
          </Wrapper>

          <FeedbackModal
            show={feedback}
            closeModal={this.toggleDrawer}
          />
          <CareerModal
            show={career}
            closeModal={this.toggleDrawer}
          />

        </Wrapper>
      </Header>
    )
  }
}

export const HeaderComponent = withRouter(connect(({ state }) => ({
  data: state.data,
  city: state.city,
  userLogin: state.userLogin,
  token: state.token,
  userName: state.userName,
  userToken: state.userToken,
  userInfo: state.userInfo,
}))(HeaderComponentWrapper));
