import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import {
    Wrapper,
    Image,
    Title,
    TextBlock, Input, Button
} from "../components";
import { blueButton } from "../themes";
import { reservationInfoAsync, sendToAnotherEmailAsync } from "../store";
import { getCookie } from "../consts";

export class ReservationComponent extends Component {
    state = {
        focused: {
            email: false
        },
        inputValues: {
            email: ''
        }
    };

    componentDidMount() {

        this.props.dispatch(reservationInfoAsync({
            token: this.props.token ? this.props.token : getCookie('csrftoken'),
            reservation: +this.props.location.pathname.match(/\d+/g)[0]
        }))


    }

    inputHandler = ({target}) => {
        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    sendNewEmail = () => {


        this.props.dispatch(sendToAnotherEmailAsync({
            token: this.props.token ? this.props.token : getCookie('csrftoken'),
            reservation: +this.props.location.pathname.match(/\d+/g)[0],
            new_email: this.state.inputValues.email
        }));
    };

    render() {
        const { focused, inputValues } = this.state;
        const { reservationInfo } = this.props;

        return (
            <React.Fragment>
                <Wrapper
                    wrapMargin="100px 0"
                    wrapContent="center"
                >
                    <Wrapper
                        wrapPadding="70px 20px 0 "
                        className="reservation"
                        wrapMaxWidth="1442px"
                        wrapMinWidth="320px"
                        wrapContent="center"
                        className="reservationWraper"
                    >
                        <Image src=" /static/img/thanks.png" imgHeight={400} imgWidth={830} className="reservationImage"/>
                        <Title blue fontSize="45px" className={classNames("not_found_title", "reservation","reservationTitle")}>
                            Дякуємо за покупку!
                        </Title>
                        {
                            reservationInfo?
                            <TextBlock
                                color="#5b5f7d"
                                lineHeight="26px"
                                bold
                                textMargin="30px 5px 5px"
                                centered
                                className={classNames("reservation_text", "reservationText")}
                            >
                                Ви успішно придбали квитки на
                                фільм <Link to={reservationInfo.movie_info.seo_inline.url}>{reservationInfo.movie_info.title}</Link> <br/>
                                Сеанс відбудеться {reservationInfo.day} о {reservationInfo.time.slice(0, 5)}, {reservationInfo.ticket_str}
                            </TextBlock>:
                                <TextBlock
                                    color="#5b5f7d"
                                    lineHeight="26px"
                                    bold
                                >
                                    Якщо Ви не отримали квитки можете відправити їх собі на email
                                </TextBlock>
                        }
                        <TextBlock
                            color="#5b5f7d"
                            lineHeight="26px"
                            bold
                        >
                            Якщо Ви не отримали квитки можете відправити їх собі на email
                        </TextBlock>

                        <Wrapper
                            wrapMargin="30px 0"
                            className="input_wrapper"
                            wrapMinWidth="30%"
                            wrapMaxWidth="30%"
                        >
                            <label className={classNames({ focused: inputValues.email === '' ? focused.email : true })}>
                                E-mail
                            </label>
                            <Input
                                type="text"
                                className="input"
                                name="email"
                                value={inputValues.email}
                                labelClass={focused}
                                onChange={this.inputHandler}
                                minWidth="300px"
                            />
                            <Button
                                btnMargin="30px 0  0"
                                btnPadding="15px 0"
                                centered="center"
                                className={classNames({ disabled: inputValues.email === '' }, ) }
                                theme={blueButton}
                                onClick={inputValues.email !== '' ? this.sendNewEmail : null}
                            >
                                Відправити
                            </Button>
                        </Wrapper>


                    </Wrapper>
                </Wrapper>
            </React.Fragment>
        )
    }
}

export const Reservation = connect(({ state }) => ({
    token: state.token,
    reservationInfo: state.reservationInfo
}))(ReservationComponent);