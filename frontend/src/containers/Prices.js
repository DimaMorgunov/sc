import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Title,
  Wrapper,
  TextBlock,
  Span, Image
} from "../components";

export const PricesPage = (props) => (
  <React.Fragment>
    <Wrapper
      wrapMargin="50px 0 90px"
      wrapContent="center"
    >
      <Title fontSize="35px">
        Ціни
      </Title>
      <Wrapper
        wrapMargin="30px 0 0"
        wrapDirection="row"
        wrapMinWidth="auto"
        wrapContent="center"
      >
        <Wrapper
          className={classNames("blue_box", "prices")}
          wrapMinWidth="366px"
          wrapMargin="10px 20px"
          wrapContent="center"
        >
          <TextBlock centered lineHeight="28px" fontSize="19px">
            Вартість квитків від {props.cityId == 1?60:50} гривень. <br/>
            2D та 3D сеанси за однією ціною!
          </TextBlock>
        </Wrapper>
        <Wrapper
          className={classNames("blue_box", "prices")}
          wrapMinWidth="366px"
          wrapMargin="10px 20px"
          wrapContent="center"
        >
          <TextBlock centered lineHeight="28px" fontSize="19px">
              Ціни на квитки залежать<br/>
              від обраного часу відвідування та типу кінозалу.
          </TextBlock>
        </Wrapper>
      </Wrapper>

      <Wrapper
        wrapMargin="30px 0 0"
        wrapMinWidth="auto"
        wrapPadding="0 40px"
        wrapContent="center"
      >
        <TextBlock  lineHeight="28px">
            Дивани розраховані на двох глядачів, придбати одне місце на дивані неможливо.<br/>
            Діти до 5 років проходять в зал безкоштовно, якщо немає вікових обмежень на сеанс (без надання окремого місця).<br/>
            Для перегляду фільму в форматі 3D вам знадобляться 3D-окуляри, які ви можете придбати у нас в барі.<br/>
            3D-окуляри є багаторазовими, але при цьому передбачають індивідуальне використання, тому SmartCinema не видає їх в прокат / користування на час сеансу.<br/>
            Вартість пластикових 3D окулярів: 20 грн<br/>
            Придбані у нас окуляри ви зможете без проблем приносити з собою на всі наступні сеанси в форматі 3D. Також, ви можете приносити на сеанси 3D свої особисті окуляри (для пасивних 3D-систем з круговою поляризацією)
        </TextBlock>
      </Wrapper>
      <Wrapper
        wrapMargin="50px 0 0"
        wrapDirection="row"
        wrapMinWidth="80%"
        wrapMaxWidth="90%"
        wrapContent="space-between"
        wrapItems="flex-start"
        className="white_box_wrapper"
      >
        <Wrapper
          className="white_box"
          wrapMinWidth="45%"
          wrapMaxWidth="48%"
          wrapPadding="30px 20px"
        >
            {props.cityId === 1 ?
          <TextBlock centered lineHeight="28px" color="#12161f">
              Звертаємо вашу увагу, що на деякі прем'єри можуть діяти обмеження
              дистриб'юторів і ціни можуть відрізнятися від зазначених, тому рекомендуємо уточнювати ціни по
              телефону 096&nbsp;003&nbsp;50&nbsp;50 або задавати питання на електронну пошту admin_smartcinema@ukr.net.
          </TextBlock>
            :
            <TextBlock centered lineHeight="28px" color="#12161f">
                Звертаємо вашу увагу, що на деякі прем'єри можуть діяти обмеження дистриб'юторів і ціни можуть відрізнятися
                від зазначених, тому рекомендуємо уточнювати ціни по телефону 073&nbsp;002&nbsp;50&nbsp;50 або задавати питання на
                електронну пошту smartcinema.khmel@gmail.com.
            </TextBlock>
            }
        </Wrapper>
        {props.cityId === 1 ?
        <Wrapper
          className="white_box"
          wrapMinWidth="45%"
          wrapMaxWidth="48%"
          wrapPadding="30px"
        >
          <TextBlock centered lineHeight="28px" color="#12161f">
              Кінотеатр залишає за собою право змінювати вартість квитків без повідомлення відвідувачів.<br/>
              Купуйте квитки і отримуйте задоволення від кіно в «SmartCinema»!<br/>
              Умови і правила онлайн-покупки квитків
          </TextBlock>
            <Wrapper
                wrapMargin="20px 0 0"
                wrapDirection="row"
                wrapContent="center"
                className="pdf_link_prices"
                noWrap
            >
                <Image src='/static/img/pdf.png' imgWidth={32} imgHeight={36} imgMargin="0 15px 0 0" />
                <a href="/media/pdf/vn_price.pdf" target="_blank">
                    <span>Дізнатися детальніше про вартість квитків у Вінниці</span>
                </a>
            </Wrapper>
        </Wrapper>
          :
        <Wrapper
          className="white_box"
          wrapMinWidth="45%"
          wrapMaxWidth="48%"
          wrapPadding="30px"
        >
          <TextBlock centered lineHeight="28px" color="#12161f">
              Кінотеатр залишає за собою право змінювати вартість квитків без повідомлення відвідувачів.<br/>
              Купуйте квитки і отримуйте задоволення від кіно в «SmartCinema»!<br/>
              Умови і правила онлайн-покупки квитків
          </TextBlock>
          <Wrapper
            wrapMargin="20px 0 0"
            wrapDirection="row"
            wrapContent="center"
            className="pdf_link_prices"
            noWrap
          >
            <Image src='/static/img/pdf.png' imgWidth={32} imgHeight={36} imgMargin="0 15px 0 0" />
            <a href="/media/pdf/km_price.pdf" target="_blank">
              <span>Дізнатися детальніше про вартість квитків у Хмельницькому</span>
            </a>
          </Wrapper>
        </Wrapper>
        }
      </Wrapper>
    </Wrapper>
  </React.Fragment>
);

export const Prices = connect(({ state }) => ({
  cityId: state.cityId
}))(PricesPage);