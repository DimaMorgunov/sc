import {connect} from 'react-redux';
import classNames from 'classnames';
import {Link} from 'react-router-dom';
import {
    Title,
    Wrapper,
    Image,
    Label,
    ImageBlock,
    Button, TextBlock, PreloaderDiv
} from '../components';

import {
    AgeModalLayer,
    SelectModalLayer, SignInModal
} from './index';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/uk';
import {
    setScheduleAsync,
    getScheduleDatesAsync,
    getFilmSeatsAsync,
    clearFilmReservationAsync,
    setCityId,
    setCity,
    setCurrentFilm,
    setFilm, setDateToPremier
} from "../store";
import calendar from "../img/calendar.png";
import close from "../img/close.svg"
import {orangeButton} from "../themes";
import React from "react";

export class ScheduleComponent extends Component {
    state = {
        sortFilm: [],
        filmWithoutSort: [],
        showDatePikerMobile: false,
        setCurrentDate: "",
    };

    constructor(props) {
        super(props);
        this.state = {
            token: this.props.token ? this.props.token : this.getCookie('csrftoken'),
            startDate: moment(),
            ageModal: false,
            selectModal: false,
            buyModal: false,
            signIn: false,
        };
        props.dispatch(setScheduleAsync({
            token: this.props.token ? this.props.token : this.state.token,
            cityId: this.props.cityId,
            year: undefined,
            month: undefined,
            day: undefined
        }));
        props.dispatch(getScheduleDatesAsync({cityId: this.props.cityId}));
    }

    componentDidUpdate(prevProps) {
        if(prevProps.filmSeats !== this.props.filmSeats && this.props.filmSeats){
            if(!this.state.ageModal && !this.state.buyModal && !this.state.selectModal){
            }
            this.setState({
                ageModal: !this.state.ageModal
            });
          console.log('true true')
        }
        if (prevProps.schedule !== this.props.schedule) {
            if (this.props.schedule) {
                const arr = [];
                for (let i = 0; i < this.props.schedule.length; i++) {
                    for (let key in this.props.schedule[i].showtime_list) {
                        if (this.props.schedule[i].showtime_list[key] != 0) {
                            arr.push(this.props.schedule[i]);

                        }
                    }
                }
                this.setState({
                    sortFilm: arr,
                    filmWithoutSort: arr
                });
                if (arr == 0) {
                    this.setState({sortFilm: null})
                }
            }
            if (this.props.currentFilmToFilter) {
                this.sortFilm()
            }
            if (this.props.currentPremierFilm) {
                this.setCurrentDate(this.props.currentPremierFilm)
                this.props.dispatch(setDateToPremier(null))

            }
        }

        if (prevProps.cityId !== this.props.cityId) {
            this.props.dispatch(setScheduleAsync({
                token: this.props.token ? this.props.token : this.state.token,
                cityId: this.props.cityId,
                year: this.state.year,
                month: this.state.month,
                day: this.state.day
            }));
            this.props.dispatch(getScheduleDatesAsync({cityId: this.props.cityId}));
        }
    }

    setCurrentDate = () => {
        const date = this.props.currentPremierFilm;
        const getDay = date.substring(0, 2);
        const getMonth = date.substring(3, 5);
        const getYeat = date.substring(6, 10);

        let DateNow = new Date();
        let newDate = new Date(getYeat, getMonth - 1, getDay);

        if (DateNow < newDate) {
            let currentDate = moment(newDate);

            this.setState({
                startDate: currentDate,
                year: +currentDate.format('YYYY'),
                month: +currentDate.format('MM'),
                day: +currentDate.format('D')
            });

            this.props.dispatch(setScheduleAsync({
                token: this.state.token,
                cityId: this.props.cityId,
                year: +currentDate.format('YYYY'),
                month: +currentDate.format('MM'),
                day: +currentDate.format('D')
            }));
        }
    };

    sortFilm = (flag) => {
        if (this.props.currentFilmToFilter) {
            const choiceFilm = this.props.currentFilmToFilter;
            const schedule = this.props.schedule;
            const arr = [];
            for (let i = 0; i < schedule.length; i++) {
                for (let key in schedule[i].showtime_list) {
                    if (schedule[i].showtime_list[key] != 0) {

                        arr.push(schedule[i]);

                    }
                }
            }
            var array = [];
            if (choiceFilm && arr) {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].title === choiceFilm) {
                        array.push(arr[i])
                    }
                }
                this.setState({
                    sortFilm: array
                })
            }

            if (array == 0) {
                this.setState({sortFilm: null})
            }
        }
        if (flag === false) {
            this.setState({
                sortFilm: this.state.filmWithoutSort
            })
            if (this.state.filmWithoutSort == 0) {
                this.setState({sortFilm: null})
            }
        }

    };

    getCookie(name) {
        let cookieValue = null;

        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                if (cookie.substring(0, name.length + 1) === (`${name}=`)) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                }
            }
        }
        return cookieValue;
    }

    handleChange = date => {

        this.setState({
            startDate: date,
            year: +date.format('YYYY'),
            month: +date.format('MM'),
            day: +date.format('D')
        });

        this.props.dispatch(setScheduleAsync({
            token: this.state.token,
            cityId: this.props.cityId,
            year: +date.format('YYYY'),
            month: +date.format('MM'),
            day: +date.format('D')
        }));
        if (this.state.showDatePikerMobile) {
            this.showDatePickerMobile()
        }
    };

    toggleAgeModal = (el, film, filmFormat) => {
        console.log('toggle modal', el, film, filmFormat);
        if (!this.state.ageModal) this.props.dispatch((getFilmSeatsAsync({token: this.state.token, id: film.id})));
        if (this.state.ageModal) {this.setState({ageModal: false})}
        this.setState({
            el: el && el,
            filmHall: film && film.hall,
            filmFormat: filmFormat && filmFormat,
            filmTime: film &&
                `${new Date(film.start).getHours()}:${new Date(film.start).getMinutes() < 10 ?
                    '0' + new Date(film.start).getMinutes() :
                    new Date(film.start).getMinutes()}`,
            priceFrom: film && film.standard_price,
            priceTo: film && film.vip_price,
            screenPicture: el && el.detail_background,
            sessionId: film && film.id,
        });

    };

    toggleSelectModal = (e) => {
        this.setState({
            ageModal: false,
            selectModal: !this.state.selectModal
        });
        console.log('selectModal', this.state.selectModal)
        if (e && e.target.classList.contains('buyModal')) {
            this.props.dispatch(clearFilmReservationAsync({
                token: this.state.token,
                id: this.props.reservation.reservation_id
            }));
            document.body.classList.toggle('modal_opened');
            return;
        }
        if (!this.state.selectModal) {
            return;
        }
        /*document.body.classList.toggle('modal_opened');*/
    };

    handleClose = (id, city) => {
        this.props.dispatch(setCityId(id));
        this.props.dispatch(setCity(city));
        window.localStorage.setItem('chosen_city_id', id);
        window.localStorage.setItem('chosen_city', city);
    };

    toggleDrawer = (side, open) => {
        this.setState({
            selectModal: false,
            [side]: open
        });

        if (!open) {
            document.body.classList.toggle('modal_opened');

        } else {
            this.props.dispatch(clearFilmReservationAsync({
                token: this.props.token ? this.props.token : this.state.token,
                id: this.props.reservation.reservation_id
            }));
        }
    };

    purgeSortFilm = () => {
        this.props.dispatch(setFilm(null));
        this.sortFilm(false)
    };

    showDatePickerMobile = () => {
        this.setState({
            showDatePikerMobile: !this.state.showDatePikerMobile
        })
    }

    render() {
        const {
            cityId,
            schedule,
            scheduleDates,
            filmSeats,
            error
        } = this.props;

        const {
            ageModal,
            el,
            selectModal,
            filmHall,
            filmFormat,
            filmTime,
            priceFrom,
            priceTo,
            screenPicture,
            buyModal,
            sessionId,
            signIn,
            sortFilm,
        } = this.state;

        return (

            cityId ?
                <React.Fragment>


                    <Wrapper
                        wrapMargin="55px 0 20px"
                        wrapPadding="0 60px"
                        wrapItems="flex-start"
                        className="smaller_padding"
                    >
                        <Title fontSize="35px" className="title">
                            Розклад фільмів в SmartCinema
                        </Title>
                        {this.props.cityId === 1 ?
                            <TextBlock className="choosenCityBlock" color="#fff" font="20px">
                                Вінниця, TЦ SkyPark
                            </TextBlock>
                            :
                            null}
                        {this.props.cityId === 2 ?
                            <TextBlock className="choosenCityBlock" color="#fff" font="20px">
                                Хмельницький, ТЦ WoodMall
                            </TextBlock>
                            :
                            null}
                        {window.innerWidth < 768 ?
                            <Wrapper
                                wrapDirection="row"
                                wrapMargin="20px 0"
                            >
                                <Wrapper className="schedule_calendar" wrapDirection="row">
                                    <Image
                                        src={calendar}
                                        imgHeight={22}
                                        imgWidth={20}
                                        imgMargin="0 15px 0 0"
                                    />
                                    <TextBlock className='textBlockDataPickerScheduleMobile'
                                               onClick={this.showDatePickerMobile}>
                                        {this.state.startDate ?
                                            `${this.state.startDate.get('year')}-${this.state.startDate.get('month') + 1}-${this.state.startDate.get('date')}`
                                            : "Оберiть дату"}
                                    </TextBlock>
                                </Wrapper>
                                {
                                    this.state.showDatePikerMobile ?
                                        <Wrapper className="datePickerMobile" wrapItems="start">
                                            <DatePicker
                                                inline
                                                selected={this.state.startDate}
                                                onChange={this.handleChange}
                                                includeDates={scheduleDates && scheduleDates.active_days.map(date => moment(date))}
                                            />
                                        </Wrapper>
                                        : null
                                }

                            </Wrapper>
                            :
                            <Wrapper
                                wrapDirection="row"
                                wrapMargin="20px 0"
                            >
                                <Image
                                    src={calendar}
                                    imgHeight={22}
                                    imgWidth={20}
                                    imgMargin="0 15px 0 0"
                                />
                                <DatePicker
                                    required
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    className="schedule_calendar"
                                    includeDates={scheduleDates && scheduleDates.active_days.map(date => moment(date))}
                                    locale="uk"
                                />
                            </Wrapper>

                        }
                        {this.props.currentFilmToFilter ?
                            <TextBlock color="#fff">
                                Обраний вами фiльм:
                            </TextBlock>
                            : null
                        }
                        {this.props.currentFilmToFilter ?
                            <Button
                                onClick={() => this.purgeSortFilm()}
                                theme={orangeButton}
                                className="buttonSort"
                            >
                                {this.props.currentFilmToFilter}
                            </Button>
                            : null
                        }
                        {sortFilm ?
                            sortFilm.map(el => {
                                return (
                                    <Wrapper
                                        key={el.title}
                                        wrapDirection="row"
                                        wrapItems="flex-start"
                                        wrapPadding="25px 0"
                                        className="schedule_item"
                                    >
                                        <Wrapper
                                            wrapMinWidth="157px"
                                            wrapMaxWidth="157px"
                                            className="schedule_image"
                                        >
                                            <Link to={el.url_to_movie}>
                                                <ImageBlock
                                                    src={el.poster}
                                                    imgWidth={157}
                                                    className="film_img"
                                                />
                                            </Link>
                                        </Wrapper>

                                        <Wrapper
                                            wrapItems="flex-start"
                                            wrapMargin="0 25px"
                                            className="schedule_table_block"
                                        >
                                            <Title blue fontSize="25px" className="schedule_title" centered>
                                                <Link to={el.url_to_movie}>{el.title}</Link>
                                                <Label
                                                    className={classNames("block_label", "white_label")}>{el.format}</Label>
                                                <Label className="block_label">{el.age}+</Label>
                                            </Title>

                                            <Wrapper
                                                wrapMargin="0 0 0 -20px"
                                                className="schedule_table_wrap"
                                                wrapItems="flex-start"
                                            >
                                                <table>
                                                    <tbody>
                                                    {
                                                        el.showtime_list['2D'].length > 0 && el.showtime_list['3D'].length > 0 ?
                                                            <React.Fragment>
                                                                <tr>
                                                                    <td className="genre">2D</td>
                                                                    {
                                                                        el.showtime_list['2D'].map(film => (
                                                                            <td
                                                                                key={film.id}
                                                                                onClick={() => new Date().getTime() < new Date(film.start).getTime() &&
                                                                                    this.toggleAgeModal(el, film, '2D')
                                                                                }
                                                                            >
                                                                                {new Date(film.start).getHours()}:{new Date(film.start).getMinutes() < 10 ?
                                                                                '0' + new Date(film.start).getMinutes() :
                                                                                new Date(film.start).getMinutes()} <br/>
                                                                                {film.hall}
                                                                                <span>від {film.standard_price} грн</span>
                                                                            </td>
                                                                        ))
                                                                    }
                                                                </tr>
                                                                <tr>
                                                                    <td className="genre">3D</td>
                                                                    {
                                                                        el.showtime_list['3D'].map(film => (
                                                                            <td
                                                                                key={film.id}
                                                                                onClick={() => new Date().getTime() < new Date(film.start).getTime() &&
                                                                                    this.toggleAgeModal(el, film, '3D')
                                                                                }
                                                                            >
                                                                                {new Date(film.start).getHours()}:{new Date(film.start).getMinutes() < 10 ?
                                                                                '0' + new Date(film.start).getMinutes() :
                                                                                new Date(film.start).getMinutes()} <br/>
                                                                                {film.hall}
                                                                                <span>від {film.standard_price} грн</span>
                                                                            </td>
                                                                        ))
                                                                    }
                                                                </tr>
                                                            </React.Fragment>
                                                            :
                                                            el.showtime_list['2D'].length > 0 ?
                                                                <tr>
                                                                    {
                                                                        el.showtime_list['2D'].map(film => (
                                                                            <td
                                                                                key={film.id}
                                                                                className={new Date().getTime() > new Date(film.start).getTime() ? 'past_film' : ''}
                                                                                onClick={() => new Date().getTime() < new Date(film.start).getTime() &&
                                                                                    this.toggleAgeModal(el, film, '2D')
                                                                                }
                                                                            >
                                                                                {new Date(film.start).getHours()}:{new Date(film.start).getMinutes() < 10 ?
                                                                                '0' + new Date(film.start).getMinutes() :
                                                                                new Date(film.start).getMinutes()} <br/>
                                                                                {film.hall}
                                                                                <span>від {film.standard_price} грн</span>
                                                                            </td>
                                                                        ))
                                                                    }
                                                                </tr>
                                                                :
                                                                el.showtime_list['3D'].length > 0 &&
                                                                <tr>
                                                                    {
                                                                        el.showtime_list['3D'].map(film => (
                                                                            <td
                                                                                key={film.id}
                                                                                className={new Date().getTime() > new Date(film.start).getTime() ? 'past_film' : ''}
                                                                                onClick={() => new Date().getTime() < new Date(film.start).getTime() &&
                                                                                    this.toggleAgeModal(el, film, '3D')
                                                                                }
                                                                            >
                                                                                {new Date(film.start).getHours()}:{new Date(film.start).getMinutes() < 10 ?
                                                                                '0' + new Date(film.start).getMinutes() :
                                                                                new Date(film.start).getMinutes()} <br/>
                                                                                {film.hall}
                                                                                <span>від {film.standard_price} грн</span>
                                                                            </td>
                                                                        ))
                                                                    }
                                                                </tr>
                                                    }
                                                    </tbody>
                                                </table>
                                            </Wrapper>

                                        </Wrapper>
                                    </Wrapper>
                                )
                            }) : error ?
                                <Wrapper
                                    wrapPadding="0 65px"
                                    wrapMargin="85px 0 50px"
                                >
                                    {error}
                                </Wrapper> :
                                this.props.currentFilmToFilter ?
                                    <Wrapper
                                        wrapPadding="0 65px"
                                        wrapMargin="85px 0 50px"
                                    >
                                        Нажаль обраного фiльму на цю дату немає
                                    </Wrapper> :
                                    <Wrapper
                                        wrapPadding="0 65px"
                                        wrapMargin="85px 0 50px"
                                    >
                                        Розклад грузиться, почекайте будь ласка :)
                                    </Wrapper>
                        }
                        {ageModal &&
                        <AgeModalLayer
                            show={ageModal}
                            closeModal={this.toggleDrawer}
                            toggleSelectModal={this.toggleSelectModal}
                            toggleAgeModal={this.toggleAgeModal}
                            el={el}
                        />
                        }

                        {selectModal &&
                        <SelectModalLayer
                            show={selectModal}
                            toggleSelectModal={this.toggleSelectModal}
                            el={el}
                            filmFormat={filmFormat}
                            filmHall={filmHall}
                            startDate={this.state.startDate}
                            filmTime={filmTime}
                            priceFrom={priceFrom}
                            priceTo={priceTo}
                            filmSeats={filmSeats}
                            screenPicture={screenPicture}
                            buyModal={buyModal}
                            sessionId={sessionId}
                            signInOpen={this.toggleDrawer}
                        />
                        }

                    </Wrapper>

                    <SignInModal
                        fromSchedule
                        show={signIn}
                        closeModal={this.toggleDrawer}
                    />
                </React.Fragment>
                :
                <Wrapper
                    wrapContent="center"
                    wrapMargin="30px 0"
                >
                    <Title blue fontSize="25px">
                        Будь ласка, оберіть місто
                    </Title>
                    <Wrapper
                        wrapDirection="row"
                        wrapContent="center"
                        className='choiceCitiPadding'
                    >
                        <Button
                            theme={orangeButton}
                            className='choiceCityBtn'
                            onClick={e => this.handleClose(1, 'Вінниця, TЦ SkyPark')}
                        >
                            Вінниця, TЦ SkyPark
                        </Button>
                        <Button
                            theme={orangeButton}
                            className='choiceCityBtn'
                            onClick={e => this.handleClose(2, 'Хмельницький, ТЦ WoodMall')}
                        >
                            Хмельницький, ТЦ WoodMall
                        </Button>
                    </Wrapper>
                </Wrapper>
        )
    }
}

export const Schedule = connect(({state}) => ({
    token: state.token,
    cityId: state.cityId,
    schedule: state.schedule,
    scheduleDates: state.scheduleDates,
    filmSeats: state.filmSeats,
    error: state.error,
    reservation: state.reservation,
    currentFilmToFilter: state.currentFilmToFilter,
    currentPremierFilm: state.currentPremierFilm
}))(ScheduleComponent);
