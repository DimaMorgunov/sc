import { Link } from 'react-router-dom';
import {
  Wrapper,
  Image,
  Title,
  TextBlock
} from "../components";
import soon from '../img/coming-soon.png';

export const ComingSoon = () => (
  <React.Fragment>
    <Wrapper
      wrapMargin="50px 0"
      wrapContent="center"
    >
      <Wrapper
        wrapMargin="50px 0"
        wrapPadding="70px 10px 0 "
        className="not_found"
        wrapMaxWidth="1442px"
        wrapMinWidth="70%"
      >
        <Image src={soon} imgHeight={374} imgWidth={758} />
        <Title blue fontSize="45px" className="not_found_title">
          сторінка в розробці
        </Title>
        <TextBlock className="not_found_block">
          Приносимо вибачення, ця сторінка знаходиться в розробці
          Спробуйте повернутися на <Link to="/">головну сторінку</Link>
        </TextBlock>
      </Wrapper>
    </Wrapper>
  </React.Fragment>
);
