import {ModalLayer} from "../../components/PresentationalComponents";
import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Button,
  Form,
  Input,
  TextBlock,
  Wrapper,
  Textarea, PreloaderDiv
} from "../../components";
import {orangeButton} from "../../themes";
import {emailReg, getCookie} from "../../consts";
import {sendFeedbackAsync, setError, setSuccess} from "../../store";
import InputMask from "react-input-mask";

export class FeedbackModalComponent extends Component {
  state = {
    inputValues: {
      email: '',
      phone: '',
      name: '',
      message: ''
    },
      errorInput: {
          email: '',
          phone: '',
          name: '',
          message: ''
      },
    errorFields: [],
    showPreloader: false
  };


    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {

            this.setState({
                showPreloader: false,
            })
        }

        if(prevProps.success  !== this.props.success && this.props.success) {
            this.setState({
                showPreloader: false,
            })
        }
    }

  closeModal = (modal, open) => {
    this.props.dispatch(setSuccess(null));
    this.props.dispatch(setError(null));

    this.setState({
        inputValues: {
            email: '',
            phone: '',
            name: '',
            message: ''
        },
        errorInput: {
            email: '',
            phone: '',
            name: '',
            message: ''
        },
      errorFields: []
    });

    this.props.closeModal(modal, open);
  };

  inputHandler = ({target}) => {
      switch(target.name) {
          case 'email':
              if(target.name.length > 0 || this.state.inputValues.phone.length !== 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.email = null;
                  errorInput.phone = null;
                  this.setState({errorInput: errorInput});
              }
              if(this.state.errorFields.length > 0) {
                  let errStr = this.state.errorFields.filter(err => err !== 'email');
                  errStr = errStr.filter(err => err !== 'phone');
                  this.setState({ errorFields: errStr });
              }
              break;
          case 'name':
              if(target.name.length > 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.name = null;
                  this.setState({errorInput: errorInput});
              }
              break;
              if(this.state.errorFields.length > 0) {
                  let errStr = this.state.errorFields.filter(err => err !== target.name);
                  this.setState({ errorFields: errStr });
              }
          case 'phone':
              if(target.name.length > 0 || this.state.inputValues.email.length !== 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.phone = null;
                  errorInput.email = null;
                  this.setState({errorInput: errorInput});
              }
              if(this.state.errorFields.length > 0) {
                  let errStr = this.state.errorFields.filter(err => err !== 'email');
                  errStr = errStr.filter(err => err !== 'phone');
                  this.setState({ errorFields: errStr });
              }
              break;
          case 'message':
              if(target.name.length > 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.message = null;
                  this.setState({errorInput: errorInput});
              }
              if(this.state.errorFields.length > 0) {
                  let errStr = this.state.errorFields.filter(err => err !== target.name);
                  this.setState({ errorFields: errStr });
              }
              break;
          default:
              break;
      }
    this.setState({ inputValues: {
        ...this.state.inputValues,
        [target.name]: target.value
      }})
  };

  formHandler = () => {
    const { inputValues } = this.state;
    let errorFields = [];


      for(let key in inputValues) {
          switch(key) {
              case 'name':
                  if(!inputValues.name.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.name = 'Упс, забули вказати Ім`я';
                      this.setState({errorInput: errorInput});
                  }
                  break;
              case 'phone':

                  if(!inputValues.email.length && !emailReg.test(inputValues[key]) && !inputValues.phone.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.phone = 'Упс, перевірте Номер телефону';
                      this.setState({errorInput: errorInput});
                  }
                  break;
              case 'email':

                  if(!inputValues.phone.length && !inputValues.email.length && !emailReg.test(inputValues[key]) ) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.email = 'Упс, перевірте E-mail';
                      this.setState({errorInput: errorInput});
                  }
                  break;
              case 'message':
                  if(!inputValues.message.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.message = 'Упс, забули Проблему';
                      this.setState({errorInput: errorInput});
                  }
                  break;
              default:
                  break;
          }
      }

    this.setState({ errorFields });

      var purePhone = [];
      for(let i = 0; i < inputValues.phone.length; i++){
          if(inputValues.phone[i] !== ")" &&
              inputValues.phone[i] !== "(" &&
              inputValues.phone[i] !== " " &&
              inputValues.phone[i] !== "-"
          ){
              purePhone.push(inputValues.phone[i])
          }
      }
      var phoneStr = purePhone.join("");
    if(errorFields.length === 0) {
      this.props.dispatch(setSuccess(null));
      this.props.dispatch(setError(null));
      this.props.dispatch(sendFeedbackAsync({
        token: getCookie('csrftoken'),
        email: inputValues.email,
        name: inputValues.name,
        phone: phoneStr,
        message: inputValues.message,
        branch_id: this.props.cityId
      }));

      this.setState({ showPreloader: true });
    }
  };

  render() {
    const {
      show,
      success,
      error
    } = this.props;

    const {
      inputValues,
      errorFields,
      showPreloader
    } = this.state;

    return (
      <ModalLayer
        show={show}
        closeModal={() => this.closeModal('feedback', false)}
      >
        <Wrapper
          className="feedBackFormWrap"
          wrapPadding="0 10px 15px"
        >
          <TextBlock className="form_title" textMargin="-30px 40px 5px 25px">
            Зворотній зв`язок
          </TextBlock>
          <Form>

            <Wrapper
              wrapMargin="30px 0"
            >
              <label className={classNames({ focused: inputValues.name !== '' })}>
                Ім`я
              </label>
              <Input
                type="text"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^name$`).test(el)) })}
                name="name"
                value={inputValues.name}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.name? this.state.errorInput.name: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.phone !== '' })}>
                Номер телефону
              </label>
                <InputMask
                    mask="38 (999) 999-99-99"
                    className={classNames("inputMask", {error: errorFields.some((el) => !!RegExp(`^phone$`).test(el)) })}
                    name="phone"
                    value={inputValues.phone}
                    onChange={this.inputHandler}
                />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.phone? this.state.errorInput.phone: null}</p>
            <TextBlock color="#bfbfbf" textMargin="-20px 0">
              або
            </TextBlock>

            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.email !== '' })}>
                E-mail
              </label>
              <Input
                type="text"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                name="email"
                value={inputValues.email}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.message !== '' })}>
                Повідомлення
              </label>
              <Textarea
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^message$`).test(el)) })}
                name="message"
                value={inputValues.message}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.message? this.state.errorInput.message: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              wrapItems="center"
              wrapDirection="row"
              className="form_btn_wrapper"
            >
              <Button
                theme={orangeButton}
                onClick={this.formHandler}
              >
                Відправити
              </Button>
              {
                success ?
                <TextBlock color="#19bd19" textMargin="5px 5px 5px 20px" bold >
                    Дякуємо, Ваше звернення прийняте. Адміністратор зв`яжеться з Вами якнайшвидше!
                </TextBlock>
                    : error ?
                    <TextBlock color="#f20000" textMargin="5px 5px 5px 20px" bold >
                        {error}
                    </TextBlock>
                    : null
              }
            </Wrapper>
          </Form>
        </Wrapper>

        <PreloaderDiv show={showPreloader} />

      </ModalLayer>
    )
  }
}

export const FeedbackModal = connect(({ state }) => ({
  cityId: state.cityId,
  success: state.success,
  error: state.error
}))(FeedbackModalComponent);