import classNames from 'classnames';
import { connect } from 'react-redux';
import {
    Button,
    Form,
    Input,
    ModalLayer,
    PreloaderDiv,
    TextBlock,
    Wrapper,
} from "../../components";
import {orangeButton} from "../../themes";


import Select from 'react-select-v1';

import {
    changeUserInfoAsync,
    getUserInfoAsync,
    setError,
    setSuccess
} from "../../store";

export class UserEditComponent extends Component {
    state = {
        inputValues: {
            name: '',
            surname: '',
            city: '',
            email: '',
        },
        errorInput:{
            name: '',
            surname: '',
            city: '',
            email: '',
        },

        showPreloader: false,
        errorFields: [],
        error: this.props.error
    };

    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false,
            });
        }
    }

    componentWillReceiveProps (nextProps) {
        if(this.props.error != nextProps.error){
            this.setState({
                error: nextProps.error
            })
        }
    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
            switch(target.name) {
                case 'name':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'surname':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'email':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }
        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    selectHandler = (option) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== 'city');
            this.setState({ errorFields: errStr });
        }

        this.setState({ inputValues: {
                ...this.state.inputValues,
                city: option.value
            }});
    };

    closeModal = (modal, open, e) => {
        this.setState({
            inputValues: {
                name: '',
                surname: '',
                city: '',
                email: '',
            },
            showPreloader: false,
            errorFields: [],
            error: ''
        });

        this.props.dispatch(setError(null));
        this.props.closeModal(modal, open, e);
    };

    formHandler = () => {
        const { inputValues } = this.state;
        let errorFields = [];

        for(let key in inputValues) {
            switch(key) {
                case 'name':
                    if(!inputValues.name.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.name = 'Упс, забули вказати Ім`я';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'surname':
                    if(!inputValues.surname.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.surname = 'Упс, забули вказати Прізвище';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'email':
                    if(!inputValues.email.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.email = 'Упс, перевірте E-mail';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.setState({
                showPreloader: true,
            });
            this.props.dispatch(setError(null));
            this.props.dispatch(changeUserInfoAsync({
                userToken: this.props.userToken,
                token: this.props.token,
                first_name: this.state.inputValues.name,
                last_name: this.state.inputValues.surname,
                most_important_city: this.state.inputValues.city,
                email: this.state.inputValues.email
            }));

        }
    };

    render() {
        const { show, cities} = this.props;
        const {
            inputValues,
            errorFields,
            showPreloader,
            error
        } = this.state;

        return (
            <ModalLayer
                show={show}
                closeModal={(e) => this.closeModal('userEdit', false, e)}
            >
                <Wrapper
                    wrapPadding="0 10px 15px"
                >
                    <TextBlock className="form_title">
                       Змінити данні
                    </TextBlock>
                    <Form>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", { focused: inputValues.name !== '' } )}
                        >
                            <label className={classNames({ focused: inputValues.name !== '' })}>
                                Ім`я
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^name$`).test(el)) })}
                                name="name"
                                value={inputValues.name}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.name? this.state.errorInput.name: null}</p>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", { focused: inputValues.surname !== '' } )}
                        >
                            <label className={classNames({ focused: inputValues.surname !== '' })}>
                                Прізвище
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^surname$`).test(el)) })}
                                name="surname"
                                value={inputValues.surname}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.surname? this.state.errorInput.surname: null}</p>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", "select_wrapper", { focused: inputValues.city !== '' })}
                        >
                            <label className={classNames({ focused: inputValues.city !== '' })} >
                                Ваше місто
                            </label>
                            <Select
                                name="city"
                                value={inputValues.city}
                                searchable={false}
                                clearable={false}
                                multi={false}
                                className={classNames("select_block", {error: errorFields.some((el) => !!RegExp(`^city$`).test(el))} )}
                                placeholder=""
                                onChange={this.selectHandler}
                                options={cities && cities.map(el => ({ value: el.id, label: el.city }))}
                            />
                        </Wrapper>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", { focused: inputValues.email !== '' } )}
                        >
                            <label className={classNames({ focused: inputValues.email !== '' })}>
                                E-mail
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                                name="email"
                                value={inputValues.email}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
                        <Wrapper
                            wrapMargin="30px 0 10px"
                            wrapItems="flex-start"
                            className="form_btn_wrapper"
                        >
                            <Button
                                theme={orangeButton}
                                onClick={this.formHandler}
                            >
                                Зареєструватися
                            </Button>
                            {
                                error &&
                                <TextBlock color="red" textMargin="10px 0" bold >
                                    {this.state.error.detail}
                                </TextBlock>
                            }
                        </Wrapper>
                    </Form>
                </Wrapper>
                <PreloaderDiv show={showPreloader} />
            </ModalLayer>
        )
    }
}

export const UserEditModal = connect(({ state }) => ({
    token: state.token,
    userToken: state.userToken,
    error: state.error,
    name: state.first_name,
    surname: state.last_name,
    city: state.city,
    email: state.email
}))(UserEditComponent);