import { ModalLayer } from "../../components/PresentationalComponents";
import { connect } from 'react-redux';
import classNames from 'classnames';
import {
    Button,
    Form,
    Input,
    PreloaderDiv,
    TextBlock,
    Wrapper
} from "../../components";
import { orangeButton } from "../../themes";
import {resetPassFirstStep, setError} from "../../store";

export class LostPassComponent extends Component {
    state = {
        inputValues: {
            email: '',
        },
        errorInput: {
            email: '',
        },
        errorFields: [],
        showPreloader: false,
        error: false
    };

    componentDidMount() {
            this.setState({
                showPreloader: false,
                error: false,
                success: false,
                errorFields: [],
            })
    }


    componentDidUpdate(prevProps) {
        if(this.state.showPreloader){
            this.setState({
                showPreloader: false,
                error: false,
                errorFields: [],
            })
        }

        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false,
                error: true
            })
        }

        if(prevProps.success !==
            this.props.success &&
            this.props.success &&
            this.props.success.detail ==
            "Password reset e-mail has been sent.") {

            this.closeModal('sendpass', true);
        }
    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
            switch(target.name) {
                case 'email':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.email = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    closeModal = (modal, open, e) => {
        this.setState({
            inputValues: {
                email: '',
            },
            errorInput: {
                email: '',
            },
            errorFields: [],
            error: false
        });

        this.props.dispatch(setError(null));
        this.props.closeModal(modal, open, e);
    };

    formHandler = () => {
        const { inputValues } = this.state;
        let errorFields = [];

        for(let key in inputValues) {
            switch(key) {
                case 'email':
                    if(!inputValues.email.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.email = 'Упс, перевірте E-mail';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.setState({ showPreloader: true });
            this.props.dispatch(setError(null));
            this.props.dispatch(resetPassFirstStep({
                email: inputValues.email,
                token: this.props.token
            }));

        }
    };

    render() {
        const { show } = this.props;
        const {
            inputValues,
            errorFields,
            showPreloader,
        } = this.state;
        const {error} = this.props;

        return (
            <ModalLayer
                show={show}
                closeModal={(e) => this.closeModal('lostPass', false, e)}
            >

                <Wrapper
                    wrapPadding="0 10px 15px"
                    wrapItems="flex-start"
                >
                    <TextBlock className="form_title" textMargin="-30px 40px 20px 5px">
                        Скинути пароль
                    </TextBlock>
                    <Form>
                        <Wrapper
                            wrapMargin="30px 0"
                            className="input_wrapper"
                        >
                            <label className={classNames({ focused: inputValues.email !== '' })}>
                                E-mail
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                                name="email"
                                value={inputValues.email}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
                       <Wrapper
                            wrapMargin="30px 0"
                            wrapDirection="row"
                            className="form_btn_wrapper"
                        >
                            <Button
                                theme={orangeButton}
                                onClick={this.formHandler}
                            >
                                Відправити
                            </Button>
                           {
                               error &&
                               <TextBlock color="red" textMargin="10px 0" bold >
                                   Упс, щось пішло не так. Спробуйте ще раз
                               </TextBlock>
                           }
                        </Wrapper>
                    </Form>
                </Wrapper>
                <PreloaderDiv show={showPreloader} />
            </ModalLayer>
        )
    }
}

export const LostPassModal = connect(({ state }) => ({
    token: state.token,
    success: state.success,
    error: state.error
}))(LostPassComponent);
