import { connect } from 'react-redux';
import classNames from 'classnames';
import { Redirect } from 'react-router'
import {
    Button,
    Form,
    Input, PreloaderDiv,
    TextBlock,
    Wrapper
} from "../../components";
import {orangeButton} from "../../themes";

import 'react-select-v1/dist/react-select.css';
import {resetPassSeconStep, setError, setSuccess} from "../../store";
import React from "react";


export class ResetPass extends Component {
    state = {
        inputValues: {
            code: '',
            password1: '',
            password2: '',
        },
        errorInput: {
            code: '',
            password1: '',
            password2: '',
        },
        errorFields: [],
        showPreloader: false,
        navigate: true
    };

    componentDidUpdate(prevProps) {
        if(prevProps.success !== this.props.success && this.props.success) {
            this.setState({
                showPreloader: false,
            });
            if(this.props.success === "RESET_PASS_SECOND"){
                window.location.href = "https://smartcinema.ua/"; 
            }
        }
        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false
            })
        }
    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
        }
        switch(target.name) {
            case 'code':
                if(target.name.length > 0) {
                    let errorInput = this.state.errorInput;
                    errorInput.code = null;
                    this.setState({errorInput: errorInput});
                }
                break;
            case 'password1':
                if(target.name.length > 0) {
                    let errorInput = this.state.errorInput;
                    errorInput.password1 = null;
                    this.setState({errorInput: errorInput});
                }
            case 'password2':
                if(target.name.length > 0) {
                    let errorInput = this.state.errorInput;
                    errorInput.password2 = null;
                    this.setState({errorInput: errorInput});
                }
                break;
            default:
                break;
        }

        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    formHandler = () => {

        const { inputValues } = this.state;
        let errorFields = [];
        this.props.dispatch(setSuccess(null));
        this.props.dispatch(setError(null));
        for(let key in inputValues) {
            switch(key) {
                case 'code':
                    if(!inputValues.code.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.code = 'Упс, забули вказати Код';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'password1':
                    if(!inputValues.password1.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.password1 = 'Упс, забули вказати Пароль';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'password2':
                    if(!inputValues.password2.length || inputValues.password1 !== inputValues.password2) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.password2 = 'Упс, перевірте Пароль';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.props.dispatch(resetPassSeconStep({
                token: this.props.match.params.params,
                uid: inputValues.code,
                password1: inputValues.password1,
                password2: inputValues.password2,
            }));

            this.setState({ showPreloader: true });
        }

    };

    render() {
        const {
            error,
            success
        } = this.props;

        const {
            inputValues,
            errorFields,
            showPreloader
        } = this.state;

        return (
            <React.Fragment>
                <Wrapper
                    wrapPadding="100px 15px 15px"
                    className="resetPassModal"
                >
                    <TextBlock className="form_title" textMargin="-30px 0 5px">
                        Підтвердження пароля
                    </TextBlock>
                    <Form>
                        <Wrapper
                            wrapMargin="30px 0"
                            className="input_wrapper"
                        >
                            <label className={classNames({ focused: inputValues.code !== '' })}>
                               Код
                            </label>
                            <Input
                                type="password"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password$`).test(el)) })}
                                name="code"
                                value={inputValues.code}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.code? this.state.errorInput.code: null}</p>
                        <Wrapper
                            wrapMargin="30px 0"
                            className="input_wrapper"
                        >
                            <label className={classNames({ focused: inputValues.password1 !== '' })}>
                                Новий пароль
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password1$`).test(el)) })}
                                name="password1"
                                value={inputValues.password1}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.password1? this.state.errorInput.password1: null}</p>

                        <Wrapper
                            wrapMargin="30px 0"
                            className="input_wrapper"
                        >
                            <label className={classNames({ focused: inputValues.password2 !== '' })}>
                                Підтвердіть пароль
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password2$`).test(el)) })}
                                name="password2"
                                value={inputValues.password2}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.password2? this.state.errorInput.password2: null}</p>
                        <Wrapper
                            wrapMargin="30px 0"
                            wrapItems="center"
                            wrapDirection="row"
                            className="form_btn_wrapper"
                        >
                            <Button
                                theme={orangeButton}
                                onClick={this.formHandler}
                            >
                                Відправити
                            </Button>
                            {
                                error ?
                                <TextBlock color="#ff0000" textMargin="5px 5px 5px 20px" bold >
                                    Упс, щось пішло не так. Спробуйте ще раз
                                </TextBlock>
                                    :  success ?
                                    <TextBlock color="#19bd19" textMargin="5px 5px 5px 20px" bold >
                                        Ваш пароль змiнено!
                                    </TextBlock>
                                    : null
                            }
                        </Wrapper>
                    </Form>
                </Wrapper>

                <PreloaderDiv show={showPreloader} />

            </React.Fragment>
        )
    }
}


export const Reset = (connect(({ state }) => ({
    success: state.success,
    error: state.error
}))(ResetPass));
