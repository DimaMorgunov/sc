import {
  Button,
  ModalLayer,
  TextBlock,
  Title,
  Wrapper
} from "../../components";
import {
  blueButton,
  orangeButton
} from "../../themes";

export class AgeModalLayer extends Component {
  render() {
    const { show, closeModal, el, toggleSelectModal, toggleAgeModal } = this.props;

    return (
      <ModalLayer show={show} closeModal={toggleAgeModal}>
        <Wrapper
          wrapMargin="20px 0"
          wrapPadding="0 60px"
          className="predictionMessage"
        >
          <Title fontSize="35px" blue>
            Увага!
          </Title>
          <TextBlock
            color="#0d1017"
            centered
            textMargin="20px 0 0"
            lineHeight="24px"
          >
            Цей фільм мaє вікове обмеження! <br/>
            Підтвердіть, що вам вже виповнилось {el.age} років
          </TextBlock>
          <Wrapper
            wrapDirection="row"
            wrapMargin="30px 0"
            wrapContent="center"
            className="age_buttons"
          >
            <Button
              className="button"
              theme={blueButton}
              onClick={() => toggleSelectModal()}
            >
              Так
            </Button>
            <Button
              className="button"
              theme={orangeButton}
              onClick={() => toggleAgeModal()}
            >
              Ні
            </Button>
          </Wrapper>
        </Wrapper>
      </ModalLayer>
    )
  }
}