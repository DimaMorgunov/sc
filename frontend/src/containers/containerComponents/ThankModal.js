import {
  ImageBlock,
  ModalLayer,
  TextBlock,
  Wrapper
} from "../../components";

export class ThankModal extends Component {
  render() {
    return (
      <ModalLayer
        show={this.props.show}
        closeModal={(e) => this.props.closeModal('thank', false, e)}
      >
        <Wrapper
          wrapPadding="0 10px 15px"
          wrapItems="center"
        >
          <TextBlock className="form_title">
            Вітаємо!
          </TextBlock>
          <ImageBlock src="/static/img/thank-pic.png" imgHeight={137} imgWidth={258} imgMargin="30px 0"/>
          <TextBlock
            color="#0d1017"
            font="22px"
            textMargin="0 0 30px"
          >
            Ви зареєстровані!
          </TextBlock>
          <Wrapper
            className="thank_modal"
          >
            <TextBlock
              color="#626262"
              centered
              textPadding="35px 30px 25px 30px"
              textMaxWidth="75%"
              lineHeight="24px"
            >
              Після перевірки Ваших даних адміністратором ми повідомимо вам про можливість отримувати бонуси.
              Якщо ви хочете придбати квитки зараз, бонуси зарахуються, після перевірки Ваших даних адміністратором
            </TextBlock>
          </Wrapper>
        </Wrapper>
      </ModalLayer>
    )
  }

}