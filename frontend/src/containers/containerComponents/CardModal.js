import { connect } from 'react-redux';
import classNames from 'classnames';
import {
    TextBlock,
    Wrapper,
    ModalLayer,
    ImageBlock
} from "../../components";
import { setNewBonusCardAsync } from "../../store";

export class CardModalComponent extends Component {
    state = {
        plastic: false,
        error: false
    };

    componentDidUpdate(prevProps) {

        if(prevProps.error !== this.props.error) {
            this.setState({
                error: true
            })
        }
    }

    choseCard = (card) => {
        this.props.dispatch(setNewBonusCardAsync({
            token: this.props.token,
            username: this.props.userRegisterInfo.username,
            city_id: this.props.userRegisterInfo.city,
            plastic: card
        }));
        this.props.closeModal('thank', true);
    };

    render() {
        const { error } = this.state;

        return (
            <ModalLayer
                show={this.props.show}
                closeModal={(e) => this.props.closeModal('card', false, e)}
            >
                <Wrapper
                    wrapPadding="0 10px 15px"
                    wrapItems="center"
                >
                    <TextBlock className="form_title" textMargin="-30px 0 5px">
                        Хочу отримати бонусну карту
                    </TextBlock>

                    <Wrapper
                        wrapDirection="row"
                        wrapContent="center"
                        wrapMargin="50px 0"
                    >
                        <Wrapper
                            wrapContent="center"
                            wrapMinWidth="175px"
                            wrapMaxWidth="175px"
                            wrapPadding="25px 15px"
                            className={classNames("card_wrapper", { chosen: !this.state.plastic })}
                            onClick={() => this.choseCard(false)}
                        >
                            <ImageBlock
                                src="/static/img/card-electronic.png"
                                imgHeight={31}
                                imgWidth={44}
                            />
                            <TextBlock color="#00a6ea" centered lineHeight="24px" >
                                В електронному
                                вигляді
                            </TextBlock>
                        </Wrapper>

                        <Wrapper
                            wrapContent="center"
                            wrapMinWidth="175px"
                            wrapMaxWidth="175px"
                            wrapPadding="25px 15px"
                            className={classNames("card_wrapper", { chosen: this.state.plastic })}
                            onClick={() => this.choseCard(true)}
                        >
                            <ImageBlock
                                src="/static/img/card-plastic.png"
                                imgHeight={31}
                                imgWidth={44}
                            />
                            <TextBlock color="#00a6ea" centered lineHeight="24px" >
                                В пластиковому
                                вигляді
                            </TextBlock>
                        </Wrapper>
                    </Wrapper>
                    {
                        error &&
                        <TextBlock color="red" textMargin="10px 0" bold >
                            {this.props.error}
                        </TextBlock>
                    }

                </Wrapper>
            </ModalLayer>
        )
    }
}

export const CardModal = connect(({ state }) => ({
    token: state.token,
    error: state.error,
    success: state.success,
    userRegisterInfo: state.userRegisterInfo
}))(CardModalComponent);

