import { ModalLayer } from "../../components/PresentationalComponents";
import { connect } from 'react-redux';
import {
    ImageBlock,
    TextBlock,
    Wrapper
} from "../../components";
import dontready from '../../img/dontready.png'
export class CardDontReadyComponent extends Component {

    closeModal = (modal, open, e) => {
        this.props.closeModal(modal, open, e);
    };

    render() {
        const { show } = this.props;

        return (
            <ModalLayer
                show={show}
                wrapMaxWidth={480}
                closeModal={(e) => this.closeModal('dontready', false, e)}
            >
                <Wrapper
                    centered
                    wrapPadding="0 10px 15px"
                    textAling="center"
                    wrapMaxWidth={480}
                >
                    <TextBlock className="form_title" textMargin="-30px 0 5px">
                        Copi
                    </TextBlock>
                    <ImageBlock src={dontready} imgMargin="30px auto" imgWidth={215}/>
                    <TextBlock
                        color="#626262"
                        centered
                        textMaxWidth="100%"
                        lineHeight="24px"
                    >
                        Ваша карта ще не готова
                    </TextBlock>
                </Wrapper>
            </ModalLayer>
        )
    }
}

export const CardDontReadyModal = connect(({ state }) => ({
    error: state.error
}))(CardDontReadyComponent);
