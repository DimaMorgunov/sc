import { ModalLayer } from "../../components/PresentationalComponents";
import { connect } from 'react-redux';
import {
    ImageBlock,
    TextBlock,
    Wrapper
} from "../../components";
import sendpass from '../../img/sendpass.png'
import {setError, setSuccess} from "../../store";
export class SendPassComponent extends Component {

    closeModal = (modal, open, e) => {
        this.props.closeModal(modal, open, e);
        this.props.dispatch(setSuccess(null));
        this.props.dispatch(setError(null));
    };

    render() {
        const { show } = this.props;

        return (
            <ModalLayer
                show={show}
                closeModal={(e) => this.closeModal('send', false, e)}
            >
                <Wrapper
                    centered
                    wrapPadding="0 10px 15px"
                    textAling="center"
                    wrapMaxWidth={480}
                >
                    <TextBlock className="form_title" textMargin="-30px 0 5px">
                        Дякуємо!
                    </TextBlock>
                    <ImageBlock src={sendpass} imgMargin="30px auto" imgWidth={215}/>
                    <TextBlock
                        color="#626262"
                        centered
                        textMaxWidth="100%"
                        lineHeight="24px"
                    >
                        Лист з інструкцією надіслано на<br/>
                        вашу електронну пошту
                    </TextBlock>
                </Wrapper>
            </ModalLayer>
        )
    }
}

export const SendPassModal = connect(({ state }) => ({
    error: state.error
}))(SendPassComponent);
