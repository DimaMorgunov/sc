import classNames from 'classnames';
import { connect } from 'react-redux';
import {
    Button,
    Form,
    Input,
    ModalLayer,
    PreloaderDiv,
    TextBlock,
    Wrapper
} from "../../components";
import {orangeButton} from "../../themes";


import 'react-select-v1/dist/react-select.css';

import {
    addBonusCard,
    getUserInfoAsync,
    setError,
    setSuccess
} from "../../store";

export class BindCardComponent extends Component {
    state = {
        inputValues: {
            code: '',
        },
        errorInput:{
            code: '',
        },
        showPreloader: false,
        errorFields: [],
        error: this.props.error
    };

    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false,
            });
        }
    }

    componentWillReceiveProps (nextProps) {
        if(this.props.error != nextProps.error){
            this.setState({
                error: nextProps.error
            })
        }
    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
            switch(target.name) {
                case 'code':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }
        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    closeModal = (modal, open, e) => {
        this.setState({
            inputValues: {
                code: '',
            },
            showPreloader: false,
            errorFields: [],
        });

        this.props.dispatch(setError(null));
        this.props.closeModal(modal, open, e);
    };

    formHandler = () => {
        const { inputValues } = this.state;
        let errorFields = [];

        for(let key in inputValues) {
            switch(key) {
                case 'code':
                    if(!inputValues.code.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.code = 'Упс, забули вказати Код';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.setState({
                showPreloader: true,
            });
            this.props.dispatch(setError(null));
            this.props.dispatch(addBonusCard({
                token: this.props.token,
                userToken: this.props.userToken,
                key: this.state.inputValues.code
                }));
        }
    };

    render() {
        const { show } = this.props;
        const {
            inputValues,
            errorFields,
            showPreloader,
            error
        } = this.state;

        return (
            <ModalLayer
                show={show}
                closeModal={(e) => this.closeModal('bind', false, e)}
            >
                <Wrapper
                    wrapPadding="0 10px 15px"
                >
                    <TextBlock className="form_title">
                        Введіть Код
                    </TextBlock>
                    <Form>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", { focused: inputValues.code !== '' } )}
                        >
                            <label className={classNames({ focused: inputValues.code !== '' })}>
                                Код
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^code$`).test(el)) })}
                                name="code"
                                value={inputValues.code}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.code? this.state.errorInput.code: null}</p>
                        <Wrapper
                            wrapMargin="30px 0 10px"
                            wrapItems="flex-start"
                            className="form_btn_wrapper"
                        >
                            <Button
                                theme={orangeButton}
                                onClick={this.formHandler}
                            >
                                Зареєструватися
                            </Button>
                            {
                                error &&
                                <TextBlock color="red" textMargin="10px 0" bold >
                                    {this.state.error.detail}
                                </TextBlock>
                            }
                        </Wrapper>
                    </Form>
                </Wrapper>
                <PreloaderDiv show={showPreloader} />
            </ModalLayer>
        )
    }
}

export const BindCardModal = connect(({ state }) => ({
    token: state.token,
    userToken: state.userToken,
    error: state.error,
    success: state.success
}))(BindCardComponent);