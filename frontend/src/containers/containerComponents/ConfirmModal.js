import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Form,
  Input,
  TextBlock,
  Wrapper,
  ModalLayer,
  PreloaderDiv,
  Button
} from "../../components";
import { orangeButton } from "../../themes";
import {
  confirmRegistrationAsync,
  setError,
  setSuccess,
  repeatConfirmRegistrationAsync
} from "../../store";


export class ConfirmModalComponent extends Component {
  state = {
    key: '',
    showPreloader: false,
    success: false
  };

  componentDidUpdate(prevProps) {
     /*if(this.state.showPreloader){
       this.setState({
         showPreloader: false
       })
     }*/
    /*if(prevProps.error !== this.props.error && this.props.success) {
      this.setState({
        showPreloader: false,
      })
    }

    if(prevProps.success !== this.props.success && this.props.success) {
      this.setState({
        showPreloader: false,
      });
    }*/

    /*if(prevProps.userToken !== this.props.userToken && this.props.userToken && this.props.success) {
      console.log('this.props.userToken', this.props.userToken);
      console.log('this.props.bonusCard)', this.props.bonusCard);
      if(this.props.bonusCard) {
        this.closeModal('thank', true);
      } else {
        console.log('234')
        this.closeModal('card', true);
      }
    }*/
  }

  inputHandler = ({target}) => {
    this.setState({
      [target.name]: target.value,
      success: false
    })
  };

  repeatKey = () => {
    this.setState({
      showPreloader: true,
      error: false
    });

    this.props.dispatch(setError(null));
    this.props.dispatch(repeatConfirmRegistrationAsync({
      token: this.props.token,
      username: this.props.userRegisterInfo.username
    }))
  };

  closeModal = (modal, open, e) => {
    this.setState({
      key: '',
      showPreloader: false,
      error: false,
      success: false
    });
    this.props.dispatch(setError(null));
    this.props.closeModal(modal, open, e);
  };

  formHandler = () => {
    let error = false;
    if(!this.state.key) {
      error = true;

      this.setState({
        error,
      })
    }


    if(!error) {
      this.setState({
        key: '',
        showPreloader: false,
        error: false
      });
      this.props.dispatch(setError(null));
      this.props.dispatch(confirmRegistrationAsync({
        token: this.props.token,
        username: this.props.userRegisterInfo.username,
        key: this.state.key
      }));
    }
  };

  render() {
    const { show } = this.props;
    const { key, showPreloader, error, success } = this.state;

    return (
      <ModalLayer
        confirm
        show={show}
      >
        <Wrapper
          wrapPadding="0 10px 15px"
          wrapItems="flex-start"
        >
          <TextBlock className="form_title" centered textMargin="40px 0 30px" textWidth="100%">
            Підтвердження
          </TextBlock>
          <TextBlock color="#0d1017" textMaxWidth="70%" lineHeight="25px">
            Ми відправили вам код підтвердження на вказаний
            номер телефону
          </TextBlock>
          <Form>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: key !== '' })}>
                введіть код
              </label>
              <Input
                type="text"
                className={classNames("input", {error: this.state.error })}
                name="key"
                value={key}
                onChange={this.inputHandler}
              />
            </Wrapper>
            <Wrapper
              wrapMargin="30px 0"
              wrapDirection="row"
            >
              <Button
                theme={orangeButton}
                onClick={this.formHandler}
                btnMargin="0 15px 0 0"
              >
                Підтвердити
              </Button>
              <Button className="underlined_link_btn" onClick={this.repeatKey}>
                Відправити повторно
              </Button>
              {
                this.props.error ?
                <TextBlock color="red" textMargin="10px 0" bold >
                    Упс, щось пішло не так. Спробуйте ще раз
                </TextBlock>
                    : this.props.success?
                    <TextBlock color="#19bd19" textMargin="5px 5px 5px 20px" bold >
                      Дякуємо, ми відправили вам новий код
                    </TextBlock>
                  :null

              }
            </Wrapper>
          </Form>
        </Wrapper>

        <PreloaderDiv show={showPreloader} />

      </ModalLayer>
    )
  }
}

export const ConfirmModal = connect(({ state }) => ({
  token: state.token,
  error: state.error,
  success: state.success,
  userRegisterInfo: state.userRegisterInfo,
  userToken: state.userToken,
  bonusCard: state.bonusCard
}))(ConfirmModalComponent);

