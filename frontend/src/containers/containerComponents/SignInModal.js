import { ModalLayer } from "../../components/PresentationalComponents";
import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Button,
  Form,
  Input,
  PreloaderDiv,
  TextBlock,
  Wrapper
} from "../../components";

import { orangeButton } from "../../themes";
import {loginAsync, setError, setSuccess} from "../../store";
import {emailReg} from "../../consts";

export class SignInModalComponent extends Component {
  state = {
    lostPass: false,
    inputValues: {
      email: '',
      password: ''
    },
      errorInput: {
          email: '',
          password: ''
      },
    errorFields: [],
    showPreloader: false,
    error: ''
  };

  componentDidMount(){
    this.props.dispatch(setError(null));
    this.props.dispatch(setSuccess(null));
  }

  componentDidUpdate(prevProps) {
      if (prevProps.error !== this.props.error && this.props.error) {
          this.setState({
              showPreloader: false,
              error: true
          })
      }
      /*if (prevProps.success !== this.props.success) {
          this.closeModal('signIn', null);

      }*/
  }

  inputHandler = ({target}) => {
    if(this.state.errorFields.length > 0) {
      let errStr = this.state.errorFields.filter(err => err !== target.name);
      this.setState({ errorFields: errStr });
        switch(target.name) {
            case 'email':
                if(target.name.length > 0) {
                    let errorInput = this.state.errorInput;
                    errorInput.email = null;
                    this.setState({errorInput: errorInput});
                }
                break;
            case 'password':
                if(target.name.length > 0) {
                    let errorInput = this.state.errorInput;
                    errorInput.password = null;
                    this.setState({errorInput: errorInput});
                }
                break;
            default:
                break;
        }
    }

    this.setState({ inputValues: {
        ...this.state.inputValues,
        [target.name]: target.value
      }})
  };

  closeModal = (modal, open, e) => {
    this.setState({
      inputValues: {
        email: '',
        password: ''
      },
      errorFields: [],
      error: false
    });

    this.props.dispatch(setError(null));
    this.props.closeModal(modal, open, e);
  };

  formHandler = () => {
    const { inputValues } = this.state;
    let errorFields = [];

      for(let key in inputValues) {
          switch(key) {
              case 'email':
                  if(!inputValues.email.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.email = 'Упс, перевірте E-mail';
                      this.setState({errorInput: errorInput});
                  }
                  break;
              case 'password':
                  if(!inputValues.password.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.password = 'довжина пароля повинна бути не менше 8 символів та пароль повинен складатися з букв латинського алфавіту (A-z), арабських цифр (0-9) і спеціальних символів.';
                      this.setState({errorInput: errorInput});
                  }
                  break;
              default:
                  break;
          }
      }

    this.setState({ errorFields });

    if(errorFields.length === 0) {
      this.setState({ showPreloader: true });

      this.props.dispatch(setError(null));
      this.props.dispatch(loginAsync({
        token: this.props.token,
        username: inputValues.email,
        password: inputValues.password
      }));
    }
  };

  render() {
    const { show, fromSchedule } = this.props;
    const {
      inputValues,
      errorFields,
      showPreloader,
      error
    } = this.state;

    return (
      <ModalLayer
        show={show}
        closeModal={(e) => this.closeModal('signIn', false, e)}
      >
        <Wrapper
          wrapPadding="0 10px 15px"
          wrapItems="flex-start"
        >
          <TextBlock className="form_title" textMargin="-30px 0 5px" textPadding="0 30px 0 0">
            Вхід до особистого кабінету
          </TextBlock>
          <Form className={classNames({ "with_border": !fromSchedule })}>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.email !== '' })}>
                E-mail
              </label>
              <Input
                type="text"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                name="email"
                value={inputValues.email}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.password !== ''  })}>
                Пароль
              </label>
              <Input
                type="password"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password$`).test(el)) })}
                name="password"
                value={inputValues.password}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.password? this.state.errorInput.password: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              wrapDirection="row"
              className="form_btn_wrapper"
            >
              <Button
                theme={orangeButton}
                onClick={this.formHandler}
              >
                Увійти
              </Button>
              <Button className="forgot_password" onClick={(e) => this.closeModal('lostPass', true, e)}>
                Не вдалося увійти?
              </Button>
                {error &&
                    <TextBlock color="#ff0000" textMargin="5px 5px 5px 20px" bold >
                        Упс, щось пішло не так. Спробуйте ще раз
                    </TextBlock>
                }
            </Wrapper>
          </Form>
          {
            !fromSchedule &&
            <Wrapper
              wrapMargin="30px 0 0"
              wrapItems="flex-start"
              wrapDirection="row"
              className="form_btn_wrapper"
            >
              <TextBlock color="#0d1017" className="sign_up_text">
                Ще нема аккаунту?
              </TextBlock>
              <Button
                className="sign_up_link"
                onClick={(e) => this.closeModal('signUp', true, e)}
              >
                Реєстрація
              </Button>
            </Wrapper>
          }

        </Wrapper>

        <PreloaderDiv show={showPreloader} />

      </ModalLayer>
    )
  }
}

export const SignInModal = connect(({ state }) => ({
  token: state.token,
  userToken: state.userToken,
  error: state.error,
  success: state.success,
  userName: state.userName,
  userLogin: state.userLogin
}))(SignInModalComponent);
