import { connect } from 'react-redux';
import {
    ModalLayer,
    Wrapper,
    Image
} from "../../components";
import classNames from "classnames";

export class ShowZipCodeModalCmponent extends Component {

    render() {
        return (
            <ModalLayer
                wrapMinWidth="500px"
                className="modalZipWrap"
                show={this.props.show}
                closeModal={(e) => this.props.closeModal('card', false, e)}

            >
                <Wrapper className="modalZipWrap">
                    <Image
                        className="zipCodeImg"
                        src={this.props.zipCode}
                    />
                </Wrapper>
            </ModalLayer>
        )
    }
}

export const ShowZipCodeModal = connect(({ state }) => ({
    token: state.token,
    error: state.error,
    success: state.success,
    userRegisterInfo: state.userRegisterInfo
}))(ShowZipCodeModalCmponent);

