import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Button,
  Image, Input,
  Label,
  ModalLayer,
  Span,
  TextBlock,
  Title,
  Wrapper,
  CheckboxDiv,
  FormButton,
  PreloaderDiv
} from "../../components";
import calendar from "../../img/calendar.png";
import clock from "../../img/clock.png";

import DatePicker from 'react-datepicker';
import 'moment/locale/uk';
import {blueButton, orangeButton} from "../../themes";

import { Stage, Layer, Rect, Text, Circle, Tag } from 'react-konva';
import { Label as LabelKonva } from 'react-konva';
import close from "../../img/close.png";
import check from "../../img/check.png";

import {
  setFilmReservationAsync,
  updateFilmReservationAsync,
  clearFilmReservationAsync,
  sendEmailAsync
} from "../../store";

import { getCookie } from "../../consts";

export class SelectModalLayerComponent extends Component {

  state = {
    windiwWidth: null,
    scaleX: null,
    scaleY: null,
    canvasWrapWidth: 950,
    token: getCookie('csrftoken'),
    circlesSeats: null,
    tickets: 0,
    ticketPrice: 0,
    chosenSeats: [],
    buyModal: this.props.buyModal,
    show: this.props.show,
    focused: {
      email: false
    },
    inputValues: {
      email: ''
    },
    terms: true,
    showPreloader: false,
    timer: '15:00'
  };


  componentDidMount() {

    let date = new Date();
    let startTime = new Date(date.getTime() + 15 * 60000);

    const seatsData = this.props.filmSeats;
    let circlesArr = [];

    if(seatsData) {
      for(let key in seatsData.row_list) {
        circlesArr[key] = [];
        let value = seatsData.row_list[key];


        for(let i = 1; i <= value.length; i++) {

          if(value.place_type === 1) {
            if(i === 1) {
              circlesArr[key].push({
                [`x_${i}`]: value.x_position,
                y: value.y_position,
                type: value.place_type,
                except: value.except_place.some((seat) => seat === i)
              })
            } else {

              circlesArr[key].push({
                [`x_${i}`]: seatsData.row_numbering_direction === "ltr" ?
                  circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] + 30 :
                  circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] - 30,
                y: value.y_position,
                type: value.place_type,
                except: value.except_place.some((seat) => seat === i)
              })
            }
          } else {
            if(i === 1) {
              circlesArr[key].push({
                [`x_${i}`]: value.x_position,
                y: value.y_position,
                type: value.place_type,
                except: value.except_place.some((seat) => seat === i)
              })
            } else {

              circlesArr[key].push({
                [`x_${i}`]: seatsData.row_numbering_direction === "ltr" ?
                  circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] + 80 :
                  circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] - 80,
                y: value.y_position,
                type: value.place_type,
                except: value.except_place.some((seat) => seat === i)
              })
            }
          }
        }
      }

      const windiwWidth = window.innerWidth;

        if (window.innerWidth > 300){
            this.setState({
                scaleX: 0.40,
                scaleY: 0.40,
                canvasWrapWidth: 300 ,
            })
        }
        if (window.innerWidth > 400){
            this.setState({
                scaleX: 0.5,
                scaleY: 0.5,
                canvasWrapWidth: 400 ,
            })
        }
        if (window.innerWidth > 500){
            this.setState({
                scaleX: 0.6,
                scaleY: 0.6,
                canvasWrapWidth: 500 ,
            })
        }
        if (window.innerWidth > 800){
            this.setState({
                scaleX: 0.7,
                scaleY: 0.7,
                canvasWrapWidth: 600 ,
            })
        }
        if (window.innerWidth > 1000){
            this.setState({
                scaleX: 1,
                scaleY: 1,
                canvasWrapWidth: 950 ,
            })
        }

      this.setState({
        circlesSeats: circlesArr,
        textLeft: seatsData.left_text_position,
        textRight: seatsData.right_text_position,
        canvasHeight: seatsData.hall_height,
        startTime,
        windiwWidth: windiwWidth
      });
    }
  }


  startTimer = () => {

    this.setState({
      timer: this.state.timer
    });

    this.timer = setInterval(() => {
      let time = this.state.startTime - Date.now();
      let timerTime = `${new Date(time).getMinutes()}:${new Date(time).getSeconds() < 10 ?
        '0' + new Date(time).getSeconds() :
        new Date(time).getSeconds()}`;
      if(timerTime === '59:59') {
        clearInterval(this.timer);
      } else {
        this.setState({
          timer: timerTime
        })
      }
    }, 1000);
  };

  componentDidUpdate(prevProps) {
    if(this.props.reservation && prevProps.reservation !== this.props.reservation) {
      this.setState({
        buyModal: true,
        show: false,
        showPreloader: false,
        inputValues: {
          email: this.props.reservation.is_user_login ? this.props.reservation.email : ''
        }
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  chosenSeat = (row, seat, price, name, el) => {
    if(el.except) return;
    let tickets = this.state.tickets;
    let ticketPrice = this.state.ticketPrice;

    let seats = this.state.chosenSeats;
    let seatsObj = {row: row, place: seat+1, type: 1};

    if(this.refs[`${name}`].attrs.fill === "#00a6ea") {
      seats.push(seatsObj);

      this.setState({
        chosen: !this.state.chosen,
        [`color_${name}`]: "#fd5f00",
        tickets: tickets + 1,
        ticketPrice: ticketPrice + price,
        chosenSeats: seats
      })
    } else {
      seats = seats.filter(seat => JSON.stringify(seat) !== JSON.stringify(seatsObj));

      this.setState({
        chosen: !this.state.chosen,
        [`color_${name}`]: "#00a6ea",
        tickets: tickets - 1,
        ticketPrice: ticketPrice - price,
        chosenSeats: seats
      })
    }
  };

  chosenBigSeat = (row, seat, price, name, el) => {

    if(el.except) return;
    let tickets = this.state.tickets;
    let ticketPrice = this.state.ticketPrice;

    let seats = this.state.chosenSeats;
    let seatsObj = {row: row, place: seat+1, type: 2};

    if(this.refs[`${name}`].attrs.fill === "#4bb143") {
      seats.push(seatsObj);

      this.setState({
        chosen: !this.state.chosen,
        [`color_${name}`]: "#fd5f00",
        tickets: tickets + 1,
        ticketPrice: ticketPrice + price,
        chosenSeats: seats
      })
    } else {
      seats = seats.filter(seat => JSON.stringify(seat) !== JSON.stringify(seatsObj));

      this.setState({
        chosen: !this.state.chosen,
        [`color_${name}`]: "#4bb143",
        tickets: tickets - 1,
        ticketPrice: ticketPrice - price,
        chosenSeats: seats
      })
    }
  };

  selectModalBuy = () => {
    this.startTimer();

    this.props.dispatch(setFilmReservationAsync({
      token: this.state.token,
      id: this.props.sessionId,
      places: this.state.chosenSeats
    }));

    this.setState({ showPreloader: true })

  };

  toggleSelectModal = () => {
    this.setState({
      buyModal: false,
      show: false
    })


  };

  backToSelectModal = () => {

    this.props.dispatch(clearFilmReservationAsync({
      token: this.state.token,
      id: this.props.reservation.reservation_id
    }));

    this.setState({
      buyModal: false,
      show: true
    });

  };

  inputHandler = ({target}) => {
    this.setState({ inputValues: {
        ...this.state.inputValues,
        [target.name]: target.value
      }})
  };

  deleteSeat = (ticket, price) => {

    let seats = this.state.chosenSeats;
    seats = seats.filter(seat => JSON.stringify(seat) !== JSON.stringify(ticket));

    this.props.dispatch(updateFilmReservationAsync({
      token: this.state.token,
      id: this.props.reservation.reservation_id,
      places: seats
    }));

    this.setState({
      chosenSeats: seats,
      tickets: this.state.tickets - 1,
      ticketPrice: this.state.ticketPrice - price,
      [`color_${ticket.row}_${ticket.place - 1}`]: ticket.type === 1 ? "#00a6ea" : "#4bb143",
      showPreloader: true
    });
  };

  checkboxHandler = () => {
    this.setState({ terms: !this.state.terms });
  };

  submitForm = () => {

    this.props.dispatch(sendEmailAsync({
      token: this.state.token,
      id: this.props.reservation.reservation_id,
      email: this.state.inputValues.email
    }));

    this.form.setAttribute("action", "https://www.liqpay.ua/api/3/checkout");
  };

  showTooltip = (e) => {
    this.setState({ [`${e}_text`]: true });
  };

  hideTooltip = (e) => {
    this.setState({ [`${e}_text`]: false });
  };


  render() {
    const {
      toggleSelectModal,
      el,
      filmFormat,
      filmHall,
      startDate,
      filmTime,
      priceFrom,
      priceTo,
      screenPicture,
      reservation,
      signInOpen
    } = this.props;

    const {
      circlesSeats,
      textLeft,
      textRight,
      canvasHeight,
      tickets,
      ticketPrice,
      chosenSeats,
      show,
      buyModal,
      focused,
      inputValues,
      terms,
      showPreloader,
      timer
    } = this.state;

    return (
      <React.Fragment>
        <ModalLayer
          show={show}
          closeModal={toggleSelectModal}
          selectModal
          wrapMinWidth={320}
        >
          <Wrapper
            className="modal_inside"
            wrapItems="flex-start"
            wrapPadding="15px"
            wrapMinWidth={320}
          >
            <Wrapper
              className={this.state.windiwWidth? this.state.windiwWidth < 960? "buytickmodal" : null : null}
              wrapDirection="row"
              wrapItems="flex-start"
              wrapMargin="-35px 0 0 0"
              wrapMinWidth={320}
            >
              <Title blue fontSize="25px">
                {el.title}
                <Label className={classNames("block_label", "white_label")}>{filmFormat}</Label>
              </Title>
            </Wrapper>
            <TextBlock>
              {filmHall}
            </TextBlock>
            <Wrapper
              wrapDirection="row"
              wrapMargin="20px 0"
              wrapContent="space-between"
              wrapMinWidth="100%"
              className="buyTicketWrap"
            >
              <Wrapper
                wrapDirection="row"
                wrapMinWidth="40%"
                wrapMaxWidth="40%"
                className="buyTicketWrapTime"
              >
                <Image
                  src={calendar}
                  imgHeight={22}
                  imgWidth={20}
                  imgMargin="0 15px 0 0"
                />
                <DatePicker
                  selected={startDate}
                  className={classNames("schedule_calendar", "modal_input")}
                  readOnly={true}
                  locale="uk"
                />
                <Wrapper
                    wrapMaxWidth="200px"
                    wrapMinWidth="140px"
                    className="wrapClockBuyModal"
                >
                  <Image
                    src={clock}
                    imgHeight={23}
                    imgWidth={23}
                    imgMargin="0 15px 0 0"
                  />
                  <Span font="16px">{filmTime}</Span>
                </Wrapper>
              </Wrapper>
              <Wrapper
                wrapDirection="row"
                wrapMinWidth="60%"
                wrapMaxWidth="60%"
                wrapContent="flex-end"
                className="buyTicketWrapPlace"
              >
                <Wrapper
                    wrapContent="center"
                    wrapMaxWidth="200px"
                    wrapMinWidth="100px"
                    className="wrapClockBuyModal"
                >
                  <Label className="price_from"/>
                  <Span>{priceFrom} грн</Span>
                </Wrapper>
                  <Wrapper
                      wrapContent="center"
                      wrapMaxWidth="200px"
                      wrapMinWidth="100px"
                      className="wrapClockBuyModal"
                  >
                      <Label className="price_to"/>
                      <Span>{priceTo} грн</Span>
                  </Wrapper>

                  <Wrapper
                      wrapContent="center"
                      wrapMaxWidth="200px"
                      wrapMinWidth="100px"
                      className="wrapClockBuyModal"
                  >
                      <Label className="price_sold"/>
                      <Span>Вже продано</Span>
                  </Wrapper>

              </Wrapper>
            </Wrapper>
          </Wrapper>

          <Wrapper
            className="seats_map"
            wrapPadding="15px 0"
          >
            <Image
              src={screenPicture}
              imgWidth={490}
              imgHeight={90}
              className="canvas_image"
              imgMargin="20px 0"
            />
            {
              circlesSeats ?
              <Stage className='canvasWrapWidth' width={this.state.canvasWrapWidth} height={canvasHeight} scaleY={this.state.scaleY} scaleX={this.state.scaleX}>
                {
                  circlesSeats.map((circle, g) => {
                    return (
                      <Layer key={`${g}_${circle[0].y - 8}`}>
                        <Text
                          text={g}
                          fontSize={15}
                          x={textLeft - 8}
                          y={circle[0].y - 8}
                          fill="#fff"
                        />
                        {
                          circle.map((el, i) => {
                            if(el.type === 1) {
                              return (
                                <React.Fragment>
                                  <LabelKonva
                                    x={el[`x_${i + 1}`]}
                                    y={el.y - 10}
                                    visible={this.state[`${g}_${i}_text`] ? this.state[`${g}_${i}_text`] : false}
                                  >
                                    <Tag
                                      fill="#373b42"
                                      pointerDirection='down'
                                      pointerWidth={15}
                                      pointerHeight={10}
                                      cornerRadius={5}
                                      lineJoin= 'round'
                                      shadowColor= 'black'
                                    />
                                    <Text
                                      text={`${g} ряд, ${i+1} місце\n${priceFrom} грн`}
                                      fontSize={17}
                                      padding={7}
                                      lineHeight={1.5}
                                      fill='white'
                                      align="center"
                                    />
                                  </LabelKonva>
                                    {this.state.windiwWidth > 800?
                                        <Circle
                                            key={`${g}_${i}`}
                                            x={el[`x_${i + 1}`]}
                                            y={el.y}
                                            radius={11}
                                            scaleX={el.except ? 0.4 : 1}
                                            scaleY={el.except ? 0.4 : 1}
                                            fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#00a6ea"}
                                            ref={`${g}_${i}`}
                                            onClick ={() => this.chosenSeat(g, i, priceFrom, `${g}_${i}`, el)}
                                            onMouseOver={() => this.showTooltip(`${g}_${i}`)}
                                            onMouseOut={() => this.hideTooltip(`${g}_${i}`)}
                                        />
                                        :
                                        <Circle
                                            key={`${g}_${i}`}
                                            x={el[`x_${i + 1}`]}
                                            y={el.y}
                                            radius={11}
                                            scaleX={el.except ? 0.4 : 1}
                                            scaleY={el.except ? 0.4 : 1}
                                            fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#00a6ea"}
                                            ref={`${g}_${i}`}
                                            onTouchStart ={() => this.chosenSeat(g, i, priceFrom, `${g}_${i}`, el)}
                                            onMouseOver={() => this.showTooltip(`${g}_${i}`)}
                                            onMouseOut={() => this.hideTooltip(`${g}_${i}`)}
                                        />
                                    }

                                </React.Fragment>
                              )
                            } else {
                              return (
                                <React.Fragment>
                                  <LabelKonva
                                    x={el[`x_${i + 1}`] + 25}
                                    y={el.y - 10}
                                    visible={this.state[`${g}_${i}_text`] ? this.state[`${g}_${i}_text`] : false}
                                  >
                                    <Tag
                                      fill="#373b42"
                                      pointerDirection='down'
                                      pointerWidth={15}
                                      pointerHeight={10}
                                      cornerRadius={5}
                                      lineJoin= 'round'
                                      shadowColor= 'black'
                                    />
                                    <Text
                                      text={`${g} ряд, ${i+1} місце\n${priceTo} грн`}
                                      fontSize={17}
                                      padding={7}
                                      lineHeight={1.5}
                                      fill='white'
                                      align="center"
                                    />
                                  </LabelKonva>
                                    {this.state.windiwWidth > 800?
                                        <Rect
                                            key={`${g}_${i}`}
                                            x={el[`x_${i + 1}`]}
                                            y={el.y - 10}
                                            cornerRadius={12}
                                            width={54}
                                            height={22}
                                            fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#4bb143"}
                                            ref={`${g}_${i}`}
                                            onClick={() => this.chosenBigSeat(g, i, priceTo, `${g}_${i}`, el)}
                                            onMouseOver={() => this.showTooltip(`${g}_${i}`)}
                                            onMouseOut={() => this.hideTooltip(`${g}_${i}`)}
                                        />
                                        :
                                        <Rect
                                            key={`${g}_${i}`}
                                            x={el[`x_${i + 1}`]}
                                            y={el.y - 10}
                                            cornerRadius={12}
                                            width={54}
                                            height={22}
                                            fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#4bb143"}
                                            ref={`${g}_${i}`}
                                            onTouchStart={() => this.chosenBigSeat(g, i, priceTo, `${g}_${i}`, el)}
                                            onMouseOver={() => this.showTooltip(`${g}_${i}`)}
                                            onMouseOut={() => this.hideTooltip(`${g}_${i}`)}
                                        />
                                    }

                                </React.Fragment>
                              )
                            }
                          })
                        }
                        <Text
                          key={`${g}_${circle[0].y}`}
                          text={g}
                          fontSize={15}
                          x={textRight}
                          y={circle[0].y - 8}
                          fill="#fff"
                        />
                      </Layer>
                    )
                  })
                }
              </Stage>
                :
              <TextBlock color="red" textMargin="10px 0" bold >
                Упс, вибачте, щось пішло не так :(
              </TextBlock>
            }
          </Wrapper>

          <Wrapper
            wrapDirection="row"
            wrapPadding="15px"
            wrapContent="space-between"
          >
            <TextBlock>
              {tickets} {tickets === 1 ? 'квиток' : tickets >= 2 && tickets <= 4 ? 'квитка' : 'квитків'}, {ticketPrice} грн
            </TextBlock>
            <Button
              className={classNames({ "disabled": tickets === 0 })}
              theme={orangeButton}
              onClick={tickets === 0 ? null : () => this.selectModalBuy()}
            >
              Купити
            </Button>
          </Wrapper>

        </ModalLayer>

        <ModalLayer
            show={buyModal}
            closeModal={this.toggleSelectModal}
            selectModal
            buyModal
            backToSelectModal={this.backToSelectModal}
          >
            <Wrapper
              className="modal_inside"
              wrapItems="flex-start"
              wrapPadding="15px 30px"
            >
              <Wrapper
                wrapDirection="row"
                wrapItems="flex-start"
              >
                <Title blue fontSize="25px">
                  {el.title}
                  <Label className={classNames("block_label", "white_label")}>{filmFormat}</Label>
                </Title>
              </Wrapper>
              <TextBlock>
                {filmHall}
              </TextBlock>
              <Wrapper
                wrapDirection="row"
                wrapMargin="20px 0"
                wrapContent="space-between"
              >
                <Wrapper
                  wrapDirection="row"
                  wrapMinWidth="40%"
                  wrapMaxWidth="40%"
                >
                  <Image
                    src={calendar}
                    imgHeight={22}
                    imgWidth={20}
                    imgMargin="0 15px 0 0"
                  />
                  <DatePicker
                    selected={startDate}
                    className={classNames("schedule_calendar", "modal_input")}
                    readOnly={true}
                    locale="uk"
                  />
                  <Image
                    src={clock}
                    imgHeight={23}
                    imgWidth={23}
                    imgMargin="0 15px 0 0"
                  />
                  <Span font="16px">{filmTime}</Span>
                </Wrapper>

                <Wrapper
                  wrapDirection="row"
                  wrapMinWidth="60%"
                  wrapMaxWidth="60%"
                  wrapContent="flex-end"
                >
                  <Label className="timer">
                    {
                      timer === '0:00' ?
                        'Вашу бронь анульовано' :
                        `Залишилось ${timer}`
                    }

                  </Label>
                </Wrapper>
              </Wrapper>
            </Wrapper>

            <Wrapper
              wrapDirection="row"
              wrapItems="flex-start"
              className="seats_map"
              wrapPadding="15px 30px"
            >
              <Wrapper
                className={classNames("tickets_first", "modalWidth")}
              >
                <Wrapper
                  wrapItems="flex-start"
                  className={classNames("tickets_block", { small: chosenSeats.length <= 3 })}
                >
                  {
                    chosenSeats.map(ticket => {
                      return (
                        <Wrapper
                          wrapDirection="row"
                          wrapContent="space-between"
                          wrapMargin="20px 0"
                        >
                          <Label className={classNames("tickets", "transparent", { small: chosenSeats.length > 3 })}>
                            <Span className="ticket">{ticket.row} <br/> ряд</Span>
                            <Span className="ticket">{ticket.place} <br/> місце</Span>
                            {
                              ticket.type === 1 ?
                                <Span className="ticket">
                                {priceFrom} <br/> грн
                              </Span>
                                :
                                <Span className="ticket">
                                {priceTo} <br/> грн
                              </Span>
                            }

                          </Label>
                          {tickets > 1?
                              <Image
                                  src={close}
                                  imgHeight={21}
                                  imgWidth={21}
                                  onClick={() => this.deleteSeat(ticket, ticket.type === 1 ? priceFrom : priceTo)}
                                  className="close_modal"
                                  imgMargin={ chosenSeats.length > 3 ? "15px 30px 15px 0" : "15px 40px 15px 0"}
                              />
                              :null
                          }
                        </Wrapper>
                      )
                    })
                  }
                </Wrapper>
                <Wrapper
                  wrapItems="flex-start"
                  wrapMargin="10px 0"
                >
                  <Label blue className="tickets">
                  <Span className="ticket_span">
                    {tickets} {tickets === 1 ? 'квиток' : tickets >= 2 && tickets <= 4 ? 'квитка' : 'квитків'}
                    </Span>
                    <Span className="ticket_span">
                    {ticketPrice} грн
                  </Span>
                  </Label>
                </Wrapper>
              </Wrapper>

              <Wrapper
                wrapItems="flex-start"
                /*wrapMinWidth="50%"
                wrapMaxWidth="50%"*/
                className={classNames("modalWidth")}
                wrapPadding="10px 0 10px 30px"
              >
                <TextBlock>
                  Куди надіслати замовлення
                </TextBlock>
                <Wrapper
                  wrapMargin="30px 0 0 0"
                  className={classNames( "input_wrapper", "input_on_input" )}
                >
                  <label className={classNames({ focused: inputValues.email === '' ? focused.email : true })}>
                    E-mail
                  </label>
                  <Input
                    className="buyTicketInputMail"
                    type="text"
                    name="email"
                    value={inputValues.email}
                    labelClass={focused}
                    onChange={this.inputHandler}
                  />
                </Wrapper>
                <TextBlock textMargin="20px 0">
                  Aбо
                </TextBlock>
                <Button
                  className={this.props.reservation && this.props.reservation.is_user_login ? 'disabled' : ''}
                  theme={blueButton}
                  onClick={this.props.reservation && this.props.reservation.is_user_login ? null : () => signInOpen('signIn', true)}
                >
                  Вхід до особистого кабінету
                </Button>
              </Wrapper>
            </Wrapper>

            <Wrapper
              wrapDirection="row"
              wrapPadding="15px"
              wrapContent="space-between"
            >
              <Wrapper
                wrapDirection="row"
                wrapItems="center"
                wrapMargin="10px 0"
                wrapMinWidth="70%"
                wrapMaxWidth="70%"
                className="checkbox_div"
              >
                <CheckboxDiv
                  src={check}
                  className={classNames({checked: terms === true})}
                  onClick={() => this.checkboxHandler()}
                />
                <TextBlock lineHeight="24px" textMaxWidth="70%">
                  Ви погоджуєтеся з <a className="pdf_link" href=" /media/pdf/purchase_rules.pdf" target="_blank"> умовами та правилами on-line
                  покупки квитків </a>
                </TextBlock>
              </Wrapper>

              {
                tickets !== 0 && terms === true && inputValues.email !== '' ?
                <form ref={el => this.form = el} onSubmit={this.submitForm} method="POST" acceptCharset="utf-8">
                  <input type="hidden" name="data" value={reservation && reservation.result.data} />
                  <input type="hidden" name="signature" value={reservation && reservation.result.signature} />
                  <FormButton
                    type="submit"
                    theme={orangeButton}
                  >
                    Перейти до сплати
                  </FormButton>
                </form> :
                  <Button
                    className="disabled"
                    theme={orangeButton}
                  >
                    Перейти до сплати
                  </Button>
              }

            </Wrapper>
          </ModalLayer>

        <PreloaderDiv show={showPreloader}/>

      </React.Fragment>

    )
  }
}

export const SelectModalLayer = connect(({ state }) => ({
  reservation: state.reservation
}))(SelectModalLayerComponent);