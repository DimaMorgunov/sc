import {ModalLayer} from "../../components/PresentationalComponents";
import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Button,
  Form, Image,
  Input, PreloaderDiv,
  TextBlock,
  Wrapper
} from "../../components";
import {orangeButton} from "../../themes";

import Select from 'react-select-v1';
import 'react-select-v1/dist/react-select.css';
import {getVacanciesForSelectAsync, sendVacancyForSelectAsync, setSuccess} from "../../store";
import {emailReg, getCookie} from "../../consts";


export class CareerModalComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValues: {
        email: '',
        phone: '',
        name: '',
        surname: '',
        vacancy: '',
        cv: null
      },
        errorInput: {
            email: '',
            phone: '',
            name: '',
            surname: '',
            vacancy: '',
            cv: null
        },
      fileName: null,
      errorFields: [],
      showPreloader: false
    };
    props.dispatch(getVacanciesForSelectAsync({ token: getCookie('csrftoken') }))
  }

  componentDidUpdate(prevProps) {
    if(prevProps.success !== this.props.success) {
      this.setState({
        showPreloader: false,
        inputValues: {
          email: '',
          phone: '',
          name: '',
          surname: '',
          vacancy: '',
          cv: null
        },
        errorFields: [],
        fileName: null
      })
    }
  }

  closeModal = (modal, open) => {
    this.props.dispatch(setSuccess(false));

    this.setState({
      inputValues: {
        email: '',
        phone: '',
        name: '',
        surname: '',
        vacancy: '',
        cv: ''
      }
    });

    this.props.closeModal(modal, open);
  };

  inputHandler = ({target}) => {
    if(this.state.errorFields.length > 0) {
      let errStr = this.state.errorFields.filter(err => err !== target.name);
      this.setState({ errorFields: errStr });
    }
      switch(target.name) {
          case 'email':
              if(target.name.length > 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.email = null;
                  this.setState({errorInput: errorInput});
              }
              break;
          case 'name':
              if(target.name.length > 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.name = null;
                  this.setState({errorInput: errorInput});
              }
          case 'surname':
              if(target.name.length > 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.surname = null;
                  this.setState({errorInput: errorInput});
              }
              break;
          case 'phone':
              if(target.name.length > 0) {
                  let errorInput = this.state.errorInput;
                  errorInput.phone = null;
                  this.setState({errorInput: errorInput});
              }
              break;
          default:
              break;
      }

    this.setState({ inputValues: {
        ...this.state.inputValues,
        [target.name]: target.value
      }})
  };

  selectHandler = (option) => {
      this.setState({ inputValues: {
              ...this.state.inputValues,
              vacancy: option
          }});
    if(this.state.errorFields.length > 0) {
      let errStr = this.state.errorFields.filter(err => err !== 'vacancy');
      this.setState({ errorFields: errStr });
        let errorInput = this.state.errorInput;
        errorInput.vacancy = null;
        this.setState({errorInput: errorInput});
    }

  };

  fileInputHandler = (e) => {
    e.preventDefault();

    this.setState({ inputValues: {
        ...this.state.inputValues,
        cv: e.target.files[0]
      },
      fileName: e.target.files[0].name
    })
  };

  formHandler = () => {
    const { inputValues } = this.state;
    let errorFields = [];

      for(let key in inputValues) {
          switch(key) {
              case 'vacancy':
                  if(!inputValues.vacancy) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.vacancy = 'Упс, забули вказати Вакансію';
                      this.setState({errorInput: errorInput});

                  }
                  break;
              case 'name':
                  if(!inputValues.name.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.name = 'Упс, забули вказати Ім`я';
                      this.setState({errorInput: errorInput});

                  }
                  break;
              case 'surname':
                  if(!inputValues.surname.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.surname = 'Упс, забули вказати Прізвище';
                      this.setState({errorInput: errorInput});

                  }
                  break;
              case 'phone':
                  if(!inputValues.phone.length) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.phone = 'Упс, перевірте Номер телефону';
                      this.setState({errorInput: errorInput});

                  }
                  break;
              case 'email':
                  if(!inputValues.email.length && !emailReg.test(inputValues[key])) {
                      errorFields.push(key);
                      let errorInput = this.state.errorInput;
                      errorInput.email = 'Упс, перевірте E-mail';
                      this.setState({errorInput: errorInput});

                  }
                  break;
              default:
                  break;
          }
      }

    this.setState({ errorFields });

    if(errorFields.length === 0) {

      this.props.dispatch(sendVacancyForSelectAsync({
        token: getCookie('csrftoken'),
        first_name: inputValues.name,
        last_name: inputValues.surname,
        vacancies_id: inputValues.vacancy.value,
        file: inputValues.cv,
        email: inputValues.email,
        phone: inputValues.phone,
        branch_id: this.props.cityId
      }));

      this.setState({ showPreloader: true });
    }

  };

  render() {
    const {
      show,
      vacanciesList,
      success
    } = this.props;

    const {
      inputValues,
      fileName,
      errorFields,
      showPreloader
    } = this.state;

    return (
      <ModalLayer
        show={show}
        closeModal={() => this.closeModal('career', false)}
      >
        <Wrapper
          wrapPadding="0 10px 15px"
        >
          <TextBlock className="form_title" textMargin="-30px 0 5px">
            Кар`єра у нас
          </TextBlock>
          <Form>

            <Wrapper
              wrapMargin="30px 0"
              className={classNames("input_wrapper", "select_wrapper", { focused: inputValues.vacancy !== '' })}
            >
              <label className={classNames({ focused: inputValues.vacancy !== '' })}>
                Вакансія
              </label>
              <Select
                name="vacancy"
                value={inputValues.vacancy}
                searchable={false}
                clearable={false}
                multi={false}
                className={classNames("select_block", {error: errorFields.some((el) => !!RegExp(`^vacancy$`).test(el)) })}
                placeholder=""
                onChange={this.selectHandler}
                options={vacanciesList && vacanciesList.map(el => ({ value: el.id, label: el.title }))}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.vacancy? this.state.errorInput.vacancy: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.name !== '' })}>
                Ім`я
              </label>
              <Input
                type="text"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^name$`).test(el)) })}
                name="name"
                value={inputValues.name}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.name? this.state.errorInput.name: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.surname !== '' })}>
                Прізвище
              </label>
              <Input
                type="text"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^surname$`).test(el)) })}
                name="surname"
                value={inputValues.surname}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.surname? this.state.errorInput.surname: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.phone !== '' })}>
                Номер телефону
              </label>
              <Input
                type="number"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^phone$`).test(el)) })}
                name="phone"
                value={inputValues.phone}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.phone? this.state.errorInput.phone: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="input_wrapper"
            >
              <label className={classNames({ focused: inputValues.email !== '' })}>
                E-mail
              </label>
              <Input
                type="text"
                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                name="email"
                value={inputValues.email}
                onChange={this.inputHandler}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              className="file_input_wrapper"
              wrapItems="flex-start"
            >
              <label className="file_input_label" for="cv" >
                <Image src='/static/img/file.png' imgWidth={17} imgHeight={23} imgMargin="0 15px 0 0" />
                {fileName ? fileName : 'Прикріпити резюме'}
              </label>
              <Input
                type="file"
                className="file_input"
                name="cv"
                id="cv"
                onChange={(e) => this.fileInputHandler(e)}
              />
            </Wrapper>
              <p className='inputError'>{this.state.errorInput.cv? this.state.errorInput.cv: null}</p>
            <Wrapper
              wrapMargin="30px 0"
              wrapItems="center"
              wrapDirection="row"
              className="form_btn_wrapper"
            >
              <Button
                theme={orangeButton}
                onClick={this.formHandler}
              >
                Відправити
              </Button>
              {
                success &&
                <TextBlock color="#19bd19" textMargin="5px 5px 5px 20px" bold >
                  Дякуємо, Ваша заява прийнята!
                </TextBlock>
              }
            </Wrapper>
          </Form>
        </Wrapper>

        <PreloaderDiv show={showPreloader} />

      </ModalLayer>
    )
  }
}

export const CareerModal = connect(({ state }) => ({
  cityId: state.cityId,
  success: state.success,
  vacanciesList: state.vacanciesList
}))(CareerModalComponent);