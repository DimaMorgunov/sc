import classNames from 'classnames';
import { connect } from 'react-redux';
import {
    Button,
    Form,
    Input,
    ModalLayer,
    PreloaderDiv,
    TextBlock,
    Wrapper
} from "../../components";
import {orangeButton} from "../../themes";


import 'react-select-v1/dist/react-select.css';

import {
    changePasswordAsync,
    getUserInfoAsync,
    setError,
    setSuccess
} from "../../store";

export class UserEditPassComponent extends Component {
    state = {
        inputValues: {
            password1: '',
            password2: '',
        },
        errorInput:{
            password1: '',
            password2: '',
        },
        showPreloader: false,
        errorFields: [],
        error: this.props.error
    };

    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false,
            });
        }
        if (!this.props.userInfo) {
            this.props.dispatch(getUserInfoAsync({
                token: this.props.token,
                userToken: this.props.userToken
            }));
        }

    }

    componentWillReceiveProps (nextProps) {
        if(this.props.error != nextProps.error){
            this.setState({
                error: nextProps.error
            })
        }
    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
            switch(target.name) {
                case 'password1':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'password2':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }
        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    closeModal = (modal, open, e) => {
        this.setState({
            inputValues: {
                password1: '',
                password2: '',
            },
            errorInput:{
                password1: '',
                password2: '',
            },
            showPreloader: false,
            errorFields: [],
        });

        this.props.dispatch(setError(null));
        this.props.closeModal(modal, open, e);
    };

    formHandler = () => {
        const { inputValues } = this.state;
        let errorFields = [];

        for(let key in inputValues) {
            switch(key) {
                case 'password1':
                    if(!inputValues.password1.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.password1 = 'Упс, забули вказати Код';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'password2':
                    if(!inputValues.password2.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.password2 = 'Упс, забули вказати Код';
                        this.setState({errorInput: errorInput});
                    }
                    if(inputValues.password1 !== inputValues.password2) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.password2 = 'Упс, забули вказати Код';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.setState({
                showPreloader: true,
            });
            this.props.dispatch(setError(null));
            this.props.dispatch(changePasswordAsync({
                token: this.props.token,
                userToken: this.props.userToken,
                password1: this.state.inputValues.password1,
                password2: this.state.inputValues.password2
            }));

        }
    };

    render() {
        const { show } = this.props;
        const {
            inputValues,
            errorFields,
            showPreloader,
            error
        } = this.state;

        return (
            <ModalLayer
                show={show}
                closeModal={(e) => this.closeModal('editPass', false, e)}
            >
                <Wrapper
                    wrapPadding="0 10px 15px"
                >
                    <TextBlock className="form_title">
                        Змінити пароль
                    </TextBlock>
                    <Form>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", { focused: inputValues.password1 !== '' } )}
                        >
                            <label className={classNames({ focused: inputValues.password1 !== '' })}>
                                Придумайте пароль
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password1$`).test(el)) })}
                                name="password1"
                                value={inputValues.password1}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.password1? this.state.errorInput.password1: null}</p>
                        <Wrapper
                            wrapMargin="30px 0"
                            className={classNames("input_wrapper", { focused: inputValues.password2 !== '' } )}
                        >
                            <label className={classNames({ focused: inputValues.password2 !== '' })}>
                                Підтвердіть пароль
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password2$`).test(el)) })}
                                name="password2"
                                value={inputValues.password2}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.password2? this.state.errorInput.password2: null}</p>
                        <Wrapper
                            wrapMargin="30px 0 10px"
                            wrapItems="flex-start"
                            className="form_btn_wrapper"
                        >
                            <Button
                                theme={orangeButton}
                                onClick={this.formHandler}
                            >
                                Зареєструватися
                            </Button>
                            {
                                error &&
                                <TextBlock color="red" textMargin="10px 0" bold >
                                    {this.state.error.detail}
                                </TextBlock>
                            }
                        </Wrapper>
                    </Form>
                </Wrapper>
                <PreloaderDiv show={showPreloader} />
            </ModalLayer>
        )
    }
}

export const UserEditPassModal = connect(({ state }) => ({
    token: state.token,
    userToken: state.userToken,
    error: state.error,
    success: state.success
}))(UserEditPassComponent);