import { connect } from 'react-redux';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import queryString from 'query-string';

import {
    Title,
    Wrapper,
    Image,
    Label,
    ImageBlock,
    Button, TextBlock, Span, ModalLayer, Input, CheckboxDiv, FormButton, PreloaderDiv
} from '../../components';

import {
    AgeModalLayer,
    SelectModalLayer,
    SignInModal
} from './index';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/uk';
import {
    getFilmSeatsAsync,
    clearFilmReservationAsync,
    setFilmReservationAsyncMobile,
    updateFilmReservationAsync,
    sendEmailAsync, setError, setFilmReservationAsync
} from "../../store";
import calendar from "../../img/calendar.png";
import { orangeButton} from "../../themes";
import clock from "../../img/clock.png";
import {Circle, Layer, Rect, Stage, Tag, Text} from "react-konva";
import {getCookie} from "../../consts";
import close from "../../img/close.png";
import check from "../../img/check.png";

/*http://localhost:8000/reservation/mobile/get/?session_id=5216*/

export class BuyTicketMobileComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: this.props.token ? this.props.token : this.getCookie('csrftoken'),
            startDate: moment(),
            scaleX: null,
            scaleY: null,
            canvasWrapWidth: 950,
            circlesSeats: null,
            tickets: 0,
            ticketPrice: 0,
            chosenSeats: [],
            focused: {
                email: false
            },
            inputValues: {
                email: ''
            },
            buyModal: false,
            terms: true,
            session_id: null,
            showPreloader: true,
            timer: '15:00',
            filmData: null,
            userToken: null,
            pay_way: null
        }
    }

    getCookie(name) {
        let cookieValue = null;

        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                if (cookie.substring(0, name.length + 1) === (`${name}=`)) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                }
            }
        }
        return cookieValue;
    }

    componentDidMount() {

        const queryString = require('query-string');
        const parsed = queryString.parse(location.search);
        const pay_way = this.props.match.params.params
        this.props.dispatch((getFilmSeatsAsync({ token: this.state.token, id: parsed.session_id })))
        this.setState({
            session_id: parsed.session_id,
            userToken: parsed.user,
            pay_way: pay_way
        })

    }


    startTimer = () => {

        this.setState({
            timer: this.state.timer
        });

        this.timer = setInterval(() => {
            let time = this.state.startTime - Date.now();
            let timerTime = `${new Date(time).getMinutes()}:${new Date(time).getSeconds() < 10 ?
                '0' + new Date(time).getSeconds() :
                new Date(time).getSeconds()}`;
            if(timerTime === '59:59') {
                clearInterval(this.timer);
            } else {
                this.setState({
                    timer: timerTime
                })
            }
        }, 1000);
    };

    createPlaceField = ( data ) =>{
        let date = new Date();
        let startTime = new Date(date.getTime() + 15 * 60000);


        const seatsData = data;

        let circlesArr = [];
        if(seatsData) {
            for(let key in seatsData.row_list) {
                circlesArr[key] = [];
                let value = seatsData.row_list[key];


                for(let i = 1; i <= value.length; i++) {

                    if(value.place_type === 1) {
                        if(i === 1) {
                            circlesArr[key].push({
                                [`x_${i}`]: value.x_position,
                                y: value.y_position,
                                type: value.place_type,
                                except: value.except_place.some((seat) => seat === i)
                            })
                        } else {

                            circlesArr[key].push({
                                [`x_${i}`]: seatsData.row_numbering_direction === "ltr" ?
                                    circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] + 30 :
                                    circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] - 30,
                                y: value.y_position,
                                type: value.place_type,
                                except: value.except_place.some((seat) => seat === i)
                            })
                        }
                    } else {
                        if(i === 1) {
                            circlesArr[key].push({
                                [`x_${i}`]: value.x_position,
                                y: value.y_position,
                                type: value.place_type,
                                except: value.except_place.some((seat) => seat === i)
                            })
                        } else {

                            circlesArr[key].push({
                                [`x_${i}`]: seatsData.row_numbering_direction === "ltr" ?
                                    circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] + 80 :
                                    circlesArr[key].filter(el => el[`x_${i-1}`])[0][`x_${i-1}`] - 80,
                                y: value.y_position,
                                type: value.place_type,
                                except: value.except_place.some((seat) => seat === i)
                            })
                        }
                    }
                }
            }

            const windiwWidth = window.innerWidth;
            if (window.innerWidth > 300){
                this.setState({
                    scaleX: 0.40,
                    scaleY: 0.40,
                    canvasWrapWidth: 300 ,
                })
            }
            if (window.innerWidth > 400){
                this.setState({
                    scaleX: 0.5,
                    scaleY: 0.5,
                    canvasWrapWidth: 400 ,
                })
            }
            if (window.innerWidth > 500){
                this.setState({
                    scaleX: 0.6,
                    scaleY: 0.6,
                    canvasWrapWidth: 500 ,
                })
            }
            if (window.innerWidth > 800){
                this.setState({
                    scaleX: 0.7,
                    scaleY: 0.7,
                    canvasWrapWidth: 600 ,
                })
            }
            if (window.innerWidth > 1000){
                this.setState({
                    scaleX: 1,
                    scaleY: 1,
                    canvasWrapWidth: 950 ,
                })
            }
            this.setState({
                circlesSeats: circlesArr,
                textLeft: seatsData.left_text_position,
                textRight: seatsData.right_text_position,
                canvasHeight: seatsData.hall_height,
                startTime,
                windiwWidth: windiwWidth
            });
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.reservation !== this.props.reservation) {
            this.setState({
                showPreloader: false,

            });
        }
        if(prevProps.filmSeats !== this.props.filmSeats){

                this.setState({
                    showPreloader: false
                })
            if(this.props.filmSeats){
                this.createPlaceField(this.props.filmSeats)
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    chosenSeat = (row, seat, price, name, el) => {
        if(el.except) return;
        let tickets = this.state.tickets;
        let ticketPrice = this.state.ticketPrice;

        let seats = this.state.chosenSeats;
        let seatsObj = {row: row, place: seat+1, type: 1};

        if(this.refs[`${name}`].attrs.fill === "#00a6ea") {
            seats.push(seatsObj);

            this.setState({
                chosen: !this.state.chosen,
                [`color_${name}`]: "#fd5f00",
                tickets: tickets + 1,
                ticketPrice: ticketPrice + price,
                chosenSeats: seats
            })
        } else {
            seats = seats.filter(seat => JSON.stringify(seat) !== JSON.stringify(seatsObj));

            this.setState({
                chosen: !this.state.chosen,
                [`color_${name}`]: "#00a6ea",
                tickets: tickets - 1,
                ticketPrice: ticketPrice - price,
                chosenSeats: seats
            })
        }
    };

    chosenBigSeat = (row, seat, price, name, el) => {

        if(el.except) return;
        let tickets = this.state.tickets;
        let ticketPrice = this.state.ticketPrice;

        let seats = this.state.chosenSeats;
        let seatsObj = {row: row, place: seat+1, type: 2};

        if(this.refs[`${name}`].attrs.fill === "#4bb143") {
            seats.push(seatsObj);

            this.setState({
                chosen: !this.state.chosen,
                [`color_${name}`]: "#fd5f00",
                tickets: tickets + 1,
                ticketPrice: ticketPrice + price,
                chosenSeats: seats
            })
        } else {
            seats = seats.filter(seat => JSON.stringify(seat) !== JSON.stringify(seatsObj));

            this.setState({
                chosen: !this.state.chosen,
                [`color_${name}`]: "#4bb143",
                tickets: tickets - 1,
                ticketPrice: ticketPrice - price,
                chosenSeats: seats
            })
        }
    };

    selectModalBuy = () => {
        this.startTimer();
        this.props.dispatch(setFilmReservationAsync({
            token: this.state.token,
            userToken: this.state.userToken,
            id: this.state.session_id,
            pay_way: this.state.pay_way,
            places: this.state.chosenSeats
        }));

        this.setState({ showPreloader: true })
        this.toggleDrawer('buyModal', true)

    };

    toggleDrawer = (side, open) => {
        this.setState({
            [side]: open
        });
        this.props.dispatch(setError(null));
        document.body.classList.toggle('modal_opened');
    };

    backToSelectModal = () => {
        this.props.dispatch(clearFilmReservationAsync({
            token: this.state.token,
            id: this.props.reservation.reservation_id
        }));

        this.setState({
            buyModal: false,
            show: false
        });

    };

    inputHandler = ({target}) => {
        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    deleteSeat = (ticket, price) => {

        let seats = this.state.chosenSeats;
        seats = seats.filter(seat => JSON.stringify(seat) !== JSON.stringify(ticket));

        this.props.dispatch(updateFilmReservationAsync({
            token: this.state.token,
            id: this.props.reservation.reservation_id,
            places: seats
        }));

        this.setState({
            chosenSeats: seats,
            tickets: this.state.tickets - 1,
            ticketPrice: this.state.ticketPrice - price,
            [`color_${ticket.row}_${ticket.place - 1}`]: ticket.type === 1 ? "#00a6ea" : "#4bb143",
            showPreloader: true
        });
    };

    checkboxHandler = () => {
        this.setState({ terms: !this.state.terms });
    };

    submitForm = () => {
        this.props.dispatch(sendEmailAsync({
            token: this.state.token,
            id: this.props.reservation.reservation_id,
            email: this.props.reservation.email
        }));
        this.form.setAttribute("action", "https://www.liqpay.ua/api/3/checkout");
    };

    render() {
        const {
            reservation,
        } = this.props;

        const {
            circlesSeats,
            textLeft,
            textRight,
            canvasHeight,
            tickets,
            ticketPrice,
            chosenSeats,
            show,
            buyModal,
            focused,
            inputValues,
            terms,
            showPreloader,
            timer
        } = this.state;

        return (
            <React.Fragment>
                {this.state.circlesSeats?
                    <Wrapper
                        wrapMargin="55px 0 20px"
                        wrapPadding="0 60px"
                        wrapItems="flex-start"
                        className="smaller_padding"
                    >
                        <Wrapper
                            className="modal_inside"
                            wrapItems="flex-start"
                            wrapPadding="15px"
                            wrapMinWidth={320}
                        >
                            <Wrapper
                                className={this.state.windiwWidth? this.state.windiwWidth < 960? "buytickmodal" : null : null}
                                wrapDirection="row"
                                wrapItems="flex-start"
                                wrapMargin="-35px 0 0 0"
                                wrapMinWidth={320}
                            >
                                <Title blue fontSize="25px">
                                    {this.props.filmSeats.movie.title? this.props.filmSeats.movie.title: null}
                                    <Label className={classNames("block_label", "white_label")}>{this.props.filmSeats.movie.format}</Label>
                                </Title>
                            </Wrapper>
                            <TextBlock>
                                {this.props.filmSeats.movie.hall}
                            </TextBlock>
                            <Wrapper
                                wrapDirection="row"
                                wrapMargin="20px 0"
                                wrapContent="space-between"
                                wrapMinWidth="100%"
                                className="buyTicketWrap"
                            >
                                <Wrapper
                                    wrapDirection="row"
                                    wrapMinWidth="40%"
                                    wrapMaxWidth="40%"
                                    className="buyTicketWrapTime"
                                >
                                    <Wrapper
                                        wrapMaxWidth="150px"
                                        wrapMinWidth="150px"
                                        className="wrapClockBuyModal"
                                    >
                                        <Image
                                            src={calendar}
                                            imgHeight={22}
                                            imgWidth={20}
                                            imgMargin="0 15px 0 0"
                                        />
                                        <DatePicker
                                            selected={moment(this.props.filmSeats.movie.start, 'YYYY-MM-DD')}
                                            className={classNames("schedule_calendar", "modal_input")}
                                            readOnly={true}
                                            locale="uk"
                                        />
                                    </Wrapper>
                                    <Wrapper
                                        wrapMaxWidth="200px"
                                        wrapMinWidth="140px"
                                        className="wrapClockBuyModal"
                                    >
                                        <Image
                                            src={clock}
                                            imgHeight={23}
                                            imgWidth={23}
                                            imgMargin="0 15px 0 0"
                                        />
                                        <Span font="16px">{`${new Date(this.props.filmSeats.movie.start).getHours()}:${new Date(this.props.filmSeats.movie.start).getMinutes() < 10 ?
                                            '0' + new Date(this.props.filmSeats.movie.start).getMinutes() :
                                            new Date(this.props.filmSeats.movie.start).getMinutes()}`}</Span>
                                    </Wrapper>
                                </Wrapper>
                                <Wrapper
                                    wrapDirection="row"
                                    wrapMinWidth="60%"
                                    wrapMaxWidth="60%"
                                    wrapContent="flex-end"
                                    className="buyTicketWrapPlace"
                                >
                                    <Wrapper
                                        wrapContent="center"
                                        wrapMaxWidth="200px"
                                        wrapMinWidth="100px"
                                        className="wrapClockBuyModal"
                                    >
                                        <Label className="price_from"/>
                                        <Span>{this.props.filmSeats.movie.standard_price} грн</Span>
                                    </Wrapper>
                                    <Wrapper
                                        wrapContent="center"
                                        wrapMaxWidth="200px"
                                        wrapMinWidth="100px"
                                        className="wrapClockBuyModal"
                                    >
                                        <Label className="price_to"/>
                                        <Span>{this.props.filmSeats.movie.vip_price} грн</Span>
                                    </Wrapper>

                                    <Wrapper
                                        wrapContent="center"
                                        wrapMaxWidth="200px"
                                        wrapMinWidth="100px"
                                        className="wrapClockBuyModal"
                                    >
                                        <Label className="price_sold"/>
                                        <Span>Вже продано</Span>
                                    </Wrapper>

                                </Wrapper>
                            </Wrapper>
                        </Wrapper>

                        <Wrapper
                            className="seats_map"
                            wrapPadding="15px 0"
                        >
                            <Image
                                src={this.props.filmSeats.movie.detail_background}
                                imgWidth={490}
                                imgHeight={90}
                                className="canvas_image"
                                imgMargin="20px 0"
                            />
                            {
                                circlesSeats ?
                                    <Stage className='canvasWrapWidth' width={this.state.canvasWrapWidth} height={canvasHeight} scaleY={this.state.scaleY} scaleX={this.state.scaleX}>
                                        {
                                            circlesSeats.map((circle, g) => {
                                                return (
                                                    <Layer key={`${g}_${circle[0].y - 8}`}>
                                                        <Text
                                                            text={g}
                                                            fontSize={15}
                                                            x={textLeft - 8}
                                                            y={circle[0].y - 8}
                                                            fill="#fff"
                                                        />
                                                        {
                                                            circle.map((el, i) => {
                                                                if(el.type === 1) {
                                                                    return (
                                                                        <React.Fragment>
                                                                            {this.state.windiwWidth > 800?
                                                                                <Circle
                                                                                    key={`${g}_${i}`}
                                                                                    x={el[`x_${i + 1}`]}
                                                                                    y={el.y}
                                                                                    radius={11}
                                                                                    scaleX={el.except ? 0.4 : 1}
                                                                                    scaleY={el.except ? 0.4 : 1}
                                                                                    fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#00a6ea"}
                                                                                    ref={`${g}_${i}`}
                                                                                    onClick ={() => this.chosenSeat(g, i, this.props.filmSeats.movie.standard_price, `${g}_${i}`, el)}
                                                                                />
                                                                                :
                                                                                <Circle
                                                                                    key={`${g}_${i}`}
                                                                                    x={el[`x_${i + 1}`]}
                                                                                    y={el.y}
                                                                                    radius={11}
                                                                                    scaleX={el.except ? 0.4 : 1}
                                                                                    scaleY={el.except ? 0.4 : 1}
                                                                                    fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#00a6ea"}
                                                                                    ref={`${g}_${i}`}
                                                                                    onTouchStart ={() => this.chosenSeat(g, i, this.props.filmSeats.movie.standard_price, `${g}_${i}`, el)}
                                                                                />
                                                                            }

                                                                        </React.Fragment>
                                                                    )
                                                                } else {
                                                                    return (
                                                                        <React.Fragment>
                                                                            {this.state.windiwWidth > 800?
                                                                                <Rect
                                                                                    key={`${g}_${i}`}
                                                                                    x={el[`x_${i + 1}`]}
                                                                                    y={el.y - 10}
                                                                                    cornerRadius={12}
                                                                                    width={54}
                                                                                    height={22}
                                                                                    fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#4bb143"}
                                                                                    ref={`${g}_${i}`}
                                                                                    onClick={() => this.chosenBigSeat(g, i, this.props.filmSeats.movie.vip_price, `${g}_${i}`, el)}
                                                                                />
                                                                                :
                                                                                <Rect
                                                                                    key={`${g}_${i}`}
                                                                                    x={el[`x_${i + 1}`]}
                                                                                    y={el.y - 10}
                                                                                    cornerRadius={12}
                                                                                    width={54}
                                                                                    height={22}
                                                                                    fill={el.except ? "#8f8f8f" : this.state[`color_${g}_${i}`] ? this.state[`color_${g}_${i}`] : "#4bb143"}
                                                                                    ref={`${g}_${i}`}
                                                                                    onTouchStart={() => this.chosenBigSeat(g, i, this.props.filmSeats.movie.vip_price, `${g}_${i}`, el)}
                                                                                />
                                                                            }

                                                                        </React.Fragment>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                        <Text
                                                            key={`${g}_${circle[0].y}`}
                                                            text={g}
                                                            fontSize={15}
                                                            x={textRight}
                                                            y={circle[0].y - 8}
                                                            fill="#fff"
                                                        />
                                                    </Layer>
                                                )
                                            })
                                        }
                                    </Stage>
                                    :
                                    <TextBlock color="red" textMargin="10px 0" bold >
                                        Упс, вибачте, щось пішло не так :(
                                    </TextBlock>
                            }
                        </Wrapper>

                        <Wrapper
                            wrapDirection="row"
                            wrapPadding="15px"
                            wrapContent="space-between"
                        >
                            <TextBlock>
                                {tickets} {tickets === 1 ? 'квиток' : tickets >= 2 && tickets <= 4 ? 'квитка' : 'квитків'}, {ticketPrice} грн
                            </TextBlock>
                            <Button
                                className={classNames({ "disabled": tickets === 0 })}
                                theme={orangeButton}
                                onClick={tickets === 0 ? null : () => this.selectModalBuy()}
                            >
                                Купити
                            </Button>
                        </Wrapper>
                        <ModalLayer
                            show={buyModal}
                            closeModal={this.backToSelectModal}
                            selectModal
                        >
                            <Wrapper
                                className="modal_inside"
                                wrapItems="flex-start"
                                wrapPadding="15px 30px"
                            >
                                <Wrapper
                                    wrapDirection="row"
                                    wrapItems="flex-start"
                                >
                                    <Title blue fontSize="25px">
                                        {this.props.filmSeats.movie.title? this.props.filmSeats.movie.title: null}
                                        <Label className={classNames("block_label", "white_label")}>{this.props.filmSeats.movie.format}</Label>
                                    </Title>
                                </Wrapper>
                                <TextBlock>
                                    {this.props.filmSeats.movie.hall}
                                </TextBlock>
                                <Wrapper
                                    wrapDirection="row"
                                    wrapMargin="20px 0"
                                    wrapContent="space-between"
                                >
                                    <Wrapper
                                        wrapDirection="row"
                                        wrapMinWidth="40%"
                                        wrapMaxWidth="40%"
                                    >
                                        <Image
                                            src={calendar}
                                            imgHeight={22}
                                            imgWidth={20}
                                            imgMargin="0 15px 0 0"
                                        />
                                        <DatePicker
                                            selected={moment(this.props.filmSeats.movie.start, 'YYYY-MM-DD')}
                                            className={classNames("schedule_calendar", "modal_input")}
                                            readOnly={true}
                                            locale="uk"
                                        />
                                        <Image
                                            src={clock}
                                            imgHeight={23}
                                            imgWidth={23}
                                            imgMargin="0 15px 0 0"
                                        />
                                        <Span font="16px">{`${new Date(this.props.filmSeats.movie.start).getHours()}:${new Date(this.props.filmSeats.movie.start).getMinutes() < 10 ?
                                            '0' + new Date(this.props.filmSeats.movie.start).getMinutes() :
                                            new Date(this.props.filmSeats.movie.start).getMinutes()}`}</Span>
                                    </Wrapper>

                                    <Wrapper
                                        wrapDirection="row"
                                        wrapMinWidth="60%"
                                        wrapMaxWidth="60%"
                                        wrapContent="flex-end"
                                    >
                                        <Label className="timer">
                                            {
                                                timer === '0:00' ?
                                                    'Вашу бронь анульовано' :
                                                    `Залишилось ${timer}`
                                            }

                                        </Label>
                                    </Wrapper>
                                </Wrapper>
                            </Wrapper>

                            <Wrapper
                                wrapDirection="row"
                                wrapItems="flex-start"
                                className="seats_map"
                                wrapPadding="15px 30px"
                            >
                                <Wrapper
                                    className={classNames("tickets_first", "modalWidth")}
                                >
                                    <Wrapper
                                        wrapItems="flex-start"
                                        className={classNames("tickets_block", { small: chosenSeats.length <= 3 })}
                                    >
                                        {
                                            chosenSeats.map(ticket => {
                                                return (
                                                    <Wrapper
                                                        wrapDirection="row"
                                                        wrapContent="space-between"
                                                        wrapMargin="20px 0"
                                                    >
                                                        <Label className={classNames("tickets", "transparent", { small: chosenSeats.length > 3 })}>
                                                            <Span className="ticket">{ticket.row} <br/> ряд</Span>
                                                            <Span className="ticket">{ticket.place} <br/> місце</Span>
                                                            {
                                                                ticket.type === 1 ?
                                                                    <Span className="ticket">
                                                                        {this.props.filmSeats.movie.standard_price} <br/> грн
                                                                      </Span>
                                                                    :
                                                                    <Span className="ticket">
                                                                    {this.props.filmSeats.movie.vip_price     } <br/> грн
                                                                  </Span>
                                                            }

                                                        </Label>
                                                        <Image
                                                            src={close}
                                                            imgHeight={21}
                                                            imgWidth={21}
                                                            onClick={() => this.deleteSeat(ticket, ticket.type === 1 ? this.props.filmSeats.movie.standard_price : this.props.movie.vip_price)}
                                                            className="close_modal"
                                                            imgMargin={ chosenSeats.length > 3 ? "15px 30px 15px 0" : "15px 40px 15px 0"}
                                                        />
                                                    </Wrapper>
                                                )
                                            })
                                        }
                                    </Wrapper>
                                    <Wrapper
                                        wrapItems="flex-start"
                                        wrapMargin="10px 0"
                                    >
                                        <Label blue className="tickets">
                                          <Span className="ticket_span">
                                            {tickets} {tickets === 1 ? 'квиток' : tickets >= 2 && tickets <= 4 ? 'квитка' : 'квитків'}
                                            </Span>
                                          <Span className="ticket_span">
                                            {ticketPrice} грн
                                          </Span>
                                        </Label>
                                    </Wrapper>
                                </Wrapper>

                                <Wrapper
                                    wrapItems="flex-start"
                                    /*wrapMinWidth="50%"
                                    wrapMaxWidth="50%"*/
                                    className={classNames("modalWidth")}
                                    wrapPadding="10px 0 10px 30px"
                                >
                                    <TextBlock>
                                        Ваше замовлення буде надіслано вам на електронну пошту та доданно в особистому кабінеті.
                                    </TextBlock>
                                    {/*<Wrapper
                                        wrapMargin="30px 0 0 0"
                                        className={classNames( "input_wrapper", "input_on_dark" )}
                                    >
                                        <label className={classNames({ focused: inputValues.email === '' ? focused.email : true })}>
                                            E-mail
                                        </label>
                                        <Input
                                            type="text"
                                            className="input"
                                            name="email"
                                            value={inputValues.email}
                                            labelClass={focused}
                                            onChange={this.inputHandler}
                                        />
                                    </Wrapper>*/}
                                </Wrapper>
                            </Wrapper>

                            <Wrapper
                                wrapDirection="row"
                                wrapPadding="15px"
                                wrapContent="space-between"
                            >
                                <Wrapper
                                    wrapDirection="row"
                                    wrapItems="center"
                                    wrapMargin="10px 0"
                                    wrapMinWidth="70%"
                                    wrapMaxWidth="70%"
                                    className="checkbox_div"
                                >
                                    <CheckboxDiv
                                        src={check}
                                        className={classNames({checked: terms === true})}
                                        onClick={() => this.checkboxHandler()}
                                    />
                                    <TextBlock lineHeight="24px" textMaxWidth="70%">
                                        Ви погоджуєтеся з <a className="pdf_link" href=" /media/pdf/purchase_rules.pdf" target="_blank"> умовами та правилами on-line
                                        покупки квитків </a>
                                    </TextBlock>
                                </Wrapper>

                                {
                                    tickets !== 0 && terms === true ?
                                        <form ref={el => this.form = el} onSubmit={this.submitForm} method="POST" acceptCharset="utf-8">
                                            <input type="hidden" name="data" value={reservation && reservation.result.data} />
                                            <input type="hidden" name="signature" value={reservation && reservation.result.signature} />
                                            <FormButton
                                                type="submit"
                                                theme={orangeButton}
                                            >
                                                Перейти до сплати
                                            </FormButton>
                                        </form> :
                                        <Button
                                            className="disabled"
                                            theme={orangeButton}
                                        >
                                            Перейти до сплати
                                        </Button>
                                }

                            </Wrapper>
                        </ModalLayer>
                    </Wrapper>
                    :
                    <TextBlock lineHeight="24px" textMaxWidth="70%">
                    </TextBlock>
                }
                <PreloaderDiv show={showPreloader}/>
            </React.Fragment>
        )
    }
}
/*4e4bc5b4c12d33101839d5795218c1bfc9a9a420*/
export const BuyTicketMobile = connect(({ state }) => ({
    token: state.token,
    cityId: state.cityId,
    schedule: state.schedule,
    scheduleDates: state.scheduleDates,
    filmSeats: state.filmSeats,
    error: state.error,
    reservation: state.reservation
}))(BuyTicketMobileComponent);
