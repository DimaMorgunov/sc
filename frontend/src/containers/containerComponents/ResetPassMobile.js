import { connect } from 'react-redux';
import classNames from 'classnames';
import {
    Button,
    Form,
    Input,
    PreloaderDiv,
    TextBlock,
    Wrapper
} from "../../components";
import {orangeButton} from "../../themes";

import 'react-select-v1/dist/react-select.css';
import {resetPassFirstStep, setError} from "../../store";
import {SendPassModal} from "./SendPassModal";



export class ResetPassComponent extends Component {
    state = {
        inputValues: {
            email: '',
        },
        errorInput: {
            email: '',
        },
        sendpass: false,
        errorFields: [],
        showPreloader: false,
        error: false
    };

    componentDidMount() {
        this.setState({
            showPreloader: false,
            error: false,
            success: false,
            errorFields: [],
        })
    }

    toggleDrawer = (side, open) => {
        this.setState({
            [side]: open
        });
        this.props.dispatch(setError(null));
    };

    closeModal = () => {
        this.setState({
            sendpass: false
        });
    };

    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false,
                error: true
            })
        }

        if(prevProps.success !==
            this.props.success &&
            this.props.success &&
            this.props.success.detail ==
            "Password reset e-mail has been sent.") {

            this.closeModal('sendpass', true);
        }

    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
            switch(target.name) {
                case 'email':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.email = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    formHandler = () => {
        const { inputValues } = this.state;
        let errorFields = [];

        for(let key in inputValues) {
            switch(key) {
                case 'email':
                    if(!inputValues.email.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.email = 'Упс, перевірте E-mail';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.setState({ showPreloader: true });
            this.props.dispatch(setError(null));
            this.props.dispatch(resetPassFirstStep({
                email: inputValues.email,
                token: this.props.token
            }));

        }
    };

    render() {
        const {
            error,
            success
        } = this.props;

        const {
            inputValues,
            errorFields,
            showPreloader,
            sendpass
        } = this.state;

        return (
            <React.Fragment>
                <SendPassModal
                    show={sendpass}
                    closeModal={this.toggleDrawer}
                />
                <Wrapper
                    wrapPadding="100px 15px 15px"
                    className="resetPassModal"
                >
                    <TextBlock className="form_title" textMargin="-30px  5px 0">
                        Скинути пароль
                    </TextBlock>
                    <Form>
                        <Wrapper
                            wrapMargin="30px 0"
                            className="input_wrapper"
                        >
                            <label className={classNames({ focused: inputValues.email !== '' })}>
                                E-mail
                            </label>
                            <Input
                                type="text"
                                className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                                name="email"
                                value={inputValues.email}
                                onChange={this.inputHandler}
                            />
                        </Wrapper>
                        <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
                        <Wrapper
                            wrapMargin="30px 0"
                            wrapDirection="row"
                            className="form_btn_wrapper"
                        >
                            <Button
                                theme={orangeButton}
                                onClick={this.formHandler}
                            >
                                Відправити
                            </Button>
                            {
                                error &&
                                <TextBlock color="red" textMargin="10px 0" bold >
                                    Упс, щось пішло не так. Спробуйте ще раз
                                </TextBlock>
                            }
                        </Wrapper>
                    </Form>
                </Wrapper>
                <PreloaderDiv show={showPreloader} />
            </React.Fragment>
        )
    }
}


export const ResetPassMobile = (connect(({ state }) => ({
    success: state.success,
    error: state.error
}))(ResetPassComponent));
