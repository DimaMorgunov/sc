import { Link } from 'react-router-dom';
import classNames from 'classnames';
import {
  Wrapper,
  ImageBlock,
  Title,
  TextBlock
} from "../components";

export const NotFound = () => (
  <React.Fragment>
    <Wrapper
      wrapMargin="100px 0"
      wrapContent="center"
    >
      <Wrapper
        wrapPadding="70px 10px"
        className="not_found"
        wrapMaxWidth="90%"
        wrapMinWidth="80%"
      >
        <ImageBlock src=" /static/img/not-found.png" imgHeight={400} imgWidth={710} className="not_found_img" />
        <Title blue centered fontSize="45px" className={classNames("not_found_title", "title")}>
          СТорінка не знайдена :(
        </Title>
        <TextBlock className="not_found_block">
          Можливо сторінка була видалена або ніколи не існувала
          Спробуйте повернутися на <Link to="/">головну сторінку</Link>
        </TextBlock>
      </Wrapper>
    </Wrapper>
  </React.Fragment>
);
