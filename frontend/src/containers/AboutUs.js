import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  Span,
  TextBlock,
  Title,
  Wrapper,
  ImageBlock
} from '../components';
import ShareSliderWrapper from "../components/PresentationalComponents/ShareSliderWrapper";

import kh1 from '../img/kh-1.jpg';
import kh2 from '../img/kh-2.jpg';
import kh3 from '../img/kh-3.jpg';
import vin1 from '../img/vin-1.jpg';
import vin2 from '../img/vin-2.jpg';
import vin3 from '../img/vin-3.jpg';

const khImg = [kh1, kh2, kh3, kh1, kh2, kh3];
const vinImg = [vin1, vin2, vin3, vin1, vin2, vin3];

export class AboutUsComponent extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    const { cityId } = this.props;

    return (
      cityId &&
      cityId === 1 ?
        <React.Fragment>
          <Wrapper
            wrapMargin="55px 0"
            wrapPadding="0 60px"
            className="smaller_padding"
          >
            <Title centered fontSize="35px">
              Про нас
            </Title>
          </Wrapper>
          <ImageBlock
            imgMargin="0 auto"
            className="mobile_img"

            src={vin2}
          />
          <div className={classNames("big_slider", "share_slider", "not_for_mobile")} style={{width: '100%', margin: '0 0 0'}}>
            <ShareSliderWrapper aboutUsPage shares={vinImg} />
          </div>
          <Wrapper
            wrapMargin="50px auto 0"
            wrapDirection="row"
            wrapMinWidth="80%"
            wrapMaxWidth="90%"
            wrapContent="space-between"
            wrapItems="flex-start"
            className="about_us_block"
          >
            <Wrapper
              className={classNames("white_box", "about_us")}
              wrapMinWidth="45%"
              wrapMaxWidth="48%"
              wrapPadding="30px"
            >
              <TextBlock centered lineHeight="28px" color="#12161f">
                Основна концепція кінотеатру у Вінниці – невеликі затишні <Span className="textNowrap" color="#000000">зали,</Span> які переносять відвідувача в
                атмосферу домашнього комфорту.
                  <Span centered color="#00a6ea">В залах встановлені зручні крісла і дивани.</Span> Кінотеатр
                налічує <Span className="textNowrap" color="#000000"><Span centered color="#00a6ea">7 залів</Span>,</Span> що дозволяє запропонувати нашим глядачам широкий репертуар
                фільмів: <Span centered color="#00a6ea">від 10 картин одночасно в прокаті.</Span> Сеанси починаються
                кожні <Span centered color="#00a6ea">10-15 хвилин.</Span>
              </TextBlock>
            </Wrapper>
            <Wrapper
              className={classNames("white_box", "about_us")}
              wrapMinWidth="45%"
              wrapMaxWidth="48%"
              wrapPadding="30px"
            >
              <TextBlock centered lineHeight="28px" color="#12161f">
                Для кінопоказів використовується новітнє
                цифрове <Span centered color="#00a6ea">кінопроекційне обладнання</Span> провідного
                постачальника проекційних рішень компанії NEC.
                Звукове обладнання  компанії HK Audio - це <Span centered color="#00a6ea">збалансоване звучання</Span> та високий
                рівень SPL, що, безумовно, вражає.
              </TextBlock>
            </Wrapper>
            <Wrapper wrapMargin="30px 0 0">
              <TextBlock lineHeight="28px">
                Якісне 3D-зображення забезпечують сучасні екрани SilverScreen зі срібним покриттям компанії Harkness Hall
                Spectral 240 3D.  Унікальна композиція покриття забезпечує ідеальний баланс між максимальною яскравістю та
                розподілом світла, що дозволяє отримувати чіткі, динамічні та візуально видатні 3D-зображення, одночасно
                підтримуючи звичайний 2D-вміст.
              </TextBlock>
              <TextBlock lineHeight="28px">
                Всі ці технології у поєднанні зі звуковою системою Dolby Digital Cinema та Dolby Surround 5.1, дозволяють
                насолоджуватися фільмами з найбільш чітким звуком, найбільшою яскравістю картинки і найбільшим KPI.
              </TextBlock>
            </Wrapper>
            <Wrapper wrapMargin="30px 0">
              <a href="/schedule/">
              <Wrapper
                className="blue_box"
                wrapPadding="30px"
                wrapMinWidth="366px"
                wrapMaxWidth="465px"
                wrapContent="center"
              >
                <TextBlock centered lineHeight="28px">
                  Ми цінуємо персональний комфорт глядачів нашого кінотеатру, тому передбачили зручну систему купівлі
                  квитків онлайн, а також за допомогою нашого мобільного додатку на базі Android та  IOS.
                  <br/>
                  Купувати квитки в кіно зручно і легко!
                </TextBlock>
              </Wrapper>
              </a>
            </Wrapper>
          </Wrapper>
        </React.Fragment> :

        cityId === 2 ?
          <React.Fragment>
            <Wrapper
              wrapMargin="55px 0"
              wrapPadding="0 60px"
            >
              <Title centered fontSize="35px">
                Про нас
              </Title>
            </Wrapper>
            <div className={classNames("big_slider", "share_slider")} style={{width: '100%', margin: '0 0 60px'}}>
              <ShareSliderWrapper aboutUsPage shares={khImg}/>
            </div>
            <Wrapper
              wrapMargin="50px auto 0"
              wrapDirection="row"
              wrapMinWidth="80%"
              wrapMaxWidth="90%"
              wrapContent="space-between"
              wrapItems="flex-start"
            >
              <Wrapper
                className={classNames("white_box", "about_us")}
                wrapMinWidth="45%"
                wrapMaxWidth="48%"
                wrapPadding="30px"
              >
                <TextBlock centered lineHeight="28px" color="#12161f">
                  Ми зберегли основну концепцію нашого кінотеатру – невеликі <Span centered color="#00a6ea">затишні
                  зали</Span> та <Span centered color="#00a6ea">домашня атмосфера</Span>.
                  Але ми незмінно прагнемо розширити межі можливого, щоб вивести емоції, які переживають глядачі,
                  на абсолютно безпрецедентний рівень!
                  <br/>
                  То ж з гордістю представляємо вашій увазі проекційно-акустичну <Span centered color="#00a6ea">систему нового покоління</Span>!

                </TextBlock>
              </Wrapper>

              <Wrapper
                className={classNames("white_box", "about_us")}
                wrapMinWidth="45%"
                wrapMaxWidth="48%"
                wrapPadding="30px"
              >
                <TextBlock centered lineHeight="28px" color="#12161f">
                    Це перший у Хмельницькому зал з лазерним проектором та системою імерсивного звуку.
                  Нова, по-іншому обміркована концепція кінотеатру, яка відкриває перед
                  очима <Span centered color="#00a6ea">чітке, яскраве та насичене зображення</Span> у
                  поєднанні з кришталевим звуком, що виводить <Span centered color="#00a6ea">"ефект занурення"</Span> на новий рівень!
                </TextBlock>
              </Wrapper>
              <Wrapper wrapMargin="60px 0 0">
                <TextBlock lineHeight="28px">
                  Лазерний проектор NC1201L Digital Cinema є найновішим в лінійці компактних легких лазерів на базі 2K та
                  забезпечує роздільну здатність 2048 x 1080 та контрастність 1750: 1, що дозволяє проектувати зображення
                  в форматах 2D та 3D з неперевершеною яскравістю та найширшою кольоровою гамою.
                </TextBlock>
                <TextBlock lineHeight="28px">
                  Акустична система Lexicon BOB-32 та Lexicon's QLI-32, призначена для передачі до 32 каналів об'ємного
                  звучання відтворює звук ще більш потужно і точно, ніж раніше, посилюючи «ефект занурення», що дозволяє
                  ефективно позиціонувати звук, щоб кожне місце в кінозалі стало «найкращим».
                </TextBlock>
                <TextBlock lineHeight="28px">
                  Також ви маєте можливість насолодитися екстра-комфортом при перегляді фільмів у нашому VIP залі всього
                  на 20 місць, тобто 20 великих м`яких ергономічні крісел з USB-портом для вашого ґаджета, які розкладаються
                  до майже лежачого стану. Ви насправді відчуєте максимальне розслаблення та повне занурення в перегляд
                  новітньої кінострічки.
                </TextBlock>
              </Wrapper>
              <Wrapper wrapMargin="60px 0">
                  <a href="/schedule/">
                <Wrapper
                  className="blue_box"
                  wrapPadding="30px"
                  wrapMinWidth="366px"
                  wrapMaxWidth="465px"
                  wrapContent="center"
                >
                  <TextBlock centered lineHeight="28px">
                    Ми цінуємо персональний комфорт глядачів нашого кінотеатру, тому передбачили зручну систему купівлі
                    квитків онлайн, а також за допомогою нашого мобільного додатку на базі Android та  IOS.
                    <br/>
                    Купувати квитки в кіно зручно і легко!
                  </TextBlock>
                </Wrapper>
                  </a>
              </Wrapper>
            </Wrapper>
          </React.Fragment> :

          <Wrapper
            wrapContent="center"
            wrapMargin="30px 0"
          >
            <Title>
              Будь ласка, оберіть місто
            </Title>
          </Wrapper>
    )
  }
}

export const AboutUs = connect(({ state }) => ({ cityId: state.cityId }))(AboutUsComponent);
