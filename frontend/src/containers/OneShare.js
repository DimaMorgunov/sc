import { connect } from 'react-redux';
import { setCurrentShareAsync, setCurrentShare } from "../store";
import classNames from 'classnames';
import {
  Wrapper,
  TextBlock,
  Title, TitleH2,
  ImageBlock
} from "../components";
import ShareSliderWrapper from "../components/PresentationalComponents/ShareSliderWrapper";

export class OneShareComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    props.dispatch(setCurrentShareAsync({ token: this.getCookie('csrftoken'), url: props.location.pathname }));
  }

  getCookie(name) {
    let cookieValue = null;

    if (document.cookie && document.cookie !== '') {
      let cookies = document.cookie.split(';');
      for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i].trim();
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        }
      }
    }
    return cookieValue;
  };

  componentDidMount() {
    if(Object.keys(this.props.data).length > 0) {
      let shares = [];
      if(this.props.data.shares.length <= 5) {
        shares = [
          ...this.props.data.shares,
          ...this.props.data.shares,
          ...this.props.data.shares,
          ...this.props.data.shares,
          ...this.props.data.shares
        ];
      } else {
        shares = this.props.data.shares;
      }
      this.setState({ shares });
    }
  }

  componentDidUpdate(prevProps) {
    window.scrollTo(0, 0);

    if(prevProps.location.pathname !== this.props.location.pathname) {
      this.props.dispatch(
        setCurrentShareAsync({
          token: this.getCookie('csrftoken'),
          url: this.props.location.pathname
        }));
    }

    if(prevProps.data !== this.props.data) {

      let shares = [];
      if(this.props.data.shares.length < 5) {
        shares = [
          ...this.props.data.shares,
          ...this.props.data.shares,
          ...this.props.data.shares,
          ...this.props.data.shares,
          ...this.props.data.shares
        ];
      } else {
        shares = this.props.data.shares;
      }
      this.setState({ shares });
    }
  }

  componentWillUnmount() {
    this.props.dispatch(setCurrentShare(null));
  }

  render() {
    const { currentShare, data } = this.props;
    const { shares } = this.state;


    return (
      currentShare && Object.keys(data).length > 0 &&
      <React.Fragment>
        <Wrapper
          wrapMargin="55px 0"
          wrapPadding="0 60px"
          wrapItems="flex-start"
          className="smaller_padding"
        >
          <TextBlock color={ currentShare.type_of_post === 'Акція' ? '#fd5f00' : '#009ad7' }>
            {currentShare.type_of_post}
          </TextBlock>
          <Title fontSize="35px" className="title">
            {currentShare.title}
          </Title>

        </Wrapper>
        <div className="smaller_padding" style={{padding: '0 60px 20px 60px', width: '100%'}}>
          <ImageBlock src={currentShare.image} alt={currentShare.type_of_post + " " + currentShare.title} className="share_image"/>
          <TextBlock lineHeight="36px" font="19px" className="justified_text"
                     dangerouslySetInnerHTML={{__html: currentShare.description}}>
            {/*{{__html: currentShare.description}}*/}
          </TextBlock>
        </div>
        <Wrapper
          wrapItems="flex-start"
          wrapPadding="0 65px"
          wrapMargin="30px 0"
          className={classNames("smaller_padding", "not_for_mobile")}
        >
          <TitleH2 fontSize="35px">
            Інші новини та акції
          </TitleH2>
        </Wrapper>
        {
          shares &&
          <div className={classNames("big_slider", "share_slider", "not_for_mobile")} style={{width: '100%', marginBottom: '60px'}}>
            <ShareSliderWrapper shares={shares.filter(share => share.seo_inline.url !== this.props.location.pathname)}/>
          </div>
        }
      </React.Fragment>
    )
  }
}

export const OneShare = connect(({ state }) => ({
  currentShare: state.currentShare,
  data: state.data
}))(OneShareComponent);
