import { connect } from 'react-redux';
import classNames from 'classnames';
import {
  TextBlock,
  Title, TitleH2,
  Wrapper
} from '../components';
import ShareSliderWrapper from "../components/PresentationalComponents/ShareSliderWrapper";

import kh1 from '../img/kh-1.jpg';
import kh2 from '../img/kh-2.jpg';
import kh3 from '../img/kh-3.jpg';
import vin1 from '../img/vin-1.jpg';
import vin2 from '../img/vin-2.jpg';
import vin3 from '../img/vin-3.jpg';

const khImg = [kh1, kh2, kh3, kh1, kh2, kh3];
const vinImg = [vin1, vin2, vin3, vin1, vin2, vin3];

export class ContactsComponent extends Component {
  state = {
    location: null,
    error: null,
    show: false
  };

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    const { cityId } = this.props;

    return (
      cityId &&
      cityId === 1 ?
        <React.Fragment>
          <Wrapper
            wrapMargin="55px 0 20px"
            wrapPadding="0 60px"
            wrapItems="flex-start"
            className="smaller_padding"
          >
            <Title fontSize="35px" className="title">
              Контакти
            </Title>
          </Wrapper>

          <div style={{width: '100%', padding: '0 60px 50px 60px'}} className="smaller_padding" >
            <Wrapper
              wrapItems="flex-start"
              wrapMinWidth="40%"
              wrapMaxWidth="40%"
              wrapPadding="0 20px 0 0"
              className="floated_with_map"
            >
              <TextBlock textMargin="10px 0" lineHeight="28px">
                Кінотеатр «SmartCinema» розташований за адресою:
              </TextBlock>
              <TextBlock textMargin="0" lineHeight="28px">
                Україна, м.&nbsp;Вінниця, вул.&nbsp;М.&nbsp;Оводова, 51, ТЦ&nbsp;«Sky Рark», 4&nbsp;поверх
              </TextBlock>
              <TextBlock textMargin="0" lineHeight="28px">
                (096) 003 50 50
              </TextBlock>
              <TextBlock textMargin="0" lineHeight="28px">
                  admin_smartcinema@ukr.net
              </TextBlock>
              <TextBlock textMargin="10px 0" lineHeight="28px">
                Звертаємо вашу увагу, що кінотеатр «SmartCinema» працює щодня з 9:00 до 23:00
              </TextBlock>
            </Wrapper>

            <div className="map_wrapper">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2605.2536075841354!2d28.468870615125315!3d49.23368078216329!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472d5b64d58580bb%3A0x2a66b140dd77c66f!2sSmartCinema!5e0!3m2!1sen!2sua!4v1536409562661"
                width="60%" height="238" frameBorder="0" style={{ border: "0", borderRadius: '10px' }} allowFullScreen></iframe>
            </div>
          </div>

          <Wrapper
            wrapMargin="0 0 40px"
            wrapPadding="0 60px"
            wrapItems="flex-start"
            className="not_for_mobile"
          >
            <Title fontSize="35px">
              Галерея
            </Title>
          </Wrapper>
          <div className={classNames("big_slider", "not_for_mobile")} style={{width: '100%', margin: '0 0 60px'}}>
            <ShareSliderWrapper aboutUsPage shares={vinImg}/>
          </div>
        </React.Fragment> :
        cityId === 2 ?
          <React.Fragment>
            <Wrapper
              wrapMargin="55px 0 20px"
              wrapPadding="0 60px"
              wrapItems="flex-start"
              className="smaller_padding"
            >
              <Title fontSize="35px" className="title">
                Контакти
              </Title>
            </Wrapper>

            <div style={{width: '100%', padding: '0 60px 50px 60px'}} className="smaller_padding">
              <Wrapper
                wrapItems="flex-start"
                wrapMinWidth="40%"
                wrapMaxWidth="40%"
                wrapPadding="0 20px 0 0"
                className="floated_with_map"
              >
                <TextBlock textMargin="10px 0" lineHeight="28px">
                  Кінотеатр «SmartCinema» розташований за адресою:
                </TextBlock>
                <TextBlock textMargin="0" lineHeight="28px">
                  Україна, м.&nbsp;Хмельницький, вул.&nbsp;Трудова&nbsp;6&nbsp;А, ТЦ&nbsp;«WOODMALL», 2&nbsp;поверх
                </TextBlock>
                <TextBlock textMargin="0" lineHeight="28px">
                  (073) 002 50 50
                </TextBlock>
                <TextBlock textMargin="0" lineHeight="28px">
                  smartcinema.khmel@gmail.com
                </TextBlock>
                <TextBlock textMargin="10px 0" lineHeight="28px">
                  Звертаємо вашу увагу, що кінотеатр «SmartCinema» працює щодня з 9:00 до 23:00
                </TextBlock>
              </Wrapper>

              <div className="map_wrapper">
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2595.1322567415423!2d27.019056615133213!3d49.42531356857583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473207b6743f48e3%3A0xe9ea025910400754!2sSmartCinema!5e0!3m2!1sen!2sua!4v1536411922156"
                  width="60%" height="238" frameBorder="0" style={{ border: "0", borderRadius: '10px' }} allowFullScreen></iframe>
              </div>
            </div>

            <Wrapper
              wrapMargin="0 0 40px"
              wrapPadding="0 60px"
              wrapItems="flex-start"
            >
              <TitleH2 fontSize="35px">
                Галерея
              </TitleH2>
            </Wrapper>
            <div className="big_slider" style={{width: '100%', margin: '0 0 60px'}}>
              <ShareSliderWrapper aboutUsPage shares={khImg}/>
            </div>
          </React.Fragment> :

          <Wrapper
            wrapContent="center"
            wrapMargin="30px 0"
          >
            <Title>
              Будь ласка, оберіть місто
            </Title>
          </Wrapper>
    )
  }
}

export const Contacts = connect(({ state }) => ({ cityId: state.cityId }))(ContactsComponent);
