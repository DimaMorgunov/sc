import { connect } from 'react-redux';
import classNames from 'classnames';
import {
    Image, TextBlock,
    Title,
    TitleH2,
    TitleH3,
    Wrapper
} from "../components";

import scrollToComponent from 'react-scroll-to-component';
import MainSliderWrapper from "../components/PresentationalComponents/MainSliderWrapper";
import FilmSliderWrapper from '../components/PresentationalComponents/FilmSliderWrapper';
import loader from "../img/loader.gif";

export class MainComponent extends Component {
  state = {
    tabValue: 'rent',
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    if(Object.keys(this.props.data).length > 0) {

      let soon = [];
      let rent = [];
      if(this.props.data.soon.length <= 5) {
        soon = [...this.props.data.soon, ...this.props.data.soon, ...this.props.data.soon];
      } else {
        soon = this.props.data.soon;
      }
      if(this.props.data.rent.length <= 5) {
        rent = [...this.props.data.rent, ...this.props.data.rent, ...this.props.data.rent];
      } else {
        rent = this.props.data.rent;
      }

      this.setState({ soon, rent });
    }
  }

  scrollTo = () => {
    scrollToComponent(this.News, { offset: 0, align: 'top', duration: 0});
  }

  componentDidUpdate(prevProps) {

    if(prevProps.data !== this.props.data) {
        if(location.href.substring(location.href.length - 5, location.href.length) == '#news'){
          setTimeout(this.scrollTo, 500);
        }

      let soon = [];
      let rent = [];

      if(this.props.data.soon.length <= 5) {
        soon = [
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon
        ];
      } else {
        soon = this.props.data.soon;
      }
      if(this.props.data.rent.length <= 5) {
        rent = [
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent
        ];
      } else {
        rent = this.props.data.rent;
      }

      this.setState({ soon, rent });
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  tabsHandler = (tabName) => {
    this.setState({ tabValue: tabName })
  };

  render() {
    const { tabValue, soon, rent } = this.state;
    const { data } = this.props;

    return (
      Object.keys(data).length > 0 ?
      <React.Fragment>
        <div className={classNames("big_slider", "main_page_big_slider")} style={{width: '100%'}}>
          <MainSliderWrapper films={data.rent} mainPage={true} />
        </div>
        <Wrapper
          wrapItems="flex-start"
          wrapPadding="0 65px"
          wrapMargin="85px 0 50px"
          className="title_block"
        >
          <Title fontSize="35px" className="title">
            Фільми в SmartCinema
          </Title>
                {this.props.cityId === 1?
                    <TextBlock className="choosenCityBlock" color="#fff"  font="20px">
                    Вінниця, TЦ SkyPark
                    </TextBlock>
                        :
                    null}
              {this.props.cityId === 2?
                  <TextBlock className="choosenCityBlock" color="#fff"  font="20px">
                      Хмельницький, ТЦ WoodMall
                  </TextBlock>
                  :
                  null}
        </Wrapper>
        <Wrapper
          wrapPadding="0 65px"
          wrapItems="flex-start"
          className="smaller_padding"
        >
          {
            soon && rent &&
            <Wrapper wrapItems="flex-start">
              <Wrapper
                wrapDirection="row"
                wrapItems="flex-start"
                wrapPadding="10px 0 0 0"
                wrapMargin="0 0 25px 0"
                className="bordered_title"
              >
                <TitleH3
                  className={classNames("tabs_title", {bordered_blue_title: tabValue === 'rent'})}
                  fontSize="19px"
                  onClick={(e) => this.tabsHandler('rent')}
                >
                  Дивитися зараз
                </TitleH3>
                <TitleH3
                  className={classNames("tabs_title", {bordered_blue_title: tabValue === 'soon'})}
                  fontSize="19px"
                  onClick={(e) => this.tabsHandler('soon')}
                >
                  Дивитися незабаром
                </TitleH3>
              </Wrapper>
              {tabValue === 'rent' &&
              <div className={classNames("big_slider", "films_slider")} style={{width: '100%'}}>
                <FilmSliderWrapper films={rent} rent />
              </div>
              }
              {tabValue === 'soon' &&
              <div className={classNames("big_slider", "films_slider")} style={{width: '100%'}}>
                <FilmSliderWrapper films={soon} />
              </div>
              }
            </Wrapper>
          }

        </Wrapper>

        <Wrapper
          id="news"
          className="news"
          ref={(section) => { this.News = section; }}
          wrapItems="flex-start"
          wrapPadding="110px 65px 0"
          wrapMargin="0 0 30px 0"
        >
          <TitleH2  fontSize="35px" className="share_title">
            Новини та акції
          </TitleH2>
          <Wrapper

            wrapItems="flex-start"
            wrapPadding="0 25px"
            wrapMargin="40px 0"
            className="slider_big"

          >
            <div className={classNames("big_slider", "shares_slider")} style={{width: '100%'}}>
              <MainSliderWrapper big shares={data.shares} />
            </div>
          </Wrapper>

          <Wrapper
            wrapItems="flex-start"
            wrapPadding="0 25px"
            wrapMargin="40px 0"
            className="slider_small"

          >
            <div className={classNames("big_slider", "shares_slider")} style={{width: '100%'}}>
              <MainSliderWrapper shares={data.shares} />
            </div>
          </Wrapper>
        </Wrapper>


      </React.Fragment>
        :
      <Wrapper
        wrapPadding="0 65px"
        wrapMargin="85px 0 50px"
      >
        <Image
          src={loader}
          imgHeight={150}
          imgWidth={150}
          className="preloader_gif"
        />
      </Wrapper>
    );
  }
}

export const Main = connect(({ state }) => ({
    data: state.data,
    cityId: state.cityId
}))(MainComponent);
