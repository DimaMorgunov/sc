import classNames from 'classnames';
import { connect } from 'react-redux';
import {
    Button, CheckboxDiv,
    Form,
    Input,
    PreloaderDiv,
    TextBlock,
    Wrapper
} from "../components";
import {orangeButton} from "../themes";
import { ThankModal } from "./containerComponents/ThankModal";

import DatePicker from 'react-datepicker';
import moment from 'moment';
import check from "../img/check.png";

import Select from 'react-select-v1';
import 'react-select-v1/dist/react-select.css';
import {emailReg} from "../consts";

import {registerAsync, setError, setSuccess} from "../store";
import InputMask from "react-input-mask";


export class RegistredComponent extends Component {
    state = {
        inputValues: {
            email: '',
            password: '',
            name: '',
            surname: '',
            birthday: '',
            city: '',
            phone: '',
            repeatPassword: '',
            subscribe: true
        },
        errorInput:{
            email: '',
            password: '',
            name: '',
            surname: '',
            birthday: '',
            phone: '',
            repeatPassword: '',
            subscribe: ''
        },
        cities:[
            {
                id: 1,
                city: 'Вінниця, TЦ SkyPark'
            },
            {
                id: 2,
                city: 'Хмельницький, ТЦ WoodMall'
            }
        ],
        showPreloader: false,
        errorFields: [],
        error: false,
        thank: false
    };

    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {
            let errorFields = [];

            for (let key in this.props.error.SET_ERROR){
                switch (key) {
                    case 'username':
                        if (this.props.error.SET_ERROR.username) {
                            errorFields.push('email');
                            let errorInput = this.state.errorInput;
                            errorInput.email = this.props.error.SET_ERROR.username[0];
                            this.setState({errorInput: errorInput});
                        }
                        break;
                    case 'first_name':
                        if (this.props.error.SET_ERROR.first_name) {
                            errorFields.push('name');
                            let errorInput = this.state.errorInput;
                            errorInput.name = this.props.error.SET_ERROR.first_name[0];
                            this.setState({errorInput: errorInput});
                        }
                        break;
                    case 'last_name':
                        if (this.props.error.SET_ERROR.last_name) {
                            errorFields.push('surname');
                            let errorInput = this.state.errorInput;
                            errorInput.surname = this.props.error.SET_ERROR.last_name[0];
                            this.setState({errorInput: errorInput});
                        }
                        break;
                    case 'phone':
                        if (this.props.error.SET_ERROR.phone) {
                            errorFields.push('phone');
                            let errorInput = this.state.errorInput;
                            errorInput.phone = this.props.error.SET_ERROR.phone[0];
                            this.setState({errorInput: errorInput});
                        }
                        break;
                    case 'password':
                        if (this.props.error.SET_ERROR.password) {
                            errorFields.push('password');
                            let errorInput = this.state.errorInput;
                            errorInput.password = this.props.error.SET_ERROR.password[0];
                            this.setState({errorInput: errorInput});
                        }
                        break;
                    default:
                        break
                }
            }
            this.setState({
                errorFields,
                showPreloader: false,
                error: true
            });
        }
        if(prevProps.success !== this.props.success && this.props.success) {
            this.toogleModal('thank', true);
            this.props.dispatch(setSuccess(null));
            this.setState({
                showPreloader: false,
            });
        }

    }

    inputHandler = ({target}) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== target.name);
            this.setState({ errorFields: errStr });
            switch(target.name) {
                case 'name':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.name = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'surname':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.surname = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'birthday':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.birthday = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'email':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.email = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'phone':

                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.phone = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'password':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.password = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'repeatPassword':
                    if(target.name.length > 0) {
                        let errorInput = this.state.errorInput;
                        errorInput.repeatPassword = null;
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }
        this.setState({ inputValues: {
                ...this.state.inputValues,
                [target.name]: target.value
            }})
    };

    selectHandler = (option) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== 'city');
            this.setState({ errorFields: errStr });
            let errorInput = this.state.errorInput;
            errorInput.city = null;
            this.setState({errorInput: errorInput});
        }

        this.setState({ inputValues: {
                ...this.state.inputValues,
                city: option
            }});
    };

    handleDateChange = (date) => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== 'birthday');
            this.setState({ errorFields: errStr });
            let errorInput = this.state.errorInput;
            errorInput.birthday = null;
            this.setState({ errorInput: errorInput})
        }
        this.setState({ inputValues: {
                ...this.state.inputValues,
                birthday: date
            }})
    };

    checkboxHandler = () => {
        if(this.state.errorFields.length > 0) {
            let errStr = this.state.errorFields.filter(err => err !== 'subscribe');
            this.setState({ errorFields: errStr });
        }

        this.setState({ inputValues: {
                ...this.state.inputValues,
                subscribe: !this.state.inputValues.subscribe
            }})
    };

    formHandler = () => {
        const { inputValues } = this.state;
        let errorFields = [];

        for(let key in inputValues) {
            switch(key) {
                case 'name':
                    if(!inputValues.name.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.name = 'Упс, забули вказати Ім`я';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'surname':
                    if(!inputValues.surname.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.surname = 'Упс, забули вказати Прізвище';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'birthday':
                    if(!inputValues.birthday) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.birthday = 'Упс, перевірте Дату народження';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'email':
                    if(inputValues.email.length < 5 && !emailReg.test(inputValues[key])) {
                        errorFields.push(key);;
                        let errorInput = this.state.errorInput;
                        errorInput.email = 'Упс, перевірте E-mail';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'city':
                    if(!inputValues.city) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.city = 'Упс, перевірте Місто';
                        this.setState({errorInput: errorInput});
                    }
                case 'phone':
                    if(!inputValues.phone.length) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.phone = 'Упс, перевірте Номер телефону';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'password':
                    if(inputValues.password <= 8) {
                        errorFields.push(key);
                        let errorInput = this.state.errorInput;
                        errorInput.password ='Довжина пароля повинна бути не менше 8 символів та пароль повинен складатися з букв латинського алфавіту (A-z), арабських цифр (0-9) і спеціальних символів.';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                case 'repeatPassword':

                    if(!inputValues.repeatPassword.length || inputValues.password !== inputValues.repeatPassword) {
                        errorFields.push('repeatPassword');
                        let errorInput = this.state.errorInput;
                        errorInput.repeatPassword ='Невірне значення паролю';
                        this.setState({errorInput: errorInput});
                    }
                    break;
                default:
                    break;
            }
        }

        this.setState({ errorFields });

        if(errorFields.length === 0) {
            this.setState({
                showPreloader: true,
            });
            var purePhone = [];
            for(let i = 0; i < inputValues.phone.length; i++){
                if(inputValues.phone[i] !== ")" &&
                    inputValues.phone[i] !== "(" &&
                    inputValues.phone[i] !== " " &&
                    inputValues.phone[i] !== "-"
                ){
                    purePhone.push(inputValues.phone[i])
                }
            }
            var phoneStr = purePhone.join("");
            this.props.dispatch(setError(null));
            this.props.dispatch(registerAsync({
                token: this.props.token,
                username: inputValues.email,
                password: inputValues.password,
                first_name: inputValues.name,
                last_name: inputValues.surname,
                phone: phoneStr,
                most_important_city: inputValues.city.value,
                birthday: `${inputValues.birthday.get('year')}-${inputValues.birthday.get('month')+1}-${inputValues.birthday.get('date')}`
            }));

        }
    };
    toogleModal = (side, open) => {
        this.setState({
            thank: false,
            [side]: open
        });
        document.body.classList.toggle('modal_opened');
    }

    render() {
        const {
            cities,
            inputValues,
            errorFields,
            showPreloader,
            error
        } = this.state;

        return (
            <React.Fragment>
                <Wrapper
                    wrapPadding="100px 15px 15px"
                    className="resetPassModal"
                >
                    <Wrapper
                        wrapMaxWidth='768px'
                        wrapPadding="0 20px 15px"
                    >
                        <TextBlock className="form_title">
                            Реєстрація
                        </TextBlock>
                        <Form>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", { focused: inputValues.name !== '' } )}
                            >
                                <label className={classNames({ focused: inputValues.name !== '' })}>
                                    Ім`я
                                </label>
                                <Input
                                    type="text"
                                    className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^name$`).test(el)) })}
                                    name="name"
                                    value={inputValues.name}
                                    onChange={this.inputHandler}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.name? this.state.errorInput.name: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", { focused: inputValues.surname !== '' } )}
                            >
                                <label className={classNames({ focused: inputValues.surname !== '' })}>
                                    Прізвище
                                </label>
                                <Input
                                    type="text"
                                    className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^surname$`).test(el)) })}
                                    name="surname"
                                    value={inputValues.surname}
                                    onChange={this.inputHandler}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.surname? this.state.errorInput.surname: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("datepicker_wrapper", "input_wrapper")}
                            >
                                <label className={classNames({ focused: inputValues.birthday !== '' })}>
                                    Дата народження
                                </label>
                                <DatePicker
                                    className={classNames("datepicker", {error: errorFields.some((el) => !!RegExp(`^birthday$`).test(el)) })}
                                    selected={inputValues.birthday}
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    onChange={this.handleDateChange}
                                    locale="uk"
                                    maxDate={moment()}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.birthday? this.state.errorInput.birthday: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", "select_wrapper", { focused: inputValues.city !== '' })}
                            >
                                <label className={classNames({ focused: inputValues.city !== '' })} >
                                    Ваше місто
                                </label>
                                <Select
                                    name="city"
                                    value={inputValues.city}
                                    searchable={false}
                                    clearable={false}
                                    multi={false}
                                    className={classNames("select_block", {error: errorFields.some((el) => !!RegExp(`^city$`).test(el))} )}
                                    placeholder=""
                                    onChange={this.selectHandler}
                                    options={cities && cities.map(el => ({ value: el.id, label: el.city }))}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.city? this.state.errorInput.city: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", { focused: inputValues.phone !== '' } )}
                            >
                                <label className={classNames({ focused: inputValues.phone !== '' })}>
                                    Номер телефону
                                </label>
                                <InputMask
                                    mask="38 (999) 999-99-99"
                                    className={classNames("inputMask", {error: errorFields.some((el) => !!RegExp(`^phone$`).test(el)) })}
                                    name="phone"
                                    value={inputValues.phone}
                                    onChange={this.inputHandler}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.phone? this.state.errorInput.phone: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", { focused: inputValues.email !== '' } )}
                            >
                                <label className={classNames({ focused: inputValues.email !== '' })}>
                                    E-mail
                                </label>
                                <Input
                                    type="text"
                                    className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^email$`).test(el)) })}
                                    name="email"
                                    value={inputValues.email}
                                    onChange={this.inputHandler}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.email? this.state.errorInput.email: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", { focused: inputValues.password !== '' } )}
                            >
                                <label className={classNames({ focused: inputValues.password !== '' })}>
                                    Придумайте пароль
                                </label>
                                <Input
                                    type="password"
                                    className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^password$`).test(el)) })}
                                    name="password"
                                    value={inputValues.password}
                                    onChange={this.inputHandler}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.password? this.state.errorInput.password: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                className={classNames("input_wrapper", { focused: inputValues.repeatPassword !== '' } )}
                            >
                                <label className={classNames({ focused: inputValues.repeatPassword !== ''  })}>
                                    Підтвердіть пароль
                                </label>
                                <Input
                                    type="password"
                                    className={classNames("input", {error: errorFields.some((el) => !!RegExp(`^repeatPassword$`).test(el)) })}
                                    name="repeatPassword"
                                    value={inputValues.repeatPassword}
                                    onChange={this.inputHandler}
                                />
                            </Wrapper>
                            <p className='inputError'>{this.state.errorInput.repeatPassword? this.state.errorInput.repeatPassword: null}</p>
                            <Wrapper
                                wrapMargin="30px 0"
                                wrapDirection="row"
                                onClick={() => this.checkboxHandler()}
                                className={classNames("pointer", {error: errorFields.some((el) => !!RegExp(`^subscribe$`).test(el)) })}
                            >
                                <CheckboxDiv
                                    src={check}
                                    className={classNames({ checked: inputValues.subscribe === true })}
                                />
                                <TextBlock lineHeight="24px" color="#0d1017">
                                    Я хочу отримувати розсилку з новинами кінотеатру
                                </TextBlock>
                            </Wrapper>

                            <Wrapper
                                wrapMargin="30px 0 10px"
                                wrapItems="flex-start"
                                className="form_btn_wrapper"
                            >
                                <Button
                                    theme={orangeButton}
                                    onClick={this.formHandler}
                                >
                                    Зареєструватися
                                </Button>
                                {
                                    error &&
                                    <TextBlock color="red" textMargin="10px 0" bold >
                                        Упс, щось пішло не так. Спробуйте ще раз
                                    </TextBlock>
                                }
                                <TextBlock
                                    color="#626262"
                                    font="15px"
                                    lineHeight="22px"
                                    textMargin="15px 0 0"
                                >
                                    Продовженням реєстрації глядач дає свою згоду на збір, зберігання, використання та поширення
                                    наданої їм персональної інформації, в т.ч. стороннім організаціям та особам в межах Закону
                                    України «Про захист персональних даних» від 01.06.2010&nbsp;р.
                                </TextBlock>
                            </Wrapper>
                        </Form>
                    </Wrapper>
                </Wrapper>
                <ThankModal
                    show={this.state.thank}
                    closeModal={this.toogleModal}
                />
                <PreloaderDiv show={showPreloader} />
            </React.Fragment>
        )
    }
}


export const Registred = (connect(({ state }) => ({
    token: state.token,
    error: state.error,
    success: state.success
}))(RegistredComponent));
