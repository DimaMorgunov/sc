import {connect} from 'react-redux';
import classNames from 'classnames';
import moment from 'moment'
import {
    Image,
    ImageBlock,
    Label,
    Span,
    TextBlock, Title,
    Wrapper
} from "../components";

import {getTicketsInfoAsync} from "../store";
import React from "react";
import calendar from "../img/calendar.png";
import clock from "../img/clock.png";


export class TicketComponent extends Component {
    state = {
        counterTicketsArr: [],
        sumPriceTicketsArr: [],
        ukraineMonth: [
            'Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень',
            'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'
        ],
        ukraineDaysofWeek: [
            'Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота'
        ]
    };

    componentDidMount() {


        if (this.props.token && this.props.userToken) {
            this.props.dispatch(getTicketsInfoAsync({
                token: this.props.token,
                userToken: this.props.userToken
            }));
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.token && this.props.userToken && !this.props.ticketsInfo) {
            this.props.dispatch(getTicketsInfoAsync({
                token: this.props.token,
                userToken: this.props.userToken
            }));
        }
        if (prevProps.ticketsInfo !== this.props.ticketsInfo) {

            let mainArr = [];
            let counerArr = [];
            for (let i = 0; i < this.props.ticketsInfo.length; i++) {
                let sumCouterTicket = this.props.ticketsInfo[i].ticket_list.length;
                sumCouterTicket.toString();
                counerArr.push(sumCouterTicket);
                let priceArr = [];
                for (let j = 0; j < this.props.ticketsInfo[i].ticket_list.length; j++) {
                    let sumTicket = this.props.ticketsInfo[i].ticket_list[j].price;
                    priceArr.push(sumTicket);
                    if (priceArr.length === this.props.ticketsInfo[i].ticket_list.length) {
                        var sum = priceArr.reduce((a, b) => a + b, 0);
                        sum.toString();
                        mainArr.push(sum);
                        priceArr = []
                    }
                }
            }
            this.setState({
                counterTicketsArr: counerArr,
                sumPriceTicketsArr: mainArr
            })
        }
    }


    render() {
        const {ticketsInfo} = this.props;
        const {sumPriceTicketsArr, counterTicketsArr, ukraineMonth, ukraineDaysofWeek} = this.state;

        return (
            <React.Fragment>
                {ticketsInfo ?
                    <Wrapper
                        wrapMaxWidth="768px"
                        wrapMinWidth="768px"
                        className="userTicketsWraper"
                    >
                        {ticketsInfo.map((item, i) =>
                            <Wrapper wrapDirection="row" wrapItems="center">
                                <Wrapper
                                    wrapMinWidth="60%"
                                    wrapMaxWidth="60%"
                                    wrapItems="flex-start"
                                    className="userTicketsLeftSide"
                                >
                                    <Title blue fontSize="25px" className='userTicketsLeftSidePadding'>
                                        {item.movie_info.title}
                                    </Title>
                                    <Wrapper
                                        wrapDirection="row"
                                        wrapItems="flex-start"
                                        wrapPadding="10px 0 10px 20px"
                                    >
                                        <Label
                                            className={classNames("block_label", "white_label")}>{item.movie_info.format}</Label>
                                        <Label
                                            className={classNames("block_label", "orange_label")}>{item.movie_info.age}+</Label>
                                    </Wrapper>

                                    <TextBlock textPadding="0 0 0 30px">
                                        {item.hall}
                                    </TextBlock>

                                    <Wrapper wrapDirection="row" className='userTicketsLeftSidePadding'>
                                        <Image
                                            src={calendar}
                                            imgHeight={22}
                                            imgWidth={20}
                                            imgMargin="0 15px 0 0"
                                        />
                                        <Span font="16px">{
                                            `${new Date(item.showtime).getDate()} ${ukraineMonth[new Date(item.showtime).getMonth()]},
                                    ${ukraineDaysofWeek[new Date(item.showtime).getDay()]}`}
                                    </Span>
                                    </Wrapper>

                                    <Wrapper wrapDirection="row" className='userTicketsLeftSidePadding'>
                                        <Image
                                            src={clock}
                                            imgHeight={23}
                                            imgWidth={23}
                                            imgMargin="0 15px 0 0"
                                        />

                                        <Span font="16px">
                                            {new Date(item.showtime).getHours()}:{new Date(item.showtime).getMinutes() < 10 ?
                                            '0' + new Date(item.showtime).getMinutes() :
                                            new Date(item.showtime).getMinutes()}
                                        </Span>

                                    </Wrapper>
                                </Wrapper>

                                <Wrapper
                                    wrapMinWidth="40%"
                                    className="userTicketsLeftSide"
                                >
                                    {item.ticket_list.map((item) =>
                                        <Wrapper wrapDirection="row">
                                            <Wrapper
                                                wrapMinWidth="120px"
                                                wrapMaxWidth="120px"
                                                className="userTicketsRightInfoTin"
                                            >
                                                <Wrapper
                                                    wrapDirection="row"
                                                    wrapContent="space-between"
                                                    wrapMargin="20px 0"
                                                    wrapMinWidth="100px"
                                                    wrapMaxWidth="100px"
                                                >
                                                    <Label className="userTicketsRightInfo">
                                                        <Span className="ticketInCabinet">{item.row} ряд</Span>
                                                        <Span className="ticketInCabinet">{item.place} місце</Span>
                                                        <Span className="ticketInCabinetLast">{item.price} грн</Span>
                                                    </Label>
                                                </Wrapper>
                                            </Wrapper>
                                            <Wrapper wrapMinWidth="160px">
                                                <ImageBlock
                                                    src={item.qr_code}
                                                    imgWidth={160}
                                                />
                                            </Wrapper>
                                        </Wrapper>
                                    )}
                                    <Wrapper wrapItems="start">
                                        <Label className="userTicketsRightInfoSum">
                                        <Span className="ticketInCabinetSum">
                                            {counterTicketsArr[i] ? counterTicketsArr[i] : null}
                                            {counterTicketsArr[i] === 1 ? ' квиток' : counterTicketsArr[i] >= 2 && counterTicketsArr[i] <= 4 ? ' квитка' : ' квитків'}
                                        </Span>
                                            <Span
                                                className="ticketInCabinetSumLast">{sumPriceTicketsArr[i] ? sumPriceTicketsArr[i] : null} грн</Span>
                                        </Label>
                                    </Wrapper>
                                </Wrapper>
                                <Wrapper wrapMinWidth="90%" className="userTicketThin"/>
                            </Wrapper>
                        )}
                    </Wrapper>
                    :
                    <Wrapper>
                        Упс, щось пішло не так. Спробуйте ще раз
                    </Wrapper>
                }
            </React.Fragment>
        )
    }
}


export const Ticket = (connect(({state}) => ({
    success: state.success,
    error: state.error,
    ticketsInfo: state.ticketsInfo,
    token: state.token,
    userToken: state.userToken
}))(TicketComponent));
