import {
  Span,
  TextBlock,
  Title,
  Wrapper,
  Image,
  ImageBlock
} from "../components";
import classNames from 'classnames';

import bonus1 from '../img/bonus1.png';
import bonus2 from '../img/bonus2.png';
import bonus3 from '../img/bonus3.png';
import bonus4 from '../img/bonus4.png';

export class Bonus extends Component {

  render() {
    return (
      <React.Fragment>
        <Wrapper
          wrapMargin="55px 0"
          wrapPadding="0 60px"
        >
          <Title centered fontSize="35px">
            Бонусна програма
          </Title>
        </Wrapper>
        <Wrapper
          wrapContent="center"
          wrapMargin="0 auto 50px"
        >
          <Wrapper
            wrapDirection="row"
            wrapMargin="0 0 125px 0"
            wrapContent="center"
            className="bonus_wrapper"
          >
            <Wrapper
              className={classNames("white_box", "bonus")}
              wrapMinWidth="416px"
              wrapMaxWidth="416px"
              wrapItems="flex-start"
              wrapPadding="20px"
              wrapContent="space-between"
            >
              <ImageBlock src={bonus1} imgHeight={196} imgWidth={314} className="bonus_img" />
              <TextBlock
                centered
                lineHeight="24px"
                color="#12161f"
                textPadding="10px 0 34px 0"
                textMargin="5px auto"
              >
                Для того, щоб стати учасником SmartCinema Club <Span bold color="#00a6ea">зареєструйся</Span> на
                сайті або заповни <Span bold color="#00a6ea">анкету</Span> в нашому кінотеатрі
              </TextBlock>
            </Wrapper>

            <Wrapper
              className={classNames("white_box", "bonus")}
              wrapMinWidth="416px"
              wrapMaxWidth="416px"
              wrapItems="flex-start"
              wrapPadding="20px"
              wrapContent="space-between"
            >
              <ImageBlock src={bonus2} imgWidth={306} imgHeight={173} className="bonus_img" />
              <TextBlock
                centered
                lineHeight="24px"
                color="#12161f"
                textPadding="10px"
                textMargin="5px auto"
              >
                Отримай <Span bold color="#00a6ea">бонусну карту</Span> у адміністратора
                або завантаж до свого ґаджету віртуальну бонусну карту
                та <Span bold color="#00a6ea">авторизуйся</Span> в особистому кабінеті за допомогою пароля з смс
              </TextBlock>
            </Wrapper>
          </Wrapper>

          <Wrapper
            wrapDirection="row"
            wrapContent="center"
            className="bonus_wrapper"
          >
            <Wrapper
              className={classNames("white_box", "bonus")}
              wrapMinWidth="416px"
              wrapMaxWidth="416px"
              wrapItems="flex-start"
              wrapPadding="20px"
              wrapContent="space-between"
            >
              <ImageBlock src={bonus3} imgWidth={323} imgHeight={183} className="bonus_img" />
              <TextBlock
                centered
                lineHeight="24px"
                color="#12161f"
                textPadding="10px"
                textMargin="5px auto"
              >
                <Span bold color="#00a6ea">Купуй квитки</Span> на сайті та товари в
                кінобарі, <Span bold color="#00a6ea">отримуй бонуси</Span> за кожну витрачену гривню:  10 грн = 5 балів. <br/>
                За кожні 300 накопичених балів - ми даруємо тобі квиток :)
              </TextBlock>
            </Wrapper>

            <Wrapper
              className={classNames("white_box", "bonus")}
              wrapMinWidth="416px"
              wrapMaxWidth="416px"
              wrapItems="flex-start"
              wrapPadding="20px"
              wrapContent="space-between"
            >
              <ImageBlock src={bonus4} imgWidth={314} imgHeight={172} className="bonus_img" />
              <TextBlock
                centered
                lineHeight="24px"
                color="#12161f"
                textPadding="10px 0 34px 0"
                textMargin="5px auto"
              >
                SmartCinema Club – це:<br/>
                <Span bold color="#00a6ea">спеціальні ціни</Span> у нашому кінобарі <br/>
                <Span bold color="#00a6ea">безкоштовний квиток</Span> на  День Народження <br/>
                а також додаткові <Span bold color="#00a6ea">знижки</Span> у наших <a href="/media/pdf/partners.pdf" target="_blank"><Span style={{textDecoration: 'underline'}} bold color="#00a6ea">партнерів</Span></a> ;)
              </TextBlock>
            </Wrapper>
          </Wrapper>


          <Wrapper
            wrapMargin="50px 50px 10px 50px"
            wrapDirection="column"
            wrapContent="center"
            className="pdf_link"
          >
            <Wrapper
              wrapDirection="column"
            >
              <a href="/media/pdf/bonus_program_rules.pdf" target="_blank">
                <Image src='/static/img/pdf.png' imgWidth={32} imgHeight={36} imgMargin="0 15px 0 0" />
                <TextBlock lineHeight="24px">
                  Умови і правила бонусної програми
                </TextBlock>
              </a>
              <a href="/media/pdf/partners.pdf" target="_blank" style={{marginTop: '10px'}}>
                <Image src='/static/img/pdf.png' imgWidth={32} imgHeight={36} imgMargin="0 15px 0 0" />
                <TextBlock className="pdf_link_partners" lineHeight="24px">
                  Наші партнери
                </TextBlock>
              </a>
            </Wrapper>

          </Wrapper>
        </Wrapper>
      </React.Fragment>
    )
  }
}
