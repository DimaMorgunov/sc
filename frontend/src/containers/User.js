import { connect } from 'react-redux';
import moment from 'moment';
import {
    Title,
    Wrapper,
    ImageBlock,
    TextBlock,
    Span, ModalLayer,
} from "../components";

import {
    BindCardModal,
    UserEditModal,
    UserEditPassModal,
    CardDontReadyModal,
    ShowZipCodeModal,
    UserTicketsModal,
    UserTicketsComponent
} from './index'

import {getUserInfoAsync, getBonusInfoAsync, getTicketsInfoAsync, setError, checkBonusCard} from "../store";
import { blueButton } from "../themes";
import {Button} from "../components/StyledComponents";

export class UserComponent extends Component {

    state = {
        dontready: false,
        userEdit: false,
        editPass: false,
        bind: false,
        showZip: false,
        showUserTickets: false,
        startDate: moment(),
        birthday: null,
        countToFreeTicket: null
    };


    toggleDrawer = (side, open) => {
        this.setState({
            dontready: false,
            userEdit: false,
            editPass: false,
            bind: false,
            showZip: false,
            showUserTickets: false,
            [side]: open
        });
    };

    reloadBonus = () => {
        this.props.dispatch(getBonusInfoAsync({
            token: this.props.token,
            userToken: this.props.userToken
        })) ;
    };

  componentDidMount() {
      if(this.props.token && this.props.userToken) {
        this.props.dispatch(getUserInfoAsync({
          token: this.props.token,
          userToken: this.props.userToken
        }));
        this.props.dispatch(getBonusInfoAsync({
          token: this.props.token,
          userToken: this.props.userToken
        })) ;
        this.props.dispatch(getTicketsInfoAsync({
          token: this.props.token,
          userToken: this.props.userToken
        }));

      }
  }
  TimeToFreeTicket = () =>{
    var dateNow = new Date();
    var fullyear = dateNow.getFullYear();
    dateNow.setFullYear(fullyear);
    const userBD = this.props.userInfo.birthday;
    const year = userBD.substring(0, 4);
    const month = userBD.substring(5,7);
    const day = userBD.substring(8,10);
    let getYearNow = dateNow.getFullYear();
    const birthday = new Date(getYearNow, (+month - 1), day, 0, 0, 0, 0);
    let res =   birthday - dateNow;
    res < 0? res = 31536000000 + res: res;
    var  today = Math.floor((res)/1000);
    var  tsec=today%60; today=Math.floor(today/60); if(tsec<10)tsec='0'+tsec;
    var  tmin=today%60; today=Math.floor(today/60); if(tmin<10)tmin='0'+tmin;
    var  thour=today%24; today=Math.floor(today/24);
    var  timestr=today +" дней "+ thour+" часов "+tmin+" минут "+tsec+" секунд";


      this.setState({
        countToFreeTicket: `${today} : ${thour} : ${tmin}`
      })
  };



    checkCard = () =>{
      this.props.dispatch(checkBonusCard({
          token: this.props.token,
          userToken: this.props.userToken
      }));
    };

    componentDidUpdate(prevProps) {
        if(prevProps.error !== this.props.error && this.props.error) {
            this.setState({
                showPreloader: false,
                error: true
            })
        }

        if(prevProps.checkCard !== this.props.checkCard){
            this.props.checkCard.card === false?
                this.toggleDrawer("dontready", true):
                this.toggleDrawer("bind", true)
        }
            if(prevProps.userInfo !== this.props.userInfo && this.props.userInfo){
                this.TimeToFreeTicket()
            }
        if(this.props.token && this.props.userToken && !this.props.userInfo) {
            this.props.dispatch(getUserInfoAsync({
                token: this.props.token,
                userToken: this.props.userToken
            }));
            this.props.dispatch(getBonusInfoAsync({
                token: this.props.token,
                userToken: this.props.userToken
            })) ;
            this.props.dispatch(getTicketsInfoAsync({
                token: this.props.token,
                userToken: this.props.userToken
            }));
        }
    }

  render() {
    const {
      userInfo,
      userToken,
      ticketsInfo,
      data,
      success
    } = this.props;

  const { bind ,editPass ,userEdit, dontready, showZip, countToFreeTicket, showUserTickets}  = this.state;

    return (
      <React.Fragment>
        {
          userToken ?
            userInfo ?
            <Wrapper
              wrapMargin="50px 0 90px"
              wrapContent="center"
            >
                <UserTicketsModal
                    selectModal
                    show={showUserTickets}
                    closeModal={this.toggleDrawer}
                >
                </UserTicketsModal>

                <ShowZipCodeModal
                    show={showZip}
                    zipCode={this.props.userInfo.card_svg}
                    showZip
                    closeModal={this.toggleDrawer}
                >
                </ShowZipCodeModal>
                <CardDontReadyModal
                    show={dontready}
                    closeModal={this.toggleDrawer}
                >
                </CardDontReadyModal>
                <BindCardModal
                    show={bind}
                    closeModal={this.toggleDrawer}
                >
                </BindCardModal>
                <UserEditModal
                    cities={Object.keys(data) != 0 && data.branch}
                    show={userEdit}
                    closeModal={this.toggleDrawer}
                >
                </UserEditModal>
                <UserEditPassModal
                    show={editPass}
                    closeModal={this.toggleDrawer}
                >
                </UserEditPassModal>
              <Title fontSize="35px">
                Особистий кабінет
              </Title>
              <Wrapper
                wrapDirection="row"
                wrapItems="flex-start"
                wrapContent="center"
              >
                <Wrapper
                  wrapMaxWidth="768px"
                  wrapMinWidth="320px"
                  wrapPadding="40px 30px"
                  wrapMargin="20px"
                  className="white_box"
                >
                  <Wrapper
                    wrapDirection="row"
                    wrapContent="space-between"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/tickets.png" imgWidth={36} imgMargin="0 10px 0 0" />
                      <TextBlock color="#12161f" font="20px">
                        Мої квитки
                      </TextBlock>
                    </Wrapper>
                      <Wrapper wrapDirection="row" wrapMinWidth="auto">
                          {
                              ticketsInfo &&
                              <TextBlock bold font="20px">
                                  {ticketsInfo.length > 0 ?
                                      <Span color="#00a6ea" onClick={() => this.toggleDrawer('showUserTickets', true)}>
                              {ticketsInfo.length }  {ticketsInfo.length === 1 ? 'квиток' : ticketsInfo.length >= 2 && ticketsInfo.length <= 4 ? 'квитка' : 'квитків'}
                            </Span>
                                      :
                                      <Span color="#5b5f7d">Квитків немає</Span>
                                  }
                              </TextBlock>
                          }
                      </Wrapper>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapContent="space-between"
                  >
                    <Wrapper
                      wrapMinWidth="auto"
                      wrapMaxWidth="450px"
                      wrapItems="flex-start"
                      wrapPadding="0 0 0 45px"
                    >
                      <TextBlock color="#12161f"  font="20px">
                        Безкоштовний квиток
                      </TextBlock>
                      <TextBlock color="#5b5f7d" font="15px" textMaxWidth="70%">
                        При накопиченні на карті кожних 300 балів ти отримаєш
                        1 безкоштовний квиток
                      </TextBlock>
                    </Wrapper>
                      {
                          userInfo.bonus?
                              <Wrapper
                                  wrapMinWidth="145px"
                                  wrapContent="center"
                                  className="orange_wrapper"
                              >
                                  <TextBlock color="#12161f" font="14px">
                                      Ще необхідно
                                  </TextBlock>
                                  <Span color="#fd5f00" bold  textMargin="0 0 5px 0">{300 - (userInfo.bonus % 300)} балів</Span>
                              </Wrapper>
                              :
                              <Wrapper
                                  wrapMinWidth="145px"
                                  wrapContent="center"
                                  className="orange_wrapper"
                              >
                                  <TextBlock color="#12161f" font="14px">
                                      немає бонусів
                                  </TextBlock>
                              </Wrapper>
                      }
                  </Wrapper>
                  <Wrapper
                    wrapDirection="row"
                    wrapContent="space-between"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/birthday.png" imgWidth={37} imgMargin="0 10px 0 0" className="freeTicketImg"/>
                      <TextBlock color="#12161f" font="20px">
                        Квиток у подарунок до дня народження
                      </TextBlock>
                    </Wrapper>

                    <Wrapper
                      wrapMinWidth="145px"
                      wrapContent="center"
                      className="orange_wrapper"
                    >
                      <Span color="#fd5f00" bold textMargin="5px 0 0 0">
                          {countToFreeTicket? countToFreeTicket: 'Упс'}
                      </Span>
                      <TextBlock color="#12161f" font="14px">
                        днів год хв
                      </TextBlock>
                    </Wrapper>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapContent="space-between"
                    wrapMargin="20px 0"
                  >
                    <TextBlock color="#00a6ea" font="25px" bold>
                      Персональні дані
                    </TextBlock>
                    <TextBlock color="#00a6ea" font="16px" onClick={() => this.toggleDrawer('userEdit', true)}>
                      Редагувати
                    </TextBlock>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/birthday-calendar.png" imgWidth={30} imgMargin="0 10px 0 0" />
                      <TextBlock color="#12161f" font="20px">
                        {userInfo.birthday}
                      </TextBlock>
                    </Wrapper>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/city-icon.png" imgWidth={26} imgMargin="0 10px 0 0" />
                      <TextBlock color="#12161f" font="20px">
                        {userInfo.most_important_city === 1 ? 'Вінниця' : 'Хмельницький'}
                      </TextBlock>
                    </Wrapper>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                    wrapContent="space-between"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/password-icon.png" imgWidth={23} imgMargin="0 10px 0 0" />
                      <TextBlock color="#5b5f7d" font="20px">
                        ********
                      </TextBlock>

                    </Wrapper>
                      <TextBlock color="#00a6ea" font="16px" onClick={(e) => this.toggleDrawer('editPass', true, e)}>
                          Редагувати
                      </TextBlock>
                  </Wrapper>
                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/email.png" imgWidth={31} imgMargin="0 10px 0 0" />
                      <TextBlock color="#12161f" font="20px">
                        {userInfo.email}
                      </TextBlock>
                    </Wrapper>
                  </Wrapper>
                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/phone.png" imgWidth={23} imgMargin="0 10px 0 0" />
                      <TextBlock color="#12161f" font="20px">
                        {userInfo.phone ? userInfo.phone : 'Телефон не вказаний'}
                      </TextBlock>
                    </Wrapper>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto">
                      <ImageBlock src="/static/img/user-icon.png" imgWidth={30} imgMargin="0 10px 0 0" />
                      <TextBlock color="#12161f" font="20px">
                        {userInfo.first_name} {userInfo.last_name}
                      </TextBlock>
                    </Wrapper>
                  </Wrapper>

                  <Wrapper
                    wrapDirection="row"
                    wrapMargin="10px 0"
                    wrapContent="space-between"
                  >
                    <Wrapper wrapDirection="row" wrapMinWidth="auto" className="userbindCardWrap">
                      <ImageBlock src="/static/img/card.png" imgWidth={36} imgMargin="0 10px 0 0" />
                      <TextBlock color="#5b5f7d" font="20px" className='bindcardText'>
                          {userInfo.card ? userInfo.card : 'Карта не прив`язана'}
                      </TextBlock>
                    </Wrapper>
                      {userInfo.is_confirmed?
                          null
                          :
                          <Button
                          className="userbindCard"
                          theme={blueButton}
                          onClick={() => this.checkCard()}
                          >
                          Карта не прив`язана
                          </Button>
                      }
                  </Wrapper>

                </Wrapper>

                <Wrapper
                  wrapMinWidth="365px"
                  wrapPadding="10px"
                  wrapMargin="20px"
                  className="blue_box"
                >
                    <Wrapper
                        wrapItems="flex-end"
                        className="reload"
                        onClick={() => this.reloadBonus()}
                    >
                        <ImageBlock src="/static/img/reload.png" imgWidth={22} imgMargin="5px 10px" />
                    </Wrapper>
                    <Wrapper
                        wrapContent="center"
                    >
                    {
                        userInfo.bonus ?
                                <Wrapper
                                    wrapMargin="-20px 0 0 0"
                                >
                                    <Wrapper className="bonus_text_wrapper">
                                        <TextBlock className="bonus_text" font="18px">На вашому рахунку</TextBlock>
                                        <TextBlock className="bonus_text">
                              <Span
                                  className="bonus_text"
                                  font="40px"
                                  textMargin="0 10px 0 0"
                              >{userInfo.bonus?userInfo.bonus:null}</Span>
                                            <Span className="bonus_text" font="25px">бонусів</Span>
                                        </TextBlock>
                                    </Wrapper>
                                    <Wrapper
                                        wrapDirection="row"
                                        wrapContent="space-between"
                                        wrapPadding="20px 30px 10px"
                                    >
                                        <Span>Бонусна  карта</Span>
                                        <Span className="bonus_card" onClick={() => this.toggleDrawer("showZip", true)}>Штрих-код</Span>
                                    </Wrapper>
                                </Wrapper>
                            :
                            <TextBlock className="bonus_text" font="25px">немає бонусів</TextBlock>
                    }
                    </Wrapper>
                </Wrapper>
              </Wrapper>
            </Wrapper>
              :
            <Wrapper
              wrapMargin="30px 0"
              wrapPadding="0 20px"
              wrapContent="center"
            >
              <Title fontSize="25px">
                Вибачте, щось пішло не так :(
              </Title>
            </Wrapper>
            :
            <Wrapper
              wrapMargin="30px 0"
              wrapPadding="0 20px"
              wrapContent="center"
            >
              <Title fontSize="25px">
                Будь ласка, зареєструйтеся на сайті
              </Title>
            </Wrapper>
        }

      </React.Fragment>
    )
  }
}

export const User = connect(({ state }) => ({
  token: state.token,
  userToken: state.userToken,
  userInfo: state.userInfo,
  bonusInfo: state.bonusInfo,
  ticketsInfo: state.ticketsInfo,
  error: state.error,
  data: state.data,
  checkCard: state.checkCard,
  success: state.success
}))(UserComponent);
