import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import {setCurrentFilmAsync, setFilm} from '../store/actions';
import {
  Wrapper,
  TextBlock,
  Button,
  MovieContainer,
  MoviePosterWrapper,
  Title, TitleH2,
  Label,
  ImageBlock
} from "../components";

import {orangeButton} from "../themes";
import FilmScreenSliderWrapper from '../components/PresentationalComponents/FilmScreenSliderWrapper';
import FilmSliderWrapper from "../components/PresentationalComponents/FilmSliderWrapper";
import { setDateToPremier } from "../store";
import { getCookie } from "../consts";

export class OneMovieComponent extends Component {
  state = {
    tabValue: 'rent',
  };

  tabsHandler = (tabName) => {
    this.setState({ tabValue: tabName })
  };

  constructor(props) {
    super(props);
    props.dispatch(setCurrentFilmAsync({ token: getCookie('csrftoken'), url: props.location.pathname }));
  }

  componentDidMount() {

    if(Object.keys(this.props.data).length > 0) {

      let soon = [];
      let rent = [];
      if(this.props.data.soon.length <= 5) {
        soon = [
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon
        ];
      } else {
        soon = this.props.data.soon;
      }
      if(this.props.data.rent.length <= 5) {
        rent = [
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent
        ];
      } else {
        rent = this.props.data.rent;
      }
      this.setState({ soon, rent });
    }

    window.scrollTo(0, 0);
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.location.pathname !== this.props.location.pathname) {
      this.props.dispatch(
        setCurrentFilmAsync({
          token: getCookie('csrftoken'),
          url: this.props.location.pathname
        }));
    }
    if(prevState.tabValue !== this.state.tabValue) {
      return;
    }
    if(prevProps.data !== this.props.data) {

      let soon = [];
      let rent = [];
      if(this.props.data.soon.length <= 5) {
        soon = [
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon,
          ...this.props.data.soon
        ];
      } else {
        soon = this.props.data.soon;
      }
      if(this.props.data.rent.length <= 5) {
        rent = [
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent,
          ...this.props.data.rent
        ];
      } else {
        rent = this.props.data.rent;
      }
      this.setState({ soon, rent });
    }

    if(prevProps.currentFilm !== this.props.currentFilm) {

      let pictures = [];
      if(this.props.currentFilm.pictures.length <= 5) {
        pictures = [
          ...this.props.currentFilm.pictures,
          ...this.props.currentFilm.pictures,
          ...this.props.currentFilm.pictures,
          ...this.props.currentFilm.pictures,
          ...this.props.currentFilm.pictures
        ];
      } else {
        pictures = this.props.currentFilm.pictures;
      }
      this.setState({ pictures })
    }
    window.scrollTo(0, 0);
  }
  setFilmForFilter = (data) => {
    this.props.dispatch(setFilm(data.title))
    this.props.dispatch(setDateToPremier(data.film_release_date))
  };


  render() {
    const { currentFilm } = this.props;
    const { tabValue, soon, rent, pictures } = this.state;

    return (
      currentFilm &&
      <Wrapper wrapMargin="0 0 50px 0">

        <MoviePosterWrapper currentFilm={currentFilm}/>

        <Wrapper
          wrapMargin="50px 0 0"
          wrapPadding="0 60px"
          wrapItems="flex-start"
          className={classNames("one_movie_title", "smaller_padding")}
        >
          <Title fontSize="35px">
              {'Опис фільму - ' +  currentFilm.title}
          </Title>
        </Wrapper>

        <Wrapper wrapPadding="30px 40px 30px 65px" className="smaller_padding">
          <Wrapper
            wrapDirection="row"
            wrapItems="flex-start"
            noWrap
            className="one_movie_block"
          >
            <Wrapper
              wrapDirection="row"
              wrapItems="flex-start"
              wrapMinWidth="250px"
              wrapMaxWidth="250px"
              className="movie_poster"
            >

              <Wrapper
                wrapMinWidth="250px"
                wrapMaxWidth="250px"
                className={classNames("film_wrapper" , "one_movie")}
              >
                <Wrapper
                  wrapItems="flex-start"
                  wrapDirection="row"
                  className="film_labels"
                >
                  <Label blue>{currentFilm.format}</Label>
                  <Label>{currentFilm.age}+</Label>
                </Wrapper>

                <ImageBlock
                  src={currentFilm.poster}
                  alt={currentFilm.title}
                  imgWidth={250}
                  className="share_img"
                />
                <Button theme={orangeButton} className="film_btn" onClick={() => this.setFilmForFilter(currentFilm)}>
                  <Link to="/schedule/">
                    Придбати квиток
                  </Link>
                </Button>
              </Wrapper>
            </Wrapper>


            <Wrapper
              wrapMinWidth="auto"
              wrapPadding="0 0 0 20px"
              wrapItems="flex-start"
              wrapContent="space-between"
              className="film_description"
            >
              <table>
                <tbody>
                  <tr>
                    <th>Виробництво:</th>
                    <td>{currentFilm.country}</td>
                  </tr>
                  <tr>
                    <th>Жанр:</th>
                    <td>
                      {currentFilm.genres.map((genre, i) => {
                        if (i === currentFilm.genres.length - 1) {
                          return genre.name;
                        }
                        return `${genre.name}, `
                      })}
                    </td>
                  </tr>
                  <tr>
                    <th>Прем'єра:</th>
                    <td>{currentFilm.film_release_date}</td>
                  </tr>
                  <tr>
                    <th>Рік випуску:</th>
                    <td>{currentFilm.year}</td>
                  </tr>
                  <tr>
                    <th>Режисер:</th>
                    <td>{currentFilm.director}</td>
                  </tr>
                  <tr>
                    <th>Сценарій:</th>
                    <td>{currentFilm.screenplay}</td>
                  </tr>
                  <tr>
                    <th>Продюсер:</th>
                    <td>{currentFilm.producer}</td>
                  </tr>
                  <tr>
                    <th>Тривалість:</th>
                    <td>{currentFilm.duration} хвил.</td>
                  </tr>
                  <tr>
                    <th>Вікове обмеження:</th>
                    <td>{currentFilm.age}+</td>
                  </tr>
                  <tr>
                    <th>У головних ролях:</th>
                    <td>{currentFilm.actors}</td>
                  </tr>
                </tbody>

              </table>
            </Wrapper>

          </Wrapper>

          <Wrapper wrapMargin="20px 0">
            <TextBlock color="#fff" lineHeight="32px">
              {currentFilm.description}
            </TextBlock>
          </Wrapper>
        </Wrapper>
        <div style={{width: '100%', display: 'block'}}>
        <Wrapper wrapItems="flex-start" className="one_movie_frame_title">
          <div style={{width: '100%', display: 'block'}}>
          <Wrapper

            wrapItems="flex-start"
            wrapPadding="0 65px"
            wrapMargin="30px 0"
            className="smaller_padding"
          >
            <TitleH2 fontSize="35px" className="title">
              Кадри до фільму
            </TitleH2>
          </Wrapper>
          <div className={classNames("big_slider", "share_slider")} style={{width: '100%', display: 'block'}}>
            <FilmScreenSliderWrapper screens={pictures}/>
          </div>
          </div>
        </Wrapper>
</div>

        {
          soon && rent &&
          <div style={{width: '100%', display: 'block'}}>
          <Wrapper
            wrapItems="flex-start"
            wrapPadding="0 65px"
            wrapMargin="0 0 25px 0"
            className="smaller_padding"
          >

            <Wrapper
              wrapDirection="row"
              wrapItems="flex-start"
              wrapPadding="10px 0 0 0"
              wrapMargin="0 0 25px 0"
              className={classNames("bordered_title")}
            >
              <TitleH2
                className={classNames("tabs_title", {bordered_blue_title: tabValue === 'rent'})}
                fontSize="19px"
                onClick={(e) => this.tabsHandler('rent')}
              >
                Дивитися зараз
              </TitleH2>
              <TitleH2
                className={classNames("tabs_title", {bordered_blue_title: tabValue === 'soon'})}
                fontSize="19px"
                onClick={(e) => this.tabsHandler('soon')}
              >
                Дивитися незабаром
              </TitleH2>
            </Wrapper>
            {tabValue === 'rent' &&
            <div className={classNames("big_slider", "films_slider", "one_movie_bottom_slider")} style={{width: '100%', display: 'block'}}>
              <FilmSliderWrapper films={rent} rent />
            </div>
            }
            {tabValue === 'soon' &&
            <div className={classNames("big_slider", "films_slider", "one_movie_bottom_slider")} style={{width: '100%', display: 'block'}}>
              <FilmSliderWrapper films={soon} />
            </div>
            }

          </Wrapper>
            </div>
        }


      </Wrapper>
    )
  }
}

export const OneMovie = connect(({ state }) => ({
  currentFilm: state.currentFilm,
  token: state.token,
  data: state.data
}))(OneMovieComponent);


