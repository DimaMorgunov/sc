  import {
  Title,
  Wrapper,
  Button
} from "../components";
import {blueButton} from "../themes";
import ReactDisqusComments from 'react-disqus-comments';
import {FeedbackModal} from "./index";

export class Feedback extends Component {
  state = {
    feedback: false
  };

  handleNewComment = (comment) => {

  };

  toggleDrawer = (side, open) => {
    this.setState({
      feedback: false,
      [side]: open
    });
    document.body.classList.toggle('modal_opened');
  };

  render() {
    const { feedback } = this.state;

    return (
      <React.Fragment>

        <Wrapper
          wrapMargin="55px 0 20px"
          wrapPadding="0 60px"
          wrapItems="flex-start"
        >
          <Title fontSize="35px">
            Відгуки про SmartCinema
          </Title>
          <Wrapper
            wrapItems="flex-start"
            wrapMargin="30px 0"
          >
            <Button
              theme={blueButton}
              btnPadding="20px 35px"
              btnRadius="33px"
              onClick={() => this.toggleDrawer('feedback', true)}
            >
              Зворотній зв’язок
            </Button>
          </Wrapper>
          <ReactDisqusComments
            className="feedback_disqus"
            shortname="smartcinema-ua-2"
            identifier="1"
            title="Feedback SmartCinema"
            url="http://test.smartcinema.ua/feedback/"
            category_id="cinema"

            onNewComment={this.handleNewComment}
          />
        </Wrapper>

        <FeedbackModal
          show={feedback}
          closeModal={this.toggleDrawer}
        />

      </React.Fragment>
    )
  }
}
