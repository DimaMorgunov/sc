import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import {
  Wrapper,
  Title, TitleH3,
  MovieContainer, TextBlock
} from "../components";
import scrollToComponent from 'react-scroll-to-component';
import React from "react";

export class MoviesComponent extends Component {
  state = {
    currentPathname: this.props.location.pathname,
    token: this.props.token
  };

  scrollTo = () => {
    scrollToComponent(this.Future, { offset: 0, align: 'top', duration: 0});
  }
  componentDidMount(){
    if(location.href.substring(location.href.length - 7, location.href.length) == '#future'){

      setTimeout(this.scrollTo, 500);
    }
  };

  render() {
    const { data } = this.props;

    return (
      <React.Fragment>
        <Wrapper
          wrapItems="flex-start"
          wrapPadding="50px 40px"
          className="movies"
        >
          <Wrapper
            wrapItems="flex-start"
            wrapPadding="0 25px"
            className="title"
          >
            <Title blue fontSize="35px" className="title">
              Фільми в SmartCinema
            </Title>
            {this.props.cityId === 1?
                <TextBlock className="choosenCityBlock" color="#fff"  font="20px">
                  Вінниця, TЦ SkyPark
                </TextBlock>
                :
                null}
            {this.props.cityId === 2?
                <TextBlock className="choosenCityBlock" color="#fff"  font="20px">
                  Хмельницький, ТЦ WoodMall
                </TextBlock>
                :
                null}
          </Wrapper>

          <Wrapper wrapItems="flex-start">
            <Wrapper
              wrapItems="flex-start"
              wrapPadding="10px 0 0 0"
              wrapMargin="10px 25px"
              className="bordered_title"
            >
              <TitleH3 className="bordered_blue_title">
                Дивитися зараз
              </TitleH3>
            </Wrapper>
            <Wrapper
              wrapItems="flex-start"
              wrapContent="center"
              wrapDirection="row"
              wrapMargin="20px 0"
            >
              {
                Object.keys(data) != 0 &&
                  data.rent.map(film => (
                    <MovieContainer
                      film={film}
                      rent
                      hovered
                      key={film.id}
                    />
                  ))
              }
            </Wrapper>
          </Wrapper>

          <Wrapper wrapItems="flex-start">
            <Wrapper
              wrapItems="flex-start"
              wrapPadding="10px 0 0 0"
              wrapMargin="10px 25px"
              className="bordered_title"
            >
              <TitleH3 className="bordered_blue_title">
                Дивитися незабаром
              </TitleH3>
            </Wrapper>
            <Wrapper
              ref={(section) => { this.Future = section; }}
              wrapItems="flex-start"
              wrapContent="center"
              wrapDirection="row"
              wrapMargin="20px 0"
            >
              {
                Object.keys(data) != 0 &&
                data.soon.map(film => (
                  <MovieContainer
                    film={film}
                    hovered
                    key={film.id}
                  />
                ))
              }
            </Wrapper>
          </Wrapper>

        </Wrapper>
      </React.Fragment>
    )
  }
}

export const Movies = connect(({ state }) => ({
  data: state.data,
  cityId: state.cityId
}))(MoviesComponent);