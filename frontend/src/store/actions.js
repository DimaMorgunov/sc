export const SET_DATA_ASYNC = 'Set main data (async)';
export const setDataAsync = data => ({ type: SET_DATA_ASYNC, data });

export const SET_DATA = 'Set main data';
export const setData = data => ({ type: SET_DATA, data });

export const SET_SEO_ASYNC = 'Set seo of the page (async)';
export const setSeoAsync = data => ({ type: SET_SEO_ASYNC, data });

export const SET_SEO = 'Set seo of the page';
export const setSeo = data => ({ type: SET_SEO, data });

export const SET_CURRENT_FILM_ASYNC = 'Set current film (async)';
export const setCurrentFilmAsync = data => ({ type: SET_CURRENT_FILM_ASYNC, data });

export const SET_CURRENT_FILM = 'Set current film';
export const setCurrentFilm = data => ({ type: SET_CURRENT_FILM, data });

export const SET_CURRENT_SHARE_ASYNC = 'Set current share (async)';
export const setCurrentShareAsync = data => ({ type: SET_CURRENT_SHARE_ASYNC, data });

export const SET_CURRENT_SHARE = 'Set current share';
export const setCurrentShare = data => ({ type: SET_CURRENT_SHARE, data });

export const SET_CITY = 'Set chosen city';
export const setCity = data => ({ type: SET_CITY, data });

export const SET_CITY_ID = 'Set chosen city id';
export const setCityId = data => ({ type: SET_CITY_ID, data });

export const SET_CURRENT_TOKEN = 'Set current token';
export const setCurrentToken = data => ({ type: SET_CURRENT_TOKEN, data });

export const SET_SCHEDULE_ASYNC = 'Set schedule (async)';
export const setScheduleAsync = data => ({ type: SET_SCHEDULE_ASYNC, data });

export const SET_SCHEDULE = 'Set schedule to the store';
export const setSchedule = data => ({ type: SET_SCHEDULE, data });

export const GET_SCHEDULE_DATES_ASYNC = 'Get schedule dates for calendar(async)';
export const getScheduleDatesAsync = data => ({ type: GET_SCHEDULE_DATES_ASYNC, data });

export const SET_SCHEDULE_DATES = 'Set schedule dates ';
export const setScheduleDates = data => ({ type: SET_SCHEDULE_DATES, data });

export const GET_FILM_SEATS_ASYNC = 'Get film seats for canvas (async)';
export const getFilmSeatsAsync = data => ({ type: GET_FILM_SEATS_ASYNC, data });

export const SET_FILM_SEATS = 'Set film seats for canvas ';
export const setFilmSeats = data => ({ type: SET_FILM_SEATS, data });

export const SET_ERROR = 'Set error';
export const setError = data => ({ type: SET_ERROR, data });

export const SET_RESERVATION_ASYNC = 'Set film reservation to backend(async)';
export const setFilmReservationAsync = data => ({ type: SET_RESERVATION_ASYNC, data });

export const SET_RESERVATION_ASYNC_MOBILE = 'Set film reservation to backend(async)';
export const setFilmReservationAsyncMobile = data => ({ type: SET_RESERVATION_ASYNC_MOBILE, data });

export const SET_RESERVATION = 'Set film reservation to store';
export const setFilmReservation = data => ({ type: SET_RESERVATION, data });

export const UPDATE_RESERVATION_ASYNC = 'Update film reservation to backend(async)';
export const updateFilmReservationAsync = data => ({ type: UPDATE_RESERVATION_ASYNC, data });

export const CLEAR_RESERVATION_ASYNC = 'Clear film reservation to backend(async)';
export const clearFilmReservationAsync = data => ({ type: CLEAR_RESERVATION_ASYNC, data });

export const SEND_EMAIL_ASYNC = 'Send user\' to backend(async)';
export const sendEmailAsync = data => ({ type: SEND_EMAIL_ASYNC, data });

export const SEND_FEEDBACK_ASYNC = 'Send user\'s feedback to backend(async)';
export const sendFeedbackAsync = data => ({ type: SEND_FEEDBACK_ASYNC, data });

export const SET_SUCCESS = 'Set success';
export const setSuccess = data => ({ type: SET_SUCCESS, data });

export const GET_VACANCIES_FOR_SELECT_ASYNC = 'Get list all vacancies (async)';
export const getVacanciesForSelectAsync = data => ({ type: GET_VACANCIES_FOR_SELECT_ASYNC, data });

export const SET_VACANCIES_FOR_SELECT = 'Set list all vacancies';
export const setVacanciesForSelect = data => ({ type: SET_VACANCIES_FOR_SELECT, data });

export const SEND_VACANCY_SELECT_ASYNC = 'Set list all vacancies (async)';
export const sendVacancyForSelectAsync = data => ({ type: SEND_VACANCY_SELECT_ASYNC, data });

export const LOGIN_ASYNC = 'Login (async)';
export const loginAsync = data => ({ type: LOGIN_ASYNC, data });

export const SET_EMAIL = 'set email after reset pass';
export const setEmail = data => ({type: SET_EMAIL, data});

export const SET_CURRENT_FILM_TO_FILTER = 'set film to film';
export const setFilm = data => ({type: SET_CURRENT_FILM_TO_FILTER, data});

export const SET_CURRENT_DATE_TO_PREMIER = 'set date to premier';
export const setDateToPremier = data => ({type: SET_CURRENT_DATE_TO_PREMIER, data});

export const RESET_PASS_FIRST_STEP = 'reset pass first step';
export const resetPassFirstStep = data => ({ type: RESET_PASS_FIRST_STEP, data});

export const SET_USER = 'Set user (userToken)';
export const setUser = data => ({ type: SET_USER, data });

export const LOGOUT_ASYNC = 'Logout (async)';
export const logoutAsync = data => ({ type: LOGOUT_ASYNC, data });

export const DELETE_USER = 'Set user';
export const deleteUser = data => ({ type: DELETE_USER, data });

export const CHECK_USER_ASYNC = 'Check user (async)';
export const checkUserAsync = data => ({ type: CHECK_USER_ASYNC, data });

export const GET_USER_INFO_ASYNC = 'Get user info (async)';
export const getUserInfoAsync = data => ({ type: GET_USER_INFO_ASYNC, data });

export const CHECK_USER_CARD = 'Check user card';
export const checkBonusCard = data => ({ type: CHECK_USER_CARD, data });

export const SET_CHECK_USER_CARD = 'get user card';
export const setUserCard = data => ({ type: SET_CHECK_USER_CARD, data });

export const SET_USER_INFO = 'Set user info';
export const setUserInfo = data => ({ type: SET_USER_INFO, data });

export const SET_USER_NAME = 'Set user name';
export const setUserName = data => ({ type: SET_USER_NAME, data });

export const REGISTER_ASYNC = 'Register (async)';
export const registerAsync = data => ({ type: REGISTER_ASYNC, data });

export const RESET_PASS_SECOND_STEP = 'reset second step';
export const resetPassSeconStep = data => ({ type: RESET_PASS_SECOND_STEP, data });

export const ADD_BONUS_CARD = 'add bonus card';
export const addBonusCard = data => ({ type: ADD_BONUS_CARD, data });

export const SET_REGISTERED_INFO = 'Set registered info';
export const setRegisteredInfo = data => ({ type: SET_REGISTERED_INFO, data });

export const SET_BONUS_CARD = 'Bonus card - true/false';
export const setBonusCard = data => ({ type: SET_BONUS_CARD, data });

export const CONFIRM_REGISTRATION_ASYNC = 'Confirm registration (async)';
export const confirmRegistrationAsync = data => ({ type: CONFIRM_REGISTRATION_ASYNC, data });

export const REPEAT_CONFIRM_REGISTRATION_ASYNC = 'Repeat confirm registration (async)';
export const repeatConfirmRegistrationAsync = data => ({ type: REPEAT_CONFIRM_REGISTRATION_ASYNC, data });

export const SET_NEW_BONUS_CARD_ASYNC = 'Set new bonus card - plastic: true/false';
export const setNewBonusCardAsync = data => ({ type: SET_NEW_BONUS_CARD_ASYNC, data });

export const GET_BONUS_INFO_ASYNC = 'Get bonus info (async)';
export const getBonusInfoAsync = data => ({ type: GET_BONUS_INFO_ASYNC, data });

export const SET_BONUS_INFO = 'Set bonus info';
export const setBonusInfo = data => ({ type: SET_BONUS_INFO, data });

export const GET_TICKETS_INFO_ASYNC = 'Get tickets info (async)';
export const getTicketsInfoAsync = data => ({ type: GET_TICKETS_INFO_ASYNC, data });

export const SET_TICKETS_INFO = 'Set tickets info';
export const setTicketsInfo = data => ({ type: SET_TICKETS_INFO, data });

export const CHANGE_PASSWORD_ASYNC = 'Change password (async)';
export const changePasswordAsync = data => ({ type: CHANGE_PASSWORD_ASYNC, data });

export const CHANGE_USER_INFO_ASYNC = 'Change user info (async)';
export const changeUserInfoAsync = data => ({ type: CHANGE_USER_INFO_ASYNC, data });

export const RESERVATION_INFO_ASYNC = 'Reservation info (async)';
export const reservationInfoAsync = data => ({ type: RESERVATION_INFO_ASYNC, data });

export const SET_RESERVATION_INFO = 'Set reservation info';
export const setReservationInfo = data => ({ type: SET_RESERVATION_INFO, data });

export const SET_RESERVATION_INFO_MOBILE = 'Set reservation info mobile';
export const setReservationInfoMobile = data => ({ type: SET_RESERVATION_INFO_MOBILE, data });

export const SEND_TO_ANOTHER_EMAIL_ASYNC = 'Set new email for reservation (async)';
export const sendToAnotherEmailAsync = data => ({ type: SEND_TO_ANOTHER_EMAIL_ASYNC, data });