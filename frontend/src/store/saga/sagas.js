import {takeEvery, put, all} from 'redux-saga/effects';
import {
    SET_DATA_ASYNC,
    SET_SEO_ASYNC,
    SET_CURRENT_FILM_ASYNC,
    SET_CURRENT_SHARE_ASYNC,
    SET_SCHEDULE_ASYNC,
    GET_SCHEDULE_DATES_ASYNC,
    GET_FILM_SEATS_ASYNC,
    SET_RESERVATION_ASYNC,
    UPDATE_RESERVATION_ASYNC,
    CLEAR_RESERVATION_ASYNC,
    SEND_EMAIL_ASYNC,
    SEND_FEEDBACK_ASYNC,
    GET_VACANCIES_FOR_SELECT_ASYNC,
    SEND_VACANCY_SELECT_ASYNC,
    RESERVATION_INFO_ASYNC,
    SEND_TO_ANOTHER_EMAIL_ASYNC,
    SET_RESERVATION_INFO_MOBILE,
    setData,
    setSeo,
    setCurrentFilm,
    setCurrentShare,
    setSchedule,
    setScheduleDates,
    setFilmSeats,
    setError,
    setFilmReservation,
    setSuccess,
    setVacanciesForSelect,
    setReservationInfo,
    setReservationInfoMobile,
} from '../actions';

export function* getDataAsync({data}) {
    try {
        const films = yield fetch('/api/movies/', {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                cityId: data.cityId ? data.cityId : ""
            })
        });
        const dataJson = yield films.json();
        yield put(setData(dataJson))
    } catch (err) {
        console.log(err)
    }
}

export function* getSeoAsync({data}) {
    try {
        const seoData = yield fetch('/api/seo/', {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json',
            },
            body: JSON.stringify({
                "url": `${data.pathname}`,
            })
        });
        const dataJson = yield seoData.json();
        yield put(setSeo(dataJson[0]))
    } catch (err) {
        console.log(err)
    }
}

export function* getFilmDetailedInfo({data}) {
    try {
        const filmData = yield fetch('/api/movies/detail/', {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                'url': data.url
            })
        });
        const dataJson = yield filmData.json();
        yield put(setCurrentFilm(dataJson))
    } catch (err) {
        console.log(err)
    }
}

export function* getShareDetailedInfo({data}) {
    try {
        const shareData = yield fetch('/api/about_cinema/posts/', {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                'url': data.url
            })
        });
        const dataJson = yield shareData.json();
        yield put(setCurrentShare(dataJson))
    } catch (err) {
        console.log(err)
    }
}

export function* getScheduleAsync({data}) {
    try {
        const schedule = yield fetch(`/api/movies/schedule/${data.cityId}/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                year: data.year,
                month: data.month,
                day: data.day
            })
        });

        const dataJson = yield schedule.json();
        if (dataJson["Error"]) {
            yield put(setError(dataJson["Error"]));
        } else {
            yield put(setSchedule(dataJson))
        }


    } catch (err) {
        console.log(err)
    }
}

export function* getScheduleDates({data}) {
    try {
        const newData = yield fetch(`/api/movies/schedule/${data.cityId}/`);
        const dataJson = yield newData.json();

        if (dataJson["Error"]) {
            yield put(setError(dataJson["Error"]));
        } else {
            yield put(setScheduleDates(dataJson))
        }


    } catch (err) {
        console.log(err)
    }
}

export function* getFilmsSeats({data}) {
    try {
        const newData = yield fetch(`/api/reservation/reserved/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                session: data.id
            })
        });
        const dataJson = yield newData.json();

        yield put(setFilmSeats(dataJson))

    } catch (err) {
        console.log(err)
    }
}

export function* setReservation({data}) {
    const userToken = data.userToken;
    if (userToken) {
        try {
            const newData = yield fetch(`/api/reservation/`, {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Authorization': 'Token ' + data.userToken,
                    "X-CSRFToken": data.token,
                    'Content-type': 'application/json; charset=utf-8'
                },
                body: JSON.stringify({
                    pay_way: data.pay_way,
                    session: data.id,
                    places: data.places
                })
            });
            const dataJson = yield newData.json();
            if (dataJson["Error"]) {
                yield put(setError(dataJson["Error"]));
            } else {
                yield put(setFilmReservation(dataJson))
            }
        } catch (err) {
            console.log(err)
        }
    }
    if (!userToken) {
        try {
            const newData = yield fetch(`/api/reservation/`, {
                method: 'POST',
                credentials: 'include',
                headers: {
                    "X-CSRFToken": data.token,
                    'Content-type': 'application/json; charset=utf-8'
                },
                body: JSON.stringify({
                    session: data.id,
                    places: data.places
                })
            });
            const dataJson = yield newData.json();
            if (dataJson["Error"]) {
                yield put(setError(dataJson["Error"]));
            } else {
                yield put(setFilmReservation(dataJson))
            }
        } catch (err) {
            console.log(err)
        }
    }

}

export function* updateReservation({data}) {
    try {
        console.log('data', data);
        const newData = yield fetch(`/api/reservation/change_place/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                reservation_id: data.id,
                places: data.places
            })
        });
        const dataJson = yield newData.json();

        if (dataJson["Error"]) {
            yield put(setError(dataJson["Error"]));
        } else {
            console.log(dataJson);
            yield put(setFilmReservation(dataJson))
        }


    } catch (err) {
        console.log(err)
    }
}

export function* clearReservation({data}) {
    try {
        const newData = yield fetch(`/api/reservation/cancel/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                reservation_id: data.id
            })
        });
        const dataJson = yield newData.json();

        if (dataJson["Error"]) {
            yield put(setError(dataJson["Error"]));
        } else {
            yield put(setFilmReservation(null))
        }

    } catch (err) {
        console.log(err)
    }
}

export function* sendUsersEmail({data}) {
    try {
        const newData = yield fetch(`/api/reservation/change_params/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                reservation_id: data.id,
                email: data.email
            })
        });
        const dataJson = yield newData.json();

        if (dataJson.error) {
            yield put(setError(dataJson.error));
        } else {
            console.log('email saved');
        }

    } catch (err) {
        console.log(err)
    }
}

export function* sendFeedback({data}) {
    try {
        console.log(data);
        const newData = yield fetch(`/api/feedback/feedback/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                email: data.email,
                name: data.name,
                phone: data.phone,
                message: data.message,
                branch_id: data.cityId
            })
        });
        const dataJson = yield newData.json();

        if (dataJson.error) {
            yield put(setError(dataJson.error));
        } else {
            yield put(setSuccess(true));
        }

    } catch (err) {
        console.log(err)
    }
}

export function* getListOfVacancies({data}) {
    try {
        console.log(data);
        const newData = yield fetch(`/api/feedback/list/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({})
        });
        const dataJson = yield newData.json();

        if (dataJson.error) {
            yield put(setError(dataJson.error));
        } else {
            yield put(setVacanciesForSelect(dataJson));
        }

    } catch (err) {
        console.log(err)
    }
}

export function* sendVacancy({data}) {
    try {
        console.log(data);

        const formData = new FormData();

        formData.append('resume', data.file);
        formData.append('email', data.email);
        formData.append('first_name', data.first_name);
        formData.append('last_name', data.last_name);
        formData.append('phone', data.phone);
        formData.append('branch_id', data.cityId);
        formData.append('vacancies_id', data.vacancies_id);

        const newData = yield fetch(`/api/feedback/summary/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
            },
            body: formData
        });
        const dataJson = yield newData.json();

        if (dataJson.error) {
            yield put(setError(dataJson.error));
        } else {
            yield put(setSuccess(true));
        }

    } catch (err) {
        console.log(err)
    }
}

export function* getReservationInfo({data}) {
    try {
        const newData = yield fetch(`/api/reservation/info/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                reservation: data.reservation
            })
        });
        const dataJson = yield newData.json();

        if (!dataJson.movie_info) {
            yield put(setError(dataJson.error));
        } else {
            yield put(setReservationInfo(dataJson));
        }

    } catch (err) {
        console.log(err)
    }
}

export function* sendToNewEmail({data}) {
    try {
        console.log(data);
        const newData = yield fetch(`/api/reservation/resend-ticket/`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                reservation: data.reservation,
                new_email: data.new_email
            })
        });
        const dataJson = yield newData.json();
        if (!dataJson.result) {
            yield put(setError(dataJson.error));
        }

    } catch (err) {
        console.log(err)
    }
}

export function* watchAll() {
    yield all([
        takeEvery(SET_RESERVATION_INFO_MOBILE, setReservationInfoMobile),
        takeEvery(SET_DATA_ASYNC, getDataAsync),
        takeEvery(SET_SEO_ASYNC, getSeoAsync),
        takeEvery(SET_CURRENT_FILM_ASYNC, getFilmDetailedInfo),
        takeEvery(SET_CURRENT_SHARE_ASYNC, getShareDetailedInfo),
        takeEvery(SET_SCHEDULE_ASYNC, getScheduleAsync),
        takeEvery(GET_SCHEDULE_DATES_ASYNC, getScheduleDates),
        takeEvery(GET_FILM_SEATS_ASYNC, getFilmsSeats),
        takeEvery(SET_RESERVATION_ASYNC, setReservation),
        takeEvery(UPDATE_RESERVATION_ASYNC, updateReservation),
        takeEvery(CLEAR_RESERVATION_ASYNC, clearReservation),
        takeEvery(SEND_EMAIL_ASYNC, sendUsersEmail),
        takeEvery(SEND_FEEDBACK_ASYNC, sendFeedback),
        takeEvery(GET_VACANCIES_FOR_SELECT_ASYNC, getListOfVacancies),
        takeEvery(SEND_VACANCY_SELECT_ASYNC, sendVacancy),
        takeEvery(RESERVATION_INFO_ASYNC, getReservationInfo),
        takeEvery(SEND_TO_ANOTHER_EMAIL_ASYNC, sendToNewEmail),
    ]);
}