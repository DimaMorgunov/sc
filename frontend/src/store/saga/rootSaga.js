import { all } from 'redux-saga/effects';
import { watchAll } from './sagas';
import { watchAllLogin } from './loginSagas';

export function* rootSaga() {
  yield all([
    watchAll(),
    watchAllLogin()
  ])
}