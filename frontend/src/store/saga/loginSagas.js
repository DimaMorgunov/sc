import { takeEvery, put, all } from 'redux-saga/effects';
import {
  LOGIN_ASYNC,
  LOGOUT_ASYNC,
  CHECK_USER_ASYNC,
  GET_USER_INFO_ASYNC,
  REGISTER_ASYNC,
  CONFIRM_REGISTRATION_ASYNC,
  RESET_PASS_SECOND_STEP,
  REPEAT_CONFIRM_REGISTRATION_ASYNC,
  SET_NEW_BONUS_CARD_ASYNC,
  GET_BONUS_INFO_ASYNC,
  GET_TICKETS_INFO_ASYNC,
  CHANGE_PASSWORD_ASYNC,
  CHANGE_USER_INFO_ASYNC,
  RESET_PASS_FIRST_STEP,
  ADD_BONUS_CARD,
  SET_BONUS_INFO,
  CHECK_USER_CARD,
  SET_CHECK_USER_CARD,
  setUserCard,
  setError,
  setUser,
  deleteUser,
  setCurrentToken,
  setUserInfo,
  setUserName,
  setBonusCard,
  setSuccess,
  setRegisteredInfo,
  setBonusInfo,
  setTicketsInfo
} from "../actions";

export function* loginUser({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/login/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        username: data.username,
        password: data.password
      })
    });
    const dataJson = yield newData.json();

      if(newData.status !== 200){
          throw dataJson
      }

      yield put(setUser(dataJson.key));
      yield put(setUserInfo(dataJson));
      yield put(setCurrentToken(dataJson.csrf));
      yield put(setUserName(dataJson.first_name));
      yield put(setSuccess(dataJson))

  } catch(err) {
      yield put(setError({'SET_ERROR': err}));
  }
}

export function* addBonusCard({ data }) {
    try {
        const newData = yield fetch(`/api/accounts/card/confirm/`, {
            method: 'POST',
            credentials: 'include',
            headers:{
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8',
                'Authorization': 'Token ' + data.userToken
            },
            body: JSON.stringify({
                key: data.key,
            })
        });
        const dataJson = yield newData.json();
          if(!newData.status !== 200){
            throw dataJson
          }
    } catch(err) {
        yield put(setError(err));
    }
}
export function* checkBonusCard({ data }) {

    try {
        const newData = yield fetch(`/api/accounts/card/`, {
            method: 'POST',
            credentials: 'include',
            headers:{
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8',
                'Authorization': 'Token ' + data.userToken
            },
        });

        const dataJson = yield newData.json();
        yield put(setUserCard(dataJson))
        yield put(setSuccess(dataJson))

    } catch(err) {
        yield put(setError(err));
    }
}

export function* resetPassFirstStep({ data }) {
    yield put(setSuccess(false));
    try {
        const newData = yield fetch(`/api/accounts/password/reset/`, {
            method: 'POST',
            credentials: 'include',
            headers:{
                "X-CSRFToken": data.token,
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                email: data.email,
            })
        });
        const dataJson = yield newData.json();
        if(newData.status !== 200){
            throw dataJson
        }
        yield put(setSuccess(dataJson));


    } catch(err) {
        yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
    }
}

export function* registerUser({ data }) {
    yield put(setSuccess(null));
    yield put(setError(null));
  try {
    const newData = yield fetch(`/api/accounts/sign-up/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        username: data.username,
        password: data.password,
        first_name: data.first_name,
        last_name: data.last_name,
        phone: data.phone,
        most_important_city: data.most_important_city,
        birthday: data.birthday
      })
    });

    const dataJson = yield newData.json();
      if(newData.status !== 200){
          throw dataJson
      }
      if (dataJson.token) {
          yield put(setSuccess(true));
          yield put(setUser(dataJson.token));
          yield put(setUserInfo(dataJson.user));
          yield put(setCurrentToken(dataJson.csrf));
          yield put(setUserName(dataJson.user.first_name));
      }

      /*if(dataJson.hasOwnProperty('card')) {
          yield put(setBonusCard(dataJson.card));
          yield put(setRegisteredInfo({ userRegisterInfo: {
              username: data.username,
              city: data.most_important_city
          }}));
          yield put(setSuccess(true));
      }*/
  } catch(err) {
    yield put(setError({'SET_ERROR': err.errors}));
  }
}

export function* confirmRegisterUser({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/confirm/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        username: data.username,
        key: data.key
      })
    });
    const dataJson = yield newData.json();

    if(!dataJson.token) {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
    } else {
      yield put(setSuccess(true));
      yield put(setUser(dataJson.token));
      yield put(setUserInfo(dataJson.user));
      yield put(setCurrentToken(dataJson.csrf));
      yield put(setUserName(dataJson.user.first_name));
    }

  } catch(err) {
    yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* repeatConfirmRegisterUser({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/confirm/new_key/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        username: data.username
      })
    });
    const dataJson = yield newData.json();

    if(!dataJson.result) {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
    } else {
      yield put(setSuccess('Дякуємо, ми відправили вам новий код'));
    }

  } catch(err) {
    yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* logoutUser({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/logout/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        'Authorization': 'Token ' + data.userToken,
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({})
    });
    const dataJson = yield newData.json();

    if(dataJson.error) {
      yield put(setError(dataJson.error));
    } else {
      yield put(deleteUser(null));
    }

  } catch(err) {}
}

export function* checkUser() {
  try {
    const newData = yield fetch(`/api/accounts/check/`);
    const dataJson = yield newData.json();

    if(!dataJson.error) {
      yield put(setUser(dataJson.key));

      yield put(setCurrentToken(dataJson.csrf));
      yield put(setUserName(dataJson.first_name));
    }

  } catch(err) {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* getInfo({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/user/`, {
      method: 'GET',
      credentials: 'include',
      headers:{
        'Authorization': 'Token ' + data.userToken,
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      }
    });
    const dataJson = yield newData.json();

      if(newData.status !== 200){
          throw dataJson
      }

    if(dataJson.error) {
      yield put(setError(dataJson.error));
    } else {
      yield put(setUserInfo(dataJson));
    }

  } catch(err) {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* setNewCard({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/create/new_card/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        username: data.username,
        city_id: data.city_id,
        plastic: data.plastic
      })
    });
    const dataJson = yield newData.json();

    if(dataJson.result) {
      yield put(setSuccess(true));
    } else {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
    }

  } catch(err) {
    yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* getBonusInfo({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/get_bonus_info/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        'Authorization': 'Token ' + data.userToken,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({})
    });

    const dataJson = yield newData.json();

      yield put(setBonusInfo(dataJson));
      yield put(setSuccess(true));

  } catch(err) {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* getTicketsInfo({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/user_tickets/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        'Authorization': 'Token ' + data.userToken,
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({})
    });
    const dataJson = yield newData.json();

    if(!dataJson.detail) {
      yield put(setTicketsInfo(dataJson));
    } else {
        yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
    }

  } catch(err) {
      yield put(setError("Упс, щось пішло не так. Спробуйте ще раз"));
  }
}

export function* changeUsersPassword({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/password/change/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        'Authorization': 'Token ' + data.userToken,
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        new_password1: data.password1,
        new_password2: data.password2
      })
    });
    const dataJson = yield newData.json();

      if(!newData.status !== 200){
          throw dataJson
      }
      if(dataJson.result) {
          yield put(setSuccess(true));
      }

  } catch(err) {
      yield put(setError(err));
  }
}

export function* changeUserInfo({ data }) {
  try {
    const newData = yield fetch(`/api/accounts/user_update/`, {
      method: 'POST',
      credentials: 'include',
      headers:{
        'Authorization': 'Token ' + data.userToken,
        "X-CSRFToken": data.token,
        'Content-type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
          first_name: data.first_name,
          last_name: data.last_name,
          most_important_city: data.most_important_city,
          email: data.email,
      })
    });
    const dataJson = yield newData.json();
      if(!newData.status !== 200){
          throw dataJson
      }
    if(dataJson.result) {
      yield put(setSuccess(true));
    }
  } catch(err) {
      yield put(setError(err));
  }
}
export function* resetPassSecondStep({ data }) {
    try {
        const newData = yield fetch(`/api/accounts/password/reset/confirm/`, {
            method: 'POST',
            credentials: 'include',
            headers:{
                'Content-type': 'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                token: data.token,
                uid: data.uid,
                new_password1: data.password1,
                new_password2: data.password2,
            })
        });
        const dataJson = yield newData.json();

        if(newData.status === 200){

            yield put(setSuccess("RESET_PASS_SECOND"));
        }else{

            throw dataJson
        }
    } catch(err) {
        yield put(setError(err));
    }
}

export function* watchAllLogin() {
  yield all([
      takeEvery(SET_CHECK_USER_CARD, setUserCard),
    takeEvery(CHECK_USER_CARD, checkBonusCard),
    takeEvery(SET_BONUS_INFO, setBonusInfo),
    takeEvery(RESET_PASS_SECOND_STEP ,resetPassSecondStep),
    takeEvery(ADD_BONUS_CARD ,addBonusCard),
    takeEvery(RESET_PASS_FIRST_STEP, resetPassFirstStep),
    takeEvery(LOGIN_ASYNC, loginUser),
    takeEvery(LOGOUT_ASYNC, logoutUser),
    takeEvery(CHECK_USER_ASYNC, checkUser),
    takeEvery(GET_USER_INFO_ASYNC, getInfo),
    takeEvery(REGISTER_ASYNC, registerUser),
    takeEvery(CONFIRM_REGISTRATION_ASYNC, confirmRegisterUser),
    takeEvery(REPEAT_CONFIRM_REGISTRATION_ASYNC, repeatConfirmRegisterUser),
    takeEvery(SET_NEW_BONUS_CARD_ASYNC, setNewCard),
    takeEvery(GET_BONUS_INFO_ASYNC, getBonusInfo),
    takeEvery(GET_TICKETS_INFO_ASYNC, getTicketsInfo),
    takeEvery(CHANGE_PASSWORD_ASYNC, changeUsersPassword),
    takeEvery(CHANGE_USER_INFO_ASYNC, changeUserInfo),
  ])
}