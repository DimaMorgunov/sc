import {
  SET_DATA,
  SET_CITY,
  SET_SEO,
  SET_CURRENT_TOKEN,
  SET_CURRENT_FILM,
  SET_CURRENT_SHARE,
  SET_SCHEDULE,
  SET_CITY_ID,
  SET_SCHEDULE_DATES,
  SET_FILM_SEATS,
  SET_ERROR,
  SET_RESERVATION,
  SET_SUCCESS,
  SET_VACANCIES_FOR_SELECT,
  SET_USER,
  DELETE_USER,
  SET_USER_INFO,
  SET_USER_NAME,
  SET_BONUS_CARD,
  SET_REGISTERED_INFO,
  SET_BONUS_INFO,
  SET_TICKETS_INFO,
  SET_RESERVATION_INFO,
  SET_CHECK_USER_CARD,
  SET_CURRENT_FILM_TO_FILTER,
  SET_CURRENT_DATE_TO_PREMIER,
} from './actions';

const initState = {
  data: {},
  city: window.localStorage.getItem('chosen_city') ? window.localStorage.getItem('chosen_city') : null,
  cityId: window.localStorage.getItem('chosen_city_id') ? +window.localStorage.getItem('chosen_city_id') : null,
  seo: null,
  token: null,
  currentFilm: null,
  userLogin: false,
  currentShare: null,
  schedule: null,
  scheduleDates: null,
  filmSeats: null,
  error: null,
  reservation: null,
  success: false,
  vacanciesList: null,
  userToken: null,
  userInfo: null,
  userName: null,
  bonusCard: false,
  userRegisterInfo: null,
  bonusInfo: null,
  ticketsInfo: null,
  reservationInfo: null,
  checkCard: null,
  currentFilmToFilter: null,
  currentPremierFilm: null
};

export const state = (state = initState, { type, data }) => {
  switch (type) {

    case SET_CURRENT_DATE_TO_PREMIER:{

      return {...state, currentPremierFilm: data}
    }

    case SET_CURRENT_FILM_TO_FILTER: {

      return {...state, currentFilmToFilter: data}
    }

    case SET_CHECK_USER_CARD: {
      return {...state, checkCard: data}
    }

    case SET_DATA: {
      return { ...state, data: data };
    }

    case SET_CITY: {
      return { ...state, city: data };
    }

    case SET_SEO: {
      return { ...state, seo: data };
    }

    case SET_CURRENT_TOKEN: {
      return { ...state, token: data };
    }

    case SET_CURRENT_FILM: {
      return { ...state, currentFilm: data };
    }

    case SET_CURRENT_SHARE: {
      return { ...state, currentShare: data };
    }

    case SET_SCHEDULE: {
    return { ...state, schedule: data };
  }

    case SET_SCHEDULE_DATES: {
      return { ...state, scheduleDates: data };
    }

    case SET_FILM_SEATS: {
      return { ...state, filmSeats: data };
    }

    case SET_CITY_ID: {
      return { ...state, cityId: data };
    }

    case SET_ERROR: {
      return { ...state, error: data };
    }

    case SET_RESERVATION: {
      return { ...state, reservation: data };
    }

    case SET_SUCCESS: {
      return { ...state, success: data };
    }

    case SET_VACANCIES_FOR_SELECT: {
      return { ...state, vacanciesList: data };
    }

    case SET_USER: {
      return { ...state, userToken: data, userLogin: true };
    }

    case DELETE_USER: {
      return {
        ...state,
        user: null,
        userLogin: false,
        success: false,
        userName: null,
        userInfo: null,
        userToken: null,
        userRegisterInfo: null
      };
    }

    case SET_USER_INFO: {
      return { ...state, userInfo: data };
    }

    case SET_USER_NAME: {
      return { ...state, userName: data, userLogin: true };
    }

    case SET_BONUS_CARD: {


      return { ...state, bonusCard: data };
    }

    case SET_REGISTERED_INFO: {
      return { ...state, userRegisterInfo: data.userRegisterInfo };
    }

    case SET_BONUS_INFO: {
      return { ...state, bonusInfo: data };
    }

    case SET_TICKETS_INFO: {
      return { ...state, ticketsInfo: data };
    }

    case SET_RESERVATION_INFO: {
      return { ...state, reservationInfo: data };
    }
  }

  return state;
};

