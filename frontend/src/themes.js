export const orangeButton = {
  backgroundColor: '#fd5f00',
  hoveredBackgroundColor: '#ff841b'
};

export const blueButton = {
  backgroundColor: '#009ad7',
  hoveredBackgroundColor: '#42b3d7'
};