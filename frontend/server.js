let webpack = require('webpack');
let WebpackDevServer = require('webpack-dev-server');
let config = require('./webpack-dev.config');
let host = 'localhost';
let port = '3000';

if(process.argv[4]){
    host = process.argv[4];
}
if(process.argv[5]){
    port = process.argv[5];
}

new WebpackDevServer(webpack(config), {
  publicPath: 'http://'+host+':'+port+"/assets/bundles/",
  hot: true,
  inline: true,
  historyApiFallback: true,
  headers: { 'Access-Control-Allow-Origin': '*' }
}).listen(port, host, function (err, result) {
  if (err) {

  }


});