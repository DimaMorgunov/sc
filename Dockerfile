FROM python:3.7.1
MAINTAINER Dovgal Oleksii <dovgaloleksii@gmail.com>

ENV PYTHONUNBUFFERED 1
RUN mkdir -p cinemaproject

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get update && apt-get -y install gcc nodejs npm libxml2-dev libxslt1-dev libxslt-dev python-lxml pgloader build-essential
RUN pip install lxml gevent
COPY /deploy_config /cinemaproject/deploy_config

WORKDIR /cinemaproject/deploy_config
RUN pip install -r requirements.txt

WORKDIR /cinemaproject/frontend
RUN rm -rf node_modules