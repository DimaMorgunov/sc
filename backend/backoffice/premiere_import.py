from movie.models import *
from premiere.premiere_import import PremieraImport
import logging
logger = logging.getLogger('premiere')
import datetime

def premiere_import(branch):
    logger.info("[Premiere import] Start Import for %s " % branch.HOST)
    PI = PremieraImport(branch)
    logger.info("[Premiere import] Import Hall %s" % branch.HOST)
    hall_set = PI.get_hall()
    save_hall(hall_set, branch)
    logger.info("[Premiere import] Import Movie %s " % branch.HOST)
    movie_set = PI.get_movies()
    save_movies(movie_set, branch)
    logger.info("[Premiere import] Import Session %s " % branch.HOST)
    session_set = PI.get_session()
    save_sessions(session_set, branch)
    logger.info("[Premiere import] End Import for %s " % branch.HOST)


def save_hall(data_set, branch):
    for item in data_set:
        item['branch'] = branch
        try:
            obj = Hall.objects.get(hall_id=item['hall_id'], branch=branch),
            obj[0].save()
        except Hall.DoesNotExist:
            obj = Hall.objects.create(**item)
            obj.save()


def save_movies(data_set, branch):

    for item in data_set:
        try:
            obj = MovieToBranch.objects.get(premiere_id=item['premiere_id'], branch_id=branch)
            item['branch_id'] = branch
            for key, value in item.items():
                setattr(obj, key, value)
            obj.save()
        except MovieToBranch.DoesNotExist:
            obj = MovieToBranch.objects.create(
                premiere_id=item['premiere_id'],
                premiere_name=item['premiere_name'],
                branch_id=branch,
            )
            obj.save()


def save_sessions(data_set, branch):
    cur_time = datetime.datetime.now()
    Showtime.objects.filter(
        hall_id__branch=branch,
        start__day__gte=cur_time.day,
        start__month__gte=cur_time.month,
        start__year__gte=cur_time.year,
        start__hour__gte=cur_time.hour - 1,
    ).update(public=False)

    for item in data_set:
        data = {
            'hall_id': Hall.objects.get(hall_id=item['hall_id'], branch=branch),
            'movietobranch_id': MovieToBranch.objects.get(premiere_id=item['movie_id'], branch_id=branch),
            'start': item['date'],
            'premiere_id': item['premiere_id'],
            'standard_price': int(item['price'].get('Стандартные места', 0)) / 100,
            'vip_price': int(item['price'].get('Диваны', 0)) / 100,
        }
        try:
            obj = Showtime.objects.get(premiere_id=data['premiere_id'], movietobranch_id__branch_id=branch)
            for key, value in data.items():
                setattr(obj, key, value)
        except Showtime.DoesNotExist:
            obj = Showtime(**data)

        try:
            obj.show_format = MovieFormat.objects.get(slug=item['tehnology'], )
        except MovieFormat.DoesNotExist:
            obj.show_format = MovieFormat.objects.create(slug=item['tehnology'], )

        obj.public = True
        obj.save()


