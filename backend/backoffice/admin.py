from django.contrib import admin
from .models import *
from . import premiere_import


def import_session(self, request, queryset):
    for branch in queryset:
        # import from premiere
        premiere_import.premiere_import(branch)
        self.message_user(request, "Успешно импортированы сеансы для кинотеатра " + branch.__str__())


import_session.short_description = "Импортировать фильмы"


class RowInline(admin.TabularInline):
    model = Row
    extra = 1


class BranchAdmin(admin.ModelAdmin):
    ordering = ["id"]
    actions = [import_session]
    list_filter = ["city", ]
    list_display = ["id","name", "city", "HOST", "PORT", ]
    search_fields = ["name", "HOST", ]
    fieldsets = (
        ('LiqPay', {
            'fields': ('cash_keeper',),
        }),
        ('TV', {
            'fields': ('scale',),
        }),
        ('Стандартная информация', {
            'fields': ('city', 'address', 'name', 'cabinet', ),
                       }),
        ('Email', {
            'fields': ('address_for_email_template', 'phone', ),
        }),
        ('Настройки', {
            'classes': ('collapse',),
            'fields': (
                'HOST', 'PORT', 'BUFFER_SIZE', 'TIMEOUT', 'ENCODING', 'DISCOUNT_ACCOUNT_TYPE_ID',
                'VERSION', 'ADMIN_CARDCODE', 'SERVICE_ID', 'DEFAULT_CARD_NUMBER', 'DEFAULT_CARD_PASSWORD',
                'lat', 'lon'
                ),
        }),
    )

    class Meta:
        model = Branch
        verbose_name = "Отделение"
        verbose_name_plural = "Отделения"


class HallAdmin(admin.ModelAdmin):
    ordering = ["id"]
    inlines = [RowInline]
    list_filter = ["branch", ]
    list_display = ["name", "branch", ]
    search_fields = ["name", "HOST", ]

    class Meta:
        model = Branch
        verbose_name = "Зал"
        verbose_name_plural = "Залы"


class KeeperAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_filter = ["name", ]
    list_display = ["name", "LIQPAY_KEY_PUBLIC", "is_in_branch", "LIQPAY_Active"]
    search_fields = ["LIQPAY_KEY_PUBLIC", ]

    def is_in_branch(self, obj):
        try:
            return Branch.objects.get(cash_keeper=obj).name
        except Branch.DoesNotExist:
            return "-"

    is_in_branch.short_description = "Работает"


    class Meta:
        model = CashCeeper
        verbose_name = "Получатель"
        verbose_name_plural = "Получатели"


class CabinetAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_filter = ["name", ]
    list_display = ["name", "HOST", "PORT", ]
    search_fields = ["HOST", ]

    class Meta:
        model = Cabinet
        verbose_name = "Получатель"
        verbose_name_plural = "Получатели"


admin.site.register(City)
admin.site.register(Cabinet, CabinetAdmin)
admin.site.register(Branch, BranchAdmin)
admin.site.register(Hall, HallAdmin)
admin.site.register(CashCeeper, KeeperAdmin)
