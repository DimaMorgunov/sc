from backoffice.models import Branch, City, Hall, Row
from rest_framework import serializers


class BranchGlobalSerializer(serializers.ModelSerializer):
    city = serializers.CharField(
        min_length=3,
        source='city.name'
    )

    class Meta:
        model = Branch
        fields = ("id", "name", "address", "phone", "lon", "lat", "city", )
        depth = 5


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ("id", )


class HallSerializer(serializers.ModelSerializer):
    row_list = serializers.SerializerMethodField()

    def get_row_list(self, obj):
        rows = Row.objects.filter(hall=obj).order_by('row_verbose')

        serialized_data = RowSerializer(
            instance=rows,
            context=self.context,
            many=True
        )

        return {index+1: serialized_data.data[index] for index in range(len(serialized_data.data))}

    class Meta:
        model = Hall
        fields = ('row_list', 'hall_height', 'hall_width', 'row_numbering_direction',
                  'left_text_position', 'right_text_position')


class RowSerializer(serializers.ModelSerializer):
    except_place = serializers.SerializerMethodField()

    def get_except_place(self, obj):
        res_list = []
        busy_places = self.context.get('places', False)
        row_id = obj.row_verbose
        for row_place in busy_places:
            if int(row_place['row']) == row_id:
                res_list.append(int(row_place['place']))

        res_list.sort()
        return res_list

    class Meta:
        model = Row
        fields = ('length', 'row_verbose', 'place_type', 'except_place', 'x_position', 'y_position', )
