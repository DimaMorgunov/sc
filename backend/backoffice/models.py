from django.db import models

PLACE_CHOICES = (
    (1, 'Кресла'),
    (2, 'Диваны'),
)

DIRECTIONS_CHOICES = (
    ('ltr', 'C лева на право'),
    ('rtl', 'C права на лево')
)

class City(models.Model):
    name = models.CharField(
        max_length=128,
        help_text="Укажите название зала",
        verbose_name="Название")

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"

    def __str__(self):
        return self.name


class Cabinet(models.Model):
    name = models.CharField(
        max_length=128,
        help_text="Укажите читаемое название",
        verbose_name="Название")

    HOST = models.GenericIPAddressField(
        help_text="Ip адрес сервера премьеры, например 46.164.140.22",
        verbose_name="Ip",
        default='46.164.140.22',
    )

    PORT = models.IntegerField(
        help_text=" Port сервера премьеры, например 9194",
        verbose_name="Port",
        default=9191,
    )

    TERMINAL_TYPE = models.IntegerField(
        verbose_name="номер службы",
        default=2,
    )

    TIMEOUT = models.IntegerField(
        help_text="Время ожидания ответа от сокета",
        default=60,
    )

    BUFFER_SIZE = models.IntegerField(
        verbose_name="Размер буфера",
        default=1024,
    )

    GLOBAL_TYPE = models.CharField(
        max_length=128,
        verbose_name="Код верификации",
        default='1TSZcMN9RczFF0kZWLQk',
    )

    INITIAL_TRANSACTION_RANGE_DAYS = models.IntegerField(
        help_text="Период за который выводиться история транзакций",
        verbose_name="Период отображения транзакций",
        default=90,
    )
    INITIAL_TRANSACTION_CACHE_DAYS = models.IntegerField(
        help_text="Период кеширования данных о бонусных картах на сервере",
        verbose_name="Период хранения транзакций",
        default=365,
    )
    BONUS_ACCOUNT_TYPE_ID = models.IntegerField(
        help_text="Id бонусного кабинета",
        verbose_name="Id бонусного кабинета",
        default=1,
    )
    CONTACT_PHONE_TYPE_ID = models.IntegerField(
        help_text="Код получаемый от системы CRM если контакт по умолчанию телефон",
        verbose_name="Тип контакта телефон",
        default=250,
    )

    EMAIL_TYPE_ID = models.IntegerField(
        help_text="Код получаемый от системы CRM если контакт по умолчанию email",
        verbose_name="Тип контакта email",
        default=251,
    )

    DEFAULT_TYPE_ID = models.IntegerField(
        help_text="Отправка информации по умолчанию",
        verbose_name="Стандартный тип контакта",
        default=250,
    )

    class Meta:
        verbose_name = "CRM сервер"
        verbose_name_plural = "СRM сервера"

    def __str__(self):
        return self.name


class CashCeeper(models.Model):
    name = models.CharField(
        max_length=128,
        unique=True,
        help_text="Укажите имя получателя",
        verbose_name="Имя/Название")

    LIQPAY_KEY_PRIVATE = models.CharField(
        verbose_name="Liqpay private key",
        help_text="Приватный ключ получателя",
        max_length=256,
    )

    LIQPAY_KEY_PUBLIC = models.CharField(
        verbose_name="Liqpay public key",
        help_text="Публичный ключ получателя без i",
        max_length=256,
    )

    LIQPAY_Active = models.BooleanField(
        verbose_name="Активен",
        help_text="Работает предприниматель?",
        default=False,
    )

    class Meta:
        verbose_name = "Предпринематель"
        verbose_name_plural = "Предпринематель"

    def __str__(self):
        return self.name


class Branch(models.Model):
    name = models.CharField(
        max_length=128,
        help_text="Укажите название отделения",
        verbose_name="Название",
    )

    address = models.CharField(
        max_length=200,
        default='ул. Козицкого 51, ТЦ «Sky Рark», 4 этаж',
        verbose_name="Адресс",
    )

    lon = models.CharField(
        max_length=100,
        default="49.2",
        verbose_name="Широта",
    )

    lat = models.CharField(
        max_length=100,
        default="28.4",
        verbose_name="Долгота",
    )

    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        verbose_name="Город"
        )

    cabinet = models.ForeignKey(
        Cabinet,
        on_delete=models.CASCADE,
        verbose_name="CRM система",
        default=None,
        )

    cash_keeper = models.ForeignKey(
        CashCeeper,
        on_delete=models.CASCADE,
        verbose_name='Получатель денег',
        blank=True,
        null=True,
    )

    HOST = models.GenericIPAddressField(
        help_text="Ip адрес сервера премьеры, например 46.164.140.22",
        verbose_name="Ip",
        default='46.164.140.22',
    )

    PORT = models.IntegerField(
        help_text="Port сервера премьеры, например 9194",
        verbose_name="Port",
        default=9194,
    )

    DEFAULT_CARD_NUMBER = models.IntegerField(
        help_text="Номер стандартной карты в CRM, например 9194",
        verbose_name="Card",
        default=1001,
    )

    DEFAULT_CARD_PASSWORD = models.IntegerField(
        help_text="Пароль стандартной карты в CRM, например 1234",
        verbose_name="Pass",
        default=1234
    )

    DISCOUNT_ACCOUNT_TYPE_ID = models.IntegerField(
        help_text="Id определяющий тип дисконта",
        verbose_name="Id аккаунта",
        default=1,
    )

    SERVICE_ID = models.IntegerField(
        help_text="Номер шлюза в ПО премьера",
        verbose_name="Шлюз",
        default=3,
    )

    ADMIN_CARDCODE = models.IntegerField(
        help_text="Номер админ карты карты в CRM, например 1003",
        verbose_name="Карта администратора",
        default=1003,
    )

    VERSION = models.IntegerField(
        verbose_name="Версия премьеры",
        default=3,
    )

    ENCODING_CHOICES = (
        ('W1251', 'Windows-1251'),
        ('utf-8', 'utf-8'),
    )

    ENCODING = models.CharField(
        help_text="Кодировка исходящих сообщений от Премьеры. Например Windows-1251",
        verbose_name="Кодировка",
        choices=ENCODING_CHOICES,
        default='W1251',
        max_length=128,
    )

    TIMEOUT = models.IntegerField(
        help_text="Время ожидания ответа от премьеры",
        default=60,
    )

    BUFFER_SIZE = models.IntegerField(
        verbose_name="Размер буфера",
        default=1024,
    )

    address_for_email_template = models.CharField(
        help_text="Адрес кинотеатра",
        verbose_name="Адрес",
        default='ул. Козицкого 51, ТЦ «Sky Рark», 4 этаж',
        max_length=128,
    )

    phone = models.CharField(
        help_text="Телефон тех поддержки",
        verbose_name="Телефон",
        default='(096) 003 50 50',
        max_length=128,
    )

    scale = models.IntegerField(
        help_text="Масштаб для телевизоров",
        verbose_name="Масштаб",
        default=100,
    )

    class Meta:
        verbose_name = "Отделение"
        verbose_name_plural = "Отделения"

    def __str__(self):
        return self.name


class Hall(models.Model):
    branch = models.ForeignKey(
        Branch,
        on_delete=models.CASCADE,
        verbose_name="Отделения"
        )

    hall_id = models.IntegerField(
        help_text="ID зала. Берется из базы данных Rkeeper",
        verbose_name="номер")

    verbose = models.IntegerField(
        help_text="Номер зала который будет показан клиентам",
        verbose_name="отображаемый номер",
        default=1,
        blank=True,
        null=True,
    )

    premiere_layer = models.IntegerField(
        help_text="Слой на котором размещенны места",
        verbose_name="Слой",
        default=1,
        blank=True,
        null=True,
    )

    name = models.CharField(
        max_length=128,
        help_text="Укажите название зала",
        verbose_name="Название")

    row_numbering_direction = models.CharField(
        help_text="Напрвление мест в ряду",
        verbose_name="Направление",
        choices=DIRECTIONS_CHOICES,
        default='ltr',
        max_length=128,
    )

    left_text_position = models.PositiveIntegerField(
        help_text="В пикселях",
        verbose_name="Положение нумерации рядов с лева",
        default=0
    )

    right_text_position = models.PositiveIntegerField(
        help_text="В пикселях",
        verbose_name="Положение нумерации рядов с права",
        default=0
    )

    hall_height = models.PositiveIntegerField(
        help_text="Высота зала в пикселей",
        verbose_name="Высота",
        default=0
    )

    hall_width = models.PositiveIntegerField(
        help_text="Ширина зала в пикселях ",
        verbose_name="Ширина",
        default=0
    )

    class Meta:
        verbose_name = "Зал"
        verbose_name_plural = "Залы"
        unique_together = ('hall_id', 'branch',)

    def __str__(self):
        return self.name


class Row(models.Model):

    hall = models.ForeignKey(
        Hall,
        on_delete=models.CASCADE,
        verbose_name='Зал'
    )

    length = models.PositiveIntegerField(
        help_text="Длина ряда",
        verbose_name="Мест",
    )

    place_type = models.IntegerField(
        help_text="Тип мест в ряду",
        verbose_name="Тип",
        choices=PLACE_CHOICES,
        default=1,
    )

    row_verbose = models.IntegerField(
        help_text="Номер ряда",
        verbose_name="Номер",
    )

    x_position = models.PositiveIntegerField(
        help_text="Координаты начала рядa по X",
        verbose_name="X",
    )

    y_position = models.PositiveIntegerField(
        help_text="Координаты начала рядa по Y",
        verbose_name="Y",
    )



