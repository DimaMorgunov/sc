from django.apps import AppConfig


class PaymentKeeperConfig(AppConfig):
    name = 'backoffice'
    verbose_name = "Настройки кинотеатров"
    verbose_name_plural = "Настройки кинотеатра"
