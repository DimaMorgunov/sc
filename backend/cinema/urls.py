from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from seo.views import SeoInPage
from movie.admin import admin
from django.urls import include, path, re_path
from .admin_dashboard import DashboardView, csv_export
from seo.views import robots_txt, sitemap

urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin/dashboard/', DashboardView.as_view(), name='dashboard'),
    path('admin/dashboard/csv/export/<int:id>', csv_export, name='csv_export', ),
    path('api/accounts/', include('accounts.urls')),
    path('api/movies/', include('movie.urls')),
    path('api/seo/', include('seo.urls')),
    path('api/feedback/', include('feedback.urls')),
    path('api/about_cinema/', include('pages.urls')),
    path('api/payment/', include('backoffice.urls')),
    path('api/reservation/', include('reservation.urls')),
    path('api/payment/', include('payment.urls')),
    path('anons/', include('tv.urls')),
    path('robots.txt', robots_txt),
    path('sitemap.xml', sitemap),
    re_path(r'^(?!media|static|api|admin|anons).*', SeoInPage)
]


admin.autodiscover()

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_DIR)
urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG_TOOLBAR_ON:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls))
    ]

