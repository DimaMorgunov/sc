import os
from configurations import Configuration
from cinema import local_settings


class Base(Configuration):
    DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
    STATIC_DIR = os.path.join(BASE_DIR, 'static')
    STATIC_ROOT_DIR = os.path.join(BASE_DIR, 'static_root')
    MEDIA_DIR = os.path.join(BASE_DIR, 'media')
    LOGS_DIR = os.path.join(BASE_DIR, 'log')
    BOWER_COMPONENTS_ROOT = os.path.join(STATIC_ROOT_DIR, 'bower')

    PDF_DIR = os.path.join(MEDIA_DIR, 'pdf')
    BARCODE_DIR = os.path.join(MEDIA_DIR, 'card_code')

    SECRET_KEY = local_settings.SECRET_KEY

    EMAIL_TEMPLATE_DIR = os.path.join(TEMPLATE_DIR, 'email_template')

    DEFAULT_TICKET_GET_WAY = 2
    MAX_FEEDBACK_IN_DAY = 4

    WSGI_APPLICATION = 'cinema.wsgi.application'

    SITE_ID = 1

    SMS_TOKEN = local_settings.SMS_TOKEN
    SMS_LOGIN = local_settings.SMS_LOGIN
    SMS_PASSWORD = local_settings.SMS_PASSWORD
    SMS_ALPHANAME = local_settings.SMS_ALPHANAME
    SMS_CONNECTION_URL = local_settings.SMS_CONNECTION_URL

    # Registration
    DEFAULT_REGISTRATION_KEY_RENEW_TIME = 2
    DEFAULT_REGISTRATION_CONFIRM_TIME = 15

    # Payment
    RESERVATION_EXPIRATION_MINS = 15

    INSTALLED_APPS = [
        'daterange_filter',
        'django.contrib.sites',
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django_extensions',

        'djangobower',
        'webpack_loader',
        'rest_framework',
        'rest_framework.authtoken',
        'rest_auth',
        'sorl.thumbnail',
        'movie',
        'accounts',
        'reservation',
        'backoffice',
        'pages',
        'feedback',
        'payment',
        'seo',
    ]

    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': [

        ],
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.BasicAuthentication',
            'rest_framework.authentication.SessionAuthentication',
            'rest_framework.authentication.TokenAuthentication',
        )
    }

    REST_AUTH_SERIALIZERS = {
        'USER_DETAILS_SERIALIZER': 'accounts.serializers.UserSerializer',
    }

    PASSWORD_HASHERS = [
        'django.contrib.auth.hashers.PBKDF2PasswordHasher',
        'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
        'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    ]

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
    )

    ROOT_URLCONF = 'cinema.urls'

    @property
    def TEMPLATES(self):
        return [{
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [self.TEMPLATE_DIR, os.path.join(self.TEMPLATE_DIR, 'email_template')],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
        ]

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = local_settings.EMAIL_HOST
    EMAIL_HOST_PASSWORD = local_settings.EMAIL_HOST_PASSWORD
    EMAIL_HOST_USER = local_settings.EMAIL_HOST_USER
    EMAIL_PORT = local_settings.EMAIL_PORT
    EMAIL_TIMEOUT = 25
    EMAIL_USE_LOCALTIME = 'False'

    EMAIL_USE_SSL = local_settings.EMAIL_USE_SSL
    EMAIL_USE_TLS = local_settings.EMAIL_USE_TLS

    BCC_CONFIRM_MAILS = ['admin@smartcinema.ua',
                         'smartcinema.dev@gmail.com',
                         'smartcinemak@gmail.com',
                         'admin_smartcinema@ukr.net',
                         'smartcinema.khmel@gmail.com']
    EMAIL_FROM = 'Кінотеатр SmartCinema <admin@smartcinema.ua>'
    DEFAULT_FROM_EMAIL = EMAIL_FROM
    SERVER_EMAIL = EMAIL_FROM

    EMAIL_BCC = ['smartcinema.dev@gmail.com',
                 'smartcinemak@gmail.com',
                 'admin_smartcinema@ukr.net',
                 'smartcinema.khmel@gmail.com']

    ADMINS = [('Vinnitsa', "smartcinema.dev@gmail.com"), ('Khmelnitskiy', "smartcinema.dev@gmail.com")]

    STAFF_EMAIL = {
        0: ['smartcinema.dev@gmail.com', 'admin_smartcinema@ukr.net', 'smartcinema.khmel@gmail.com'],  # Все города
        1: ['smartcinema.dev@gmail.com', 'admin_smartcinema@ukr.net'],  # Винница
        2: ['smartcinema.dev@gmail.com', 'smartcinema.khmel@gmail.com'],  # Хмель
    }

    FEEDBACK_STAFF_EMAIL = STAFF_EMAIL
    REVIEW_STAFF_EMAIL = STAFF_EMAIL
    RESERVATION_STAFF_EMAIL = STAFF_EMAIL
    REGISTRATION_STAFF_EMAIL = STAFF_EMAIL

    # URL
    CITE_URL = local_settings.CITE_URL
    LIQPAY_RETURN_BASE_URL = CITE_URL
    ALLOWED_HOSTS = [local_settings.CITE_URL, '*']

    AUTH_PASSWORD_VALIDATORS = [
    ]

    LANGUAGE_CODE = 'uk'

    TIME_ZONE = 'Europe/Kiev'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    STATIC_URL = '/static/'

    STATICFILES_DIRS = [
        STATIC_DIR, BOWER_COMPONENTS_ROOT,
    ]

    MEDIA_ROOT = MEDIA_DIR

    STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')

    MEDIA_URL = '/media/'

    MEDIAFILES_DIRS = [
        MEDIA_DIR,
    ]

    # LiqPay
    LIQPAY_RESULT_URL = '/api/payment/result/'
    LIQPAY_CALLBACK_URL = '/api/payment/callback/'
    LIQPAY_RETURN_BASE_URL = CITE_URL
    LIQPAY_DEFAULT_CURRENCY = 'UAH'
    LIQPAY_DEFAULT_TICKET_PAGE = '/reservation/'

    # Reservation
    DEFAULT_RESERVATION_KEY = 1000000000000500

    # CRM
    DEFAULT_CRM_OBJECT_ID = 1

    # PREMIERE
    DEFAULT_PREMIERE_OPR_TIME = 15  # minutes

    VIBER_URL = 'https://wep.wf/6egkx9'
    TELEGRAM_URL = 'https://wep.wf/9u8b3x'
    FACEBOOK_URL = 'https://www.facebook.com/smartcinemaua/'
    MESSENDGER_URL = 'https://wep.wf/eyqf6m'
    INSTAGRAM_URL = 'https://www.instagram.com/smart_cinema/'
    PLAY_MARKET_URL = 'https://play.google.com/store/apps/details?id=ua.kinoteatr.SmartCinema'
    APP_STORE_URL = 'https://itunes.apple.com/ru/app/smartcinema/id1186208172?mt=6'

    BOWER_INSTALLED_APPS = (
        "jquery#1.11.3",
        "jquery-ui#1.11.4",
        "jquery.cookie#1.4.1",
        "perfect-scrollbar#0.6.5",
        "select2#4.0.0",
        "timepicker",
    )

    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False

    SEO_SOCIAL_DEFAULT_BACK_IMAGE = STATIC_URL + 'img/logo.png'
    SEO_DEFAULT_DESCRIPTION = 'Кращий кінотеатр'
    SEO_DEFAULT_KEYWORDS = 'SmartCinema'
    SEO_DEFAULT_TITLE = '| Кінотеатр SmartCinema'

    @property
    def LOGGING(self):
        return {
            'version': 1,
            'disable_existing_loggers': False,
            'handlers': {
                'mail_admins': {
                    'level': 'ERROR',
                    'class': 'django.utils.log.AdminEmailHandler',
                    'include_html': True,
                    'formatter': 'verbose',
                },
                'django_file': {
                    'level': 'DEBUG',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(self.LOGS_DIR, 'django.log'),
                    'formatter': 'verbose',
                },
                'payment_file': {
                    'level': 'DEBUG',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(self.LOGS_DIR, 'payment.log'),
                    'formatter': 'verbose',
                },
                'debug_file': {
                    'level': 'DEBUG',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(self.LOGS_DIR, 'debug.log'),
                    'formatter': 'verbose',
                },
                'other': {
                    'level': 'DEBUG',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(self.LOGS_DIR, 'other.log'),
                    'formatter': 'verbose',
                },
                'console': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'simple',
                },
            },

            'formatters': {
                'verbose': {
                    'format': '>>>>>> {levelname} {asctime} {module} {process:d} {thread:d}\n{message}\n\n',
                    'style': '{',
                },
                'simple': {
                    'format': '>>>>>> {asctime} [{levelname}]\n{message}\n\n',
                    'style': '{',
                },
            },

            'loggers': {
                'sender': {
                    'handlers': ['console', 'django_file', 'mail_admins'],
                    'level': 'INFO',
                    'propagate': True,
                },
                'payment': {
                    'handlers': ['console', 'payment_file', 'mail_admins'],
                    'level': 'DEBUG',
                    'propagate': True,
                },
                'premiere': {
                    'handlers': ['console', 'payment_file', 'mail_admins'],
                    'level': 'INFO',
                    'propagate': True,
                },
                'django.email': {
                    'handlers': ['console', 'django_file', 'mail_admins'],
                    'level': 'INFO',
                    'propagate': True,
                },
                'django': {
                    'handlers': ['console', 'django_file'],
                    'level': 'INFO',
                    'propagate': True,
                },
                'other': {
                    'handlers': ['console', 'other'],
                    'level': 'DEBUG',
                    'propagate': True,
                },
                'debug': {
                    'handlers': ['console', 'debug_file'],
                    'level': 'DEBUG',
                    'propagate': True,
                },
            },
        }


class Local(Base):
    DEBUG = True
    LIQPAY_DEBUG = False
    DEBUG_TOOLBAR_ON = False

    INTERNAL_IPS = ['*']
    ALLOWED_HOSTS = ['*', ]

    INSTALLED_APPS = Base.INSTALLED_APPS + ['debug_toolbar']

    MIDDLEWARE = Base.MIDDLEWARE + ['debug_toolbar.middleware.DebugToolbarMiddleware']

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(Base.BASE_DIR, 'db.sqlite3'),
        }
    }

    WEBPACK_LOADER = {
        'DEFAULT': {
            'BUNDLE_DIR_NAME': '/',
            'STATS_FILE': os.path.join(Base.BASE_DIR, 'static', 'webpack-stats.json'),
            'POLL_INTERVAL': 0.1,
            'TIMEOUT': None,
        }
    }


class Prod(Base):
    DEBUG = False
    LIQPAY_DEBUG = False
    DEBUG_TOOLBAR_ON = False

    INTERNAL_IPS = ['127.0.0.1']
    ALLOWED_HOSTS = ['*', ]
    INSTALLED_APPS = Base.INSTALLED_APPS

    # SSL settings
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    SECURE_SSL_REDIRECT = True

    WEBPACK_LOADER = {
        'DEFAULT': {
            'BUNDLE_DIR_NAME': '/',
            'STATS_FILE': os.path.join(Base.BASE_DIR, 'static_root', 'webpack-stats.json'),
            'POLL_INTERVAL': 0.1,
            'TIMEOUT': None,
        }
    }

    DATABASES = {
        'default': local_settings.PROD,
    }

    # Reservation
    DEFAULT_RESERVATION_KEY = 1000000000050500

    # URL
    CITE_URL = local_settings.PROD_CITE_URL
    LIQPAY_RETURN_BASE_URL = CITE_URL
    ALLOWED_HOSTS = [local_settings.CITE_URL, '*']

    # LiqPay
    LIQPAY_RESULT_URL = '/api/payment/result/'
    LIQPAY_CALLBACK_URL = '/api/payment/callback/'
    LIQPAY_RETURN_BASE_URL = CITE_URL
    LIQPAY_DEFAULT_CURRENCY = 'UAH'
    LIQPAY_DEFAULT_TICKET_PAGE = '/reservation/'


class Stage(Base):
    DEBUG = True
    LIQPAY_DEBUG = True
    DEBUG_TOOLBAR_ON = False

    INTERNAL_IPS = ['127.0.0.1']
    ALLOWED_HOSTS = ['*', ]
    INSTALLED_APPS = Base.INSTALLED_APPS

    WEBPACK_LOADER = {
        'DEFAULT': {
            'BUNDLE_DIR_NAME': '/',
            'STATS_FILE': os.path.join(Base.BASE_DIR, 'static_root/webpack-stats.json'),
            'POLL_INTERVAL': 0.1,
            'TIMEOUT': None,
        }
    }

    DATABASES = {
        'default': local_settings.STAGE,
    }

    # Stage emails section
    EMAIL_FROM = 'Кінотеатр SmartCinema <admin@smartcinema.ua>'
    DEFAULT_FROM_EMAIL = EMAIL_FROM
    SERVER_EMAIL = EMAIL_FROM

    BCC_CONFIRM_MAILS = ['smartcinema.dev@gmail.com']

    EMAIL_BCC = ['smartcinema.dev@gmail.com']

    ADMINS = [('Vinnitsa', "smartcinema.dev@gmail.com"), ('Khmelnitskiy', "smartcinema.dev@gmail.com")]

    STAFF_EMAIL = {
        0: ['smartcinema.dev@gmail.com'],  # Все города
        1: ['smartcinema.dev@gmail.com'],  # Винница
        2: ['smartcinema.dev@gmail.com'],  # Хмель
    }

    # Reservation
    DEFAULT_RESERVATION_KEY = 1000000000010500

    # URL
    CITE_URL = local_settings.STAGE_CITE_URL
    LIQPAY_RETURN_BASE_URL = CITE_URL
    ALLOWED_HOSTS = [local_settings.CITE_URL, '*']

    # LiqPay
    LIQPAY_RESULT_URL = '/api/payment/result/'
    LIQPAY_CALLBACK_URL = '/api/payment/callback/'
    LIQPAY_RETURN_BASE_URL = CITE_URL
    LIQPAY_DEFAULT_CURRENCY = 'UAH'
    LIQPAY_DEFAULT_TICKET_PAGE = '/reservation/'
