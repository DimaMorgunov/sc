import os

Test = True

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

EMAIL_HOST_PASSWORD = 'cinemashka'
EMAIL_HOST_USER = 'admin@smartcinema.ua'
EMAIL_HOST = 'mail.smartcinema.ua'
EMAIL_PORT = '465'
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True

CITE_URL = 'http://test.smartcinema.ua'
STAGE_CITE_URL = 'http://test.smartcinema.ua'
PROD_CITE_URL = 'https://smartcinema.ua'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SECRET_KEY = '9x@^wxha9)2y#vt!3(tcrshu1rhc5@ul((rf=&m$v9rwuj+ozx'

SMS_TOKEN = '8c8ef070bda32ef98dc44b3e18a7a07a4c7a7ddc'
SMS_LOGIN = '+380634391909'
SMS_PASSWORD = 'pbt5838b'
SMS_ALPHANAME = 'SmartCinema'
SMS_CONNECTION_URL = 'https://alphasms.ua/api/xml.php'

STAGE = {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'cinema_stage',
    'USER': 'cinema_stage',
    'PASSWORD': '4n4essZdRej56VJH?tE%V+n6@MGK4VjHA=e4#RS3Ke?H&YzStsgAZ9Yg_np%%@=u',
    'HOST': '193.169.189.168',
    'PORT': '5432',
}

# Prod user key (no grant)
# PROD = {
#     'ENGINE': 'django.db.backends.postgresql_psycopg2',
#     'NAME': 'cinema_prod',
#     'USER': 'cinema_prod',
#     'PASSWORD': '$zL_Ay8zvWN7Q7VfE+Pd!S!Srh^7Gh^j3eMLhMs^%U+6uf&F5H*pzBfQ8@-gHxPu',
#     'HOST': '193.169.189.168',
#     'PORT': '5432',
# }

PROD = {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'cinema_prod',
    'USER': 'cinema_stage',
    'PASSWORD': '4n4essZdRej56VJH?tE%V+n6@MGK4VjHA=e4#RS3Ke?H&YzStsgAZ9Yg_np%%@=u',
    'HOST': '193.169.189.168',
    'PORT': '5432',
}

# pg_dump -U cinema_stage -h 193.169.189.168 -p 5432 -W -d cinema_stage > dump.sql
# pg_dump -U test -h 193.169.189.168 -p 8432 -W -d cinema_db > dump.sql
