from django.views.generic import TemplateView
from backoffice.models import Branch, Hall
from qsstats import QuerySetStats
from django.db.models import Sum, Count
from django.utils import timezone
from payment import models
from movie.models import Showtime
import datetime
import xlwt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse


class DashboardView(LoginRequiredMixin, TemplateView):
    PERIOD = 90
    login_url = '/admin/login/'
    redirect_field_name = 'next'

    extra_context = {
        'site_header': 'SmartAdmin',
        'site_title': 'SmartCinema',
        'has_permission': True,
        'site_url': '/',
        'title': 'Панель управления',
    }

    template_name = "admin/admin_dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        start_date = timezone.localtime() - datetime.timedelta(days=self.PERIOD)
        end_date = timezone.localtime()
        context.update({
            'city_list': [],
            'start_date': start_date,
            'end_date': end_date,
        })

        for city in Branch.objects.all():
            context['city_list'] += [self.make_city_dict(city, start_date, end_date)]

        context.update({
            'legend': self.add_graph_legend_to_context()
        })
        return context

    def make_city_dict(self, branch, start_date, end_date):
        res = self.get_graph_context(branch.id,  start_date, end_date)
        return branch.name, res, branch.id

    def add_graph_legend_to_context(self):
        legend = ['Период', ] + [pay_way[1] for pay_way in models.PAY_WAY]
        return legend

    def get_graph_context(self, id,  start_date, end_date):

        month = start_date.month
        res = []

        while month <= end_date.month:
            cur_month_res = [datetime.datetime(month=month, year=start_date.year, day=1)]
            for pay_type in range(len(models.PAY_WAY)):
                queryset = models.Checkout.objects.filter(
                    invoice__pay_way=pay_type,
                    status='success',
                    invoice__cash_keeper__branch__id=id,
                    invoice__reservation__datetime__month=month,
                ).aggregate(Sum('amount'))
                cur_month_res += [queryset['amount__sum'] if queryset['amount__sum'] is not None else 0]
            month += 1
            res += [cur_month_res]
        return res


class ExcelImport:
    def __init__(self, branch_id, request):
        self.request = request
        self.branch = Branch.objects.get(id=branch_id)
        self.datetime_obj = timezone.localtime()
        self.active_days = self.get_active_days()

        self.xls_book = xlwt.Workbook(encoding='utf-8')

        self.xls_capitalize_style = xlwt.Style.easyxf('font: bold on; align: wrap on, vert centre, horiz center')
        self.xls_default_font_style = xlwt.Style.easyxf('font: bold on; align: wrap on')

        self.columns = ['№', 'Название сеанса', 'Начало сеанса', ]
        self.columns_style = [
            {'width': 5, 'style': self.xls_capitalize_style},
            {'width': 60, 'style': self.xls_capitalize_style},
            {'width': 20, 'style': self.xls_capitalize_style},
        ]

    def get_data(self):

        for date in self.active_days:
            row_num = 1
            sheet = self.create_sheet(date.day, date.month)
            for hall in self.branch.hall_set.all():
                query_set = Showtime.objects.filter(
                    movietobranch_id__branch_id=self.branch,
                    hall_id=hall,
                    public=True,
                    start__date=date
                ).order_by('start')
                row_num = self.insert_data(sheet, query_set, row_num, hall)

    def get_active_days(self):

        shows_day_list = Showtime.objects.filter(
            movietobranch_id__branch_id=self.branch,
            start__date__gte=self.datetime_obj.date()
        ).order_by('start').values_list('start', flat=True)

        shows_day_list = [show.date() for show in shows_day_list]
        return set(shows_day_list)

    def get_response(self):
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="%s.xls"' % self.branch.id
        self.get_data()
        self.xls_book.save(response)
        return response

    def create_sheet(self, day, month):
        sheet = self.xls_book.add_sheet('%s.%s' % (day, month))

        for col_num in range(len(self.columns)):
            sheet.write(0, col_num, self.columns[col_num], self.columns_style[col_num]['style'])
            sheet.col(col_num).width = int(self.columns_style[col_num]['width'] * 256)

        return sheet

    def insert_data(self, sheet, queryset, row_num, hall):

        row_num += 2
        sheet.write_merge(row_num, row_num, 0, 2, 'Зал %s' % hall.verbose, self.xls_capitalize_style)

        for id in range(len(queryset)):
            row_num += 1
            show = queryset[id]

            try:
                title = show.movietobranch_id.movie_id.title
            except:
                title = '_'

            obj_time = timezone.localtime(show.start)
            show_start = obj_time.strftime('%H-%M')
            sheet.write(row_num, 0, id + 1,  self.xls_default_font_style)
            sheet.write(row_num, 1, title, self.xls_default_font_style)
            sheet.write(row_num, 2,  show_start, self.xls_capitalize_style)

        return row_num


def csv_export(request, id):
    excel_generator = ExcelImport(id, request)
    response = excel_generator.get_response()
    return response

