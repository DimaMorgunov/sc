import os
from io import BytesIO

import qrcode
from django.conf import settings
from accounts.models import User
from django.core.files import File
from django.db import models
from django.utils import timezone
from feedback.sender_util import EmailFactory
from movie.models import Showtime
from premiere.premiere_import import PremieraImport

import logging

payment_logger = logging.getLogger('payment')
sender_logger = logging.getLogger('sender')
debug = logging.getLogger('debug')

RESERVATION_STATUSES = (
    ('web', "Заказ через сайт"),
    ('android', "Заказ через мобильное приложение на android"),
    ('ios', "Заказ через мобильное приложение на iOS"),
)

Get_ticket_way = (
    ('1', "Через бонусный аккаунт"),
    ('2', "Через почту"),
    ('3', "Через Viber"),
)


def _qr_upload_path(instance, filename):
    session = instance.reservation.show_time.start
    return 'tickets/%s/%s/%s/%s' % (session.year, session.month, session.day, filename)


class Reservation(models.Model):
    user_account = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    email = models.EmailField(
        verbose_name='Email',
        blank=True,
        null=True
    )

    phone = models.CharField(
        verbose_name='Телефон',
        max_length=12,
        blank=True,
        null=True
    )

    datetime = models.DateTimeField(
        verbose_name='Дата',
        help_text=u'Дата и время резервирования',
        default=timezone.now
    )

    show_time = models.ForeignKey(
        Showtime,
        verbose_name='Сеанс',
        on_delete=models.CASCADE
    )

    flg_input = models.CharField(
        verbose_name='Источник',
        help_text='Название источника резервирования',
        max_length=12,
        choices=RESERVATION_STATUSES,
        default='site'
    )

    get_way = models.CharField(
        verbose_name='Получение билетов',
        help_text='Выберите способ получения билетов',
        max_length=12,
        choices=Get_ticket_way,
        default='2'
    )

    EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'tickets')
    STAFF_EMAIL = settings.RESERVATION_STAFF_EMAIL

    def send_client_opr_email(self):
        self.EMAIL_TXT = 'opr.txt'
        self.EMAIL_HEAD = 'opr_head.txt'
        self.EMAIL_HTML = 'opr.html'

        message = EmailFactory(
            self,
            extra_data={
                'status': 'opr',
                'invoice': self.invoice_set,
                'reservation': self,
                'branch_id': self.show_time.movietobranch_id.branch_id,
                'tickets_query': self.ticket_set,
                'settings': settings,
            },
            email_to=[self.email, ], )
        sender_logger.info('[EmailFactory] start sending opr ticket email')
        message.send_message()

    def send_client_default_email(self):
        self.EMAIL_TXT = 'default.txt'
        self.EMAIL_HEAD = 'default_head.txt'
        self.EMAIL_HTML = 'default.html'

        message = EmailFactory(
            self,
            extra_data={
                'status': 'default',
                'invoice': self.invoice_set.first(),
                'reservation': self,
                'branch_id': self.show_time.movietobranch_id.branch_id,
                'tickets_query': self.ticket_set.all(),
                'settings': settings,
                'recommendation': self.show_time.movietobranch_id.movie_id.email_recommendation(self.show_time.movietobranch_id.movie_id_id)
            },
            email_to=[self.email, ],
            file_attach=self.ticket_set.all())
        sender_logger.info('[EmailFactory] start sending default ticket email')
        message.send_message()

    def send_stuff_opr_email(self):
        self.EMAIL_TXT = 'opr_for_stuff.txt'
        self.EMAIL_HEAD = 'opr_for_stuff_head.txt'
        self.EMAIL_HTML = False

        message = EmailFactory(
            self,
            extra_data={
                'status': 'opr_for_stuff',
                'invoice': self.invoice_set,
                'reservation': self,
                'branch_id': self.show_time.movietobranch_id.branch_id,
                'tickets_query': self.ticket_set,
                'settings': settings,
            },
            email_to=self.STAFF_EMAIL[self.show_time.movietobranch_id.branch_id.id], )
        sender_logger.info('[EmailFactory] start sending stuff opr email')
        message.send_message()

    def send_email(self, status):
        if status == 'default':
            self.send_client_default_email()
        elif status == 'opr':
            self.send_client_opr_email()
            self.send_stuff_opr_email()

    def description_str(self):
        ticket_query = Ticket.objects.filter(reservation=self).order_by('row')
        base_str = 'Ряд {row}, Мicце {place}<br/>'
        base_verbose_str = 'Ряд {row}, Мicця {place}<br/>'
        place_dict = {}
        for ticket in ticket_query:
            if place_dict.get(str(ticket.row), False):
                place_dict[str(ticket.row)] += ',%d' % ticket.place
            else:
                place_dict[str(ticket.row)] = '%d' % ticket.place
        result = ''
        for row in place_dict:
            if len(place_dict[row]) > 1:
                result += base_verbose_str.format(row=row, place=place_dict[row])
            else:
                result += base_str.format(row=row, place=place_dict[row])

        return result[:-5]

    def sum(self):
        query = Ticket.objects.filter(reservation=self)
        result = 0
        for cost in query:
            result += cost.price
        return result

    class Meta:
        verbose_name = "Резервирование"
        verbose_name_plural = "Резервирования"

    def __str__(self):
        return 'Резериовние {}'.format(self.show_time)


class Ticket(models.Model):
    reservation = models.ForeignKey(
        Reservation,
        verbose_name='id резервирование',
        on_delete=models.CASCADE,
    )

    row = models.IntegerField(
        verbose_name="Ряд",
    )

    place = models.IntegerField(
        verbose_name="Место",
    )

    price = models.IntegerField(
        verbose_name="цена",
        help_text="Цена билета",
        default=0,
    )

    query = models.CharField(
        verbose_name="Код места в премьере",
        help_text="Код места в премьере",
        max_length=128,
    )

    reservation_key = models.CharField(
        verbose_name="Код резервирования билета",
        max_length=64,
    )

    key = models.CharField(
        verbose_name="Код входного билетa",
        max_length=64,
        blank=True,
        null=True,
    )

    qr_code = models.ImageField(
        verbose_name="Код билета",
        help_text="Код билета для сканирования в кинотеатре",
        upload_to=_qr_upload_path,
        blank=True,
        null=True,
    )

    status = models.CharField(
        verbose_name='Status',
        max_length=32,
        default="Зарезервировано",
        blank=True,
        null=True,
    )

    def save(self, *args, **kwargs):
        if self.key is not None and self.qr_code is None:
            self.generate_qr_code()
        super(Ticket, self).save(*args, **kwargs)

    @property
    def branch(self):
        return self.reservation.show_time.movietobranch_id.branch_id

    @property
    def premiere_socket(self):
        return PremieraImport(self.branch)

    @property
    def session(self):
        return self.reservation.show_time.premiere_id

    @property
    def card(self):
        try:
            if self.reservation.user_account.bonuscrm.card is not None:
                return self.reservation.user_account.bonuscrm.card
            else:
                return self.branch.DEFAULT_CARD_NUMBER
        except (AttributeError or models.ObjectDoesNotExist):
            return self.branch.DEFAULT_CARD_NUMBER

    @property
    def card_pass(self):
        try:
            if self.reservation.user_account.bonuscrm.card_pass is not None:
                return self.reservation.user_account.bonuscrm.card_pass
            else:
                return self.branch.DEFAULT_CARD_PASSWORD
        except (AttributeError or models.ObjectDoesNotExist):
            return self.branch.DEFAULT_CARD_PASSWORD

    def __login(self):
        self.premiere_socket.get_login(self.card, self.card_pass)

    def get_ticket_key(self):
        self.__login()

        try:
            data_set = self.premiere_socket.get_reservation(
                CardCode=self.card,
                Sessions=self.session,
                ReservationID=self.reservation_id,
            )
        except Exception as e:
            debug.info("[Ticket][Data] Failed\n[Error] %s" % e)

            return False, e

        if isinstance(data_set, tuple):
            debug.info('[Ticket] Check ticket error - %s' % data_set[1])

            return False, data_set[1]

        debug.info("[Ticket][Data] %s" % str(data_set))
        return True, data_set.find('./Reservation/Places/Place').attrib['Code']

    def cancel(self):
        self.__login()

        data_set = self.premiere_socket.cancel_reservation(
            CardCode=self.card,
            Sessions=self.session,
            ReservationID=self.reservation_key
        )

        if isinstance(data_set, tuple):
            payment_logger.info('[Ticket] Error cancel - %s' % data_set[1])
            return False, data_set[1]
        else:
            payment_logger.debug('[Ticket] cancel - %s' % self.reservation_key)
            self.status = 'Резервирование снято'
            return True, "Отмена резервирования для билета ряд - %s место - %s " % (self.row, self.place)

    def confirm(self):
        self.__login()

        data_set = self.premiere_socket.approved_reservation(
            CardCode=self.card,
            Sessions=self.session,
            ReservationID=self.reservation_key
        )

        if isinstance(data_set, tuple):
            payment_logger.info('[Ticket] Error confirm - %s' % self.reservation_key)

            return False, data_set[1]
        else:
            payment_logger.debug('[Ticket] confirm - %s' % self.reservation_key)
            self.status = 'Подтверждена оплата'
            self.key = data_set.find('./Reservation/Places/Place').attrib['Code']
            self.save()
            return True, "Подтверждение резервирования для билета ряд - %s место - %s " % (self.row, self.place)

    def pay_return(self):
        self.__login()

        data_set = self.premiere_socket.pay_return(
            CardCode=self.card,
            Sessions=self.session,
            ReservationID=self.reservation_key
        )

        if isinstance(data_set, tuple):
            payment_logger.info('[Ticket] Error pay_return - %s' % data_set[1])
            return False, data_set[1]
        else:
            payment_logger.debug('[Ticket] pay_return - %s' % self.reservation_key)
            self.status = 'Возвращено'
            self.save()
            return True, "Выполнен возврат резервирования для билета ряд - %s место - %s" % (self.row, self.place)

    def sale(self):

        data_set = self.premiere_socket.sale_reservation(
            CardCode=self.card,
            Sessions=self.session,
            ReservationID=self.reservation_key
        )

        if isinstance(data_set, tuple):
            payment_logger.info('[Ticket] Error sale - %s' % data_set[1])
            return False, data_set[1]
        else:
            payment_logger.debug('[Ticket] sale - %s' % self.reservation_key)
            self.status = 'Подтверждена оплата'
            self.key = data_set.find('./Reservation/Places/Place').attrib['Code']
            self.save()
            return True, "Подтверждение быстрого резервирования для билета " \
                         "ряд - %s место - %s " % (self.row, self.place)

    def generate_qr_code(self):
        if self.key is None:
            raise AttributeError("No key for QR code has been provided. Are you sure that ticket is valid?")

        filename = '%s.png' % self.key
        file_path = os.path.join(settings.MEDIA_DIR, _qr_upload_path(self, filename))

        if os.path.isfile(file_path):
            os.remove(file_path)

        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=20,
            border=1,
        )

        qr.add_data(self.key)
        qr.make(fit=True)

        image = qr.make_image(fill_color="black", back_color="white")
        image_file = BytesIO()
        image.save(image_file, format='PNG')

        self.qr_code.save(filename, File(image_file, filename))

    class Meta:
        verbose_name = "Билет"
        verbose_name_plural = "Билеты"
        get_latest_by = 'reservation_key'

    def __str__(self):
        return 'Резериовние {}, р - {}, м - {}'.format(self.reservation, self.row, self.place)
