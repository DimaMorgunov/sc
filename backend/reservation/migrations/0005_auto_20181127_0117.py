# Generated by Django 2.1.2 on 2018-11-26 23:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0004_auto_20181118_2123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='user_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
