from django.urls import path
from . import views

urlpatterns = [
    path('reserved/', views.GetBusyPlace.as_view(), name='get-busy-place'),
    path('', views.GetReservation.as_view(), name='get-new-reservation'),
    path('cancel/', views.ReservationCancel.as_view(), name='cancel-reservation'),
    path('change_params/', views.ChangeReservationParams.as_view(), name='change-reservation-params'),
    path('change_place/', views.ReservationPlacesChange.as_view(), name='change-reservation-place'),
    path('resend-ticket/', views.ReSendTicket.as_view(), name='re-send-ticket'),
    path('info/', views.GetReservationInfo.as_view(), name='get-reservation-info'),
]