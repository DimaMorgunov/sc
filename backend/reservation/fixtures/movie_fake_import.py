import datetime
import pytz

tz = pytz.timezone('Europe/Kiev')

movie_set = [
    {'premiere_id': '605', 'premiere_name': 'Альфа'},
]

session_set = [
    {'premiere_id': '51841', 'hall_id': '1', 'movie_id': '605', 'date': datetime.datetime(2018, 8, 29, 21, 20, 0, tzinfo=tz),
    'tehnology': '3D Сеанс', 'price': {'Стандартные места': '9000', 'Диваны': '20000'}},
    {'premiere_id': '51840', 'hall_id': '1', 'movie_id': '605', 'date': datetime.datetime(2018, 8, 29, 19, 0, 0, tzinfo=tz),
     'tehnology': '3D Сеанс', 'price': {'Стандартные места': '9000', 'Диваны': '20000'}},
    {'premiere_id': '51839', 'hall_id': '1', 'movie_id': '605', 'date': datetime.datetime(2018, 8, 29, 16, 40, 0, tzinfo=tz),
     'tehnology': '3D Сеанс', 'price': {'Стандартные места': '5500', 'Диваны': '13000'}}
]

hall_set = [
    {'hall_id': '1', 'name': 'Зал №1', 'branch': 1},
]
