from premiere.premiere_import import PremieraImport
from backoffice.serializers import HallSerializer
from movie.models import Showtime
from movie.serializers import SessionSerializer
from . import models, pre_reservation, serializers
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from backoffice.models import Branch
from django.conf import settings

import logging

payment_logger = logging.getLogger('payment')


class GetBusyPlace(APIView):

    def post(self, request):
        try:
            try:
                show = Showtime.objects.get(id=request.data['session'])

            except Showtime.DoesNotExist:
                payment_logger.error('[GetBusyPlace] Showtime does not exist')
                hall_serializer = {'error': 'Showtime does not exist'}
                status = 400
                return Response(hall_serializer, status)

            movie_id = show.premiere_id
            data = PremieraImport(show.movietobranch_id.branch_id)
            data_set = data.get_busy_place(movie_id=movie_id)

            if data_set.get('error', False):
                payment_logger.error('[GetBusyPlace] %s' % data_set.get('error'))
                status = 400
                return Response(data_set.get('error'), status)
            else:
                status = 200

            # todo: move this to serializer?
            hall_serializer = HallSerializer(instance=show.hall_id, context=data_set)
            data = hall_serializer.data
            data['movie'] = SessionSerializer(instance=show).data
            data['movie']['title'] = show.movietobranch_id.movie_id.title
            data['movie']['detail_background'] = show.movietobranch_id.movie_id.detail_background.url
            data['movie']['format'] = show.show_format.title

            return Response(data, status)
        except Exception as e:
            return Response({
                'detail': "Failed %s." % ("to get film data" if str(e.args).strip() == "[Errno 111] Connection refused" else "to serve request",)
            }, 500)


class GetReservationInfo(APIView):

    def post(self, request):
        reserv_id = request.data.get('reservation')

        try:
            reserv_obj = models.Reservation.objects.get(id=reserv_id)
        except models.Reservation.DoesNotExist:
            return Response({'error': 'reservation does not exist'}, status=401)

        serialize = serializers.ReservationFlatSerializers(instance=reserv_obj)

        return Response(serialize.data)


class ReSendTicket(APIView):

    def post(self, request):
        reservation_id = request.data.get('reservation', None)
        new_email = request.data.get('new_email', None)
        new_phone = request.data.get('new_phone', None)

        if not reservation_id and (not new_email or not new_phone):
            status = 400
            data_set = dict(error='Not enough input arguments')
        else:

            try:
                reservation_object = models.Reservation.objects.get(id=reservation_id)
                if new_email:
                    reservation_object.email = new_email
                    reservation_object.save()
                    reservation_object.send_email(status='default')
                    data_set = dict(result='ok')
                    status = 200

                else:
                    data_set = dict(error='No phone functionality')
                    status = 400
            except models.Reservation.DoesNotExist:
                data_set = dict(error='Showtime does not exist')
                status = 400

        return Response(data_set, status)


class GetReservation(APIView):

    def post(self, request):

        reservation_params = request.data

        if request.user.is_authenticated:
            reservation_params['user_account'] = request.user

        else:
            reservation_params['user_account'] = None

        try:
            # Get current showtime object
            show = Showtime.objects.get(id=int(reservation_params['session']))

            # Get actual config from branch
            config = show.movietobranch_id.branch_id

        except Showtime.DoesNotExist:
            payment_logger.error('[RESERVATION PROCESS][ReservationPlacesChange] session %s does not exist'
                                 % request.data['session'])
            return pre_reservation.ReservationUtils.return_json_message(error='Showtime does not exist')

        premiere_socket = PremieraImport(config)
        new_reservation = pre_reservation.ReservationUtils(premiere_socket, show, reservation_params)
        return new_reservation.reservation_process()


class ReservationPlacesChange(APIView):

    def post(self, request):
        try:
            reservation = models.Reservation.objects.get(id=request.data['reservation_id'])
        except models.Reservation.DoesNotExist:
            payment_logger.error('[RESERVATION PROCESS][ReservationPlacesChange] reservation %s does not exist'
                                 % request.data['reservation_id'])
            return Response({"error": 'reservation does not exist'})

        request.data['session'] = reservation.show_time.id
        tickets = reservation.ticket_set.all()

        for ticket in tickets:
            payment_logger.info('[RESERVATION PROCESS][ReservationPlacesChange] cancel ticket row %s, place %s'
                                % (ticket.row, ticket.place))
            ticket.cancel()

        reservation.delete()
        new_reservation = GetReservation()
        return new_reservation.post(request)


class ChangeReservationParams(APIView):
    FIELDS = ['email', 'phone', ]

    def post(self, request):

        try:
            reservation = models.Reservation.objects.get(id=request.data['reservation_id'])
        except models.Reservation.DoesNotExist:
            payment_logger.error('[RESERVATION PROCESS][ChangeReservationParams] reservation %s does not exist'
                                 % request.data['reservation_id'])
            return Response({"error": 'reservation does not exist'})

        for field in self.FIELDS:
            if request.data.get(field, None):
                setattr(reservation, field, request.data.get(field))
        reservation.save()

        if request.user.is_authenticated and request.data.get('user_account', None):
            if request.user.bonuscrm.card and request.user.bonuscrm.card_pass:
                if not reservation.user_account:
                    tickets = models.Ticket.objects.filter(reservation=reservation)
                    reservation.user_account = request.user
                    reservation.save()

                    reinvoice_data = {
                        'tickets_queries': [],
                        'user_account': request.user,
                        'reservation': reservation
                    }

                    for ticket in tickets:
                        reinvoice_data['tickets_queries'] += [{
                            'row': ticket.row,
                            'place': ticket.place,
                            'query_for_place': ticket.query,
                            'price': ticket.price,
                        }]
                        ticket.ticket_cancel()

                    premiere_socket = PremieraImport(reservation.show_time.movietobranch_id.branch_id)

                    change_reservation = pre_reservation.ReservationUtils(
                        premiere_socket,
                        reservation.show_time,
                        reinvoice_data
                    )
                    return change_reservation.change_card_process()

        return Response({"result": 'ok'})


class ReservationCancel(APIView):

    def post(self, request):

        try:
            reservation = models.Reservation.objects.get(id=request.data['reservation_id'])
        except models.Reservation.DoesNotExist:
            payment_logger.error('[RESERVATION PROCESS][ReservationCancel] reservation %s does not exist'
                                 % request.data['reservation_id'])
            return Response({"error": 'reservation does not exist'})

        tickets = models.Ticket.objects.filter(reservation=reservation)

        for ticket in tickets:
            payment_logger.info('[RESERVATION PROCESS][ReservationPlacesChange] cancel ticket row %s, place %s'
                                % (ticket.row, ticket.place))
            ticket.cancel()

        setattr(reservation, 'status', 'canceled by user')

        return Response({"result": 'ok'})
