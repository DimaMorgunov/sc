from django.contrib import admin

from payment.liqpayment import LiqPayFunctionality, Invoice
from .models import Reservation, Ticket


def send_email_from_admin_panel(self, request, queryset):
    for obj in queryset:
        obj.send_email(status='default')
        self.message_user(request, "Успешно отправлено письмо на почту %s" % obj.email)


send_email_from_admin_panel.short_description = "Повторно отправить письмо клиенту"


def send_opr_email_from_admin_panel(self, request, queryset):
    for obj in queryset:
        obj.send_email(status='opr')
        self.message_user(request, "Успешно отправлено письмо на почту %s" % obj.email)


send_opr_email_from_admin_panel.short_description = "Повторно отправить ОПР клиенту"


def cancel_for_reservation(self, request, queryset):
    for query in queryset:
        tickets = Ticket.objects.filter(reservation=query)
        cancel_for_ticket(self, request=request, queryset=tickets)


cancel_for_reservation.short_description = "Отмена резервирования"


def cancel_for_ticket(self, request, queryset):
    for ticket in list(queryset):
        status, msg = ticket.cancel()
        if status:
            self.message_user(request, msg)
        else:
            self.message_user(request, msg, level='ERROR')


cancel_for_ticket.short_description = "Отмена резервирования билета"


def approve_for_reservation(self, request, queryset):
    for query in queryset:
        tickets = Ticket.objects.filter(reservation=query)
        approve_for_ticket(self, request=request, queryset=tickets)


approve_for_reservation.short_description = "Подтверждение резервирования"


def approve_for_ticket(self, request, queryset):
    for ticket in list(queryset):
        status, msg = ticket.confirm()
        if status:
            self.message_user(request, msg)
        else:
            self.message_user(request, msg, level='ERROR')


approve_for_ticket.short_description = "Подтверждение резервирования"


def ticket_return_for_reservation(self, request, queryset):
    try:
        reservation_list = list(queryset)
    except:
        reservation_list = [queryset]

    for query in reservation_list:
        tickets = Ticket.objects.filter(reservation=query)
        pay_return_for_ticket(self, request=request, queryset=tickets)


ticket_return_for_reservation.short_description = "Вовзрат мест из резервирования"


def pay_return_for_reservation(self, request, queryset):
    for reservation in queryset:
        current_invoice = Invoice.objects.get(reservation=reservation)
        LiqPayFunctionality.pay_return(current_invoice, )
        self.message_user(request, "Возврат счета выполнен успешно")
        ticket_return_for_reservation(self, request, reservation)


pay_return_for_reservation.short_description = "Вовзрат резервирования"


def pay_return_for_ticket(self, request, queryset):

    try:
        reservation_list = list(queryset)
    except:
        reservation_list = [queryset]

    for ticket in reservation_list:
        status, msg = ticket.pay_return()
        if status:
            self.message_user(request, msg)
        else:
            self.message_user(request, msg, level='ERROR')


pay_return_for_ticket.short_description = "Возврат резервирования"


class ReservationAdmin(admin.ModelAdmin):
    actions = [send_email_from_admin_panel, send_opr_email_from_admin_panel, cancel_for_reservation,
               approve_for_reservation, ticket_return_for_reservation, pay_return_for_reservation]
    list_filter = ['get_way', 'flg_input', 'show_time__movietobranch_id__branch_id']
    list_display = ['id',  "get_movie", "get_branch", "get_start_date",  "get_start_time", "user_account", "datetime",
                    "payment_sum", ]

    list_display_links = ('get_movie', )

    search_fields = ["email", "phone", ]

    def get_branch(self, obj):
        return obj.show_time.movietobranch_id.branch_id.name

    def get_start_date(self, obj):
        return obj.show_time.start.date()

    def get_start_time(self, obj):
        return obj.show_time.start.time()

    def get_movie(self, obj):
        return obj.show_time.movietobranch_id.premiere_name

    def payment_sum(self, obj):
        return obj.sum()

    get_start_date.short_description = "Дата"
    get_start_time.short_description = "Время"
    get_branch.short_description = "Кинотеатр"
    get_movie.short_description = "Фильм"
    payment_sum.short_description = "Цена"

    class Meta:
        model = Reservation
        verbose_name = "Резервироввание"
        verbose_name_plural = "Резервирования"


class TicketAdmin(admin.ModelAdmin):
    actions = [cancel_for_ticket, approve_for_ticket, pay_return_for_ticket, ]
    ordering = ["reservation__datetime"]
    list_filter = (
        "reservation__show_time__movietobranch_id__branch_id__name",
        "reservation__show_time__start",
        )

    list_display = ["get_movie", "get_branch", "get_start_date", "get_start_time", "row", "place", "price",
                    "reservation_key", "key", "get_status"]
    search_fields = ["reservation_key", "key", "reservation__phone", "reservation__email", ]

    fieldsets = (
        ("Резервирование", {
            'fields': ('reservation', 'qr_code', 'status',),
        }),
        ("Свойства места", {
            'fields': ('row', 'place', 'query', 'price',),
        }),
        ('Сеанс', {
            'fields': ('reservation_key', 'key',)
        })
    )

    def get_branch(self, obj):
        return obj.reservation.show_time.movietobranch_id.branch_id.name

    def get_start_date(self, obj):
        return obj.reservation.show_time.start.date()

    def get_start_time(self, obj):
        return obj.reservation.show_time.start.time()

    def get_movie(self, obj):
        return obj.reservation.show_time.movietobranch_id.premiere_name

    def get_status(self, obj):
        return obj.status

    get_start_date.short_description = "Дата"
    get_start_time.short_description = "Время"
    get_branch.short_description = "Кинотеатр"
    get_movie.short_description = "Фильм"
    get_status.short_description = "Статус"

    class Meta:
        model = Ticket
        verbose_name = "Билет"
        verbose_name_plural = "Билеты"


admin.site.register(Reservation, ReservationAdmin)
admin.site.register(Ticket, TicketAdmin)

