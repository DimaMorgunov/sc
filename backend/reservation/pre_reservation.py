from .models import Reservation, Ticket
from django.http import HttpResponse
from django.conf import settings
from payment.models import Invoice
from backoffice.models import CashCeeper
import json
from django.db import models

import logging
logger = logging.getLogger('payment')


class ReservationUtils:
    PAY_WAY_CHOICES = ['web', 'android', 'ios']

    def __init__(self, premiere_socket, show, reservation_params):

        self.get_last_reservation_id()
        self.show = show
        self.hall_id = self.show.hall_id.hall_id
        self.DEFAULT_RESERVATION_STEP = 1
        self.socket = premiere_socket

        self.params = reservation_params
        self.input_flag = reservation_params.get('pay_way', 'web') \
            if reservation_params.get('pay_way', 'web') in self.PAY_WAY_CHOICES else 'web'

        self.pay_way_id = self.PAY_WAY_CHOICES.index(self.input_flag)
        self.current_reservation_id = self.DEFAULT_RESERVATION_KEY
        self.reservation = None
        self.places_data_list = []
        self.tickets = []
        self.error_message = 'Some individual error'
        self.user = reservation_params['user_account'] or None

        self.card = self.show.movietobranch_id.branch_id.DEFAULT_CARD_NUMBER
        self.card_pass = self.show.movietobranch_id.branch_id.DEFAULT_CARD_PASSWORD

    def reservation_process(self):
        try:
            self.price = self.socket.get_price(self.show.premiere_id)
            self.check_errors(self.price)

            try:
                self.get_type_of_place()

            except Exception as ex:
                logger.error('[RESERVATION PROCESS][Reservation] Some combination of row and place does not exist: '
                             '%s, %s' % (self.params['places'], ex))
                return self.return_json_message(
                    error='Some combination of row and place does not exist'
                )

            self.get_card_data()
            self.create_reservation_object()
            logger.info('[RESERVATION PROCESS][Reservation] place data - %s' % self.places_data_list)
            for place in self.places_data_list:

                result = self.get_ticket_reservation(place['query_for_place'])
                self.tickets += [self._create_ticket_object(place)]
                logger.info('[RESERVATION PROCESS][Reservation] tickets - %s' % self.tickets)
                if isinstance(result, tuple):
                    self.fall_back(result)
            self.create_invoice()
            setattr(self.reservation, 'status', 'cash wait')

            return self.return_payment_data(self.reservation)
        except RuntimeError:
            return self.return_json_message(error=self.error_message)

    def get_last_reservation_id(self):

        try:
            self.DEFAULT_RESERVATION_KEY = int(Ticket.objects.last().reservation_key)

        except Ticket.DoesNotExist:
            self.DEFAULT_RESERVATION_KEY = settings.DEFAULT_RESERVATION_KEY

        if self.DEFAULT_RESERVATION_KEY < settings.DEFAULT_RESERVATION_KEY:
            self.DEFAULT_RESERVATION_KEY = settings.DEFAULT_RESERVATION_KEY

    def change_card_process(self):
        try:

            self.reservation = self.params['reservation']
            self.get_card_data()

            for place in self.params['tickets_queries']:
                result = self.get_ticket_reservation(place['query_for_place'])
                self.tickets += [self._create_ticket_object(place)]
                if isinstance(result, tuple):
                    self.fall_back(result)
            self.create_invoice()
            setattr(self.reservation, 'status', 'card change')
            return self.return_payment_data(self.reservation)
        except RuntimeError:
            logger.info('[RESERVATION PROCESS][Reservation] %s' % self.error_message)
            return self.return_json_message(error=self.error_message)

    def fall_back(self, result):
        for ticket in self.tickets:
            ticket.cancel()
            ticket.delete()
        self.delete_reservation_object()
        self.error_message = result[0]+"-"+result[1]
        raise RuntimeError

    def create_reservation_object(self):
        self.reservation = Reservation.objects.create(
            show_time=self.show,
            flg_input=self.input_flag,
            get_way=settings.DEFAULT_TICKET_GET_WAY,
            user_account=self.params['user_account']
        )
        self.reservation.save()

    def _create_ticket_object(self, place):
        ticket_query = Ticket.objects.create(
            row=place['row'],
            place=place['place'],
            query=place['query_for_place'],
            reservation=self.reservation,
            price=place['price'],
            reservation_key=self.current_reservation_id
        )
        ticket_query.save()
        return ticket_query

    def create_invoice(self):
        order_id = Invoice.next_order_id(self.reservation)
        self.invoice = Invoice.objects.create(
            reservation=self.reservation,
            cash_sum=self.reservation.sum(),
            order_id=order_id,
            cash_keeper=CashCeeper.objects.get(
                branch=self.show.movietobranch_id.branch_id,
                LIQPAY_Active=True),
            pay_way=self.pay_way_id,
        )
        self.invoice.save()

    def return_payment_data(self, reservation):
        if reservation.user_account:
            return self.return_json_message(
                reservation_id=reservation.id,
                is_user_login=True,
                email=reservation.user_account.email,
                result=self.invoice.get_data_for_liqpay(json=True)
            )
        else:
            return self.return_json_message(
                reservation_id=reservation.id,
                is_user_login=False,
                result=self.invoice.get_data_for_liqpay(json=True)
            )

    def delete_reservation_object(self):
        if self.reservation is not None:
            self.reservation.delete()

    def _get_ticket_reservaton_code(self):
        return self.DEFAULT_RESERVATION_KEY + self.DEFAULT_RESERVATION_STEP

    def get_ticket_reservation(self, query_for_place, ):
        self.current_reservation_id = self._get_ticket_reservaton_code()
        data_set = self.socket.get_reservation(
            CardCode=self.card,
            Places=query_for_place,
            Sessions=self.show.premiere_id,
            ReservationID=self.current_reservation_id,
        )
        if data_set[0] == 'CEE-068':
            logger.info('[RESERVATION PROCESS][Reservation] Failed reservation - %s' % data_set[1])
            self.DEFAULT_RESERVATION_STEP += 1
            return self.get_ticket_reservation(query_for_place)
        self.DEFAULT_RESERVATION_STEP = 1
        self.DEFAULT_RESERVATION_KEY = self.current_reservation_id
        return data_set

    def check_errors(self, data):
        if isinstance(data, tuple):
            self.error_message = data[1]
            raise RuntimeError
        return data

    def get_card_data(self):
        """
        Try get user bonus card from user object
        :return:
        """
        if self.user:
            try:
                if self.params['user_account'].bonuscrm.card and self.params['user_account'].bonuscrm.card_pass:
                    self.card = self.params['user_account'].bonuscrm.card
                    self.card_pass = self.params['user_account'].bonuscrm.card_pass
            except AttributeError:
                pass
            except models.ObjectDoesNotExist:
                pass

        self.socket.get_login(self.card, self.card_pass)

    def get_price(self):
        """
        create and send request to premiere server and get price data for show
        :return: dict with price
        """
        return self.socket.get_price(self.show.premiere_id)

    @staticmethod
    def return_json_message(**kwargs):
        status = 400 if kwargs.get('error', False) else 200

        if status is 400:
            logger.info('[RESERVATION PROCESS] tickets - %s' % kwargs.get('error'))

        result = json.dumps(kwargs, ensure_ascii=False)
        return HttpResponse(result, content_type='application/json', status=status)

    def get_type_of_place(self):
        layer, hall_plan = self.socket.get_hall_plan(hall_id=self.hall_id)
        for row_place in self.params['places']:
            for place_params in hall_plan:
                if place_params['Row'] == str(row_place['row']) and\
                                place_params['Place'] == str(row_place['place']):
                    self.places_data_list += self.create_place_data(place_params, layer)

    def create_place_data(self, place_params, layer):
        return [{
            'row': place_params['Row'],
            'place': place_params['Place'],
            'query_for_place': self.get_query_for_place(place_params, layer),
            'price': self.get_cost_for_place(place_params),
            }]

    def get_cost_for_place(self, place_params):
        if place_params['Type'] == "1":
            return int(self.price['Стандартные места'])/100
        else:
            return int(self.price['Диваны'])/100

    def get_query_for_place(self, places_params, layer):
        return "[l={layer};f=0;r={row};p={place}]".format(
            layer=layer,
            row=places_params['Row'],
            place=places_params['Place'],
            fragment=places_params["Fragment"]
        )

