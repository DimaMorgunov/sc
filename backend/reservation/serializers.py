import pytz, datetime
from rest_framework import serializers
from .models import Ticket, Reservation
from movie.serializers import MovieForTicketSerializer, MovieForTicketFlatSerializer
from cinema import settings


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = ('row', 'place', 'key', 'qr_code', 'price')


class ReservationSerializers(serializers.ModelSerializer):
    ticket_list = serializers.SerializerMethodField('get_tickets')
    showtime = serializers.SerializerMethodField('get_show')
    hall = serializers.SerializerMethodField()
    movie_info = serializers.SerializerMethodField('get_movie')

    @staticmethod
    def get_tickets(reservation_object):
        qs = Ticket.objects.filter(reservation=reservation_object)
        serialize = TicketSerializer(instance=qs, many=True)
        return serialize.data

    @staticmethod
    def get_hall(reservation_object):
        return reservation_object.show_time.hall_id.name

    @staticmethod
    def get_show(reservation_object):
        start = reservation_object.show_time.start

        # if 'datetime' == type(start).__name__ and 'UTC' == str(start.tzinfo):
        #     start = start.astimezone(pytz.timezone(settings.Base.TIME_ZONE))

        return start

    @staticmethod
    def get_movie(reservation_object):
        qs = reservation_object.show_time.movietobranch_id.movie_id
        serialize = MovieForTicketSerializer(instance=qs, many=False)
        return serialize.data

    class Meta:
        model = Reservation
        fields = ('ticket_list', 'showtime', 'hall', 'movie_info',)


class ReservationFlatSerializers(serializers.ModelSerializer):
    ticket_str = serializers.SerializerMethodField('get_tickets_str')
    day = serializers.SerializerMethodField('get_show_day')
    time = serializers.SerializerMethodField('get_show_time')
    hall = serializers.SerializerMethodField()
    movie_info = serializers.SerializerMethodField('get_movie')

    @staticmethod
    def get_tickets_str(reservation_object):
        qs = Ticket.objects.filter(reservation=reservation_object).order_by('-row')
        new_row_str = 'ряд '
        row = None
        for ticket in qs:

            if row is None:
                new_row_str += '{}, місця {}'.format(ticket.row, ticket.place)
                row = ticket.row

            elif row == ticket.row:
                new_row_str += ', {}'.format(ticket.place)

            elif row != ticket.row:
                new_row_str += ", ряд {}, місця {}".format(ticket.row, ticket.place)

        return new_row_str

    @staticmethod
    def get_show_day(reservation_object):
        date_object = datetime.datetime.combine(reservation_object.show_time.start.date(),
                                                reservation_object.show_time.start.time())

        return pytz.timezone(settings.Base.TIME_ZONE).fromutc(date_object).strftime("%Y-%m-%d")

    @staticmethod
    def get_hall(reservation_object):
        return reservation_object.show_time.hall_id.name

    @staticmethod
    def get_show_time(reservation_object):
        date_object = datetime.datetime.combine(reservation_object.show_time.start.date(),
                                                reservation_object.show_time.start.time())

        return pytz.timezone(settings.Base.TIME_ZONE).fromutc(date_object).strftime("%H:%M:%S")

    @staticmethod
    def get_movie(reservation_object):
        qs = reservation_object.show_time.movietobranch_id.movie_id
        serialize = MovieForTicketFlatSerializer(instance=qs, many=False)
        return serialize.data

    class Meta:
        model = Reservation
        fields = ('ticket_str', 'day', 'time', 'hall', 'movie_info',)
