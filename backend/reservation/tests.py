from rest_framework.test import APITestCase
import backoffice.models
import movie.models
from django.urls import reverse
from rest_framework import status
from backoffice import premiere_import
from reservation.fixtures.movie_fake_import import *


class ReservationTest(APITestCase):

    def setUp(self):
        self.test_cabinet = backoffice.models.Cabinet.objects.create()
        self.test_city = backoffice.models.City.objects.create()
        self.test_branch = backoffice.models.Branch.objects.create(city=self.test_city, cabinet=self.test_cabinet)

        # premiere_import.premiere_import(self.test_branch)
        premiere_import.save_hall(hall_set, self.test_branch)
        premiere_import.save_movies(movie_set, self.test_branch)
        premiere_import.save_sessions(session_set, self.test_branch)

        self.get_busy_place = reverse('get-busy-place')
        self.get_new_reservation = reverse('get-new-reservation')
        self.cancel_reservation = reverse('cancel-reservation')
        self.change_params = reverse('change-reservation-params')
        self.change_place = reverse('change-reservation-place')

    def test_get_busy_place_for_existing_show(self):

        show = movie.models.Showtime.objects.all()

        data = {
            'session': show.first().id
        }

        response = self.client.post(self.get_busy_place, data, format='json')
        print('test_get_busy_place_for_existing_show', response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('error', None), '')

    def test_get_busy_place_for_does_not_existing_show(self):

        data = {
            'session': 99999
        }

        response = self.client.post(self.get_busy_place, data, format='json')

        print('test_get_busy_place_for_does_not_existing_show', response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotEqual(response.data.get('error', None), '')

    def test_get_reservation_for_existing_show(self):

        show = movie.models.Showtime.objects.all()

        data = {
            'session': show.first().id,
            'places': [[2, 5], [2, 6]],
        }

        response = self.client.post(self.get_new_reservation, data, format='json')
        print('test_get_reservation_for_existing_show', response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('error', None), '')

