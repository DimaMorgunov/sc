import pytz
import datetime
from .models import Movie, Showtime, Genre, PicturesFromMovie, MovieFormat
from rest_framework import serializers
from seo.serializers import ForFilmSerializer, ForFilms
from backoffice.models import Branch


class SessionSerializer(serializers.ModelSerializer):
    hall = serializers.CharField(
        source='hall_id.name',
    )

    class Meta:
        model = Showtime
        fields = (
            'id',
            'hall',
            'standard_price',
            'start',
            'vip_price',
        )


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = (
            'id',
            'name',
        )


class FormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovieFormat
        fields = ('title',)


class PosterSerializer(serializers.ModelSerializer):
    class Meta:
        model = PicturesFromMovie
        fields = (
            'poster',
            'top',
            'position',
        )


class PosterListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PicturesFromMovie
        fields = ('poster',)


class ScheduleSerializer(serializers.ModelSerializer):
    showtime_list = serializers.SerializerMethodField('get_showtime')
    format = serializers.CharField(
        source='displayed_format.title',
    )

    url_to_movie = serializers.SerializerMethodField('get_url')

    @staticmethod
    def get_url(movie):
        return ForFilms.objects.filter(fk=movie).first().url

    @staticmethod
    def get_format(movie_format, qs):
        return qs.filter(show_format=movie_format)

    def get_showtime(self, some_movie):
        mf = MovieFormat.objects.all()

        show_qs = Showtime.objects.filter(
            hall_id__branch=self.context['branch'],
            movietobranch_id__movie_id_id=some_movie.id,
            public=True,
            start__day=self.context['day'],
            start__month=self.context['month'],
            start__year=self.context['year']
        ).order_by('start')

        data_dict = {}
        for movie_format in mf:
            data_dict[movie_format.title] = SessionSerializer(
                instance=ScheduleSerializer.get_format(movie_format, show_qs),
                many=True).data

        return data_dict

    class Meta:
        model = Movie
        fields = (
            'title',
            'showtime_list',
            'poster',
            'detail_background',
            'age',
            'format',
            'url_to_movie',
        )


class MovieSerializer(serializers.ModelSerializer):
    genres = GenreSerializer(source='genre', many=True)
    pictures = serializers.SerializerMethodField('get_act_poster')
    seo_inline = serializers.SerializerMethodField('get_seo_for_object')
    film_release_date = serializers.SerializerMethodField('get_formatted_date')
    format = serializers.CharField(
        source='displayed_format.title',
    )

    @staticmethod
    def get_seo_for_object(movie_object):
        qs = ForFilms.objects.get(fk=movie_object)
        serialize = ForFilmSerializer(instance=qs, many=False)
        return serialize.data

    @staticmethod
    def get_act_poster(film_id):
        qs = PicturesFromMovie.objects.filter(movie_id=film_id).order_by('position')
        serializer = PosterSerializer(instance=qs, many=True)
        return serializer.data

    @staticmethod
    def get_formatted_date(movie):
        return movie.start_demonstration.strftime("%d.%m.%Y")

    class Meta:
        model = Movie
        fields = (
            'id',
            'title',
            'genres',
            'poster',
            'age',
            'producer',
            'film_release_date',
            'pictures',
            'trailer',
            'seo_inline',
            'actors',
            'description',
            'year',
            'detail_background',
            'country',
            'director',
            'screenplay',
            'duration',
            'format',
        )


class MovieListSerializer(MovieSerializer):
    pictures = serializers.SerializerMethodField('get_act_poster')

    @staticmethod
    def get_act_poster(film_id):
        qs = PicturesFromMovie.objects.filter(movie_id=film_id).order_by('-position').first()
        serializer = PosterListSerializer(instance=qs)
        return serializer.data['poster']

    class Meta:
        model = Movie
        fields = (
            'id',
            'title',
            'age',
            'poster',
            'have_showtime',
            'format',
            'seo_inline',
            'film_release_date',
            'pictures',
            'trailer',
        )


class MovieForTicketSerializer(MovieSerializer):
    class Meta:
        model = Movie
        fields = (
            'title',
            'age',
            'genres',
            'poster',
            'format',
            'seo_inline',
            'year',
        )


class MovieForTicketFlatSerializer(MovieSerializer):
    class Meta:
        model = Movie
        fields = (
            'title',
            'seo_inline',
        )


class MovieMobileAPKSerializer(serializers.ModelSerializer):
    showtime_list = serializers.SerializerMethodField()
    genres = GenreSerializer(source='genre', many=True)
    film_release_date = serializers.SerializerMethodField('get_formatted_date')

    format = serializers.CharField(
        source='displayed_format.title',
    )

    @staticmethod
    def get_formatted_date(movie):
        return movie.start_demonstration.strftime("%d.%m.%Y")

    @staticmethod
    def get_format(movie_format, qs):
        return qs.filter(show_format=movie_format)

    def get_showtime_list(self, some_movie):
        dt = self.context.get('dt')

        if not dt:
            tz_info = pytz.timezone('Europe/Kiev')
            dt = datetime.datetime.now().replace(tzinfo=tz_info)

        mf = MovieFormat.objects.all()
        branch_qs = Branch.objects.all()
        res = {}

        for branch_obj in branch_qs:
            res[branch_obj.id] = self.get_showtime(some_movie, mf, branch_obj, dt)

        return res

    def get_showtime(self, some_movie, format_query_set, branch, date):
        show_qs = Showtime.objects.filter(
            movietobranch_id__movie_id_id=some_movie.id,
            hall_id__branch=branch,
            start__gte=date,
            public=True,
        ).order_by('start')

        print(show_qs.query)

        data_dict = {}
        for movie_format in format_query_set:
            data_dict[movie_format.title] = SessionSerializer(
                instance=ScheduleSerializer.get_format(movie_format, show_qs),
                many=True,
            ).data

        # if some_movie.title == 'Аліта: Бойовий ангел':
            #     show_qs = Showtime.objects.filter(
            #         hall_id__branch=branch,
            #         movietobranch_id__movie_id_id=some_movie.id,
            #     ).order_by('start')
            #
            #     super_dict = {}
            #     for movie_format in format_query_set:
            #         super_dict[movie_format.title] = SessionSerializer(
            #             instance=ScheduleSerializer.get_format(movie_format, show_qs),
            #             many=True,
            #         ).data
            #
            #     print(super_dict)
            # print(show_qs.query)

        return data_dict

    class Meta:
        model = Movie
        fields = (
            'title',
            'age',
            'genres',
            'poster',
            'producer',
            'film_release_date',
            'trailer',
            'actors',
            'description',
            'year',
            'detail_background',
            'country',
            'director',
            'screenplay',
            'duration',
            'format',
            'showtime_list',
        )
