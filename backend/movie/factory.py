import factory
from django.utils import timezone
from . import models
from factory.fuzzy import FuzzyDateTime, FuzzyInteger, FuzzyChoice
import random
import datetime
import faker


class MoviesFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = models.Movie

    title = FuzzyChoice(choices=['Матрица - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'День Сурка - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'Норм Фильм - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'Мстители - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'Петухи - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'Капитан  - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'Крутизна - ' + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 'Продуссер не смог выбрать норм название для фильма и выбрал - '
                                 + str(faker.Faker('ru_RU').word()) + str(faker.Faker('ru_RU').word()),
                                 ])
    original_title = FuzzyChoice(choices=['Matrix', 'Mad Max', 'CocoMo'])
    start_publication = FuzzyDateTime(timezone.now())
    start_demonstration = timezone.now() + datetime.timedelta(days=7*random.randrange(0, 1))
    end_demonstration = timezone.now() + datetime.timedelta(days=7*random.randrange(2, 3))
    trailer = 'https://www.youtube.com/watch?v=UsnDPok1ZOU'
    description = factory.Faker('paragraphs')
    year = timezone.now().year
    country = factory.Faker('country')
    director = factory.Faker('name')
    screenplay = factory.Faker('name')
    producer = factory.Faker('name')
    actors = factory.Faker('name')
    duration = 150
    public = True
    age = FuzzyInteger(3, 18)
    position = FuzzyInteger(0, 100)

    @factory.post_generation
    def genre_generator(self, create, extracted, **kwargs):
        movie = models.Movie.objects.get(title=self)

        movie.genre.add(
            models.Genre.objects.all().last(),
            models.Genre.objects.all().first()
        )

        for i in range(random.randrange(5, 12)):
            models.PicturesFromMovie.objects.create(
                movie_id=movie,
            )

        schedule = models.MovieToBranch.objects.filter(movie_id=None, ).last()
        schedule.movie_id = movie
        schedule.save()

