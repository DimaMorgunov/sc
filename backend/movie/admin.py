from django.contrib import admin
from .models import *
from sorl.thumbnail.admin import AdminImageMixin
from seo.admin import FilmsSeoInline
from daterange_filter.filter import DateRangeFilter


def make_published(self, request, queryset):
    queryset.update(public=True)
    self.message_user(request, "Успешно")


make_published.short_description = "Опубликовать"


def make_unpublished(self, request, queryset):
    queryset.update(public=False)
    self.message_user(request, "Успешно")


make_unpublished.short_description = "Скрыть"


class PosterInline(AdminImageMixin, admin.TabularInline):
    model = PicturesFromMovie
    extra = 1


class MovieAdmin(AdminImageMixin, admin.ModelAdmin):
    ordering = ["id"]
    inlines = [FilmsSeoInline, PosterInline, ]
    list_editable = ["position", ]
    actions = [make_published, make_unpublished]
    list_filter = ["title"]
    list_display_links = ['id', "title", ]
    list_display = ['id', "title", "public", 'show_exist', 'rec_in_email', "position", ]
    search_fields = ["id", "title"]
    fieldsets = (
        ("Основная информация о фильме", {
            'fields': ('title', 'poster', 'genre', 'trailer', 'year',   'public', 'age', 'position', 'displayed_format', )
        }),
        ('Даты', {
            'fields': ('start_publication', 'start_demonstration', 'end_demonstration')
        }),
        ('Описание фильма', {
            'fields': ('description',  'duration', 'detail_background')
        }),

        ('Семочная группа', {
            'fields': ('country', 'director', 'screenplay', 'producer', 'actors',)
        }),

        ('Email', {
            'fields': ('rec_in_email', 'email_background', )
        }),

    )

    def show_exist(self, obj):
        return obj.have_showtime
    show_exist.boolean = True
    show_exist.short_description = "Сеансы"

    class Meta:
        model = Movie
        verbose_name = "Фильм"
        verbose_name_plural = "Фильмы"


class MovieToBranchAdmin(admin.ModelAdmin):
    list_display = ["id", "premiere_id", "premiere_name", "movie_id", "branch_id", ]
    search_fields = ["movie_id", "branch"]
    list_filter = ["movie_id", "branch_id"]

    class Meta:
        model = MovieToBranch


class ShowtimeAdmin(admin.ModelAdmin):
    list_display = ["id", "premiere_name", "branch_name", "hall_id", "public", "start", "show_format"]
    search_fields = ["movietobranch_id__premiere_name", "start", "premiere_id"]
    list_filter = ["movietobranch_id__branch_id__name", "show_format",
                   ('start', DateRangeFilter)]
    actions = [make_published, make_unpublished]
    ordering = ["start"]
    fieldsets = (
        ("Фильм", {
            'fields': ('movietobranch_id', 'show_format', 'public', )
        }),
        ('Сеанс', {
            'fields': ('hall_id', 'premiere_id', 'start')
        }),

        ('Цены', {
            'fields': ('vip_price', 'standard_price',)
        }),

    )

    def premiere_name(self, obj):
        return obj.movietobranch_id.premiere_name

    def branch_name(self, obj):
        return obj.movietobranch_id.branch_id.name

    branch_name.short_description = "Отделения"
    premiere_name.short_description = "Название фильма в премьере"

    class Meta:
        model = Showtime
        verbose_name = "Сеанс"
        verbose_name_plural = "Сеансы"


admin.site.register(Genre)
admin.site.register(MovieFormat)
admin.site.register(MovieToBranch, MovieToBranchAdmin)
admin.site.register(Movie, MovieAdmin)
admin.site.register(Showtime, ShowtimeAdmin)
