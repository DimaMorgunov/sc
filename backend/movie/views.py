from cinema import settings
from .models import Movie, Showtime
from backoffice.models import Branch
from django.utils import timezone
from movie.serializers import MovieSerializer, ScheduleSerializer, MovieListSerializer, MovieMobileAPKSerializer
from backoffice.serializers import BranchGlobalSerializer
from pages.serializers import PostsListSerializer, Posts
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.utils import ProgrammingError, OperationalError
from django.db.models import Count, Q
from django.views.decorators.csrf import csrf_exempt
import datetime
import pytz
import logging

logger = logging.getLogger('django')


class ScheduleInJson(APIView):
    authentication_classes = []

    def get_active_days(self, branch):
        tz_info = pytz.timezone('Europe/Kiev')
        date_obj = datetime.datetime.now().replace(tzinfo=tz_info)

        shows_day_list = Showtime.objects.filter(
            movietobranch_id__branch_id=branch,
            start__date__gte=date_obj.date()
        ).values_list('start', flat=True)

        if not shows_day_list.exists():
            logger.info('[Schedule] Shows does not exist')

        shows_day_list = [show.date() for show in shows_day_list]
        return set(shows_day_list)

    def get(self, request, branch):

        active_days = self.get_active_days(branch)
        return Response({'active_days': active_days})

    def post(self, request, branch):
        tz_info = pytz.timezone('Europe/Kiev')
        date_obj = datetime.datetime.now().replace(tzinfo=tz_info)

        day = request.data.get('day', date_obj.day)
        month = request.data.get('month', date_obj.month)
        year = request.data.get('year', date_obj.year)

        active_days = self.get_active_days(branch)

        date = datetime.date(year=year, month=month, day=day, )
        if date not in active_days:
            logger.info('[Schedule] Now sessions for this date')
            return Response({'Error': 'На цю дату ще не визначені сеанси. Спробуйте іншу дату.'}, status=404)

        movie_query = Movie.objects.filter(
            movietobranch__showtime__start__day=day,
            movietobranch__showtime__start__month=month,
            movietobranch__showtime__start__year=year,
            movietobranch__branch_id=branch
        ).annotate(dcount=Count('id'))

        serializer = ScheduleSerializer(movie_query, many=True, context=dict(day=day, month=month,
                                                                             year=year, branch=branch))

        return Response(serializer.data)


class MovieDetailInJson(APIView):
    authentication_classes = []

    def post(self, request):
        try:
            query = Movie.objects.get(forfilms__url=request.data['url'])
            serializer = MovieSerializer(query)
            return Response(serializer.data)
        except Movie.DoesNotExist:
            logger.info('[Movie] This movie - %s - Does not exist ' % request.data['url'])
            return Response({"error": 'DoesNotExist'})


class AllInJson(APIView):
    authentication_classes = []

    def post(self, request):
        city_id = request.data.get('city_id', None)
        return Response(self.get_info(city_id))

    def get_info(self, city_id):
        dt = datetime.datetime.now()
        dt = dt.astimezone(pytz.timezone(settings.Base.TIME_ZONE))

        try:
            main_query = Movie.objects.filter(
                public=True,
                start_publication__lte=dt.date(),
                end_demonstration__gte=dt.date(),
            ).order_by('-position')

            post_queryset = Posts.objects.filter(
                public=True,
            ).order_by('-position')

            if city_id:
                main_query = main_query.filter(movietobranch__branch_id=city_id)
                post_queryset = post_queryset.filter(cinema=city_id)

            queryset_rent = main_query.filter(start_demonstration__lte=dt.date())
            queryset_soon = main_query.filter(start_demonstration__gt=dt.date())

            branch_queryset = Branch.objects.all()

            # Фильмы сейчас в прокате
            rent_data = MovieListSerializer(queryset_rent, many=True).data

            # Фильмы скоро в прокате
            soon_data = MovieListSerializer(queryset_soon, many=True).data

            # Кинотеатры
            branch_data = BranchGlobalSerializer(branch_queryset, many=True).data

            # Акции
            post_data = PostsListSerializer(post_queryset, many=True).data

            return {
                'soon': soon_data,
                'rent': rent_data,
                'branch': branch_data,
                'shares': post_data,
            }

        except (ProgrammingError, OperationalError):
            logger.info('[AllInJson] data in DB does not exist')
            return {
                'error': 'data in DB does not exist'
            }


class MobileApiMainList(APIView):
    authentication_classes = []

    @csrf_exempt
    def post(self, request):
        return Response(self.get_info())

    def get_info(self):
        dt = datetime.datetime.now().astimezone(pytz.timezone(settings.Base.TIME_ZONE))

        try:
            main_query = Movie.objects.filter(
                public=True,
                start_publication__lte=dt.date(),
                end_demonstration__gte=dt.date(),
            ).order_by('-position')

            queryset_rent = main_query.filter(start_demonstration__lte=dt.date())
            queryset_soon = main_query.filter(start_demonstration__gte=dt.date())

            # Фильмы сейчас в прокате
            rent_data = MovieMobileAPKSerializer(
                queryset_rent,
                many=True,
                context={'dt': dt},
            ).data

            # Фильмы скоро в прокате
            soon_data = MovieMobileAPKSerializer(
                queryset_soon,
                many=True,
                context={'dt': dt},
            ).data

            # Кинотеатры
            branch_queryset = Branch.objects.all()
            branch_data = BranchGlobalSerializer(branch_queryset, many=True).data

            return {
                'soon': soon_data,
                'rent': rent_data,
                'branch': branch_data,
            }

        except (ProgrammingError, OperationalError):
            logger.info('[MobileApiMainList] data in DB does not exist')
            return {
                'error': 'data in DB does not exist'
            }
