from django.db import models
from django.db.models import Q
from sorl.thumbnail import ImageField
from backoffice.models import Hall, Branch
from django.utils.text import slugify
from django.utils import timezone
from transliterate import translit
import os


class DetailImgDirectoryPath(os.PathLike):
    slug = 'background'

    def get_file_name(self, filename, instance):
        return self.slug + '.' + filename.split('.')[1]

    def __init__(self, instance, filename):
        self.instance = instance
        self.filename = filename

    def __fspath__(self, *args, **kwargs):

        filename = self.get_file_name(self.filename, self.instance)

        if isinstance(self.instance, Movie):
            return 'img/{0}/{1}'.format(slugify(translit(self.instance.title, reversed=True)), filename)

        if isinstance(self.instance, PicturesFromMovie):
            return 'img/{0}/{1}'.format(slugify(translit(self.instance.movie_id.title, reversed=True)), filename)


class EmailImgDirectoryPath(DetailImgDirectoryPath):
    slug = 'email'


class PosterImgDirectoryPath(DetailImgDirectoryPath):
    slug = 'poster'


class PicturesDirectoryPath(DetailImgDirectoryPath):
    slug = 'picture'

    def get_file_name(self, filename, instance):
        try:
            num = PicturesFromMovie.objects.last().id
        except (AttributeError, PicturesFromMovie.DoesNotExist):
            num = 1

        return self.slug + '-' + str(num) + '.' + filename.split('.')[1]


class MovieFormat(models.Model):
    title = models.CharField(
        max_length=128,
        help_text="Текстовое отображение формата. ",
        verbose_name="Название:",
    )

    slug = models.CharField(
        max_length=128,
        help_text="Внутреннее название формата. ",
        verbose_name="Внутр:",
        unique=True
    )

    def __str__(self):
        return "%s формат" % self.slug

    class Meta:
        verbose_name = "Формат"
        verbose_name_plural = "Форматы"


class Genre(models.Model):
    name = models.CharField(
        max_length=128,
        help_text="Название жанра",
        verbose_name="Жанр",
        unique=True
    )

    def natural_key(self):
        return self.name

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанры"

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField(
        max_length=128,
        help_text="Название фильма. ",
        verbose_name="Название:",
        unique=True
    )

    genre = models.ManyToManyField(
        Genre,
        help_text="Выберите подходящий жанр",
        verbose_name="Жанры",
    )

    start_publication = models.DateField(
        verbose_name="Время начала отображения",
        help_text="Дата начала показа фильма на cайте",
    )

    start_demonstration = models.DateField(
        verbose_name="Начало проката:",
        help_text="Дата начала проката.",
    )

    end_demonstration = models.DateField(
        verbose_name="Конец проката:",
        help_text="Дата окончания проката.",
    )

    displayed_format = models.ForeignKey(
        MovieFormat,
        on_delete=models.CASCADE,
        verbose_name="Формат:",
        help_text="Формат который будет отображаться в описании фильма, а также на постере .",
        default=1,
    )

    trailer = models.CharField(
        max_length=256,
        help_text="Вставьте код с Youtube. Прямая ссылка на ролик.",
        verbose_name="Трейлер:",
        blank=True,
        null=True
    )

    poster = ImageField(
        upload_to=PosterImgDirectoryPath,
        help_text="Выберите путь к постеру",
        verbose_name="Постер:",
        default='/default/poster.jpg',
        blank=True,
        null=True
    )

    detail_background = ImageField(
        upload_to=DetailImgDirectoryPath,
        help_text="Выберите путь к картинке",
        verbose_name="Фон для детальной страницы фильма:",
        default='/default/detail_background.jpg',
        blank=True,
        null=True
    )

    email_background = ImageField(
        upload_to=EmailImgDirectoryPath,
        help_text="Выберите путь к картинке",
        verbose_name="Фон в email сообщении:",
        default='/default/email_background.jpg',
        blank=True,
        null=True
    )

    description = models.TextField(
        help_text="Введите описание",
        verbose_name="Описание:",
        blank=True,
        null=True,
    )

    year = models.IntegerField(
        help_text="Укажите год выпуска фильма",
        verbose_name="Год:",
        default='2018',
        blank=True,
        null=True
    )

    country = models.CharField(
        max_length=128,
        help_text="Укажите страну производства",
        verbose_name="Страна",
        blank=True,
        null=True
    )

    director = models.CharField(
        max_length=128,
        help_text="Укажите имя режисера",
        verbose_name="Режисер",
        blank=True,
        null=True
    )

    screenplay = models.CharField(
        max_length=128,
        help_text="Укажите имя сценариста",
        verbose_name="Сценарист",
        blank=True,
        null=True
    )

    producer = models.CharField(
        max_length=128,
        help_text="Укажите имя продюссера",
        verbose_name="Продюсер",
        blank=True,
        null=True,
    )

    actors = models.TextField(
        max_length=128,
        help_text="Укажите список актеров фильма. Имена должны быть разделены запятой",
        verbose_name="В ролях",
        blank=True,
        null=True,
    )

    duration = models.IntegerField(
        help_text="Укажите продолжительность фильма в минутах",
        verbose_name="Время",
        blank=True,
        null=True,
    )

    public = models.BooleanField(
        help_text="Если чекбокс активирован, то фильм будет опубликован на сайте. "
                  "Если чекбокс не активирован, то фильм будет скрыт",
        default=True,
        verbose_name="Опубликовать",
    )

    rec_in_email = models.BooleanField(
        help_text="Если чекбокс активирован, то фильм будет рекомендован в Email сообщениях после покупки билета, для "
                  "фильмов дата начала проката которых меньше чем дата на которую были куплены билеты.",
        default=True,
        verbose_name="Рекомендивать после покупки",
    )

    age = models.IntegerField(
        help_text="Укажите возрастные ограничения, например, \"16\"",
        verbose_name="Возраст",
        default=12
    )

    position = models.IntegerField(
        default=0,
        help_text="Вес сортировки фильмов в списке. Чем больше значение, тем ближе к началу",
        verbose_name="Вес",
        blank=True,
        null=True
    )

    @property
    def main_picture_url(self):
        return PicturesFromMovie.objects.filter(movie_id=self, ).order_by('position').first().poster.url

    @property
    def email_picture_url(self):
        return self.email_background.url

    @staticmethod
    def email_recommendation(current_movie_id=None):
        q = Movie.objects.filter(
            start_demonstration__gt=timezone.now().date(), rec_in_email=True
        ).order_by('start_demonstration')

        if current_movie_id:
            for movie in q:
                if movie.id != current_movie_id:
                    return movie

        return q.first()

    @property
    def have_showtime(self):
        try:
            shows = Showtime.objects.filter(
                movietobranch_id__movie_id__id=self.id,
            ).order_by('start').count()

            if shows > 0:
                return True

        except Showtime.DoesNotExist:
            pass

        return False

    def today_showtime(self):
        try:
            branch_filter = Q(movietobranch_id__branch_id=self.branch)
        except:
            branch_filter = None

        try:
            shows = Showtime.objects.filter(
                movietobranch_id__movie_id__id=self.id,
                public=True,
                start__date=timezone.now().date()
            ).order_by('start')

            if branch_filter:
                shows = shows.filter(branch_filter)

            if shows.__len__() > 0:
                return shows
        except Showtime.DoesNotExist:
            pass

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Фильм"
        verbose_name_plural = "Фильмы"


class PicturesFromMovie(models.Model):
    movie_id = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        verbose_name="Фильм"
    )

    poster = ImageField(
        upload_to=PicturesDirectoryPath,
        help_text="Выберите путь к изображению",
        verbose_name="Изображение:",
        default='/default/frame.jpg',
    )

    top = models.BooleanField(
        help_text="Укажите, является ли этот кадр главным для фильма. "
                  "Если кадр указан как главный, то он будет показан в большом слайдере на главной странице сайта.",
        default=False,
        verbose_name="Главный кадр",
    )

    position = models.IntegerField(
        default=0,
        help_text="Вес сортировки изображения в списке. Чем больше значение, тем ближе к началу",
        verbose_name="Вес"
    )

    def __str__(self):
        name = "{}".format(self.poster.name)
        return name

    class Meta:
        verbose_name = "Фотография"
        verbose_name_plural = "Фотографии"


class MovieToBranch(models.Model):
    movie_id = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        help_text="выберите фильм к которому будут привязаны импортированные сеансы",
        verbose_name="Фильм:",
        default=None,
        null=True,
    )

    branch_id = models.ForeignKey(
        Branch,
        on_delete=models.CASCADE,
        editable=False,
    )

    premiere_id = models.IntegerField(
        help_text="ID фильма. Импортируется из базы данных \"Премьера\".",
        verbose_name="ID фильма:",
        editable=False,
    )

    premiere_name = models.CharField(
        help_text="Название фильма. Импортируется из базы данных \"Премьера\".",
        verbose_name="Название фильма:",
        editable=False,
        default=None,
        max_length=124,
    )

    def __str__(self):
        name = "{}".format(self.movie_id)
        return name

    class Meta:
        unique_together = ('movie_id', 'branch_id',)
        verbose_name = "Связь фильма с обьектом"
        verbose_name_plural = "Связь фильма с обьектами"


class Showtime(models.Model):
    premiere_id = models.IntegerField(
        verbose_name="id сеанса в ПО Премьера",
    )

    movietobranch_id = models.ForeignKey(
        MovieToBranch,
        on_delete=models.CASCADE,
        verbose_name="фильм",
        blank=True,
        null=True,
    )

    hall_id = models.ForeignKey(
        Hall,
        on_delete=models.CASCADE,
        verbose_name="зал",
    )

    standard_price = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="Обычные места",
        help_text="Цена на обычные места",
    )

    vip_price = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="VIP места",
        help_text="Цена на диваны",
    )

    start = models.DateTimeField(
        verbose_name="время начала сеанса"
    )

    public = models.BooleanField(
        help_text="поставте галочку если сеанс готов к публикации",
        default=True,
        verbose_name="опубликовать",
    )

    show_format = models.ForeignKey(
        MovieFormat,
        on_delete=models.CASCADE,
        verbose_name="Формат фильма",
        default=None,
        blank=True,
        null=True,
    )

    def get_time(self):
        return self.start.time()

    def __str__(self):
        return "Сеанс - {} на - {} ".format(self.movietobranch_id, self.start)

    class Meta:
        unique_together = ('premiere_id', 'movietobranch_id',)
        verbose_name = "Сеанс"
        verbose_name_plural = "Сеансы"
