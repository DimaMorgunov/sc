from django.urls import path
from . import views


urlpatterns = [
    path('schedule/<int:branch>/', views.ScheduleInJson.as_view()),
    path('detail/', views.MovieDetailInJson.as_view()),
    path('', views.AllInJson.as_view()),
    path('main_list/', views.MobileApiMainList.as_view()),
]

