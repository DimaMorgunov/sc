from django.urls import path
from . import views


urlpatterns = [
    path('<int:id>/', views.tv_anons, name='tv_anons_page'),
    path('', views.city_select),
]
