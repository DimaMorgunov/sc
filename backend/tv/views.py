from django.shortcuts import render
from movie.models import Movie
from backoffice.models import Branch
from django.db.models import Count
import datetime
import pytz


def tv_anons(request, id):

    tz_info = pytz.timezone('Europe/Kiev')
    date_obj = datetime.datetime.now().replace(tzinfo=tz_info)

    day, month, year = date_obj.day, date_obj.month, date_obj.year

    movie_query = Movie.objects.filter(
        movietobranch__showtime__start__day=day,
        movietobranch__showtime__start__month=month,
        movietobranch__showtime__start__year=year,
        movietobranch__branch_id=id
    ).annotate(dcount=Count('id'))

    for movie in movie_query:
        movie.branch = id

    branch_scale = Branch.objects.filter(id=id).first().scale
    return render(request, 'tv_templates/index.html', locals())


def city_select(request):

    branch_list = Branch.objects.all()
    return render(request, 'tv_templates/city_list.html', locals())

