import re, socket, time, random, logging, platform

from datetime import datetime
from xml.etree import ElementTree
from backoffice.models import Branch
from django.conf import settings

logger = logging.getLogger('premiere')
OS_NAME = platform.system()


class CabinetSocket:
    """
    A class for connecting to Premiera Software via TCP/IP socket and getting
    raw XML-data.
    """

    def __init__(self, host, port, timeout, buffer_size):
        if buffer_size < 512:
            buffer_size = 512
        self.buffer_size = buffer_size  # Setting socket buffer type buffer_size: int
        self.timeout = timeout  # Setting socket timeout type: timeout: int
        self.host = host  # IP-address of Premiera Software type host: str or unicode
        self.port = port  # port number of Premiera Software type port: int
        self.closed = False

    def connect(self):
        """
        Connecting to socket or raising timeout error
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        try:
            self.sock.connect((self.host, self.port))
        except socket.timeout:
            logger.info('[Premiere][Cabinet] Socket timeout error')
            raise Exception("Превышено время ожидания соединения с сервером")

    def send_query(self, query):
        try:
            self.connect()  # connect to host
            self.sock.send(query)  # send to host query messsege

            res = self.get_xml()
            self.socket_close()
            return res
        except Exception as e:
            self.socket_close()
            raise e

    def socket_close(self):
        self.closed = True

        try:
            if OS_NAME == 'Linux':
                self.sock.shutdown(socket.SHUT_RDWR)
                return
        except:
            pass

        self.sock.close()

    def get_xml(self):
        data = b""
        chunk = self.get_part()
        length = self.get_data_length(chunk)
        data += chunk
        content = self.get_messege_text(data, False)

        while len(content) < length:  # get data by pieces
            chunk = self.get_part()
            data += chunk
            # updated for content (not whole data) length
            content += chunk

        data = data.decode(encoding='utf-8', errors='replace')

        logger.info(
            '[CabinetSocket][get_xml] length - %s, data len - %s, content len - %s, message "%s"' %
            (length, len(data), len(content), data[-10:])
        )

        if self.get_messege_type(data) == 'Error':
            return self.get_messege_text(data)
        else:
            str_root = self.get_messege_text(data)

            if str_root == 'OK':
                return {"result": 'OK'}
            else:
                root = self.get_root(str_root)
            return root

    def get_data_length(self, chunk):
        rechunk = chunk.decode('utf-8', errors="replace")
        reg = re.compile('(Content-Length: )(\d+)')
        length = reg.findall(rechunk)
        return int(length[0][1])

    def get_messege_type(self, data):
        reg = re.compile('(Message-Type: )(\w+)')
        type = reg.findall(data)
        return type[0][1]

    def get_messege_text(self, data, encoded=True):
        if encoded:
            reg = re.compile('(Content-Length: \d+\s+)([\S\s]+)')
        else:
            reg = re.compile(b'(Content-Length: \d+\s+)([\S\s]+)')
        mesg = reg.findall(data)
        return mesg[0][1]

    def get_part(self):
        try:
            if OS_NAME == 'Linux':
                chunk = self.sock.recv(self.buffer_size, socket.MSG_WAITALL)
            else:
                chunk = self.sock.recv(self.buffer_size)
            return chunk
        except socket.timeout:
            logger.info('[Premiere][Cabinet] Socket timeout error')
            raise Exception("Превышено время ожидания соединения с сервером")

    def get_root(self, xml):
        # Linux stage and production server dont get last 1-5 symbols by socket
        # Remove this if fix socket

        xml = xml.strip()

        try:
            if xml[-5:] == 'lders':
                xml += '>'
            elif xml[-5:] == 'older':
                xml += 's>'
            elif xml[-5:] == 'Holde':
                xml += 'rs>'
            elif xml[-5:] == '/Hold':
                xml += 'ers>'
            elif xml[-5:] == '</Hol':
                xml += 'ders>'
            elif xml[-4:] == '</Ho':
                xml += 'lders>'
            elif xml[-3:] == '</H':
                xml += 'olders>'

            return ElementTree.fromstring(xml)
        except:
            logger.info('[Premiere][Cabinet] Premiere return none valide xml')
            raise Exception(u'ПО Премьера вернула невалидный XML %s' % xml[-10:])

    def __exit__(self, *args, **kwargs):
        if not self.closed:
            logging.info("[Cabinet][Socket] Closing")
            self.socket_close()


class CabinetQuery(object):
    """
    A base class for building an XML-query to send to Personal Cabinet
    """

    header_template = (u'Message-ID: {message_id}\n'
                       u'Message-Type: Request\n'
                       u'Time: {time}\n'
                       u'Terminal-Type: {terminal_type}\n'
                       u'Content-Length: {query_len}\n\n')

    body_template = (u'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n'
                     u'<Message Action="{action}" Terminal_Type="{terminal_type}"\n'
                     u'Global_Type="{global_type}" Unit_ID="1" User_ID="1">\n'
                     u'{body_message}\n'
                     u'</Message>')

    def __init__(self, terminal_type, global_type):
        """
        :param terminal_type: a param needed to fill in the Terminal_Type
                attribute of the Message tag of XML-query
        :type terminal_type: str or unicode
        :param global_type: a param needed to fill in the Global_Type
                attribute of the Message tag of XML-query
        :type: str or unicode
        """

        self.terminal_type = terminal_type
        self.global_type = global_type

    def get_query(self, action, body_message_template, kwargs):
        """
        Getting final query string to send to Personal Cabinet
        """

        body = self._get_body(action, body_message_template, kwargs)
        header = self._get_header(body)
        return header + body

    def _get_body(self, action, body_message_template, kwargs):
        """
        Constructing the request body
        """

        self.build_body_message(body_message_template, kwargs)
        return self.body_template.format(terminal_type=self.terminal_type,
                                         global_type=self.global_type,
                                         action=action,
                                         body_message=self.body_message)

    def _get_header(self, body):
        """
        Constructing the request header
        """

        # Encode body xml to utf-8 and then get its length
        query_len = len(body.encode('utf-8'))
        now = datetime.now()

        # Formatting the now according to the Personal Cabinet specification
        send_time = now.strftime("%Y-%m-%d %H:%M:%S")

        message_id = int("%d%d" % (time.time(), random.getrandbits(64)))

        return self.header_template.format(message_id=message_id,
                                           time=send_time,
                                           terminal_type=self.terminal_type,
                                           query_len=query_len)

    def build_body_message(self, body_message_template, kwargs):
        """
        Constructing the body message and updating the value of self.body_message
        """
        self.body_message = body_message_template.format(**kwargs)


class CabinetQueryTemplate(CabinetQuery):
    """
    Class for generate query body
    """

    def version_query(self):
        action = u'Get version'
        body_message_template = u''
        return self.get_query(action, body_message_template, {})

    def login_query(self, **kwargs):
        action = u'Login'
        body_message_template = (u'<Card_Code>{cardcode}</Card_Code>\n'
                                 u'<Password>{password}</Password>\n')
        return self.get_query(action, body_message_template, kwargs)

    def bonus_balance_query(self, **kwargs):
        """
        'GetPersonalData' query class
        """
        action = u'Get card info'
        body_message_template = (u'<Card_Code>{cardcode}</Card_Code>\n'
                                 u'<Include>Account</Include>\n')
        return self.get_query(action, body_message_template, kwargs)

    def personal_data_query(self, **kwargs):
        """
        'GetPersonalData' query class
        """
        action = u'Get card info'

        body_message_template = (u'<Card_Code>{cardcode}</Card_Code>\n'
                                 u'<Include>Account, Holder, Holder_Contact, '
                                 u'Holder_Card, Holder_Address</Include>\n')
        return self.get_query(action, body_message_template, kwargs)

    def remind_password_query(self, **kwargs):
        """
        'RemindPassword' query class
        """

        action = u'Change password'
        body_message_template = (u'<Card_Code>{cardcode}</Card_Code>'
                                 u'<Phone>{phone}</Phone>')
        return self.get_query(action, body_message_template, kwargs)

    def set_new_card_pass(self, holder, card, password):
        """
        Change password for card

        :param holder: Holder identifier
        :param card: Card identifier
        :param password: New password for card
        :return: Rendered change password query
        """

        if len(password) > 20:
            password = password[:20]

        action = u'Edit holders'
        body = (
            u'<Holder>\n'

            u'<Holder_ID>{holder}</Holder_ID>'

            u'<Cards><Card>'
            u'<Card_Code>{card}</Card_Code>'  # Get card
            u'<Password>{password}</Password>'  # Set card password
            u'</Card></Cards>'

            u'</Holder>\n'
        )

        return self.get_query(action, body, {
            'holder': holder,
            'card': card,
            'password': password,
        })

    def check_phone_exist_query(self, **kwargs):
        """
        Trying find user phone and email in CRM
        :param kwargs: email, phone
        :return:
        """
        action = u'Search holders'
        body_message_template = (
            u'<Include> Holder, Holder_Contact, '
            u'Holder_Card</Include>\n'

            u'<Account_Filter>\n'
            u'<Account_Class>2</Account_Class>\n'
            u'<Account_Type_ID>1</Account_Type_ID>\n'
            u'</Account_Filter>\n'
            u'<Count>25</Count>\n'
            u'<Index>1</Index>\n'
            u'<Item Mode="Add">\n'
            u'<Contacts>\n'
            u'<Phone Value="{phone}" IsNumber="True"/>\n'
            u'</Contacts>\n'
            u'</Item>\n'
        )

        return self.get_query(action, body_message_template, kwargs)

    def check_email_exist_query(self, **kwargs):
        """
        Trying find user phone and email in CRM
        :param kwargs: email, phone
        :return:
        """
        action = u'Search holders'
        body_message_template = (
            u'<Include> Holder, Holder_Contact, '
            u'Holder_Card</Include>\n'

            u'<Account_Filter>\n'
            u'<Account_Class>2</Account_Class>\n'
            u'<Account_Type_ID>1</Account_Type_ID>\n'
            u'</Account_Filter>\n'
            u'<Count>25</Count>\n'
            u'<Index>1</Index>\n'
            u'<Item Mode="Add">\n'
            u'<Contacts>\n'
            u'<EMail Value="{email}"/>\n'
            u'</Contacts>\n'
            u'</Item>\n'
        )

        return self.get_query(action, body_message_template, kwargs)

    def clear_search_query(self, **kwargs):
        action = u'Search holders'
        body_message_template = (
            u'<Search_GUID>{guid}</Search_GUID>\n'
            u'<Item Mode="Clear"/>'
        )
        return self.get_query(action, body_message_template, kwargs)


class CabinetAPI(object):
    """
    A factory class to instantiate a socket and parser objects
    """

    def __init__(self, branch=None):
        """
        :param config: settings object
        :type config: settings instance
        :param Sock: socket class to retrieve data from Premiera Software socket
        :type: Sock: socket class
        :param Parser: parser class to parse xml received from Premiera Software
        :type Parser: parser class
        """
        if branch is None:
            branch = Branch.objects.get(id=settings.DEFAULT_CRM_OBJECT_ID)
        config = branch.cabinet
        self.global_type = config.GLOBAL_TYPE
        self.terminal_type = config.TERMINAL_TYPE
        self.card_pin = branch.DEFAULT_CARD_PASSWORD
        self.default_query_dict = {'ServiceID': branch.SERVICE_ID,
                                   'Version': branch.VERSION,
                                   'Encoding': branch.ENCODING}

        self.socket = CabinetSocket(config.HOST, config.PORT, config.TIMEOUT, config.BUFFER_SIZE)
        self.query = CabinetQueryTemplate(self.terminal_type, self.global_type)
