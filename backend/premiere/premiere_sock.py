import time, socket, logging, platform
from xml.etree import ElementTree

logger = logging.getLogger('premiere')
OS_NAME = platform.system()


class PremieraSocket:
    """
    A class for connecting to Premiera Software via TCP/IP socket and getting
    raw XML-data.
    """

    def __init__(self, host, port, timeout, buffer_size):
        self.buffer_size = buffer_size  # Setting socket buffer type buffer_size: int
        self.timeout = timeout  # Setting socket timeout type: timeout: int
        self.host = host  # IP-address of Premiera Software type host: str or unicode
        self.port = port  # port number of Premiera Software type port: int
        self.try_number = 5
        self.closed = False

    def socket_close(self):
        self.closed = True

        try:
            if OS_NAME == 'Linux':
                self.sock.shutdown(socket.SHUT_RDWR)
                return
        except:
            pass

        self.sock.close()

    def connect(self):
        """
        Connecting to socket or raising timeout error
        """
        self.sock = socket.socket()
        self.sock.settimeout(self.timeout)
        try:
            for i in range(self.try_number):
                try:
                    self.sock.connect((self.host, self.port))
                    break
                except socket.timeout:
                    logger.error('[Premiere] %s timeout connection to server %s:%s' % (i, self.host, self.port))
                    time.sleep(1)
                    pass
        except Exception as e:
            logger.error('[Premiere] Error - %s' % str(e))
            raise

    def send_query(self, query):
        try:
            self.connect()  # connect to host
            self.sock.send(query)  # send to host query messsege
            return self.get_xml()
        finally:
            self.sock.close()

    def get_xml(self):
        data = b""
        chunk = self.get_part()
        while chunk != b'':  # get data by pices
            data += chunk
            chunk = self.get_part()
        self.sock.close()
        return data[11:]  # removing 000000082&
        # return data.decode(encoding="utf-8", errors="replace")

    def get_part(self):
        try:
            chunk = self.sock.recv(self.buffer_size)
        except socket.timeout:
            logger.error('[Premiere] timeout connection to server %s:%s' % (self.host, self.port))
            raise Exception("Превышено время ожидания соединения с сервером")
        else:
            return chunk

    def __exit__(self, *args, **kwargs):
        if not self.closed:
            logging.info("[Premiere][Socket] Closing")
            self.socket_close()


class PremieraQuery():
    """
    Base class for building an XML-query to send to Premiera Software
    """

    def __init__(self, default):
        self.default_query_dict = default

    def query_build(self, new_data_dict):
        query_dict = self.default_query_dict
        query_dict.update(new_data_dict)
        return self.get_query(query_dict)

    def get_query(self, qurent_query_dict):
        premiere_query_str = "&".join(['{0}={1}'.format(t, qurent_query_dict[t]) for t in qurent_query_dict])
        premiere_query_set_str = bytes('{0:010d}&{1}'.format(len(premiere_query_str), premiere_query_str), 'utf-8')
        logger.info('[Premiere][GetQuery] Query - %s ' % str(premiere_query_set_str))
        return premiere_query_set_str

    def GetSessionPrice(self, **kwargs):
        QueryDict = {
            'QueryCode': 'GetSessionPrices',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def SaleReservationQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'SaleReservation',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def SaleApprovedQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'SaleApproved',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def SaleCancelQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'SaleCancel',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def GetHallPlanQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'GetHallPlan',
            'ListType': 'Info;Row;Place;X;Y;Width;Height;Type;Fragment;Object;Background',
            # Row;Place;X;Y;Width;Height;Type;Fragment;Object;Status',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def GetHallPlanBusyQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'GetHallPlan',
            'ListType': 'Fragment;Row;Place;Busy',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def LoginQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'Login',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def GetHallsQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'GetHalls',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def GetMoviesQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'GetMovies',
            'ListType': 'PropertiesShow',
            'DateList': '',
            'Expect': 60,
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def GetSessionsQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'GetSessions',
            'DateList': ''
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)

    def SalePayReturnQuery(self, **kwargs):
        QueryDict = {
            'QueryCode': 'SalePayReturn',
        }
        QueryDict.update(kwargs)
        return self.query_build(QueryDict)


class PremieraResponseCleaner():
    def get_data(self, xml):
        root = self.get_root(xml)
        result_tag = root.find('.//Result')
        if result_tag.text == 'Ok':
            return root.find('.//Data')
        elif result_tag.text == 'Error':
            error_code_tag = root.find('.//Error')
            error_code = error_code_tag.text

            remark_tag = root.find('.//Remark')
            error_text = remark_tag.text
            return (error_code, error_text)

    def get_root(self, xml):
        try:
            root_dir = ElementTree.fromstring(xml)
        except:
            logger.error('[Premiere] Not valid XML file from API endpoint')
            msg = u'ПО Премьера вернула невалидный XML "%s"' % xml
            raise Exception('', msg)
        else:
            return root_dir


class PremieraApi:
    def __init__(self, config):
        self.config = config
        self.anonymous_cardcode = config.DEFAULT_CARD_NUMBER
        self.cardpin = config.DEFAULT_CARD_PASSWORD
        self.default_query_dict = {'ServiceID': config.SERVICE_ID,
                                   'Version': config.VERSION,
                                   'Encoding': config.ENCODING}
        self.socket = PremieraSocket(config.HOST, config.PORT, config.TIMEOUT, config.BUFFER_SIZE)
        self.query = PremieraQuery(self.default_query_dict)
        self.parser = PremieraResponseCleaner()
