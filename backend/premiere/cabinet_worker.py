from .cabinet_sock import CabinetAPI
from logging import getLogger

dlog = getLogger("debug")


class Worker(CabinetAPI):
    def get_server_version(self):
        query_str = str.encode(self.query.version_query())
        return self.socket.send_query(query_str)

    def set_new_card_password(self, **kwargs):
        query_str = str.encode(self.query.set_new_card_pass(holder=kwargs.get('holder'), card=kwargs.get('card'), password=kwargs.get('password')))
        return self.socket.send_query(query_str)

    def get_card_holder_info(self, cardnum):
        query_str = str.encode(self.query.personal_data_query(cardcode=cardnum))

        data_str = self.socket.send_query(query_str)

        holder_info = './Card/Holders/Holder/'
        holder__contact = './Card/Holders_Contacts/'
        holder__cart = './Card/Accounts/Account/'

        try:
            phone = data_str.find(holder__contact)[1].find('Value').text
        except IndexError:
            phone = ''

        try:
            email = data_str.find(holder__contact)[2].find('Value').text
        except IndexError:
            email = ''

        return {
            'last_name': data_str.find(holder_info + 'L_Name').text,
            'first_name': data_str.find(holder_info + 'F_Name').text,
            'middle_name': data_str.find(holder_info + 'M_Name').text,
            'Birth': data_str.find(holder_info + 'Birth').text,
            "Balance": data_str.find(holder__cart + 'Balance').text,
            'Phone': phone,
            'Email': email,
        }

    def remind_password(self, card_num, phone, _log=None):
        if not _log:
            _log = ""

        query_str = str.encode(self.query.remind_password_query(cardcode=card_num, phone=phone))
        _log += "\nQuery string:\n%s\n\n" % str(query_str)
        data_str = self.socket.send_query(query_str)
        _log += "Data string:\n%s\n" % str(data_str)

        dlog.info(_log)
        return data_str

    def login(self, card_num, password):

        query_str = str.encode(self.query.login_query(cardcode=card_num, password=password))
        data_str = self.socket.send_query(query_str)

        return data_str

    def get_user_by_phone(self, phone, email=None):

        query_str = str.encode(self.query.check_phone_exist_query(phone=phone))

        data_str = self.socket.send_query(query_str)

        # Get Search_GUID data
        Search_GUID = data_str.find('.').attrib['Search_GUID']

        # Clean search
        self.clear_search(Search_GUID)

        data_str_by_phone = self.socket.send_query(query_str)
        if data_str_by_phone.find('.').attrib['Count'] == '0':
            if email:
                self.clear_search(Search_GUID)
                query_str = str.encode(self.query.check_email_exist_query(email=email))

                data_str_by_email = self.socket.send_query(query_str)

                if data_str_by_email.find('.').attrib['Count'] == '0':
                    return {'card': False}
                return self.clean_holder_data(data_str_by_email)
            return {'card': False}
        return self.clean_holder_data(data_str_by_phone)

    def clean_holder_data(self, xml):
        holder__contact = './Holder/Holders_Contacts/'

        result = {}

        try:
            result['Phone'] = xml.find(holder__contact)[1].find('Value').text
        except (TypeError, AttributeError):
            result['Phone'] = ''

        try:
            for card in xml.findall("./Holder/Holders_Cards/Holder_Card/Card"):
                status = card.find('Status').text
                if status == "Active":
                    result['Card_Code'] = card.find('Card_Code').text
                    break
            else:
                result['Card_Code'] = ''
        except (TypeError, AttributeError):
            result['Card_Code'] = ''

        return result

    def clear_search(self, guid):

        query_str = str.encode(self.query.clear_search_query(guid=guid))
        self.socket.send_query(query_str)

        return True

    def get_bonus(self, cardnum):

        query_str = str.encode(self.query.bonus_balance_query(cardcode=cardnum))
        data_str = self.socket.send_query(query_str)

        holder__cart = './Card/Accounts/Account/'

        return {
            "Balance": data_str.find(holder__cart + 'Balance').text,
        }
