import datetime
from django.utils import timezone
from .premiere_sock import PremieraApi
import logging
logger = logging.getLogger('premiere')


class PremieraImport(PremieraApi):

    def get_hall(self):
        query = self.query.GetHallsQuery()
        data = self.socket.send_query(query)
        hall_dir = "./Theatres/Theatre/Halls"
        root_dir = self.parser.get_data(data)
        data_set = []
        for element in root_dir.find(hall_dir):
            set = {
                'hall_id': element.attrib['ID'],
                'name': element.find('Name').text
            }
            data_set += [set]
        return data_set

    def get_movies(self):
        query = self.query.GetMoviesQuery()
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        hall_dir = "./Movies"
        data_set = []
        for element in root_dir.find(hall_dir):
            info = {
                'premiere_id': element.attrib['ID'],
                'premiere_name':  element.find('Name').text,
            }
            data_set += [info]

        return data_set

    def get_session(self):
        logger.info("[Premiere import] Importing start at: %s " % datetime.datetime.now().time())
        query = self.query.GetSessionsQuery()
        logger.info("[Premiere import] Importing get query at: %s " % datetime.datetime.now().time())

        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        logger.info("[Premiere import] Importing get response at: %s " % datetime.datetime.now().time())

        hall_dir = "./Sessions"
        data_set = []

        logger.info("[Premiere import] Importing parsing start at: %s " % datetime.datetime.now().time())
        for element in root_dir.find(hall_dir):
            new_socket = PremieraImport(self.config)
            showtime_date = element.find('Date').text.split('.')
            showtime_time = element.find('Time').text.split(':')
            showtime_datetime = timezone.make_aware(datetime.datetime(
                year=int(showtime_date[2]),
                month=int(showtime_date[1]),
                day=int(showtime_date[0]),
                hour=int(showtime_time[0]),
                minute=int(showtime_time[1]),
                tzinfo=None
            ), timezone.get_current_timezone())
            set = {
                'premiere_id': element.attrib['ID'],
                'hall_id': element.find('./Theatre/Hall').attrib['ID'],
                'movie_id':  element.find('Movie').attrib['ID'],
                'date': showtime_datetime,
                'tehnology': element.find('Format').text,
                'price': new_socket.get_price(movie_id=element.attrib['ID'])
            }

            data_set += [set]
        logger.info("[Premiere import]importing end at: %s " % datetime.datetime.now().time())
        return data_set

    def get_price(self, movie_id):
        query = self.query.GetSessionPrice(Sessions=movie_id)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        hall_dir = "./PlacesTypes"
        data_set = {}
        if isinstance(root_dir, tuple):
            return root_dir
        else:
            for element in root_dir.find(hall_dir):
                try:
                    data_set[element.find('Name').text] = element.find('Sum').attrib['Sum']
                except AttributeError:
                    pass
            return data_set

    def get_full_str_price(self, movie_id):
        query = self.query.GetSessionPrice(Sessions=movie_id)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        hall_dir = "./PlacesTypes"
        data_set = {}
        if isinstance(root_dir, tuple):
            return root_dir
        else:
            for element in root_dir.find(hall_dir):
                logger.info("[Premiere import] Get price %s, %s, %s" % (element.attrib, element.tag, element.text))
            return data_set

    def get_busy_place(self, movie_id):
        query = self.query.GetHallPlanBusyQuery(Sessions=movie_id)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        hall_dir = "./Session/Theatre/Hall/Levels/Level/Places"
        list_set = []
        if isinstance(root_dir, tuple):
            return {'error': root_dir[1]}
        else:
            element = root_dir.find(hall_dir)

            for child in element:
                list_set += [{
                    'row': child.attrib['Row'],
                    'place': child.attrib['Place'],
                    }]
            data_result = {
                    'error': '',
                    'places': list_set,
            }
            return data_result

    def get_hall_plan(self, hall_id):
        query = self.query.GetHallPlanQuery(Halls=hall_id)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        hall_dir = "./Theatres/Theatre/Halls/Hall/Levels/Level/Places"
        data_set = []
        data_level = root_dir.find("./Theatres/Theatre/Halls/Hall/Levels/Level").attrib['ID']
        for element in root_dir.find(hall_dir):
            data_set += [element.attrib]
        return data_level, data_set,

    def get_login(self, CardCode, CardPIN):

        query = self.query.LoginQuery(
                CardCode=CardCode,
                CardPIN=CardPIN,
                )
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        if isinstance(root_dir, tuple):
            return {'error': root_dir[1]}
        else:
            Account = root_dir.find('./Accounts/Account').attrib['AccountNumber']
            Owner = root_dir.find('./Owner').attrib['ID']
            return Owner, Account

    def get_reservation(self, **kwargs):
        query = self.query.SaleReservationQuery(**kwargs)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        return root_dir

    def cancel_reservation(self, **kwargs):
        query = self.query.SaleCancelQuery(**kwargs)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        return root_dir

    def pay_return(self, **kwargs):
        query = self.query.SalePayReturnQuery(**kwargs)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_root(data)
        return root_dir

    def approved_reservation(self, **kwargs):
        query = self.query.SaleApprovedQuery(**kwargs)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)

        return root_dir

    def sale_reservation(self, **kwargs):
        query = self.query.SaleReservationQuery(**kwargs)
        data = self.socket.send_query(query)
        root_dir = self.parser.get_data(data)
        return root_dir

