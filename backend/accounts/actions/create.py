import logging

from . import _check_user, _get_bonus, _login_crm, _send_sms, get_or_create_csrf_token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from accounts.serializers import UserSerializer
from django.conf import settings
from accounts.models import User, UserConfirm
from datetime import timedelta
from django.utils import timezone
from django.db import IntegrityError
from django.contrib.auth import login

sender_logger = logging.getLogger('sender')
dlog = logging.getLogger("debug")


class UserCreate(APIView):

    def save_serialized_user_data(self):
        try:
            user = User.objects.get(
                username=self.request.data.get('username'),
                is_active=False,
            )

            user.delete()
            # TODO refactor this
        except User.DoesNotExist:
            pass

        serializer = UserSerializer(data=self.request.data)

        if serializer.is_valid():
            return None, serializer.save(), serializer.data
        else:
            return serializer.errors, None, None

    def post(self, request):
        if not self.request:
            self.request = request

        error, self.user, self.user_info = self.save_serialized_user_data()

        if self.user:
            self.user.is_active = False
            self.user.save()

            try:
                self.card_info = _check_user(
                    email=self.user.email,
                    phone=self.user.usercrm.phone
                )
            except Exception as e:
                self.user.delete()
                return Response({'description': str(e)}, status=status.HTTP_400_BAD_REQUEST)

            if self.card_info.get('card', True):
                json = {'card': self.card_info['Card_Code']}
                try:
                    self.save_bonus_card()
                except IntegrityError:
                    self.user.delete()
                    return Response({'description': 'User with this card already exist'}, status=status.HTTP_400_BAD_REQUEST)

                dlog.info("[SMS] create new user")

                _send_sms(
                    card=self.card_info['Card_Code'],
                    phone=self.card_info['Phone']
                )
            else:
                UserConfirm.objects.create(
                    user=self.user
                )

                json = {'card': False}
            return Response(json, status=status.HTTP_201_CREATED)

        return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def save_bonus_card(self):
        setattr(self.user.bonuscrm, 'card', self.card_info['Card_Code'])
        self.user.bonuscrm.save()


class RegistrationConfirm(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):

        try:
            user_name = request.data.get('username', None)

            if not user_name:
                return Response({
                    'detail': 'key mismatch'
                }, status=status.HTTP_400_BAD_REQUEST)

            self.user = User.objects.get(username=user_name)

        except User.DoesNotExist:
            return Response({'detail': 'user Does Not Exist'}, status=status.HTTP_400_BAD_REQUEST)

        if self.user.is_active:
            return Response({'detail': 'user already confirmed'}, status=status.HTTP_400_BAD_REQUEST)

        if self.user.bonuscrm.card is not None:
            if timezone.now() - self.user.bonuscrm.sent >= timedelta(minutes=settings.DEFAULT_REGISTRATION_CONFIRM_TIME):
                return Response({'detail': 'user already confirmed'}, status=status.HTTP_400_BAD_REQUEST)

            res = _login_crm(
                card=self.user.bonuscrm.card,
                card_pass=self.request.data.get('key')
            )

            try:
                if isinstance(res, dict):
                    if res['result'] == 'OK':
                        bonus = _get_bonus(self.user.bonuscrm.card)

                        setattr(self.user.bonuscrm, 'card', self.user.bonuscrm.card)
                        setattr(self.user.bonuscrm, 'card_pass', request.data.get('key'))
                        setattr(self.user.bonuscrm, 'bonus', float(bonus.get('Balance')))
                        setattr(self.user.bonuscrm, 'is_confirmed', True)
                        self.user.bonuscrm.save()
                        self.confirm_registration()
                else:
                    raise AttributeError
            except AttributeError:
                return Response({'detail': res}, status=status.HTTP_400_BAD_REQUEST)

        elif self.user.userconfirm.confirmation_key == request.data.get('key'):
            if timezone.now() - self.user.userconfirm.sent >= timedelta(minutes=settings.DEFAULT_REGISTRATION_CONFIRM_TIME):
                return Response({'detail': 'time expiried'}, status=status.HTTP_400_BAD_REQUEST)
            self.confirm_registration()
        else:
            return Response({
                'detail': 'key mismatch'
            }, status=status.HTTP_400_BAD_REQUEST)

        try:
            token = Token.objects.create(user=self.user)
        except IntegrityError:
            token = Token.objects.get(user=self.user)

        return Response({
            'token': token.key,
            'csrf': get_or_create_csrf_token(self.request),
            'user': UserSerializer(instance=self.user).data
        })

    def confirm_registration(self):
        self.user.is_active = True
        self.user.save()
        login(request=self.request, user=self.user, backend='django.contrib.auth.backends.ModelBackend')
