from . import _send_sms
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from django.conf import settings
from accounts.models import User, UserConfirm
from datetime import timedelta
from django.utils import timezone
from backoffice.models import Branch
import logging

sender_logger = logging.getLogger('sender')
dlog = logging.getLogger("debug")


class SendBonusCardInfoToAdmin(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        username = request.data.get('username', None)
        plastic = request.data.get('plastic', False)

        if username:
            userquery = User.objects.filter(username=username)

            if userquery.exists():
                user = userquery.first()
                if not user.is_active:
                    return Response({'detail': 'you must confirm registration before send email to admin'}, status=status.HTTP_400_BAD_REQUEST)

                if user.bonuscrm.card is not None:
                    return Response({'detail': 'you already have card'}, status=status.HTTP_400_BAD_REQUEST)

                user.bonuscrm.get_card = Branch.objects.get(id=int(request.data.get('city_id', 1)))
                user.bonuscrm.save()
                user.bonuscrm.send_new_user_email(plastic)
                return Response({'result': 'OK', 'message': 'Mail to administrators has been sent'})

        return Response({
            'result': 'OK',
            'message': 'Mail to administrators has not been sent. Probably there is no user data found.',
            'request': {
                'username': username,
                'plastic': plastic,
            }
        })


class RenewRegistrationConfirmKey(APIView):

    def post(self, request):
        try:
            self.user = User.objects.get(username=request.data['username'])
        except User.DoesNotExist:
            return Response({'detail': 'user Does Not Exist'}, status=status.HTTP_400_BAD_REQUEST)

        if self.user.is_active:
            return Response({'detail': 'user already confirmed'}, status=status.HTTP_400_BAD_REQUEST)

        phone = request.data.get('phone', None)

        if phone is not None:
            if self.user.usercrm.phone != phone:
                setattr(self.user.usercrm, 'phone', phone)
                self.user.save()

        if self.user.bonuscrm.card is not None:
            if timezone.now() - self.user.bonuscrm.sent >= timedelta(
                    seconds=settings.DEFAULT_REGISTRATION_KEY_RENEW_TIME):

                dlog.info("[SMS] renew registration confirm key")
                _send_sms(
                    card=self.user.bonuscrm.card,
                    phone=self.user.usercrm.phone
                )
            else:
                return Response({'detail': 'not match time'}, status=status.HTTP_400_BAD_REQUEST)
            return Response({'result': 'OK'})
        else:
            try:
                user_confim_info = UserConfirm.objects.get(user=self.user)
            except UserConfirm.DoesNotExist:
                return Response({'detail': 'nothing to confirm'}, status=status.HTTP_400_BAD_REQUEST)

            # todo: change global DEFAULT_REGISTRATION_KEY_RENEW_TIME back to 90 or whatever
            # todo: костыль
            if timezone.now() - user_confim_info.sent >= timedelta(
                    seconds=settings.DEFAULT_REGISTRATION_KEY_RENEW_TIME):
                user_confim_info.sent = timezone.now()
                user_confim_info.save()

                return Response({'result': 'OK'})
            else:
                return Response({'detail': 'no match time to renew key'}, status=status.HTTP_400_BAD_REQUEST)
