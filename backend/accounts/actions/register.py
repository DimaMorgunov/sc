from . import get_or_create_csrf_token, _get_bonus

from logging import getLogger
from rest_framework import status
from django.db import IntegrityError
from backoffice.models import Branch
from django.contrib.auth import login
from rest_framework.response import Response
from accounts.serializers import UserSerializer
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import APIException as _apex
from rest_framework.decorators import api_view, permission_classes

log = getLogger("debug")


def _get_cabinet_workers():
    from premiere.cabinet_worker import Worker

    return [Worker(branch=branch) for branch in Branch.objects.all()]


class ApiException(_apex):
    def __init__(self, detail=None, status_code=status.HTTP_400_BAD_REQUEST):
        super().__init__(detail=detail)
        self.status_code = status_code


@api_view(['POST'])
@permission_classes((AllowAny,))
def register_new_user(request):
    # Call user serializer and check request data
    user_serializer = UserSerializer(data=request.data)

    if user_serializer.is_valid(False):
        # Save new instance with activation
        user = user_serializer.save(is_active=True)
    else:
        # If data is invalid, just say to front about it
        raise ApiException({
            'detail': 'Data is invalid',
            'errors': user_serializer.errors,
        }, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)

    username = request.data.get('username').strip()
    phone = request.data.get('phone').strip()

    try:
        card_info = {}
        # Bonus card info. On any error, just delete user like in old code
        for _ in _get_cabinet_workers():
            card_info = _.get_user_by_phone(phone=phone, email=username)

            if card_info.get('card') is not None and card_info.get('card') is not False or card_info.get('Card_Code'):
                break

        if not card_info:
            card_info['card'] = False
            card_info['Card_Code'] = None
    except Exception as e:
        import traceback

        user.delete()

        raise ApiException({
            'detail': str(e),
            'provoke': "Failed when getting user info from crm",
            'trace': traceback.format_exc(),
        })

    if card_info.get('Card_Code'):
        try:
            try:
                bonus = float(_get_bonus(card_info.get('Card_Code')).get('Balance'))
            except:
                bonus = 0.0

            setattr(user.bonuscrm, 'card', card_info.get('Card_Code'))
            setattr(user.bonuscrm, 'bonus', bonus)
            setattr(user.bonuscrm, 'is_confirmed', True)
            user.bonuscrm.save()

        except IntegrityError as e:
            user.delete()

            raise ApiException({
                'detail': str(e),
                'provoke': "User with this card already exist",
            }, status_code=status.HTTP_409_CONFLICT)

    card_response = {}
    plastic = True if str(request.data.get('plastic')).lower() in ("true", "1", "y", "yes") else False

    # Send mail to administrators. If plastic is not defined, then it's always gonna be online card
    if user.bonuscrm.card is not None:
        card_response['detail'] = "User already has a card. Pass."
    else:
        from threading import Thread as __th

        city_id = request.data.get('most_important_city')
        city_id = city_id.strip() if isinstance(city_id, str) else city_id

        user.bonuscrm.get_card = Branch.objects.get(id=int(city_id))
        user.bonuscrm.save()
        sending = __th(target=user.bonuscrm.send_new_user_email, args=(plastic,))
        sending.start()

        card_response['detail'] = "Mail to admins add to queue"

    login(request=request, user=user, backend='django.contrib.auth.backends.ModelBackend')

    try:
        token = Token.objects.create(user=user)
    except IntegrityError:
        token = Token.objects.get(user=user)

    return Response({
        'token': token.key,
        'csrf': get_or_create_csrf_token(request),
        'user': UserSerializer(instance=user).data,
        'card': card_response,
    })
