import logging

from premiere.cabinet_worker import Worker
from django.middleware import csrf
from accounts.models import User

sender_logger = logging.getLogger('sender')


def _check_user(email, phone):
    cabinet = Worker()
    return cabinet.get_user_by_phone(email=email, phone=phone)


def _send_sms(card, phone):
    cabinet = Worker()

    _log = "Sending sms to: %s\n" % phone
    user: User = User.objects.filter(bonuscrm__card=card).first()

    if user:
        _log += "User:\nid: %d\nfullname: %s\nemail: %s\n" % (user.pk, user.get_full_name(), user.email)
    else:
        _log += "User: none [ACHTUNG]\n"

    sender_logger.info('[SMSFactory] start sending sms from premiere cabinet')
    return cabinet.remind_password(card_num=card, phone=phone, _log=_log)


def _login_crm(card, card_pass):
    cabinet = Worker()
    return cabinet.login(card_num=card, password=card_pass)


def _get_bonus(card):
    cabinet = Worker()
    return cabinet.get_bonus(card)


def get_or_create_csrf_token(request: 'request object'):
    token = request.META.get('CSRF_COOKIE', None)
    if token is None:
        token = csrf.get_token(request)
        request.META['CSRF_COOKIE'] = token
    request.META['CSRF_COOKIE_USED'] = True
    return token
