200 OK
Message-Type: Response
Message-ID: 9912349
Time: 2018-08-13 15:03:37
Content-Length: 4234

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Cards>
 <Card>
  <Card_Code>20444</Card_Code>
  <Is_Virtual_Card>No</Is_Virtual_Card>
  <Is_Confirm_Manager>No</Is_Confirm_Manager>
  <Status>Active</Status>
  <Carrier_Data></Carrier_Data>
  <Offered>2016-04-07</Offered>
  <Expired>2020-04-01</Expired>
  <Group_ID>2</Group_ID>
  <Group_Name>Бонусная карта</Group_Name>
  <Holder_ID>10000000004073</Holder_ID>
  <Owner_ID></Owner_ID>
  <Verification>Yes</Verification>
  
  <Accounts>
   <Account>
    <Holder_ID>10000000004073</Holder_ID>
    <Account_Number>01.00003.00010207.0001</Account_Number>
    <Status>Active</Status>
    <Account_Class>1</Account_Class>
    <Account_Type_ID>3</Account_Type_ID>
    <Account_Type_Name>Бонусное начисление</Account_Type_Name>
    <Account_Debit_Enabled>Yes</Account_Debit_Enabled>
    <Account_Debit_Priority></Account_Debit_Priority>
    <Account_Debit_K>1</Account_Debit_K>
    <Account_Credit_Enabled>Yes</Account_Credit_Enabled>
    <Account_Credit_Priority></Account_Credit_Priority>
    <Account_Credit_K>1</Account_Credit_K>
    <Account_Level_ID>1</Account_Level_ID>
    <Account_Level_Name>1</Account_Level_Name>
    <Transaction_Debit_ID>31</Transaction_Debit_ID>
    <Transaction_Credit_ID>32</Transaction_Credit_ID>
    <Scheme_ID>1</Scheme_ID>
    <External_ID>1</External_ID>
    <Scheme_Name><![CDATA[Бонус накопления балов 40%]]></Scheme_Name>
    <Base_Rate>50.00</Base_Rate>
    <Create>2016-04-11</Create>
    <Credit_Depth>0.00</Credit_Depth>
    <Balance>72.75</Balance>
    <Auto_Change_Levels>No</Auto_Change_Levels>
    <Account_Code>1</Account_Code>
    <Account_Code_Ext>1</Account_Code_Ext>
   </Account>
   <Account>
    <Holder_ID>10000000004073</Holder_ID>
    <Account_Number>02.00002.00010208.0001</Account_Number>
    <Status>Active</Status>
    <Account_Class>2</Account_Class>
    <Account_Type_ID>2</Account_Type_ID>
    <Account_Type_Name>Дисконт для бонуса</Account_Type_Name>
    <Account_Debit_Enabled>Yes</Account_Debit_Enabled>
    <Account_Debit_Priority></Account_Debit_Priority>
    <Account_Debit_K>1</Account_Debit_K>
    <Account_Credit_Enabled>No</Account_Credit_Enabled>
    <Account_Credit_Priority></Account_Credit_Priority>
    <Account_Credit_K>1</Account_Credit_K>
    <Account_Level_ID>2</Account_Level_ID>
    <Account_Level_Name>1</Account_Level_Name>
    <Transaction_Debit_ID>21</Transaction_Debit_ID>
    <Transaction_Credit_ID>22</Transaction_Credit_ID>
    <Scheme_ID>2</Scheme_ID>
    <External_ID>13</External_ID>
    <Scheme_Name>Скидка для бонуса</Scheme_Name>
    <Base_Rate>0.00</Base_Rate>
    <Create>2016-04-11</Create>
    <Credit_Depth>0.00</Credit_Depth>
    <Balance>0.00</Balance>
    <Auto_Change_Levels>No</Auto_Change_Levels>
    <Account_Code>2</Account_Code>
    <Account_Code_Ext>13</Account_Code_Ext>
   </Account>
   <Account>
    <Holder_ID>10000000004073</Holder_ID>
    <Account_Number>05.00001.00010209.0001</Account_Number>
    <Status>Active</Status>
    <Account_Class>5</Account_Class>
    <Account_Type_ID>1</Account_Type_ID>
    <Account_Type_Name>Потраченые средства</Account_Type_Name>
    <Account_Debit_Enabled>Yes</Account_Debit_Enabled>
    <Account_Debit_Priority></Account_Debit_Priority>
    <Account_Debit_K>1</Account_Debit_K>
    <Account_Credit_Enabled>Yes</Account_Credit_Enabled>
    <Account_Credit_Priority></Account_Credit_Priority>
    <Account_Credit_K>1</Account_Credit_K>
    <Account_Level_ID>0</Account_Level_ID>
    <Account_Level_Name></Account_Level_Name>
    <Transaction_Debit_ID>11</Transaction_Debit_ID>
    <Transaction_Credit_ID>12</Transaction_Credit_ID>
    <Scheme_ID></Scheme_ID>
    <External_ID></External_ID>
    <Scheme_Name></Scheme_Name>
    <Base_Rate>0.00</Base_Rate>
    <Create>2016-04-11</Create>
    <Credit_Depth>0.00</Credit_Depth>
    <Balance>1100.00</Balance>
    <Auto_Change_Levels>No</Auto_Change_Levels>
    <Account_Code>0</Account_Code>
    <Account_Code_Ext>0</Account_Code_Ext>
   </Account>
  </Accounts>
 </Card>
</Cards>