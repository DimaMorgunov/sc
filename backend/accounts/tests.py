from datetime import timedelta
from http.cookies import SimpleCookie

from accounts.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

import backoffice.models
import accounts.models
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes


def _update_reservation_time(username):
    confirm = accounts.models.UserConfirm.objects.filter(user__email=username).last()
    confirm.sent -= timedelta(minutes=2)
    confirm.save()
    return confirm


class AccountsCreateTest(APITestCase):
    def setUp(self):
        self.test_cabinet = backoffice.models.Cabinet.objects.create()
        self.test_city = backoffice.models.City.objects.create()
        self.test_branch = backoffice.models.Branch.objects.create(city=self.test_city, cabinet=self.test_cabinet)
        self.create_url = reverse('account-create')
        self.confirm_url = reverse('account-create-confirm')
        self.test_user = User.objects.create_user('test@example.com', 'test@example.com', 'testpassword')
        self.test_user_crm = accounts.models.UserCRM.objects.create(user=self.test_user, phone=380670000000)
        self.default_test_data = {
            'username': 'someusrname@gmail.com',
            'password': 'somepassword',
            'first_name': 'Name',
            'last_name': 'Family',
            'phone': 380000000000,
            'most_important_city': self.test_city.id,
            'birthday': '1994-01-01'
        }

    def test_registration_process_unit(self):
        """
        Ensure we can create a new user and a valid token is created with it.
        Check email and phone
        Check verification code
        """

        response = self.client.post(self.create_url, self.default_test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['card'], False)

        self.client.enforce_csrf_checks = True

        confirm_data = {
            'username': self.default_test_data['username'],
            'key': accounts.models.UserConfirm.objects.filter(
                user__email=self.default_test_data['username']
            ).last().confirmation_key
        }
        confirm_response = self.client.post(self.confirm_url, confirm_data, format='json',)

        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(confirm_response.data['user']['email'], confirm_response.data['user']['username'],
                         self.default_test_data['username'])
        self.assertEqual(confirm_response.data['user']['first_name'], self.default_test_data['first_name'])
        self.assertEqual(confirm_response.data['user']['last_name'], self.default_test_data['last_name'])
        self.assertEqual(confirm_response.data['user']['phone'], self.default_test_data['phone'])
        self.assertEqual(confirm_response.data['user']['birthday'], self.default_test_data['birthday'])
        self.assertEqual(confirm_response.data['user']['most_important_city'],
                         self.default_test_data['most_important_city'])
        self.assertFalse('password' in confirm_response.data)

    def test_create_user_with_token(self):
        """
        Ensure we can create a new user and a valid token is created with it.
        """

        self.client.post(self.create_url, self.default_test_data, format='json')

        confirm_data = {
            'username': self.default_test_data['username'],
            'key': accounts.models.UserConfirm.objects.filter(
                user__email=self.default_test_data['username']
            ).last().confirmation_key,
        }

        confirm_response = self.client.post(self.confirm_url, confirm_data, format='json', )

        user = User.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(confirm_response.data['token'], token.key)

    def test_create_user_with_short_password(self):
        """
        Ensure user is not created for password lengths less than 8.
        """

        self.default_test_data.update({'password': 'foo', })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_no_password(self):
        """
        Ensure user is not created without password
        """

        self.default_test_data.update({'password': '', })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_too_long_username(self):
        """
        Ensure user is not created if user have long username.
        """

        self.default_test_data.update({'username': 'foo'*30, })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 2)

    def test_create_user_with_no_username(self):
        """
        Ensure user is not created if user haven`t username.
        """

        self.default_test_data.update({'username': '', })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_create_user_with_preexisting_username(self):
        """
        Ensure user is not created if user preexisting username
        """

        self.default_test_data.update({'username': 'test@example.com', })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_create_user_with_preexisting_phone(self):
        """
        Ensure user is not created if user preexisting phone
        """

        self.default_test_data.update({'phone': 380670000000, })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)

    def test_create_user_with_invalid_email_in_username(self):
        """
        Ensure user is not created if user give invalid email
        """

        self.default_test_data.update({'username': 'foovar', })

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    # def test_check_user_email_exist_in_premiere_db(self):
    #     """
    #     Ensure user is created if user give email existing in premiere db
    #     Ensure user confirm object is not created
    #     """
    #
    #     self.default_test_data.update({'username': 'smartcinema.dev@gmail.com', })
    #
    #     response = self.client.post(self.create_url, self.default_test_data, format='json')
    #
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
    #     self.assertEqual(User.objects.count(), 2)
    #     self.assertEqual(response.data['card'], '20444')
    #
    # def test_check_user_phone_exist_in_premiere_db(self):
    #     """
    #     Ensure user is created if user give phone existing in premiere db
    #     Ensure user confirm object is not created
    #     """
    #     self.default_test_data.update({'phone': 380678422367, })
    #     # this phone exist in db !!! must send sms !!!
    #
    #     response = self.client.post(self.create_url, self.default_test_data, format='json')
    #
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
    #     self.assertEqual(User.objects.count(), 2)
    #     self.assertEqual(response.data['card'], '20444')

    def test_registration_process_if_user_field_doesnt_exist(self):
        """
        Ensure user don't create if username field doesn't exist
        """

        self.default_test_data.pop('username')

        response = self.client.post(self.create_url, self.default_test_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)

    def test_registration_process_if_password_field_doesnt_exist(self):
        """
        Ensure user don't create if password field doesn't exist
        """
        self.default_test_data.pop('password')

        response = self.client.post(self.create_url, self.default_test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)

    def test_registration_process_if_first_name_field_doesnt_exist(self):
        """
        Ensure user don't create if first_name field doesn't exist
        """
        self.default_test_data.pop('first_name')

        response = self.client.post(self.create_url, self.default_test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)

    def test_registration_process_if_last_name_field_doesnt_exist(self):
        """
        Ensure user don't create if last_name field doesn't exist
        """
        self.default_test_data.pop('last_name')

        response = self.client.post(self.create_url, self.default_test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 0)
        self.assertEqual(User.objects.count(), 1)

    def test_registration_process_if_birthday_field_doesnt_exist(self):
        """
        Ensure user don't create if last_name field doesn't exist
        """
        self.default_test_data.pop('birthday')

        response = self.client.post(self.create_url, self.default_test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 1)
        self.assertEqual(User.objects.count(), 2)

    def test_registration_process_if_most_important_city_field_doesnt_exist(self):
        """
        Ensure user don't create if most_important_city field doesn't exist
        """
        self.default_test_data.pop('most_important_city')

        response = self.client.post(self.create_url, self.default_test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 1)
        self.assertEqual(User.objects.count(), 2)


class RenewAccountsCreateKeyTest(APITestCase):
    def setUp(self):
        self.test_cabinet = backoffice.models.Cabinet.objects.create()
        self.test_city = backoffice.models.City.objects.create()
        self.test_branch = backoffice.models.Branch.objects.create(
            city=self.test_city,
            cabinet=self.test_cabinet
        )

        self.test_user_data = {
                'username': 'someusrname@gmail.com',
                'password': 'somepassword',
                'first_name': 'Name',
                'last_name': 'Family',
                'phone': 380000000000,
                'most_important_city': self.test_city.id,
                'birthday': '1994-01-01'
        }

        self.test_reservation = self.client.post(
            reverse('account-create'),
            self.test_user_data,
            format='json'
        )

        self.renew_key_url = reverse('account-create-new-key')

    def test_check_not_renew_key(self):
        """
        Ensure new key is not created if user don`t have 90 sec pause
        (DEFAULT_REGISTRATION_KEY_RENEW_TIME = 90)
        """

        confirm_data = {
            'username': self.test_user_data['username'],
            'phone': self.test_user_data['phone'],
        }

        first_confirm = accounts.models.UserConfirm.objects.filter(
            user__email=self.test_user_data['username']
        ).last().confirmation_key

        new_response = self.client.post(self.renew_key_url, confirm_data, format='json',)
        self.assertEqual(new_response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 1)
        self.assertEqual(first_confirm, accounts.models.UserConfirm.objects.filter(
            user__email=self.test_user_data['username']).last().confirmation_key)

    def test_check_renew_key(self):
        """
        Ensure new key is created if user have 90 sec pause
        (DEFAULT_REGISTRATION_KEY_RENEW_TIME = 90)
        """

        confirm_data = {
            'username': self.test_user_data['username'],
            'phone': self.test_user_data['phone'],
        }

        first_key = _update_reservation_time(self.test_user_data['username'])

        new_response = self.client.post(self.renew_key_url, confirm_data, format='json',)
        self.assertEqual(new_response.status_code, status.HTTP_200_OK)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 1)
        self.assertNotEqual(first_key.confirmation_key, accounts.models.UserConfirm.objects.filter(
            user__email=self.test_user_data['username']).last().confirmation_key)

    def test_check_renew_key_sent_time(self):
        """
        Ensure after generating new key is created new sent time
        (DEFAULT_REGISTRATION_KEY_RENEW_TIME = 90)
        """

        confirm_data = {
            'username': self.test_user_data['username'],
            'phone': self.test_user_data['phone'],
        }

        first_key = _update_reservation_time(self.test_user_data['username'])

        new_response = self.client.post(self.renew_key_url, confirm_data, format='json',)

        self.assertEqual(new_response.status_code, status.HTTP_200_OK)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 1)
        self.assertNotEqual(first_key.sent, accounts.models.UserConfirm.objects.filter(
            user__email=self.test_user_data['username']).last().sent)
        self.assertTrue(first_key.sent < accounts.models.UserConfirm.objects.filter(
            user__email=self.test_user_data['username']).last().sent)

    def test_check_renew_key_without_registration(self):
        """
        Ensure new key is don`t created if user don`t register yet
        """
        data = {
            'username': 'someusrname2@gmail.com',
            'password': 'some_password',
        }

        response = self.client.post(self.renew_key_url, data, format='json',)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(accounts.models.UserConfirm.objects.count(), 1)


class AuthTest(APITestCase):
    def setUp(self):
        self.test_cabinet = backoffice.models.Cabinet.objects.create()
        self.test_city = backoffice.models.City.objects.create()
        self.test_branch = backoffice.models.Branch.objects.create(city=self.test_city, cabinet=self.test_cabinet)

        self.test_data = {
            'username': 'someusrname@gmail.com',
            'password': 'somepassword',
        }
        self.test_user = User.objects.create_user(**self.test_data)

        self.login_url = reverse('api_login')
        self.logout_url = reverse('rest_logout')
        self.user_detail_url = reverse('rest_user_details')

    def test_auth(self):
        """
        Ensure user can auth
        """

        response = self.client.post(self.login_url, self.test_data, format='json',)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.test_user.is_authenticated)

    def test_not_auth_with_wrong_pass(self):
        """
        Ensure user can`t auth with wrong password
        """

        self.test_data['password'] = '123456'
        response = self.client.post(self.login_url, self.test_data, format='json',)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_not_auth_with_wrong_login(self):
        """
        Ensure user can`t auth with wrong username
        """

        self.test_data['username'] = '123456'

        response = self.client.post(self.login_url, self.test_data, format='json',)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_with_token(self):
        """
        Ensure user can auth by token
        """

        response = self.client.post(self.login_url, self.test_data, format='json',)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['key'])
        self.client.cookies = SimpleCookie()

        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.test_user.is_authenticated)

    def test_dont_auth_with_broken_token(self):
        """
        Ensure user can auth by token
        """

        response = self.client.post(self.login_url, self.test_data, format='json', )
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['key'][:-4])
        self.client.cookies = SimpleCookie()

        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_auth_with_session(self):
        """
        Ensure user can auth by session cookies
        """

        self.client.post(self.login_url, self.test_data, format='json',)
        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.test_user.is_authenticated)

    def test_dont_auth_with_broken_session(self):
        """
        Ensure user can`t auth by fake session cookies
        """

        self.client.post(self.login_url, self.test_data, format='json', )
        self.client.cookies = SimpleCookie({'session': '146516510'})

        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_logout(self):
        login_response = self.client.post(self.login_url, self.test_data, format='json', )
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + login_response.data['key'])
        self.client.cookies = SimpleCookie()

        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_200_OK)

        logout_response = self.client.post(self.logout_url)
        self.assertEqual(logout_response.status_code, status.HTTP_200_OK)

        new_user_response = self.client.get(self.user_detail_url)
        self.assertEqual(new_user_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_logout_by_session(self):
        self.client.post(self.login_url, self.test_data, format='json', )

        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_200_OK)

        logout_response = self.client.post(self.logout_url)
        self.assertEqual(logout_response.status_code, status.HTTP_200_OK)

        new_user_response = self.client.get(self.user_detail_url)
        self.assertEqual(new_user_response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_logout_fail(self):
        login_response = self.client.post(self.login_url, self.test_data, format='json', )
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + login_response.data['key'][:-5])
        self.client.cookies = SimpleCookie()

        user_response = self.client.get(self.user_detail_url)
        self.assertEqual(user_response.status_code, status.HTTP_401_UNAUTHORIZED)

        logout_response = self.client.post(self.logout_url)
        self.assertEqual(logout_response.status_code, status.HTTP_401_UNAUTHORIZED)


class UserEditTest(APITestCase):

    def setUp(self):
        self.test_cabinet = backoffice.models.Cabinet.objects.create()
        self.test_city = backoffice.models.City.objects.create()
        self.test_branch = backoffice.models.Branch.objects.create(city=self.test_city, cabinet=self.test_cabinet)
        self.default_test_data = {
            'username': 'someusrname@gmail.com',
            'password': 'somepassword',
            'first_name': 'Name',
            'last_name': 'Family',
            'phone': 380000000000,
            'most_important_city': self.test_city.id,
            'birthday': '1994-01-01'
        }

        self.client.post(reverse('account-create'), self.default_test_data, format='json')

        confirm_data = {
            'username': self.default_test_data['username'],
            'key': accounts.models.UserConfirm.objects.filter(
                user__email=self.default_test_data['username']
            ).last().confirmation_key,
        }

        self.client.post(reverse('account-create-confirm'), confirm_data, format='json', )
        self.update_url = reverse('rest_user_update_data')
        self.password_change_url = reverse('rest_password_change')
        self.password_reset = reverse('rest_password_reset')
        self.password_reset_confirm = reverse('rest_password_reset_confirm')

    def test_user_change_email(self):
        new_data = {'email': 'new@gmail.com'}
        self.client.post(self.update_url, new_data, format='json', )

        self.assertEqual(User.objects.get(username=self.default_test_data['username']).email, new_data['email'])
        self.assertNotEqual(self.default_test_data['username'], new_data['email'])

    def test_user_change_first_name(self):
        new_data = {'first_name': 'newname'}
        self.client.post(self.update_url, new_data, format='json', )

        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).first_name,
            new_data['first_name']
        )
        self.assertNotEqual(self.default_test_data['first_name'], new_data['first_name'])

    def test_user_change_last_name(self):
        new_data = {'last_name': 'nemname'}
        self.client.post(self.update_url, new_data, format='json', )

        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).last_name,
            new_data['last_name']
        )
        self.assertNotEqual(self.default_test_data['last_name'], new_data['last_name'])

    def test_user_change_birthday(self):
        new_data = {'birthday': '2017-10-10'}
        response = self.client.post(self.update_url, new_data, format='json', )

        self.assertNotEqual(
            User.objects.get(username=self.default_test_data['username']).usercrm.birthday,
            new_data['birthday']
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_change_username(self):
        new_data = {'username': '----username'}
        response = self.client.post(self.update_url, new_data, format='json', )

        self.assertNotEqual(
            User.objects.get(username=self.default_test_data['username']).username,
            new_data['username']
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_change_city(self):
        self.new_city = backoffice.models.City.objects.create(name='New York')
        new_data = {'most_important_city': self.new_city.id}
        self.client.post(self.update_url, new_data, format='json', )

        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).usercrm.most_important_city.id,
            new_data['most_important_city']
        )

        self.assertNotEqual(
            self.default_test_data['most_important_city'],
            User.objects.get(username=self.default_test_data['username']).usercrm.most_important_city.id,
        )

    def test_user_change_password_by_user_change_data_view(self):
        new_data = {'password': '----username'}
        response = self.client.post(self.update_url, new_data, format='json', )

        self.assertNotEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            new_data['password']
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_password_change_by_rest_password_change(self):
        new_data = {
            'new_password1': 'somepass',
            'new_password2': 'somepass'
        }
        response = self.client.post(self.password_change_url, new_data, format='json', )

        self.assertNotEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            new_data['new_password1']
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(
            self.default_test_data['password'],
            new_data['new_password1'],
        )

    def test_password_not_change_if_not_equiv(self):
        new_data = {
            'new_password1': 'somepass2',
            'new_password2': 'somepass1'
        }
        response = self.client.post(self.password_change_url, new_data, format='json', )

        self.assertNotEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            new_data['new_password1']
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotEqual(
            new_data['new_password1'],
            new_data['new_password2'],
        )

    def test_password_reset_view(self):
        data_for_reset = {
            'email': self.default_test_data['username'],
        }
        response = self.client.post(self.password_reset, data_for_reset, format='json', )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_password_reset_for_not_existed_user(self):
        data_for_reset = {
            'email': 'dovgaloleksii@gmail.com',
        }
        response = self.client.post(self.password_reset, data_for_reset, format='json', )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_password_reset_confirm(self):
        start_pass = User.objects.get(username=self.default_test_data['username']).password

        user = User.objects.get(username=self.default_test_data['username'])
        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)

        data_for_reset = {
            'uid': uid,
            'token': token,
            'new_password1': 'somepass',
            'new_password2': 'somepass'
        }
        response = self.client.post(self.password_reset_confirm, data_for_reset, format='json', )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            start_pass
        )

    def test_password_reset_confirm_dont_work_with_fake_uid(self):

        start_pass = User.objects.get(username=self.default_test_data['username']).password

        user = User.objects.get(username=self.default_test_data['username'])
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)

        data_for_reset = {
            'uid': 'somestring',
            'token': token,
            'new_password1': 'somepass',
            'new_password2': 'somepass'
        }
        response = self.client.post(self.password_reset_confirm, data_for_reset, format='json', )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            start_pass
        )

    def test_password_reset_confirm_dont_work_with_fake_token(self):

        start_pass = User.objects.get(username=self.default_test_data['username']).password

        user = User.objects.get(username=self.default_test_data['username'])
        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()

        data_for_reset = {
            'uid': uid,
            'token': 'some_string',
            'new_password1': 'somepass',
            'new_password2': 'somepass'
        }

        response = self.client.post(self.password_reset_confirm, data_for_reset, format='json', )
        self.assertEqual(response.status_code,  status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            start_pass
        )

    def test_password_reset_confirm_dont_work_if_new_pass_not_equal(self):
        start_pass = User.objects.get(username=self.default_test_data['username']).password

        user = User.objects.get(username=self.default_test_data['username'])
        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
        token_generator = PasswordResetTokenGenerator()
        token = token_generator.make_token(user)

        data_for_reset = {
            'uid': uid,
            'token': token,
            'new_password1': 'somes',
            'new_password2': 'somep'
        }
        response = self.client.post(self.password_reset_confirm, data_for_reset, format='json', )
        self.assertEqual(response.status_code,  status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).password,
            start_pass
        )


class PremiereFunctionalityTest(APITestCase):

    fixtures = []

    def setUp(self):
        self.test_cabinet = backoffice.models.Cabinet.objects.create()
        self.test_city = backoffice.models.City.objects.create()
        self.test_branch = backoffice.models.Branch.objects.create(city=self.test_city, cabinet=self.test_cabinet)
        self.default_test_data = {
            'username': 'someusrname@gmail.com',
            'password': 'somepassword',
            'first_name': 'Name',
            'last_name': 'Family',
            'phone': 380000000000,
            'most_important_city': self.test_city.id,
            'birthday': '1994-01-01'
        }

        self.client.post(reverse('account-create'), self.default_test_data, format='json')

        confirm_data = {
            'username': self.default_test_data['username'],
            'key': accounts.models.UserConfirm.objects.filter(
                user__email=self.default_test_data['username']
            ).last().confirmation_key,
        }

        self.client.post(reverse('account-create-confirm'), confirm_data, format='json', )
        self.get_ticket_url = reverse('rest_tickets')
        self.get_bonus = reverse('rest_bonus_balance')
        self.update_users_bonus_card = reverse('registration-new-card')
        self.update_confirm = reverse('registration-new-card-confirm')
        self.send_info_to_admin_url = reverse('registration-send-email-to-admin')

    def set_preexist_user_phone_card_pass(self):
        user = User.objects.get(username=self.default_test_data['username'])
        setattr(user.bonuscrm, 'card', '20444')
        setattr(user.bonuscrm, 'card_pass', 'wvru4')
        setattr(user, 'phone', 380678422367)
        user.bonuscrm.save()
        user.save()

    def set_preexist_user_phone(self):
        user = User.objects.get(username=self.default_test_data['username'])
        setattr(user.usercrm, 'phone', 380678422367)
        user.usercrm.save()

    def test_bonus_balance_renew_if_user_dont_have_card(self):
        response = self.client.post(self.get_bonus, {}, format='json', )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(response.data.get('card', False))

    def test_bonus_balance_renew_if_user_have_card(self):
        self.set_preexist_user_phone_card_pass()
        response = self.client.post(self.get_bonus, {}, format='json', )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            User.objects.get(username=self.default_test_data["username"]).bonuscrm.bonus,
            response.data.get('bonus', None)
        )

    def test_update_users_bonus_card_if_user_dont_have_card_and_phone_not_preexist(self):
        response = self.client.post(self.update_users_bonus_card, {}, format='json', )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(response.data.get('card', True))
        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).bonuscrm.card,
            None
        )

    def test_update_users_bonus_card_if_user_dont_have_card_and_phone_is_preexist(self):
        self.set_preexist_user_phone()
        response = self.client.post(self.update_users_bonus_card, {}, format='json', )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('card', '00000'), '20444')
        self.assertEqual(
            User.objects.get(username=self.default_test_data['username']).bonuscrm.card,
            '20444'
        )

