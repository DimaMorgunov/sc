from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import BonusCRM, UserCRM, User


def send_card_ready_email(self, request, queryset):
    for obj in queryset:
        obj.bonuscrm.send_card_ready_email()
        self.message_user(request, "Успешно отправлено письмо на почту %s" % obj.email)


send_card_ready_email.short_description = "Отправить письмо карта готова"


class UserCRMInline(admin.StackedInline):
    model = UserCRM
    can_delete = False
    verbose_name_plural = 'Дополнительные сведения о пользователе'


class BonusCRMInline(admin.StackedInline):
    model = BonusCRM
    can_delete = False
    verbose_name_plural = 'Бонусная Программа'


class UserAdmin(BaseUserAdmin):
    actions = [send_card_ready_email]
    inlines = (BonusCRMInline, UserCRMInline)
    list_display = ('username', 'email', 'first_name', 'last_name', 'phone', 'card', 'date_joined', 'is_confirmed', 'is_crm_confirmed')
    search_fields = ('username', 'first_name', 'last_name', 'email', 'usercrm__phone', 'bonuscrm__card')

    def phone(self, user):
        return user.usercrm.phone

    def card(self, user):
        return user.bonuscrm.card

    def is_confirmed(self, user):
        """ :type user: User """
        return user.is_active

    def is_crm_confirmed(self, user):
        """ :type user: User """
        return user.bonuscrm.is_confirmed

    is_confirmed.boolean = True
    is_confirmed.short_description = "Активирован"
    is_crm_confirmed.boolean = True
    is_crm_confirmed.short_description = "Подтвержден (карта)"
    card.short_description = "Карта"
    phone.short_description = "Телефон"


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
