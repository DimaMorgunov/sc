from django.urls import include, path
from rest_auth.views import (
    LogoutView, UserDetailsView, PasswordChangeView,
)

from . import views
from .actions import create, confirm, register
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [

    path('user/', UserDetailsView.as_view(), name='rest_user_details'),
    path('user_tickets/', views.GetTickets.as_view(), name='rest_tickets'),
    path('user_update/', views.UpdateUserData.as_view(), name='rest_user_update_data'),

    path('get_bonus_info/', views.GetBonusBalance.as_view(), name='rest_bonus_balance'),

    path('login/', csrf_exempt(views.LargeLoginView.as_view()), name="api_login"),
    path('logout/', csrf_exempt(LogoutView.as_view()), name='rest_logout'),

    path('sign-up/', register.register_new_user, name='registration-new-user'),

    path('create/', create.UserCreate.as_view(), name='account-create'),  # This
    path('create/new_card/', confirm.SendBonusCardInfoToAdmin.as_view(), name='registration-send-email-to-admin'),  # This

    path('confirm/', create.RegistrationConfirm.as_view(), name='account-create-confirm'),  # This
    path('confirm/new_key/', confirm.RenewRegistrationConfirmKey.as_view(), name='account-create-new-key'),  # This

    path('card/', views.RegisterNewBonusCardForUser.as_view(), name='registration-new-card'),
    path('card/confirm/', views.ConfirmRegisterNewBonusCardForUser.as_view(), name='registration-new-card-confirm'),

    path('check/', csrf_exempt(views.CheckIsAuth.as_view()), name='check-auth'),

    path('password/change/', PasswordChangeView.as_view(), name='rest_password_change'),
    path('password/reset/', views.LargePasswordResetView.as_view(), name='rest_password_reset'),
    path('password/reset/confirm/', views.PasswordResetConfirmation.as_view(), name='rest_password_reset_confirm'),

    path('backend/', include('django.contrib.auth.urls')),
]
