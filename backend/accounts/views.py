from premiere.cabinet_worker import Worker
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_auth.views import LoginView, PasswordResetView, PasswordResetConfirmView as __PRCView
from rest_auth.serializers import PasswordResetSerializer
from .serializers import UserSerializer, UserUpdateSerializer
from django.conf import settings
from reservation import models, serializers
from .models import User, UserConfirm
from datetime import timedelta
from django.utils import timezone
from backoffice.models import Branch
from django.views.decorators.csrf import csrf_exempt
from django.middleware import csrf
import logging

sender_logger = logging.getLogger('sender')
dlog = logging.getLogger("debug")


def _send_sms(card, phone):
    cabinet = Worker()

    _log = "Sending sms to: %s\n" % phone
    user: User = User.objects.filter(bonuscrm__card=card).first()

    if user:
        _log += "User:\nid: %d\nfullname: %s\nemail: %s\n" % (user.pk, user.get_full_name(), user.email)
    else:
        _log += "User: none [ACHTUNG]\n"

    sender_logger.info('[SMSFactory] start sending sms from premiere cabinet')
    return cabinet.remind_password(card_num=card, phone=phone, _log=_log)


def _check_user(email, phone):
    cabinet = Worker()
    return cabinet.get_user_by_phone(email=email, phone=phone)


def _login_crm(card, card_pass):
    cabinet = Worker()
    return cabinet.login(card_num=card, password=card_pass)


def _get_bonus(card):
    cabinet = Worker()
    return cabinet.get_bonus(card)


def get_or_create_csrf_token(request: 'request object'):
    token = request.META.get('CSRF_COOKIE', None)
    if token is None:
        token = csrf.get_token(request)
        request.META['CSRF_COOKIE'] = token
    request.META['CSRF_COOKIE_USED'] = True
    return token


class NewEmailTemplateInPRS(PasswordResetSerializer):
    """
    Serializer class with other email options
    """

    def get_email_options(self):
        sender_logger.info('[LargePasswordResetView][NewEmailTemplateInPRS] start sending password reset email')
        return {
            'extra_email_context': {
                'cite_url': settings.CITE_URL,
                'settings': settings,
                'branch_id': Branch.objects.last()
            },
            'subject_template_name': 'email_template/password/subject.txt',
            'html_email_template_name': 'email_template/password/reset.html',
            'email_template_name': 'email_template/password/reset.txt'
        }


class LargePasswordResetView(PasswordResetView):
    """
    Override the serializer class variable to replace the email options
    """
    serializer_class = NewEmailTemplateInPRS

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            User.objects.get(email=serializer.data['email'])
        except:
            return Response({'error': 'Користувач із цим обліковим записом не зареєстрований'}, status=400)

        return super().post(request, args, kwargs)


class CheckIsAuth(APIView):

    def get(self, request):

        if request.user.is_authenticated:
            token, create = Token.objects.get_or_create(user=request.user)
            return Response({
                'csrf': get_or_create_csrf_token(self.request),
                'key': token.key,
                "first_name": request.user.first_name
            })
        else:
            return Response({'error': 'Not enough credentials for auth'}, status=401)


class LargeLoginView(LoginView):
    authentication_classes = []

    def get_response(self):
        user_data = UserSerializer(
            instance=self.user,
            many=False
        ).data

        data_for_response = self.get_response_serializer()(
            instance=self.token,

            context={'request': self.request}
        ).data

        data_for_response.update(user_data)
        data_for_response.update(
            {
                'csrf': get_or_create_csrf_token(self.request)
            }
        )
        return Response(data_for_response, status=status.HTTP_200_OK)


class GetBonusBalance(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = [TokenAuthentication, ]

    @csrf_exempt
    def post(self, request):
        if request.user.bonuscrm.card is not None:

            user = request.user
            user_data = _get_bonus(user.bonuscrm.card)
            if user_data['Balance']:
                user.bonuscrm.bonus = str(user_data['Balance'])
                user.bonuscrm.save()
            return Response(user_data)
        else:
            return Response({'error': 'user don`t have bonus card'}, status=status.HTTP_400_BAD_REQUEST)


class GetTickets(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = [TokenAuthentication, ]

    @csrf_exempt
    def post(self, request):
        users_reservations = models.Reservation.objects.filter(
            user_account=self.request.user,
            ticket__key__isnull=False
        )
        serialize = serializers.ReservationSerializers(instance=users_reservations, many=True)

        return Response(serialize.data)


class RegisterNewBonusCardForUser(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        if self.request.user.bonuscrm.card is not None:
            return Response({'detail': 'you already have card'},
                            status=status.HTTP_400_BAD_REQUEST)

        self.card_info = _check_user(
            email=self.request.user.email,
            phone=self.request.user.usercrm.phone
        )

        if self.card_info.get('card', True):

            try:
                User.objects.get(bonuscrm__card=self.card_info['Card_Code'])
                return Response({'detail': 'User with this card already exist'}, status=status.HTTP_400_BAD_REQUEST)
            except User.DoesNotExist:
                json = {'card': self.card_info['Card_Code']}
                self.save_bonus_card()

                dlog.info("[SMS] register new bonus card for user")
                _send_sms(
                    card=self.card_info['Card_Code'],
                    phone=self.card_info['Phone']
                )

            return Response(json, )

        else:
            return Response({'detail': 'No card', 'card': self.card_info['card']}, status=status.HTTP_400_BAD_REQUEST)

    def save_bonus_card(self):
        setattr(self.request.user.bonuscrm, 'card', self.card_info['Card_Code'])
        self.request.user.bonuscrm.save()


class ConfirmRegisterNewBonusCardForUser(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        res = _login_crm(
            card=self.request.user.bonuscrm.card,
            card_pass=request.data['key']
        )

        try:
            if isinstance(res, dict):
                if res['result'] == 'OK':
                    bonus = _get_bonus(self.request.user.bonuscrm.card)

                    setattr(self.request.user.bonuscrm, 'card', self.request.user.bonuscrm.card)
                    setattr(self.request.user.bonuscrm, 'card_pass', request.data['key'])
                    setattr(self.request.user.bonuscrm, 'bonus', float(bonus['Balance']))
                    setattr(self.request.user.bonuscrm, 'is_confirmed', True)
                    self.request.user.bonuscrm.save()

            else:
                raise AttributeError
        except AttributeError:
            return Response({'detail': res}, status=status.HTTP_400_BAD_REQUEST)

        return Response(UserSerializer(instance=self.request.user).data)


class UpdateUserData(APIView):
    permission_classes = (IsAuthenticated,)
    not_editable_fields = ['birthday', 'password', 'phone', ]

    def post(self, request):

        serialized_data = UserUpdateSerializer(data=self.request.data)
        is_permission_conflict = self._check_fields()

        if is_permission_conflict:
            return is_permission_conflict

        elif serialized_data.is_valid():
            serialized_data.update(request.user, serialized_data.data)
            return Response({'result': 'ok'})
        else:
            return Response(serialized_data.errors)

    def _check_fields(self):
        for field in self.request.data:
            if field in self.not_editable_fields:
                return Response(
                    {'premission denied': 'This field - `%s` is nit editable' % field},
                    status=status.HTTP_400_BAD_REQUEST)
        return False


class CsrfExemptSessionAuthentication(SessionAuthentication):
    # Exclude view from csrf check, because basic uncheck not working
    def enforce_csrf(self, request, *args, **kwargs):
        return


class PasswordResetConfirmation(__PRCView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
