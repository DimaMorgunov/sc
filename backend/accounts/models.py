import random, hashlib, barcode, os, logging

from feedback.sender_util import EmailFactory
from feedback.sender_util import SMSFactory
from django.contrib.auth.models import User
from backoffice.models import Branch
from backoffice.models import City
from django.conf import settings
from django.db import models

sender_logger = logging.getLogger('sender')


class UserCRM(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    most_important_city = models.ForeignKey(
        City,
        verbose_name=u'Город',
        help_text=u'предпочитаемый город',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    phone = models.CharField(
        verbose_name=u'Телефон',
        help_text=u'Телефон клиента',
        max_length=100,
        null=True,
        blank=True,
    )

    birthday = models.DateField(
        verbose_name=u'День рождения',
        help_text=u'День рождения',
        null=True,
        blank=True,
    )

    def __str__(self):
        return 'Дополнительная информация о пользователе - %s' % self.user.username


class BonusCRM(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    card = models.CharField(
        verbose_name=u'Карта',
        help_text=u'Номер бонусной карты',
        max_length=100,
        null=True,
        blank=True,
        unique=True
    )

    card_pass = models.CharField(
        verbose_name=u'Пароль',
        help_text=u'Пароль от карты',
        max_length=100,
        null=True,
        blank=True,
    )

    sent = models.DateTimeField(
        auto_now_add=True,
        null=True,
        blank=True
    )

    is_confirmed = models.BooleanField(
        default=False,
    )

    bonus = models.CharField(
        max_length=100,
        default='0'
    )

    get_card = models.ForeignKey(
        Branch,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return 'Бонусная программа для пользователя - %s' % self.user.username

    def generate_barcode_for_bonus_card(self):
        return barcode.generate(
            'EAN13',
            u'1000000' + self.card,
            output=os.path.join(settings.BARCODE_DIR, self.card),
            text=self.card
        )

    def get_barcode_for_bonus_card(self):

        if self.card is not None:

            file_path = os.path.join(settings.BARCODE_DIR, self.card)
            url = '%scard_code/%s.svg' % (settings.MEDIA_URL, self.card)

            if not os.path.isfile(file_path + '.svg'):
                self.generate_barcode_for_bonus_card()

            return url

        return None

    EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'card')
    EMAIL_TXT = 'ready.txt'
    EMAIL_HEAD = 'ready_head.txt'
    EMAIL_HTML = 'ready.html'
    STAFF_EMAIL = settings.REGISTRATION_STAFF_EMAIL

    def send_card_ready_email(self):
        message = EmailFactory(
            self,
            extra_data={
                'branch_id': self.get_card if self.get_card else Branch.objects.all().last().id,
                'settings': settings,
            },
            email_to=(self.user.email,),
        )
        sender_logger.info('[EmailFactory] start sending card ready email')
        message.send_message()

    def send_new_user_email(self, plastic):
        self.EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'for_staff')
        self.EMAIL_TXT = 'new_user.txt'
        self.EMAIL_HEAD = 'new_user_head.txt'
        self.EMAIL_HTML = False
        message = EmailFactory(
            self,
            extra_data={
                'user': self.user,
                'plastic': 'Платстик' if plastic else 'Электронная',
                'branch_id': self.get_card,
                'settings': settings,
            },
            email_to=self.STAFF_EMAIL[self.get_card.id],
        )
        sender_logger.info('[EmailFactory] start sending new user email')
        message.send_message()


class UserConfirm(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        unique=True
    )

    confirmation_key = models.CharField(
        max_length=5,
    )

    sent = models.DateTimeField(
        auto_now_add=True,
        blank=True
    )

    def send_sms(self):
        sender_logger.info('[SMS] start sending registration sms')
        return SMSFactory(
            message_text='Код для подтверждения регистрации на сайте smartcinema.ua: %s' % self.confirmation_key,
            client_phone_number=self.user.usercrm.phone,
        )

    def get_uuid(self):
        return hashlib.sha256((str(random.random()) + self.user.email).encode('utf-8')).hexdigest()[:5]

    def save(self, *args, **kwargs):
        if not self.user.is_active:
            self.confirmation_key = self.get_uuid()
            sender_logger.info('[SMS] registration key - %s' % self.confirmation_key)
            self.send_sms()
        super(UserConfirm, self).save(*args, **kwargs)
