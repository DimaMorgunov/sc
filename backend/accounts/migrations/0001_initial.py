# Generated by Django 2.0.4 on 2018-09-06 09:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('backoffice', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BonusCRM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card', models.CharField(blank=True, help_text='Номер бонусной карты', max_length=100, null=True, unique=True, verbose_name='Карта')),
                ('card_pass', models.CharField(blank=True, help_text='Пароль от карты', max_length=100, null=True, verbose_name='Пароль')),
                ('sent', models.DateTimeField(auto_now_add=True, null=True)),
                ('bonus', models.FloatField(blank=True, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserConfirm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('confirmation_key', models.CharField(max_length=5)),
                ('sent', models.DateTimeField(auto_now_add=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserCRM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(blank=True, help_text='Телефон клиента', max_length=100, null=True, verbose_name='Телефон')),
                ('birthday', models.DateField(blank=True, help_text='День рождения', null=True, verbose_name='День рождения')),
                ('most_important_city', models.ForeignKey(blank=True, help_text='предпочитаемый город', null=True, on_delete=django.db.models.deletion.CASCADE, to='backoffice.City', verbose_name='Город')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
