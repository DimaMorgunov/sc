# Generated by Django 2.1.2 on 2018-11-08 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20181109_0025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bonuscrm',
            name='bonus',
            field=models.CharField(default='0', max_length=100),
        ),
    ]
