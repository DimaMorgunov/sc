from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import UserCRM, User, BonusCRM
from backoffice.models import City
from django.db import transaction


class UserSerializer(serializers.ModelSerializer):
    username = serializers.EmailField(
        max_length=32,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(
        min_length=8,
        write_only=True
    )

    first_name = serializers.CharField(
        min_length=3,
    )

    last_name = serializers.CharField(
        min_length=3,
    )

    phone = serializers.IntegerField(
        allow_null=False,
        source='usercrm.phone',
        validators=[UniqueValidator(queryset=UserCRM.objects.all())]
    )

    birthday = serializers.DateField(
        allow_null=False,
        source='usercrm.birthday',
        required=True,
    )

    most_important_city = serializers.IntegerField(
        allow_null=False,
        source='usercrm.most_important_city.id',
        required=True,
        min_value=1,
        max_value=City.objects.count(),
    )

    card = serializers.IntegerField(
        allow_null=True,
        source='bonuscrm.card',
        required=False,
        default=None,
    )

    card_svg = serializers.CharField(
        allow_null=True,
        required=False,
        source='bonuscrm.get_barcode_for_bonus_card'
    )

    is_confirmed = serializers.BooleanField(
        source='bonuscrm.is_confirmed',
        required=False,
        default=False,
    )

    bonus = serializers.FloatField(
        allow_null=True,
        required=False,
        source='bonuscrm.bonus'
    )

    @transaction.atomic()
    def create(self, validated_data):
        user_crm_info = validated_data.pop('usercrm')
        bonus_crm_info = validated_data.pop('bonuscrm')
        validated_data['email'] = validated_data['username']

        try:
            city = City.objects.get(id=user_crm_info.pop('most_important_city')['id'])
            user_crm_info.update({'most_important_city': city})
        except KeyError:
            pass

        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        BonusCRM.objects.create(user=user, **bonus_crm_info, )
        UserCRM.objects.create(user=user, **user_crm_info, )

        return user

    class Meta:
        model = User
        fields = (
            'username', 'email', 'first_name', 'most_important_city',
            'last_name', 'phone', 'birthday', 'password', 'card_svg', 'card', 'is_confirmed', 'bonus'
        )


class UserUpdateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        allow_null=False,
        max_length=32,
        validators=[UniqueValidator(queryset=User.objects.all())],
        required=False,
    )

    first_name = serializers.CharField(
        allow_null=False,
        min_length=3,
        required=False,
    )

    last_name = serializers.CharField(
        allow_null=False,
        min_length=3,
        required=False,
    )

    most_important_city = serializers.IntegerField(
        allow_null=False,
        source='usercrm.most_important_city.id',
        required=False,
    )

    def update(self, instance, validated_data):

        city_id = validated_data.get('most_important_city', instance.usercrm.most_important_city)
        if not isinstance(city_id, City):
            instance.usercrm.most_important_city = City.objects.get(id=city_id)

        if validated_data.get('email', None):
            instance.email = validated_data.get('email')
            instance.username = validated_data.get('email')

        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)

        instance.save()
        instance.usercrm.save()
        return instance

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'most_important_city', 'last_name',
        )
