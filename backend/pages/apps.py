from django.apps import AppConfig


class AboutCinemaConfig(AppConfig):
    name = 'pages'
    verbose_name = "Страница о кинотеатре"
    verbose_name_plural = "Страницы о кинотеатре"
