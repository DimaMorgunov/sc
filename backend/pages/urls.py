from django.urls import path
from . import views


urlpatterns = [
    path('posts/', views.PostInJson.as_view()),
]
