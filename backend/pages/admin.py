from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from seo.admin import PageSeoInline, PostSeoInline
from .models import *


class PostTypeInline(admin.StackedInline):
    model = PostsTypes


class SimplePageAdmin(AdminImageMixin, admin.ModelAdmin):
    ordering = ["id"]
    inlines = [PageSeoInline, ]
    list_filter = ["url"]
    list_display = ["id", "url", "background_image", ]
    list_display_links = ['id', "url", ]
    search_fields = ["id", "title"]
    fieldsets = (
        (None, {
            'fields': ('url', "background_image")
        }),

    )

    class Meta:
        model = SimplePages
        verbose_name = "Страница"
        verbose_name_plural = "Страницы"


class PostsAdmin(AdminImageMixin, admin.ModelAdmin):
    list_editable = ["position", "public", "post_type"]
    inlines = [PostSeoInline, ]
    list_display = ["id", "title", "start_demonstration", "end_demonstration", "post_type", "public", "position"]
    list_display_links = ['id', "title", ]
    search_fields = ["title", "id"]
    list_filter = ["title", "start_demonstration"]
    ordering = ["start_demonstration"]
    fieldsets = (
        (None, {
            'fields': ('title', 'image', 'cinema', 'start_demonstration', 'end_demonstration',
                       'short_description', 'description', 'post_type', 'public', 'position')
        }),
    )

    class Meta:
        model = Posts
        verbose_name = "Пост"
        verbose_name_plural = "Посты"


class PostsTypeAdmin(admin.ModelAdmin):
    list_display_links = ['id', "title", ]
    list_display = ["id", "title", "verbose_title",]
    search_fields = ["title", ]
    list_filter = ["title", ]
    ordering = ["id"]
    fieldsets = (
        (None, {
            'fields': ('title', 'verbose_title')
        }),
    )

    class Meta:
        model = PostsTypes
        verbose_name = "Тип поста"
        verbose_name_plural = "Тип постов"


admin.site.register(PostsTypes, PostsTypeAdmin)
admin.site.register(Posts, PostsAdmin)
admin.site.register(SimplePages, SimplePageAdmin)
