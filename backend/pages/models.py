from django.db import models
from sorl.thumbnail import ImageField

from backoffice.models import Branch


class PostsTypes(models.Model):
    title = models.CharField(
        verbose_name="Тип",
        help_text="Это внутренний текст",
        max_length=128,
        null=True,
        blank=True,
    )

    verbose_title = models.CharField(
        verbose_name="Название ",
        help_text="Это то что демонстрируеться на сайте",
        max_length=128,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Тип поста"
        verbose_name_plural = "Типы постов"


class Posts(models.Model):
    title = models.CharField(
        max_length=128,
        help_text="Название поста",
        verbose_name="Пост",
        unique=True
    )

    image = ImageField(
        upload_to='pages_img/',
        help_text="выберите путь к изображению",
        verbose_name="Изображение:",
        default='default/post.jpg',
    )

    cinema = models.ManyToManyField(
        Branch,
        help_text="Выберите кинотеатр для кторого будет демонстрироваться пост",
        verbose_name="Кинотеатр",
    )

    start_demonstration = models.DateField(
        verbose_name="Начало",
        help_text="Дата начала демонстрации",
        blank=True,
        null=True,
        )

    end_demonstration = models.DateField(
        verbose_name="Конец",
        help_text="Дата окончания демонстрации",
        blank=True,
        null=True,
        )

    short_description = models.TextField(
        help_text="Введите короткое описание",
        verbose_name="Короткое описание",
        blank=True,
        null=True,
    )

    description = models.TextField(
        help_text="Введите полное описание",
        verbose_name="Описание",
        blank=True,
        null=True,
    )

    post_type = models.ForeignKey(
        PostsTypes,
        on_delete=models.CASCADE,
        verbose_name="Тип поста",
        null=True,
        blank=True,
    )

    public = models.BooleanField(
        help_text="Если чекбокс активирован, то пост будет демонстрироваться на сайте."
                  "Если чекбокс не активирован, то пост будет скрыт",
        default=True,
        verbose_name="публикация",
    )

    position = models.IntegerField(
        default=0,
        help_text="Вес сортировки фильмов в списке. Чем больше значение, тем ближе к началу"
                  "Если 0 - не демонстрируеться",
        verbose_name="Вес",
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты"


class SimplePages(models.Model):
    url = models.CharField(
        max_length=128,
        help_text="Урл до страницы",
        verbose_name="Url",
        default='/'
    )

    background_image = ImageField(
        upload_to='background/',
        help_text="выберите путь к фону страницы",
        verbose_name="Фон страницы:",
        default='/default/seo_image.jpg',
        blank=True,
        null=True
    )

    def __str__(self):
        return 'Инфа по странице ->' + self.url

    class Meta:
        verbose_name = "Страница"
        verbose_name_plural = "Страницы"

