from rest_framework import serializers
from .models import Posts, PostsTypes
from seo.serializers import ForPostSerializer, ForPost


class PostTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostsTypes


class PostsSerializer(serializers.ModelSerializer):
    seo_inline = serializers.SerializerMethodField('get_seo_for_object')
    type_of_post = serializers.CharField(
        source='post_type.verbose_title',
        allow_blank=True,
        allow_null=True
    )

    @staticmethod
    def get_seo_for_object(post_object):
        qs = ForPost.objects.get(fk=post_object)
        serialize = ForPostSerializer(instance=qs, many=False)
        return serialize.data

    class Meta:
        model = Posts
        fields = ("title", 'image', 'short_description', 'description',
                  'seo_inline', 'type_of_post')
        depth = 3


class PostsListSerializer(serializers.ModelSerializer):
    seo_inline = serializers.SerializerMethodField('get_seo_for_object')
    type_of_post = serializers.CharField(
        source='post_type.verbose_title',
        allow_blank=True,
        allow_null=True
    )

    @staticmethod
    def get_seo_for_object(post_object):
        qs = ForPost.objects.get(fk=post_object)
        serialize = ForPostSerializer(instance=qs, many=False)
        return serialize.data

    class Meta:
        model = Posts
        fields = ("title", 'image', 'short_description', 'seo_inline', 'type_of_post')
        depth = 3
