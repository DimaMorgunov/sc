from .serializers import PostsSerializer
from .models import Posts
from rest_framework.views import APIView
from rest_framework.response import Response


class PostInJson(APIView):

    authentication_classes = []

    def post(self, request):
        try:
            print(request.data['url'])
            query = Posts.objects.get(forpost__url=request.data['url'])
            serializer = PostsSerializer(query)
            return Response(serializer.data)
        except Posts.DoesNotExist:
            return Response({"error": 'DoesNotExist'})



