var timeGate = 15;

function getClock() {
    var time =  new Date();

    var sec = time.getSeconds() > 9 ? time.getSeconds() : '0' + time.getSeconds();
    var min = time.getMinutes() > 9 ? time.getMinutes() : '0' + time.getMinutes();
    var hour = time.getHours() > 9 ? time.getHours() : '0' + time.getHours();
    document.getElementById("clock").innerHTML = '<button class="btn btn-danger">'
        + hour
        + ':'
        + min
        + ':'
        + sec
        +'</button>';
}

function checkTime() {
    var buttons = Array.from(document.getElementsByClassName("timestmap"));

    buttons.forEach(function(button, i, arr) {

        var currentTime = new Date();
        if ( currentTime.setMinutes(new Date().getMinutes()) < getTime(button)){
            button.classList.remove('show_time_pass');
            button.classList.add('show_time_not_pass');
        }  else {
            button.classList.add('show_time_pass');
            button.classList.remove('show_time_not_pass');
        }
    })

}

function getTime(button_object) {
    var showtimeDate = new Date();
    showtimeDate.setHours(button_object.dataset.hour);
    showtimeDate.setMinutes(button_object.dataset.minutes);
    return showtimeDate
}

document.addEventListener("DOMContentLoaded", function(event) {
    checkTime();
    setInterval(checkTime, 60000);
});

document.addEventListener("DOMContentLoaded", function(event) {
    getClock();
    setInterval(getClock, 1000);
});