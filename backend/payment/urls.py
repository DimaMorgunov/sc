from django.urls import path
from . import views


urlpatterns = [
    path('callback/<int:branch_id>', views.PostPayment.as_view()),
    path('callback/<int:branch_id>/', views.PostPayment.as_view()),

    path('result/<int:reservation_id>', views.ReturningPage.as_view()),
    path('result/<int:reservation_id>/', views.ReturningPage.as_view()),
]
