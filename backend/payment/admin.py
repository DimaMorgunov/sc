from django.contrib import admin
from daterange_filter.filter import DateRangeFilter

from .models import *

admin.site.site_header = 'SmartAdmin'
admin.site.site_title = 'SmartCinema'


class InvoiceAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_filter = ["cash_keeper", ]
    list_display = ["id", "cash_keeper", "get_city", "pay_way", "get_time", "cash_sum", ]
    search_fields = ["cash_keeper", ]

    def get_city(self, obj):
        return obj.reservation.show_time.movietobranch_id.branch_id.name

    def get_time(self, obj):
        return obj.reservation.datetime

    get_city.short_description = "Отделение"
    get_time.short_description = "Время"

    class Meta:
        model = Invoice
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"


def upper_case_name(obj):
    return "%s %s" % (obj.sender_first_name or '-', obj.sender_last_name or '-')


upper_case_name.short_description = 'Платильщик'


class PaymentAdmin(admin.ModelAdmin):

    list_filter = ["status", "invoice__reservation__show_time__movietobranch_id__branch_id",
                   "invoice__pay_way", ('invoice__reservation__show_time__start', DateRangeFilter)]
    list_display = ["order_id", "transaction_id", upper_case_name, 'sender_phone', 'sender_card_bank',
                    'sender_card_type', 'status', 'amount', 'end_date']
    search_fields = ["status", ]
    ordering = ('-end_date',)
    fieldsets = (
        (None, {
            'fields': ('amount', 'action', 'liqpay_order_id', 'acq_id', 'order_id',
                       'paytype', 'type', 'status', 'err_code', 'err_description','transaction_id', 'payment_id')
        }),
        ('Данные о времени покупки', {
            'fields': ('end_date', 'create_date', ),
        }),
        ('Данные о клиенте', {
            'fields': ('sender_first_name', 'sender_last_name', 'sender_phone',
                       'sender_card_mask2', 'sender_card_bank', ),
        }),
        ('Данные о возврате', {
            'classes': ('collapse',),
            'fields': ('refund_amount', 'refund_reserve_ids', 'refund_date_last'),
        }),
    )

    class Meta:
        order = 'end_date'
        model = Checkout
        verbose_name = "Счет"
        verbose_name_plural = "Счета"


admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(Checkout, PaymentAdmin)
