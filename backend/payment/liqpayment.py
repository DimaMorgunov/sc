import pytz
from payment.models import LiqPay, Checkout, Invoice
from django.conf import settings
from datetime import datetime
from logging import getLogger

log = getLogger('payment')


class LiqPayFunctionality:

    @staticmethod
    def payment_return(payment_keeper, returning_checkout):
        liqpay = LiqPay('i' + str(payment_keeper.LIQPAY_KEY_PUBLIC), payment_keeper.LIQPAY_KEY_PRIVATE)

        return liqpay.api("request", {
            "action": "refund",
            "version": "3",
            "order_id": returning_checkout.order_id
        })

    @staticmethod
    def payment_change_info(new_payment_info):
        new_payment_info['invoice'] = Invoice.objects.get(order_id=new_payment_info['order_id'])
        pure_attrs = {key: value for key, value in new_payment_info.items() if hasattr(Checkout, key)}

        if 'expired_3ds' == pure_attrs.get('err_code', ''):
            log.info("[LiqPay] Payment has been expired from LiqPay massage.")

        try:
            exist_checkout = Checkout.objects.get(order_id=new_payment_info['order_id'])
            for key, value in pure_attrs.items():
                setattr(exist_checkout, key, value)
            exist_checkout.save()
        except Checkout.DoesNotExist:
            return Checkout.objects.create(**pure_attrs)
        return exist_checkout

    @staticmethod
    def save_callback(payment_keeper, response):
        liqpay = LiqPay('i' + str(payment_keeper.LIQPAY_KEY_PUBLIC), payment_keeper.LIQPAY_KEY_PRIVATE)
        decode_response = liqpay.decode_data_from_str(response)

        params_to_remove = [
            'sender_commission',
            'receiver_commission',
            'agent_commission',
            'amount_debit',
            'ip',
            'amount_credit',
            'commission_credit',
            'commission_debit',
            'currency_debit',
            'is_3ds',
            'currency_credit',
            'sender_bonus',
            'amount_bonus',
            'authcode_debit',
            'rrn_debit',
            'mpi_eci',
            'sender_card_country',
            'currency',
            'currency_credit',
            'version',
            'description',
            'public_key',
            'code',
        ]

        date_params = ['create_date', 'end_date', 'refund_date_last', 'reserve_date']

        payment_result = LiqPayFunctionality._remove_params(decode_response, params_to_remove, date_params)
        return LiqPayFunctionality.payment_change_info(payment_result)

    @staticmethod
    def _get_date(seconds):
        return datetime.fromtimestamp(seconds, tz=pytz.timezone(settings.TIME_ZONE))

    @staticmethod
    def _remove_params(dict, params, date_params):
        for param in params:
            dict.pop(param, None)

        for param in date_params:
            try:
                dict[param] = LiqPayFunctionality._get_date(dict[param] / 1000)
            except KeyError:
                pass

        return dict

    @staticmethod
    def get_new_order_id():
        return 'new_order_1'

    @staticmethod
    def pay_return(invoice, sum=None):
        liqpay = LiqPay('i' + str(invoice.cash_keeper.LIQPAY_KEY_PUBLIC), invoice.cash_keeper.LIQPAY_KEY_PRIVATE)
        res = liqpay.api("request", {
            "action": "refund",
            "version": "3",
            "order_id": invoice.order_id,
            'amount': sum if sum is not None else invoice.cash_sum
        })
        print(res)
