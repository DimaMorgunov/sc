from datetime import datetime, timedelta
from urllib.parse import urljoin

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone

from backoffice.models import CashCeeper
from reservation.models import Reservation, Ticket
from .liqpay_api import LiqPay

import pytz
from cinema.settings import Base as base_settins

LIQPAY_STATUSES = (
    ('success', u'Успешный платеж'),
    ('failure', u'Неуспешный платеж'),
    ('wait_secure', u'Платеж на проверке'),
    ('wait_accept', u'Деньги с клиента списаны, но магазин еще не прошел проверку'),
    ('wait_lc', u'Аккредитив. Деньги с клиента списаны, ожидается подтверждение доставки товара'),
    ('processing', u'Платеж обрабатывается'),
    ('sandbox', u'Тестовый платеж'),
    ('subscribed', u'Подписка успешно оформлена'),
    ('unsubscribed', u'Подписка успешно деактивирована'),
    ('reversed', u'Возврат клиенту после списания'),
    ('cash_wait', u'Ожидание оплаты счета клиентом в терминале'),
    ('expired', u'Просрочен')
)

PAY_WAY = (
    (0, "web"),
    (1, "android"),
    (2, "iOs"),
)


class Invoice(models.Model):
    reservation = models.ForeignKey(
        Reservation,
        on_delete=models.CASCADE,
        verbose_name="Резервирования"
    )

    cash_keeper = models.ForeignKey(
        CashCeeper,
        on_delete=models.CASCADE,
        verbose_name="Получатель"
    )

    cash_sum = models.IntegerField(
        verbose_name='Сумма платежа',
    )

    order_desc = models.TextField(
        verbose_name='Описание платежа',
        help_text='Текст который выводиться клиенту в окне описания на странице LiqPay',
        blank=True,
    )

    pay_way = models.CharField(
        verbose_name=u'Откуда',
        help_text=u'Откуда получен платеж',
        max_length=12,
        choices=PAY_WAY,
        default='cash_wait',
    )

    order_id = models.CharField(
        blank=True,
        max_length=24,
        unique=True,
    )

    @staticmethod
    def next_order_id(reservation):
        title = reservation.show_time.movietobranch_id.movie_id.forfilms.url[8:12]
        return title.join(datetime.now().timestamp().__str__().split('.'))

    def get_order_description(self):
        try:
            tickets = Ticket.objects.filter(reservation=self.reservation)

            start = self.reservation.show_time.start
            dt_object = datetime.combine(start.date(), start.time())
            local_time_text = pytz.timezone(base_settins.TIME_ZONE).fromutc(dt_object).strftime('%d.%m.%Y - %H:%M:%S')

            main_str = 'Билет на фильм "%s" - %s \n' % (self.reservation.show_time.movietobranch_id.movie_id.title,
                                                        local_time_text)
            for ticket in list(tickets):
                main_str += 'ряд - %s, место - %s \n' % (ticket.row, ticket.place)
            return main_str
        except Reservation.DoesNotExist:
            return "Билеты в кинотеатр SmartCinema"

    def get_data_for_liqpay(self, json=False):

        liqpay = LiqPay(
            'i' + str(self.cash_keeper.LIQPAY_KEY_PUBLIC),
            self.cash_keeper.LIQPAY_KEY_PRIVATE
        )

        payment_data = {
            'action': 'pay',
            'amount': int(self.cash_sum) if not settings.LIQPAY_DEBUG else 2,
            'language': 'uk',
            'expired_date': (timezone.now() + timedelta(hours=3, minutes=15)).strftime('%Y-%m-%d %H:%M:%S'),
            'currency': settings.LIQPAY_DEFAULT_CURRENCY,
            'description': self.get_order_description(),
            'order_id': self.order_id,
            'version': '3',
            'result_url': urljoin(settings.LIQPAY_RETURN_BASE_URL, urljoin(
                settings.LIQPAY_RESULT_URL, str(self.reservation.id)
            )),
            'server_url': urljoin(settings.LIQPAY_RETURN_BASE_URL, urljoin(
                settings.LIQPAY_CALLBACK_URL, str(self.reservation.show_time.movietobranch_id.branch_id.id)
            ))
        }
        return liqpay.cnb_form(payment_data, json)

    def __str__(self):
        return self.order_desc

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class Checkout(models.Model):
    invoice = models.OneToOneField(
        Invoice,
        on_delete=models.CASCADE,
    )

    order_id = models.CharField(
        verbose_name=u'Заказ',
        help_text=u'Внутренний код заказа',
        max_length=128,
        unique=True,
    )

    acq_id = models.CharField(
        verbose_name=u'id транзакции в системе банка клиента',
        max_length=128,
    )

    liqpay_order_id = models.CharField(
        verbose_name=u'id транзакции в системе LiqPay',
        max_length=128,
    )

    action = models.CharField(
        verbose_name=u'Действие',
        max_length=128,
    )

    type = models.CharField(
        verbose_name=u'Тип',
        max_length=128,
    )

    paytype = models.CharField(
        verbose_name=u'Способ оплаты',
        max_length=128,
    )

    transaction_id = models.CharField(
        verbose_name='LiqPay id',
        help_text='номер транзакции',
        max_length=128,
    )

    payment_id = models.CharField(
        verbose_name='Номер транзакции',
        help_text='номер транзакции в cистеме LiqPay',
        max_length=128,
    )

    amount = models.PositiveIntegerField(
        verbose_name='Оплачено',
        help_text='Оплачено в cистеме LiqPay',
    )

    sender_phone = models.CharField(
        verbose_name=u'Телефон',
        help_text=u'Телефон плательщика в международном формате',
        max_length=64,
        blank=True,
        null=True,
    )

    sender_first_name = models.CharField(
        verbose_name=u'Имя',
        help_text=u'Имя плательщика',
        max_length=128,
        blank=True,
        null=True,
    )

    sender_last_name = models.CharField(
        verbose_name=u'Фамилия',
        help_text=u'Фамилия плательщика',
        max_length=128,
        blank=True,
        null=True,
    )

    sender_card_mask2 = models.CharField(
        verbose_name=u'Карта',
        help_text=u'Карта плательщика',
        max_length=128,
        blank=True,
        null=True,
    )

    sender_card_bank = models.CharField(
        verbose_name=u'Банк',
        help_text=u'Банк владелец карты плательщика',
        max_length=256,
        blank=True,
        null=True,
    )

    sender_card_type = models.CharField(
        verbose_name=u'Тип карты',
        help_text=u'Тип карты плательщика',
        max_length=256,
        blank=True,
        null=True,
    )

    end_date = models.DateTimeField(
        verbose_name=u'Продано',
        help_text=u'Дата и время окончания продажи',
        default=timezone.now,
    )

    create_date = models.DateTimeField(
        verbose_name=u'Счет выставлен в',
        help_text=u'Дата и время выставления счета клиенту',
        default=timezone.now,
    )

    status = models.CharField(
        verbose_name=u'Статус',
        help_text=u'Статус оплаты',
        max_length=12,
        choices=LIQPAY_STATUSES,
        default='cash_wait',
    )

    err_code = models.CharField(
        verbose_name=u'Ошибка',
        help_text=u'Код ошибки',
        max_length=256,
        blank=True,
        null=True,
    )

    err_description = models.TextField(
        verbose_name=u'Описание ошибки',
        help_text=u'Описание ошибки',
        blank=True,
        null=True,
    )

    refund_date_last = models.DateTimeField(
        verbose_name=u'Дата возврата',
        help_text=u'Дата возврата',
        blank=True,
        null=True,
    )

    refund_reserve_ids = models.TextField(
        verbose_name=u'id возврата',
        help_text=u'id возврата',
        blank=True,
        null=True,
    )

    refund_amount = models.DecimalField(
        verbose_name=u'Сумма возврата',
        help_text=u'Сумма возврата',
        max_digits=40,
        decimal_places=2,
        blank=True,
        null=True,
    )

    reserve_refund_id = models.TextField(
        verbose_name=u'id возврата',
        help_text=u'id возврата',
        blank=True,
        null=True,
    )

    reserve_payment_id = models.TextField(
        verbose_name=u'id возврата',
        help_text=u'id возврата',
        blank=True,
        null=True,
    )

    reserve_amount = models.TextField(
        verbose_name=u'id возврата',
        help_text=u'id возврата',
        blank=True,
        null=True,
    )

    reserve_date = models.DateTimeField(
        verbose_name=u'Дата возврата',
        help_text=u'Дата возврата',
        blank=True,
        null=True,
    )

    def __str__(self):
        return 'счет'

    class Meta:
        verbose_name = "Счет"
        verbose_name_plural = "Счета"


def add_desription_to_invoice(sender, **kwargs):
    instance = kwargs['instance']
    setattr(instance, 'order_desc', instance.get_order_description())


post_save.connect(add_desription_to_invoice, sender=Invoice)
