import logging
from rest_framework.views import APIView
from django.conf import settings
from backoffice.models import Branch
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .liqpayment import LiqPayFunctionality
from datetime import timedelta
from django.utils import timezone
from reservation.models import Ticket, Reservation

payment_logger = logging.getLogger('payment')


class PostPayment(APIView):

    @csrf_exempt
    def post(self, *args, **kwargs):

        self.current_branch = kwargs['branch_id']
        payment_keeper = Branch.objects.get(id=self.current_branch).cash_keeper
        self.checkout = LiqPayFunctionality.save_callback(payment_keeper, self.request.data['data'])

        # todo: debug purpose
        log_results = "[Branch] %s\n" % self.current_branch

        current_time = timezone.now()
        reservation_time = self.checkout.invoice.reservation.datetime.astimezone(current_time.tzinfo)
        delta = current_time - reservation_time

        # todo: debug purpose
        log_results += "[CurrentTime] %s\n" % current_time
        log_results += "[ReservationTime] %s\n" % reservation_time

        if self.checkout.status not in ['wait_accept'] and \
                self.checkout.invoice.reservation.ticket_set.last().status not in ['success', 'OPR']:

            # todo: debug purpose
            log_results += "[FirstCatchBlock] Not wait accept and not in OPR or success.\n"

            self.tickets_query = Ticket.objects.filter(reservation=self.checkout.invoice.reservation)

            # todo: debug purpose
            film = self.checkout.invoice.reservation.show_time.movietobranch_id.movie_id
            for tic in self.tickets_query:
                log_results += "[Ticket(%d)]:\n[Film] %s\n" % (tic.id, film.title)
                log_results += "[Ticket(%d)Info] %s\n" % (tic.id, tic)

            if delta > timedelta(minutes=settings.DEFAULT_PREMIERE_OPR_TIME):

                try:
                    self.accept_reservation()
                    setattr(self.checkout.invoice.reservation, 'status', 'success')
                    self.checkout.invoice.reservation.save()
                    self.send_tickets('default')
                except AttributeError as e:
                    payment_logger.warning("[OPR][PaymentAttributeError] %s" % e)

                    setattr(self.checkout.invoice.reservation, 'status', 'OPR')
                    self.checkout.invoice.reservation.save()
                    self.send_tickets('opr')

            else:

                setattr(self.checkout.invoice.reservation, 'status', 'success')
                self.checkout.invoice.reservation.save()

                try:
                    self.accept_reservation()
                except AttributeError as e:
                    payment_logger.info('[Reservation][LastSteps] %s' % e)
                    raise e

                self.send_tickets('default')

        elif self.checkout.status == 'reversed':
            try:
                self.reverse_reservation()
                setattr(self.checkout.invoice.reservation, 'status', 'reversed')
            except AttributeError:
                setattr(self.checkout.invoice.reservation, 'status', 'OPR')

                payment_logger.info('[LiqPay-PostBack] Checkout %s reversed' % self.checkout.order_id)

        else:
            payment_logger.info('[LiqPay-PostBack] Checkout %s data update success' % self.checkout.order_id)

        # todo: debug purpose
        payment_logger.info(log_results)

        return HttpResponse('')

    def accept_reservation(self):
        for ticket in self.tickets_query:
            payment_logger.info('[LiqPay-PostBack] Accept ticket %s' % ticket.id)
            reserved, msg = ticket.confirm()
            if not reserved:
                raise AttributeError(msg)

    def reverse_reservation(self):
        for ticket in self.tickets_query:
            payment_logger.info('[LiqPay-PostBack] Cancel ticket %s' % ticket.id)
            reserved, msg = ticket.pay_return()
            if not reserved:
                raise AttributeError(msg)

    def send_tickets(self, status):
        payment_logger.info('[LiqPay-PostBack] Send email with ticket %s' % self.checkout.order_id)
        self.checkout.invoice.reservation.send_email(status)


class ReturningPage(APIView):

    @csrf_exempt
    def get(self, *args, **kwargs):
        payment_logger.debug('[LiqPay-PostBack] Returning page data %s' % kwargs)

        try:
            reservation_id = kwargs.get('reservation_id', None)
            if reservation_id:
                reservation_obj = Reservation.objects.get(id=reservation_id)

                return HttpResponseRedirect(redirect_to='%s%s/' %
                                                        (settings.LIQPAY_DEFAULT_TICKET_PAGE, reservation_obj.id))

        except Reservation.DoesNotExist:
            pass

        return HttpResponseRedirect(redirect_to='%s%s/' %
                                                (settings.LIQPAY_DEFAULT_TICKET_PAGE, 'error'))
