from django.apps import AppConfig


class PaymentConfig(AppConfig):
    name = 'payment'
    verbose_name = "Платеж"
    verbose_name_plural = "Платежи"
