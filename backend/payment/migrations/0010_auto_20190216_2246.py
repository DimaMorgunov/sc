# Generated by Django 2.1.5 on 2019-02-16 20:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0009_auto_20190213_2213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='order_desc',
            field=models.TextField(blank=True, help_text='Текст который выводиться клиенту в окне описания на странице LiqPay', verbose_name='Описание платежа'),
        ),
    ]
