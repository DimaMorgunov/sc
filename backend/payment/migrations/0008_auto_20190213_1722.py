# Generated by Django 2.1.2 on 2019-02-13 15:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0007_auto_20190213_1644'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkout',
            name='err_code',
            field=models.CharField(blank=True, help_text='Код ошибки', max_length=256, null=True, verbose_name='Ошибка'),
        ),
    ]
