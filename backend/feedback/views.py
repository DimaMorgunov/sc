from .serializers import VacanciesSerializer
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils import timezone
from django.conf import settings
from django.http import FileResponse, Http404
import os


class VacanciesInJson(APIView):

    def post(self, request):
        query = Vacancies.objects.all()
        serializer = VacanciesSerializer(query, many=True)
        return Response(serializer.data)


class GetSummary(APIView):

    def post(self, request):
        vacancies_id = request.data.get('vacancies_id', False)
        branch_id = request.data.get('branch_id', 1)
        context_data_obj = {}
        if vacancies_id and branch_id:
            if branch_id == 'undefined':
                branch_id = 1
            context_data_obj['vacancies_id'] = Vacancies.objects.get(id=vacancies_id)
            context_data_obj['branch_id'] = Branch.objects.get(id=branch_id)
            context_data_obj['email'] = request.data.get('email', '')
            context_data_obj['first_name'] = request.data.get('first_name', '')
            context_data_obj['last_name'] = request.data.get('last_name', '')
            context_data_obj['phone'] = request.data.get('phone', '')

            try:
                context_data_obj['resume'] = request.FILES['resume']
            except:
                sender_logger.warning('[FileAttach] File Don`t attached')

            return feedback_check(Summary, context_data_obj)
        else:
            return Response({'Error': 'some params error'}, status=404)


class GetFeedback(APIView):

    def post(self, request):
        branch_query = Branch.objects.get(id=request.data.get('branch_id', 1))
        request.data['branch_id'] = branch_query
        return feedback_check(Review, request.data)


def feedback_check(feedback_model, context, ):
    today_feedback = feedback_model.objects.filter(email=context['email'], phone=context['phone'],
                                                   send__date=timezone.now().date())

    count_today_feedback = today_feedback.count()
    if count_today_feedback <= settings.MAX_FEEDBACK_IN_DAY:
        query = feedback_model.objects.create(**context)
        query.save()
        if count_today_feedback == 0:
            query.send_client_default_email({'subject': 'Новый отзыв'})
        else:
            query.send_client_default_email({'subject': 'Обновленный отзыв'})
        return Response({'result': 'ok'})
    else:
        return Response({'error': 'Сегодня заявка от вас уже была получена'})

