from django.contrib import admin
from .models import *


def send_email_from_admin_panel(self, request, queryset):
    for obj in queryset:
        obj.send_client_default_email(extra_data={'subject': 'Новый отзыв'}, repeated=True)
        self.message_user(request, "Успешно отправлено письмо на почту %s" % obj.email)


send_email_from_admin_panel.short_description = "Повторно отправить письмо клиенту"


class VacanciesAdmin(admin.ModelAdmin):
    ordering = ["position"]
    list_editable = ["position"]
    list_filter = ["title"]
    list_display = ["id", "title", "position"]
    search_fields = ["title"]

    class Meta:
        model = Vacancies
        verbose_name = "Вакансия"
        verbose_name_plural = "Вакансии"


class SummaryAdmin(admin.ModelAdmin):
    actions = [send_email_from_admin_panel]
    list_display = ["first_name", "last_name", "email", "phone", 'vacancies_id', 'send']
    search_fields = ["name", "email", "phone", ]
    list_filter = ["send", "vacancies_id", "branch_id"]
    ordering = ["send", ]

    class Meta:
        model = Summary
        verbose_name = "Резюме"
        verbose_name_plural = "Резюме"


class ReviewAdmin(admin.ModelAdmin):
    actions = [send_email_from_admin_panel]
    list_display = ["name", "email", "phone",  'send']
    search_fields = ["name", "email", "phone", ]
    list_filter = ["send", "branch_id"]
    ordering = ["send", ]

    class Meta:
        model = Review
        verbose_name = "Резюме"
        verbose_name_plural = "Резюме"


admin.site.register(Review, ReviewAdmin)
admin.site.register(Vacancies, VacanciesAdmin)
admin.site.register(Summary, SummaryAdmin)

