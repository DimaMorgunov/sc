from django.db import models
from backoffice.models import Branch
from django.utils import timezone
from django.conf import settings
import os
from feedback.sender_util import EmailFactory

import logging
sender_logger = logging.getLogger('sender')


class Vacancies(models.Model):
    title = models.CharField(
        help_text='Введите название вакансии',
        max_length=128,
        verbose_name='Название вакансии'
    )

    position = models.IntegerField(
        default=0,
        verbose_name="Вес",
        blank=True,
        null=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'


class Summary(models.Model):
    vacancies_id = models.ForeignKey(
        Vacancies,
        on_delete=models.CASCADE,
        verbose_name="Вакансия",
    )

    branch_id = models.ForeignKey(
        Branch,
        on_delete=models.CASCADE,
        verbose_name="Кинотеатр",
        default=1
    )

    first_name = models.CharField(
        help_text='Например: Иван',
        max_length=128,
        verbose_name='Имя',
        default=''
    )

    last_name = models.CharField(
        help_text='Например: Иванов',
        max_length=128,
        verbose_name='Фамилия',
        default=''
    )

    email = models.EmailField(
        verbose_name="Email",
    )

    phone = models.CharField(
        verbose_name="Телефон",
        max_length=128,
    )

    message = models.TextField(
        verbose_name="Сообщение",
        max_length=128,
    )

    resume = models.FileField(
        verbose_name="Резюме",
        upload_to="resume/",
        blank=True,
        null=True,
    )

    send = models.DateTimeField(
        verbose_name='Получено',
        help_text=u'Дата и время отправки формы',
        default=timezone.now
    )

    def __str__(self):
        return '%s %s претендент на вакансию %s' % (self.first_name, self.last_name, self.vacancies_id.__str__())

    STAFF_EMAIL = settings.FEEDBACK_STAFF_EMAIL

    def send_client_default_email(self, extra_data=None, repeated=False):
        self.EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'feedback')
        self.EMAIL_TXT = 'feedback.txt'
        self.EMAIL_HEAD = 'feedback_head.txt'
        self.EMAIL_HTML = 'feedback.html'

        data = dict(branch_id=self.branch_id, settings=settings, instance=self, )

        if extra_data:
            data.update(extra_data)

        message = EmailFactory(
            self,
            extra_data=data,
            email_to=[self.email, ],
        )
        sender_logger.info('[EmailFactory] Start send feedback default email')
        message.send_message()

        if not repeated:
            self.send_stuff_email()

    def send_stuff_email(self):
        self.EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'for_staff')
        self.EMAIL_TXT = 'vacancies.txt'
        self.EMAIL_HEAD = 'vacancies_head.txt'
        self.EMAIL_HTML = False

        message = EmailFactory(
            self,
            extra_data={
                'instance': self,
                'branch_id': self.branch_id,
                'settings': settings,
            },
            email_to=self.STAFF_EMAIL[self.branch_id.id],
            file_attach=self.resume
        )
        sender_logger.info('[EmailFactory] Start send vacancies stuff email')
        message.send_message()

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'


class Review(models.Model):

    branch_id = models.ForeignKey(
        Branch,
        on_delete=models.CASCADE,
        verbose_name="Кинотеатр",
        default=1
    )

    name = models.CharField(
        help_text='Например: Иванов Иван Иванович',
        max_length=128,
        verbose_name='ФИО'
    )

    email = models.EmailField(
        verbose_name="Email",
    )

    phone = models.CharField(
        verbose_name="Телефон",
        max_length=128,
    )

    message = models.TextField(
        verbose_name="Сообщение",
        max_length=128,
    )

    send = models.DateTimeField(
        verbose_name='Получено',
        help_text=u'Дата и время отправки формы',
        default=timezone.now
    )

    STAFF_EMAIL = settings.REVIEW_STAFF_EMAIL

    def send_client_default_email(self, extra_data=None, repeated=False):
        self.EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'feedback')
        self.EMAIL_TXT = 'feedback.txt'
        self.EMAIL_HEAD = 'feedback_head.txt'
        self.EMAIL_HTML = 'feedback.html'

        data = dict(branch_id=self.branch_id, settings=settings, instance=self,)

        if extra_data:
            data.update(extra_data)

        message = EmailFactory(
            self,
            extra_data=data,
            email_to=(self.email,),
        )
        sender_logger.info('[EmailFactory] Start send feedback default email')
        message.send_message()

        if not repeated:
            self.send_stuff_email()

    def send_stuff_email(self, extra_data=None, ):
        self.EMAIL_TEMPLATE_PATH = os.path.join(settings.EMAIL_TEMPLATE_DIR, 'for_staff')
        self.EMAIL_TXT = 'feedback.txt'
        self.EMAIL_HEAD = 'feedback_head.txt'
        self.EMAIL_HTML = False
        
        data = dict(branch_id=self.branch_id, settings=settings, instance=self, )
        
        if extra_data:
            data.update(extra_data)
            
        message = EmailFactory(
            self,
            extra_data=data,
            email_to=self.STAFF_EMAIL[self.branch_id.id],
        )
        sender_logger.info('[EmailFactory] Start send feedback stuff email')
        message.send_message()

    def __str__(self):
        return "Отзыв от "+self.name

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


