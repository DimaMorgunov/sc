from django.urls import path
from . import views


urlpatterns = [
    path('list/', views.VacanciesInJson.as_view()),
    path('summary/', views.GetSummary.as_view()),
    path('feedback/', views.GetFeedback.as_view()),
]
