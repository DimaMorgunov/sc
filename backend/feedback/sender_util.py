import os, time, socket, random, requests
from logging import getLogger
from django.conf import settings
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.template.loader import get_template
from backoffice.models import Branch

logger = getLogger('sender')
django_log = getLogger('django')
debug = getLogger('debug')


class EmailFactory:
    def __init__(self, instance, extra_data, email_to=None, file_attach=None):
        """
        :param instance: ORM object
        :param extra_data: dict with field - branch_id and template variables
        :param email_to: as default send email to STAFF_EMAIL
        :param file_attach: as default None or list 'file path'
        """

        self.instance = instance
        self.extra_data: dict = extra_data
        self.extra_data.update({'instance': self.instance})
        self.EMAIL_TO = self.get_email_to(email_to)
        self.EMAIL_FROM = settings.EMAIL_FROM

        self.head = self.get_and_render(os.path.join(self.instance.EMAIL_TEMPLATE_PATH, self.instance.EMAIL_HEAD))
        self.txt = self.get_and_render(os.path.join(self.instance.EMAIL_TEMPLATE_PATH, self.instance.EMAIL_TXT))

        if instance.EMAIL_HTML:
            self.message = self.create_message()
            self.html = self.get_and_render(os.path.join(self.instance.EMAIL_TEMPLATE_PATH, self.instance.EMAIL_HTML))
            self.attach_html()
        else:
            self.message = self.create_base_message()

        if file_attach is not None:
            self.file_attach = file_attach
            self.attach_file()

    def get_email_to(self, email_to):
        if email_to is None:
            if isinstance(self.extra_data.get('branch_id'), Branch):
                return self.instance.STAFF_EMAIL[self.extra_data['branch_id'].id]
            return self.instance.STAFF_EMAIL[self.extra_data['branch_id']]
        else:
            return email_to

    def get_and_render(self, path):
        template = get_template(path)
        return template.render(self.extra_data)

    def attach_file(self, ):
        from reservation.models import Ticket
        from django.db.models.query import QuerySet

        exceptions = ""
        if isinstance(self.file_attach, QuerySet):

            for obj in self.file_attach:
                if isinstance(obj, Ticket):
                    try:
                        self.message.attach_file(obj.qr_code.path)
                    except ValueError:
                        # todo: Change it later
                        if obj.key is not None:
                            obj.generate_qr_code()
                        elif 'Подтверждена оплата' == obj.status:
                            ok, key = obj.get_ticket_key()
                            if ok and key is not None:
                                obj.key = key
                                obj.generate_qr_code()

                        obj.save()
                        exceptions += '[QR_code] Error -- %s\n' % obj.qr_code
                        if obj.key is not None:
                            debug.error('[QR_code] Error -- %s' % obj.qr_code)
                        else:
                            logger.error('[QR_code] Error -- reservation id: %s' % obj.reservation_id)
                        self.message.attach_file(obj.qr_code.path)
                else:
                    try:
                        self.message.attach_file(obj)
                    except Exception as e:
                        exceptions += '[ObjectAttach] Error -- %s\n[Error] %s\n' % (self.instance, e)
        else:
            try:
                self.message.attach_file(self.file_attach.path)
            except ValueError:
                exceptions += '[FileAttach] Error -- %s\n' % self.instance

        if exceptions != '':
            django_log.error(exceptions)

    def create_base_message(self):
        MessageID = "<%d.%d@%s>" % (time.time(), random.getrandbits(64), socket.getfqdn())

        return EmailMessage(
            self.head,
            self.txt,
            self.EMAIL_FROM,
            self.EMAIL_TO,
            bcc=settings.EMAIL_BCC,
            headers={'Message-ID': MessageID}
        )

    def create_message(self):
        MessageID = "<%d.%d@%s>" % (time.time(), random.getrandbits(64), socket.getfqdn())

        return EmailMultiAlternatives(
            self.head,
            self.txt,
            self.EMAIL_FROM,
            self.EMAIL_TO,
            bcc=settings.EMAIL_BCC,
            headers={'Message-ID': MessageID}
        )

    def attach_html(self):
        self.message.attach_alternative(self.html, "text/html")

    def send_message(self):

        self.message.send(fail_silently=False)
        logger.info("[EmailFactory] Send Email from - %s to - %s" % (self.instance.__str__(), self.EMAIL_TO,))


class SMSFactory:
    TOKEN = settings.SMS_TOKEN
    LOGIN = settings.SMS_LOGIN
    PASSWORD = settings.SMS_PASSWORD
    ALPHANAME = settings.SMS_ALPHANAME
    URL = settings.SMS_CONNECTION_URL
    # key = "{TOKEN}
    body_template = """<?xml version="1.0" encoding="utf-8" ?><package login="{LOGIN}" """ \
                    """password ="{PASSWORD}" >{message}</package>"""

    wap_push_template = """<message><msg recipient= "{client_phone_number}" """ \
                        """sender= "{ALPHANAME}" url="{CLIENT_TICKET_URL}" type="2">{message_text}</msg></message>"""

    text_message_template = """<message><msg recipient= "{client_phone_number}" """ \
                            """sender= "{ALPHANAME}" type="0">{message_text}</msg></message>"""

    def __init__(self, message_text, client_phone_number, url=None):
        self.message_text = message_text
        self.client_phone_number = client_phone_number
        if url is not None:
            self.url = url
            self.send_message(self.build_wap_push())
        else:
            self.send_message(self.build_text_message())

    def send_message(self, xml):
        headers = {'Content-Type': 'application/xml'}
        logger.info("[SMSFactory] Send SMS to - %s" % self.client_phone_number)
        sender = requests.request('post', self.URL, data=xml.encode('utf-8'), headers=headers)
        return sender

    def build_message(self, message):
        return self.body_template.format(
            LOGIN=self.LOGIN,
            PASSWORD=self.PASSWORD,
            TOKEN=self.TOKEN,
            message=message
        )

    def build_wap_push(self):
        message = self.text_message_template.format(
            client_phone_number=self.client_phone_number,
            ALPHANAME=self.ALPHANAME,
            message_text=self.message_text,
            CLIENT_TICKET_URL=self.url
        )
        return self.build_message(message)

    def build_text_message(self):
        message = self.text_message_template.format(
            client_phone_number=self.client_phone_number,
            ALPHANAME=self.ALPHANAME,
            message_text=self.message_text,
        )
        return self.build_message(message)
