import logging
from . import models, serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse

logger = logging.getLogger('sender')

seo_models = (
    models.ForPage,
    models.ForFilms,
    models.ForPost
)


def robots_txt(request):
    robots = models.Other.objects.filter(url='robots.txt')

    if robots.exists():
        robots = robots.first()
        response = HttpResponse(robots.description, content_type='text/plain')
        return response

    default = """User-agent: *\nDisallow: */user/\nAllow:*js\nAllow:*css\nAllow:*jpg"""
    response = HttpResponse(default, content_type='text/plain')
    return response


def sitemap(request):
    sitemap_obj = models.Other.objects.filter(url='sitemap.xml')

    if sitemap_obj.exists():
        sitemap_obj = sitemap_obj.first()
        response = HttpResponse(sitemap_obj.description, content_type='text/xml')
        return response

    default = """<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"></urlset>"""

    response = HttpResponse(default, content_type='text/xml')
    return response


class SeoInJson(APIView):
    authentication_classes = []

    def post(self, request):
        queryset = models.Other.objects.filter(url=check_url_slash(request.data['url']))
        serializer = serializers.SeoSerializer(queryset, many=True)
        return Response(serializer.data)


def SeoInPage(request):
    try:
        seo = models.Other.objects.get(url=check_url_slash(request.path))
        related_object = find_related_object(seo, seo_models)

        if isinstance(related_object, models.Movie):
            seo_img = related_object.fk.poster.url
        elif isinstance(related_object.fk, models.Posts):
            seo_img = related_object.fk.image.url
        elif isinstance(related_object.fk, models.SimplePages):
            seo_img = related_object.fk.background_image.url
        else:
            seo_img = None

        return render(request, 'index.html', {
            'description': seo.description if seo.description is not None else settings.SEO_DEFAULT_DESCRIPTION,
            'keywords': seo.keywords if seo.keywords is not None else settings.SEO_DEFAULT_KEYWORDS,
            'title': seo.title if seo.title is not None else settings.SEO_DEFAULT_TITLE,
            'default_seo_img': settings.SEO_SOCIAL_DEFAULT_BACK_IMAGE,
            'seo_img': seo_img if seo_img is not None else settings.SEO_SOCIAL_DEFAULT_BACK_IMAGE,
            'url': settings.CITE_URL
        })

    except Exception as e:
        logger.info('[SEO][VIEW] not all attr in seo obj\n[SEO][VIEW][Message] %s' % e)
        return render(request, 'index.html', {
            'description': settings.SEO_DEFAULT_DESCRIPTION,
            'keywords': settings.SEO_DEFAULT_KEYWORDS,
            'title': settings.SEO_DEFAULT_TITLE,
            'default_seo_img': settings.SEO_SOCIAL_DEFAULT_BACK_IMAGE,
            'url': settings.CITE_URL
        })


def find_related_object(seo, fk_models):
    for model in fk_models:
        try:
            return model.objects.get(other_ptr=seo)
        except model.DoesNotExist:
            pass


def check_url_slash(url):
    if url[-1] == '/':
        return url
    else:
        return url + '/'
