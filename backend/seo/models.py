from django.db import models
from movie.models import Movie
from pages.models import Posts, SimplePages


class Other(models.Model):
    url = models.CharField(
        max_length=128,
        unique=True,
        blank=True,
        null=True,
    )

    title = models.CharField(
        max_length=128,
        help_text="SEO название для поисковика",
        verbose_name="SEO Title",
        blank=True,
        null=True,
    )

    description = models.TextField(
        max_length=5000,
        help_text="Введите описание",
        verbose_name="Описание:",
        blank=True,
        null=True,
    )

    keywords = models.CharField(
        max_length=128,
        help_text="SEO ключевые слова для поисковика",
        verbose_name="SEO keywords",
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s по адресу %s' % (self.title, self.url)

    class Meta:
        verbose_name = "Дополнительный"
        verbose_name_plural = "Дополнительные"


class ForFilms(Other):
    fk = models.OneToOneField(
        Movie,
        on_delete=models.CASCADE,
        verbose_name="Фильм"
    )

    class Meta:
        verbose_name = "SEO для фильма"
        verbose_name_plural = "SEO для фильмов"


class ForPost(Other):
    fk = models.OneToOneField(
        Posts,
        on_delete=models.CASCADE,
        verbose_name="Пост"
    )

    def __str__(self):
        return '%s по адресу %s' % (self.title, self.url)

    class Meta:
        verbose_name = "SEO для поста"
        verbose_name_plural = "SEO для постов"


class ForPage(Other):
    fk = models.OneToOneField(
        SimplePages,
        on_delete=models.CASCADE,
        verbose_name="Страница",
    )

    def __str__(self):
        return '%s по адресу %s' % (self.title, self.url)

    class Meta:
        verbose_name = "SEO для страницы"
        verbose_name_plural = "SEO для страниц"

