# Generated by Django 2.0.4 on 2018-09-06 09:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('pages', '0001_initial'),
        ('movie', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Other',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(blank=True, max_length=128, null=True, unique=True)),
                ('title', models.CharField(blank=True, help_text='SEO название для поисковика', max_length=128, null=True, verbose_name='SEO Title')),
                ('description', models.CharField(blank=True, help_text='SEO описание фильма', max_length=128, null=True, verbose_name='SEO description')),
                ('keywords', models.CharField(blank=True, help_text='SEO ключевые слова для поисковика', max_length=128, null=True, verbose_name='SEO keywords')),
            ],
            options={
                'verbose_name': 'Дополнительный',
                'verbose_name_plural': 'Дополнительные',
            },
        ),
        migrations.CreateModel(
            name='ForFilms',
            fields=[
                ('other_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='seo.Other')),
                ('fk', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='movie.Movie', verbose_name='Фильм')),
            ],
            options={
                'verbose_name': 'SEO для фильма',
                'verbose_name_plural': 'SEO для фильмов',
            },
            bases=('seo.other',),
        ),
        migrations.CreateModel(
            name='ForPage',
            fields=[
                ('other_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='seo.Other')),
                ('fk', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pages.SimplePages', verbose_name='Страница')),
            ],
            options={
                'verbose_name': 'SEO для страницы',
                'verbose_name_plural': 'SEO для страниц',
            },
            bases=('seo.other',),
        ),
        migrations.CreateModel(
            name='ForPost',
            fields=[
                ('other_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='seo.Other')),
                ('fk', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pages.Posts', verbose_name='Пост')),
            ],
            options={
                'verbose_name': 'SEO для поста',
                'verbose_name_plural': 'SEO для постов',
            },
            bases=('seo.other',),
        ),
    ]
