# Generated by Django 2.1.2 on 2018-12-23 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0002_auto_20181210_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='other',
            name='description',
            field=models.TextField(blank=True, help_text='Введите описание', null=True, verbose_name='Описание:'),
        ),
    ]
