from rest_framework import serializers
from .models import *

base_fields = ("title", "keywords", "url", "description")


class SeoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Other
        fields = base_fields


class ForPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = ForPost
        fields = ("url", )


class ForFilmSerializer(serializers.ModelSerializer):

    class Meta:
        model = ForFilms
        fields = ("url", )


class ForPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = ForPage
        fields = ("url", )

