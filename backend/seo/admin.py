from django.contrib import admin
from .models import *
from django.forms.models import BaseInlineFormSet
from django.utils.text import slugify
from transliterate import translit


class SeoForMovieInlineFormSet(BaseInlineFormSet):
    slug = '/movies'
    foreign_seo_model = ForFilms

    def check_url(self):
        try:
            return '%s/%s/' % (self.slug, slugify(translit(self.instance.title, reversed=True)))
        except:
            return '%s/%s/' % (self.slug, self.instance.id)

    def save_new_objects(self, commit=True):
        saved_instances = super(SeoForMovieInlineFormSet, self).save_new_objects(commit)

        if commit:
            try:
                self.seo_obj = saved_instances[0]
            except IndexError:
                self.seo_obj = None

            if self.seo_obj:
                if self.seo_obj.url is None:
                    self.seo_obj.url = self.check_url()
                    self.seo_obj.save()
                saved_instances[0] = self.seo_obj
            else:
                try:
                    exist_obj = self.check_related()
                    if exist_obj:
                        exist_obj.url = self.check_url()
                        exist_obj.save()
                        return saved_instances
                except self.foreign_seo_model.DoesNotExist:
                    pass

                saved_instances += [self.create_object()]

        return saved_instances

    def check_related(self):
        return self.instance.forfilms

    def create_object(self):
        self.foreign_seo_model.objects.create(
            fk=self.instance,
            url=self.check_url(),
            title=self.instance.title
            )
        return self.instance


class PostInlineFormSet(SeoForMovieInlineFormSet):
    slug = '/shares'
    foreign_seo_model = ForPost

    def check_related(self):
        return self.instance.forpost

    def create_object(self):
        self.foreign_seo_model.objects.create(
            fk=self.instance,
            url=self.check_url(),
            title=self.instance.title
            )
        return self.instance


class FilmsSeoInline(admin.StackedInline):
    model = ForFilms
    formset = SeoForMovieInlineFormSet


class PostSeoInline(admin.StackedInline):
    model = ForPost
    formset = PostInlineFormSet


class PageSeoInline(admin.StackedInline):
    model = ForPage


class FilmsSeoAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_editable = []
    list_filter = ["url"]
    list_display = ["title", "url", "keywords"]
    search_fields = ["id", "url"]

    class Meta:
        model = ForFilms
        verbose_name = "SEO для фильма"
        verbose_name_plural = "SEO для фильмов"


class PostSeoAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_editable = []
    list_filter = ["url"]
    list_display = ["title", "url", "keywords"]
    search_fields = ["id", "url"]

    class Meta:
        model = ForPost
        verbose_name = "SEO для поста"
        verbose_name_plural = "SEO для постов"


class PageSeoAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_editable = []
    list_filter = ["url"]
    list_display = ["title", "url", "keywords"]
    search_fields = ["id", "url"]

    class Meta:
        model = ForPage
        verbose_name = "SEO для страницы"
        verbose_name_plural = "SEO для страницы"


class AllAdmin(admin.ModelAdmin):
    ordering = ["id"]
    list_editable = []
    list_filter = ["url"]
    list_display = ["title", "url", "keywords"]
    search_fields = ["id", "url"]

    class Meta:
        model = ForPage
        verbose_name = "SEO"
        verbose_name_plural = "SEO"


admin.site.register(Other, AllAdmin)
admin.site.register(ForFilms, FilmsSeoAdmin)
admin.site.register(ForPage, PageSeoAdmin)
admin.site.register(ForPost, PostSeoAdmin)


